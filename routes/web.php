<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Для поиска
Route::get('search', 'ProductController@index')->name('search.index');
/*Route::get('/', 'ProductController@index')->name('index');*/

// Категория                                            // ->name('index_catalog'); --- это имя маршрута
Route::get('category/', 'CatalogController@index_catalog')->name('index_catalog');
Route::get('category/begovye-dorozhki/', 'CatalogController@index_catalog')->name('index_catalog');

// Создать продукт
Route::get('create', 'ProductController@create')->name('product.create');

// Показываем товар
Route::get('product/{tag}', 'ProductController@show')->name('product');

// Запись в бд нового продукта
Route::post('product', 'ProductController@store')->name('product.store');

// Запись в бд нового партнёра
Route::post('partner', 'PartnerController@create')->name('partner');

// Запись в бд нового обращения со страницы контакты
Route::post('contacts', 'ContactController@create')->name('contacts');

// Переход на страницу редактирования одной страницы
Route::get('edit/{tag}', 'ProductController@edit')->name('edit');

// Записываем в базу данных обновления
Route::patch('product/edit/{tag}', 'ProductController@update')->name('product.edit');

// Удалить продукт
Route::delete('product/destroy/{tag}', 'ProductController@destroy')->name('product.destroy');

Route::get('', function () {
    $url = 'index/';
    $title = 'Официальный сайт производителя спортивных тренажеров Koenigsmann';
    $meta_tag_description = 'Мы занимаемся производством и продажей спортивных тренажеров Koenigsmann. Закажите тренажер прямо на сайте';
    $h1 = 'Спортивные тренажеры Koenigsmann';
    return view('index', compact('url', 'meta_tag_description', 'title', 'h1'));
})->name('index');

Route::get('company/', function () {
    $title = 'О компании Koenigsmann';
    $meta_tag_description = 'Эта страница посвящена бренду Koenigsmann';
    $h1 = 'О компании Koenigsmann';
    return view('company', compact('meta_tag_description', 'title', 'h1'));
});

Route::get('account/', function () {
    return view('account');
});

// Заявка на сервисное обслуживание
Route::get('service/', function () { return view('hotline'); });
Route::post('service_sendmail/', 'PartnerController@service')->name('service_sendmail');

Route::get('contacts/', function () {
    $contact = 'contact';
    $title = 'Обратная связь Koenigsmann';
    $meta_tag_description = 'Страница обратной связи с нами';
    $h1 = 'Обратная связь Koenigsmann';
    return view('contact', compact('contact', 'meta_tag_description', 'title', 'h1'));
});

Route::get('card_product/', function () {
    return view('card_product');
});

Route::get('partnership/', function () {
    $title = 'Станьте партнером компании Koenigsmann';
    $meta_tag_description = 'Мы готовы к сотрудничеству с вами, просто заполните форму и наш менеджер ответит вам';
    $h1 = 'Стать партнером компании Koenigsmann';
    return view('partnership', compact('meta_tag_description', 'title', 'h1'));
});

Route::get('personal_area/', function () {
    return view('personal_area');
});

Route::get('personal_area-stock/', function () {
    return view('personal_area-stock');
});

Route::get('teaching/', function () {
    return view('teaching');
});

Route::get('public_stock/', function () {
    return view('public_stock');
});

Route::get('to-buyers/', function () {
    return view('to-buyers');
});

/* Обмен и возврат */
Route::get('exchange_and_return/', function () {
    return view('exchange_and_return');
});

Route::get('policy/', function () {
    return view('policy');
});

Route::get('agreement/', function () {
    return view('agreement');
});

Route::get('purchase_returns/', function () {
    return view('purchase_returns');
});

Route::get('where_buy/', function () {
    return view('where_buy');
});

// Данные пользователя переход на страницу
Route::get('userinfo/', 'UserController@getUserInfo')->name('userinfo');

Route::get('personal_area/', 'UserController@getProfile')->name('personal_area');

// Показать категории
Route::get('category/begovye-dorozhki/{category}/', 'ProductController@category')->name('category');

// Корзина
Route::get('basket/', 'ProductController@basket')->name('basket');

// Уменьшение на одну еденицу товара в корзине
Route::get('reduce/{id}', 'ProductController@getReduceByOne')->name('product.reduceByOne');

// Удаление одного товара из корзины
Route::get('remove/{id}', 'ProductController@getRemoveItem')->name('product.remove');

// Добавление в корзину
Route::get('add-to-cart/{id}', 'ProductController@getAddToCart')->name('product.addToCart');
Route::get('add-to-cart_view/{id}', 'ProductController@getAddToCart_view')->name('product.addToCart_view');

// Переход в корзину
Route::get('shoppingCart/', 'ProductController@getCart')->name('product.shoppingCart');

// Переход на страницу оформления заказа
Route::get('checkout/', 'ProductController@getCheckout')->name('checkout');

// Оформление заказа и отправка данных на email
Route::post('checkout', 'ProductController@postCheckout')->name('checkout');

// Загрузка изображений
Route::post('image-view','ImageController@store_upload_img');

// Прайс лист
Route::get('price_list/', function () {
    $page_price_list = 'page_price_list';
    return view('price_list', compact('page_price_list'));
});

// YML
Route::get('ymlupdate', 'CatalogController@getProduct')->name('getProduct');
// Отдаю YML
Route::get('yandexmarket.xml', function () {
    return response()->file('yandexmarket.xml');
});

Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    return "Кэш очищен.";
});

/*Auth::routes();*/
/*Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');*/

Auth::routes([
    'register' => false,
    'verify' => true,
    'reset' => false
]);

Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('delivery/', function () {
    return view('delivery');
});

// Страница создания нового пользователя
Route::get('usermanagement/', 'UseraddController@go_page')->name('usermanagement');

// Запись в бд нового пользователя
Route::post('user_manage', 'UseraddController@create')->name('user_manage');

// Переход на страницу редактирования одной страницы
Route::get('destroy_photo/{str}/{id_product}', 'ProductController@destroy_photo')->name('destroy_photo');