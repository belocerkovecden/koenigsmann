<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('user_id')->nullable(); // ид пользователя
            $table->text('cart'); // Товары которые пользователь выбрал
            $table->string('fio')->nullable(); // ФИО
            $table->string('email'); // Обязательные контакты для связи с заказчиком
            $table->string('phone'); // Обязательные контакты для связи с заказчиком
            $table->string('city_list')->nullable(); // Город для доставки
            $table->string('delivery_terms')->nullable(); // Условия доставки(либо самовывоз, либо доставка)
            $table->string('points_issue')->nullable(); // Метод доставки типа ПЭК
            $table->string('city_list_pickup')->nullable(); // Город самовывоза
            $table->string('points_pickup')->nullable(); // Адресс самовывоза
            $table->string('method_cash')->nullable(); // Метод оплаты
            $table->string('method_card')->nullable(); // Метод оплаты
            $table->string('comment')->nullable(); // Комментарий и адресс
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
