<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->timestamps();
            $table->string('contact_person_name');  //
            $table->string('contact_person_phone'); //
            $table->string('contact_person_email'); //
            $table->text('organization_address');   //
            $table->string('organization_inn');     //
            $table->string('contract');
            $table->string('sales_through');
            $table->string('trading_region');
            $table->string('showroom');
            $table->string('have_delivery_service');
            $table->string('location_city');
            $table->text('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
