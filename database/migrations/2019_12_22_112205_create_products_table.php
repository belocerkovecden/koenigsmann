<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigInteger('author_id')->unsigned(); // unsigned только число больше 0 и не чем больше
            $table->bigIncrements('product_id');
            $table->string('title');
            //$table->string('short_title');
            $table->text('properties')->nullable();
            $table->text('description')->nullable();
            $table->integer('price');
            $table->string('tag')->nullable();
            $table->string('category');

            $table->string('type')->nullable(); // Тип
            $table->string('level')->nullable(); // Уровень
            $table->string('dimensions')->nullable(); // Габариты (ДхШхВ)
            $table->string('weight_trainer')->nullable(); // Вес тренажера
            $table->string('package_dimensions')->nullable(); // Размеры упаковки Габариты в упаковке
            $table->string('package_weight')->nullable(); // Вес в упаковке
            $table->string('folding_design')->nullable(); // Складная конструкция
            $table->string('compact_folding')->nullable(); // Компактное складывание
            $table->string('folded_dimensions')->nullable(); // Габариты в сложенном виде
            $table->string('type_powered')->nullable(); // Тип питания
            //$table->string('power_consumption')->nullable(); // Потребляемая мощность, кВт/ч
            //$table->string('brand_origin')->nullable(); // Происхождение марки
            //$table->string('country_manufacture')->nullable(); // Страна-производитель
            $table->string('motor_warranty')->nullable(); // Гарантия на мотор
            $table->string('frame_warranty')->nullable(); // Гарантия на раму
            $table->string('warranty_electronic_components')->nullable(); // Гарантия на узлы движения и электронные блоки
            $table->string('wear_parts_warranty')->nullable(); // Гарантия на детали износа
            //$table->string('warranty')->nullable(); // Гарантия
            $table->string('engine_power')->nullable(); // Мощность двигателя, л.с.
            $table->string('top_speed')->nullable(); // Максимальная скорость, км/ч
            //$table->string('minimum_speed')->nullable(); // Минимальная скорость, км/ч
            $table->string('maximum_user_weight')->nullable(); // Максимальный вес пользователя
            $table->string('running_belt')->nullable(); // Беговое полотно
            $table->string('sizes_cloth')->nullable(); // Размеры полотна (ДхШ)
            //$table->string('running_belt_length')->nullable(); // Длина бегового полотна
            //$table->string('running_belt_width')->nullable(); // Ширина бегового полотна
            $table->string('deck_thickness')->nullable(); // Толщина деки
            //$table->string('possible_tilt')->nullable(); // Возможный наклон
            $table->string('tilt_adjustment')->nullable(); // Регулировка наклона
            $table->string('depreciation')->nullable(); // Амортизация
            $table->string('display')->nullable(); // Дисплей
            $table->string('workout_time_display')->nullable(); // Отображение времени тренировки
            //$table->string('mileage_display')->nullable(); // Отображение пройденного расстояния
            //$table->string('heart_rate_display')->nullable(); // Отображение пульса
            //$table->string('calorie_consumption_display')->nullable(); // Отображение расхода калорий
            //$table->string('current_speed_display')->nullable(); // Отображение текущей скорости
            //$table->string('number_programs')->nullable(); // Количество программ
            //$table->string('heart_rate_dependent_programs')->nullable(); // Пульсозависимые программы
            $table->string('preset_programs')->nullable(); // Предустановленные программы
            //$table->string('user_programs')->nullable(); // Пользовательские программы
            $table->string('program_specifications')->nullable(); // Спецификации программ
            $table->string('frame')->nullable(); // Рама
            $table->string('heart_rate_measurement')->nullable(); // Измерение пульса
            //$table->string('integrated_fan')->nullable(); // Встроенный вентилятор
            //$table->string('shipping_rollers')->nullable(); // Транспортировочные ролики
            $table->string('integration')->nullable(); // Интеграция
            $table->string('multimedia')->nullable(); // Мультимедиа
            $table->text('additionally1')->nullable(); // Дополнительно 1
            $table->string('additionally2')->nullable(); // Дополнительно 2
            $table->string('additionally3')->nullable(); // Дополнительно 3
            $table->string('front_shaft_diameter')->nullable(); // Диаметр переднего вала
            $table->string('rear_shaft_diameter')->nullable(); // Диаметр заднего вала
            $table->string('distance_between_racks')->nullable(); // Расстояние между стойками
            $table->string('type_of_plastic')->nullable(); // Тип пластика
            //$table->string('certifications')->nullable(); // Сертификаты
            //$table->string('floor_roughness_compensators')->nullable(); // Компенсаторы неровностей пола
            $table->string('energy_saving')->nullable(); // Энергосбережение
            //$table->string('tablet_stand')->nullable(); // Подставка под планшет
            $table->string('noise_level')->nullable(); // Уровень шума
            //$table->string('motor_controller')->nullable(); // Контроллер двигателя
            $table->text('equipment')->nullable(); // Комплектация
            //$table->string('silicone_grease')->nullable(); // Силиконовая смазка
            //$table->string('audio_cable')->nullable(); // Аудиокабель
            //$table->string('security_key')->nullable(); // Ключ безопасности
            //$table->string('running_belt_adjustment_wrench')->nullable(); // Ключ для регулировки бегового полотна
            //$table->string('set_of_keys_for_assembly')->nullable(); // Набор ключей для сборки
            //$table->string('user_manual')->nullable(); // Инструкция по эксплуатации
            //$table->string('changing_angle_handrail')->nullable(); // Изменение угла наклона поручни
            //$table->string('handrail_speed_change')->nullable(); // Изменение скорости поручни
            $table->string('speed_adjustment_step')->nullable(); // Шаг регулировки скорости, км/ч
            $table->string('instruction')->nullable(); // Путь до инструкции
            //$table->string('heart_rate_sensor')->nullable(); // Кардиодатчик
            //$table->string('cardiopoies')->nullable(); // Кардиопояс
            //$table->string('smart_scales')->nullable(); // Умные весы
            //$table->string('mat')->nullable(); // Коврик

            $table->string('slug')->nullable(); // ЧПУ
            $table->string('availability')->nullable(); // Наличие, либо есть/нету

            $table->text('img', 1000)->nullable(); // По умолчанию будет картинка
            $table->timestamps();

            /* Связывание с таблицей пользователь */
            $table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
