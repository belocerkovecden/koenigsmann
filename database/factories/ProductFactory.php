<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;


$factory->define(/**
 * @param Faker $faker
 * @return array
 */
    Product::class, function (Faker $faker) {

    $title = $faker->realText(rand(10, 40));
    $short_title = mb_strlen($title)>30 ? mb_substr($title, 0, 30) . '...' : $title;
    $properties = $faker->realText(rand(10, 40));
    $price = $faker->realText(rand(10, 60));
    $tag = $faker->realText(rand(10, 40));
    $category = $faker->realText(rand(10, 50));
    $created = $faker->dateTimeBetween('-30 days', '-1 days');
    $description = $faker->realText(rand(100, 700));

    return [
        'title' => $title,
        'short_title' => $short_title,
        'author_id' => rand(1, 4),
        'properties' => $properties,
        'price' => $price,
        'category' => $category,
        'tag' => $tag,
        'description' => $description,
        'created_at' => $created,
        'updated_at' => $created,

        'motor' => $category,
        'speed' => rand(5, 40),
        'display' => $category,
        'tilt_adjustment' => $category,
        'warranty' => rand(0, 5),
        'maximum_user_weight' => $category,
        'web_size' => $category,

    ];

    /*
            $table->bigIncrements('product_id');
            $table->bigInteger('author_id')->unsigned();
            $table->string('title');
            $table->string('short_title');
            $table->string('properties');
            $table->string('price');
            $table->string('tag');
            $table->text('description');
            $table->string('img')->nullable(); // По умолчанию будет картинка
            $table->timestamps();
     */
});
