<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\CatalogController;

class UpdateYML extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updatedate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update date YML';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controllerCatalog = new CatalogController;
        $text = $controllerCatalog->getProduct();
        $fp = fopen("public/yandexmarket.xml", "w");
        fwrite($fp, $text);
        fclose($fp);
    }
}
