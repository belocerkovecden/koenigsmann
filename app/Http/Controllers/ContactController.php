<?php

namespace App\Http\Controllers;

use App\Partner;
use App\Contact;
use Illuminate\Http\Request;
use App\Http\Requests\PartnerRequest;
use App\Cart;
use App\Order;
use Auth;
use App\TemplateEmail;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Str;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Product;
use Session;
use Illuminate\Support\Collection;


class ContactController extends Controller

{
    public function create(Request $request)
    {
        $cont = new Contact();
        $cont->contact_name = trim($request->contact_name);
        $cont->contact_email = trim($request->contact_email);
        $cont->contact_description = trim($request->contact_description);

        $cont->save(); // Пишем в бд

        // Отправка E-mail
        $name_label_email = 'Обращение через контакты';

        $mail = new PHPMailer(true);
        try {
            $mail->CharSet = 'utf-8';
            // Настройки SMTP
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPDebug = 0;
            $mail->Host = 'smtp.yandex.ru';
            $mail->Port = 465;
            $mail->Username = 'info@koenigsmann.ru';
            $mail->Password = 'W!tee6Fp45';
                        /*$mail->Username = 'belocerkovecden1@ya.ru';
                        $mail->Password = '#1983TiToVa2007';*/
            $mail->SMTPSecure = "ssl"; // or ssl

            // От кого письмо
            $mail->setFrom('info@koenigsmann.ru');
            // Устанавливаем формат сообщения в HTML
            $mail->isHTML(true);
            // Тема письма
            $mail->Subject = $name_label_email;
            // Тело письма
            $body_letter = ' Имя обратившегося: ' . $request->contact_name
                . ' | E-mail пользователя: ' . $request->contact_email
                . ' | Само обращение: ' . $request->contact_description;

            $mail->Body = $body_letter;

            $mail->addAddress('info@koenigsmann.ru', 'Петрова Елизавета');
/*            $mail->addReplyTo('info@koenigsmann.ru', 'Mailer');
            $mail->AddCC('info@koenigsmann.ru', 'Тест');*/
            $mail->send();

        } catch (Exception $e) {
            echo "Письмо не отправлено. PHPMailer ошибка: {$mail->ErrorInfo}";
        }
        return redirect()->route('contacts', ['cont' => $cont])->with('success', 'Заявка подана!');
    }
}
