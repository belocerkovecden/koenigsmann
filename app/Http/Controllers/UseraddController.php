<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Contact;
use App\Order;
use App\Partner;
use Auth;
use App\TemplateEmail;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Product;
use Session;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\UserManagement;

class UseraddController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        // Если пользователь не авторизован под админом, то ему запрещено использовать методы данного класса
        // Кроме index и show ....
        $this->middleware('auth')->except('index');
    }

    public function go_page() {
        if (\Auth::user()->id != 1) {
            return redirect('/');
        }

        return view('usermanagement.usermanagement');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    public function create(Request $request)
    {

        //dd($request);
        if (\Auth::user()->id != 1) {
            return redirect('/')->withErrors('success', 'Вы не можете создавать пользователей!');
        }

        Validator::make($request->all(), array(
                "name" => 'required|max:255',
                "email" => 'required|email|max:255|unique:users,email',
                "password" => 'required|min:6'
            )
        );

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        );

        User::create($data);
        return redirect('usermanagement')->with('success', 'Пользователь успешно создан!');
    }
}
