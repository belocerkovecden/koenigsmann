<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Order;
use Auth;
use App\TemplateEmail;
use App\Http\Middleware\DontEndSlashMiddleware;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Product;
use Session;
use Illuminate\Support\Collection;
use Illuminate\Auth\Events\Validated;
use phpDocumentor\Reflection\Types\Null_;
use App\User;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    protected $orders;

    public function __construct()
    {
        // Если пользователь не авторизован под админом, то ему запрещено использовать методы данного класса
        // Кроме index и show ....

        $this->middleware('auth')->except('index', 'getRemoveItem', 'getReduceByOne', 'show', 'getAddToCart_view', 'category', 'getAddToCart', 'basket', 'getCart', 'getCheckout', 'postCheckout', 'order_sending');
        //$this->middleware('DontEndSlashMiddleware')->except('category', 'page', 'remove', 'reduce', 'add-to-cart', 'search');
        //Устанавливаю часовой пояс
        date_default_timezone_set('Europe/Moscow');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // В случае если будет сделан ПОИСК
        if ($request->search) {
            $products = Product::join('users', 'author_id', '=', 'users.id')
                ->where('title', 'like', '%' . $request->search . '%')// Ищем в title
                ->orWhere('description', 'like', '%' . $request->search . '%')// Ищем в description
                ->orWhere('name', 'like', '%' . $request->search . '%')// Ищем в name
                ->orderBy('products.price', 'asc')
                ->get();

            $title = "Каталог спортивных тренажеров Koenigsmann";
            $meta_tag_description = 'Купите спортивный тренажер Koenigsmann. Закажите спортивный тренажер с доставкой на дом';
            $menu_active = 'Поиск';

            // dd($request);

            return view('catalog', compact('products', 'menu_active', 'title', 'meta_tag_description'));
        } else {
            //        $products = Product::all(); // Получаем все продукты из таблицы продукты
            $products = Product::join('users', 'author_id', '=', 'users.id')
                ->orderBy('products.price', 'asc')// Отсортировать по дате
                ->paginate(15); // Пагинация
            //$products->setPath('category');
            $url = 'index';
//            $url = $request::url();
            return view('catalog', compact('products', 'url')); // Генерируем вьюху и передаём туда все полученные продукты
        }

    }

    /* Категории */
    public function category($category, Request $request)
    {
        if ($category !== 'dlya-doma') {
            $products = Product::where('category', $category)->orderBy('products.price', 'asc')->get();
        } else {
            $products = Product::where('category', '<>', 'aksessuary')
                ->where('category', '<>', 'comerc')
                ->orderBy('products.price', 'asc')
                ->get();
        }

        switch ($category) {
            case 'dlya-doma':
                $title = 'Каталог беговых дорожек Koenigsmann для дома';
                $meta_tag_description = 'Купите беговую дорожку Koenigsmann для дома. Закажите беговую дорожку с доставкой на дом';
                $h1 = 'Беговые дорожки Koenigsmann для дома';
                break;
            case 'limited-edition':
                $title = 'Каталог беговых дорожек Koenigsmann Limited Edition';
                $meta_tag_description = 'Купите беговую дорожку Koenigsmann из серии Limited Edition. Закажите беговую дорожку с доставкой на дом';
                $h1 = 'Беговые дорожки Koenigsmann Limited Edition';
                break;
            case 'aksessuary':
                $title = 'Каталог аксессуаров для беговых дорожек';
                $meta_tag_description = 'Купите аксессуар для беговых дорожек. Закажите аксессуар с доставкой на дом';
                $h1 = 'Аксессуары для беговых дорожек';
                break;
            case 'commerce':
                $title = 'Каталог беговых дорожек Koenigsmann для зала';
                $meta_tag_description = 'Купите беговую дорожку Koenigsmann для зала. Закажите беговую дорожку с доставкой на дом';
                $h1 = 'Беговые дорожки Koenigsmann для зала';
                break;
            default:
                $title = 'Каталог спортивных тренажеров Koenigsmann';
                $meta_tag_description = 'Купите спортивный тренажер Koenigsmann. Закажите спортивный тренажер с доставкой на дом';
                $h1 = 'Спортивные тренажеры Koenigsmann';
        }

        $category_name = $category; // Кладу категорию которую ищут
        $menu_active = 'category';
        return view('catalog', compact('products', 'category_name', 'menu_active', 'title', 'meta_tag_description', 'h1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = new Product();
        if (\Auth::user()->id != 1) {
            return redirect('index')->withErrors('danger', 'Вы не можете создавать продукт!');
        }

        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $product = new Product();
        $product->title = $request->title;
        $product->properties = $request->properties;
        $product->tag = $request->tag;
        $product->price = $request->price;
        $product->category = $request->category;
        $product->description = $request->description;

        // Большой список колонок
        $product->type = $request->type;
        $product->level = $request->level;
        $product->weight_trainer = $request->weight_trainer;
        $product->package_dimensions = $request->package_dimensions;
        $product->package_weight = $request->package_weight;
        $product->folding_design = $request->folding_design;
        $product->compact_folding = $request->compact_folding;
        $product->folded_dimensions = $request->folded_dimensions;
        $product->dimensions = $request->dimensions;
        $product->type_powered = $request->type_powered;
        $product->motor_warranty = $request->motor_warranty;
        $product->frame_warranty = $request->frame_warranty;
        $product->warranty_electronic_components = $request->warranty_electronic_components;
        $product->wear_parts_warranty = $request->wear_parts_warranty;
        $product->engine_power = $request->engine_power;
        $product->top_speed = $request->top_speed;
        $product->maximum_user_weight = $request->maximum_user_weight;
        $product->running_belt = $request->running_belt;
        $product->sizes_cloth = $request->sizes_cloth;
        $product->deck_thickness = $request->deck_thickness;
        $product->tilt_adjustment = $request->tilt_adjustment;
        $product->depreciation = $request->depreciation;
        $product->display = $request->display;
        $product->workout_time_display = $request->workout_time_display;
        $product->program_specifications = $request->program_specifications;
        $product->frame = $request->frame;
        $product->heart_rate_measurement = $request->heart_rate_measurement;
        $product->integration = $request->integration;
        $product->multimedia = $request->multimedia;
        $product->additionally1 = $request->additionally1;
        $product->additionally2 = $request->additionally2;
        $product->additionally3 = $request->additionally3;
        $product->front_shaft_diameter = $request->front_shaft_diameter;
        $product->rear_shaft_diameter = $request->rear_shaft_diameter;
        $product->distance_between_racks = $request->distance_between_racks;
        $product->type_of_plastic = $request->type_of_plastic;
        $product->energy_saving = $request->energy_saving;
        $product->noise_level = $request->noise_level;
        $product->equipment = $request->equipment;
        $product->speed_adjustment_step = $request->speed_adjustment_step;
        $product->availability = $request->availability; // Наличие на складе
        $product->instruction = $request->instruction;


        // Пользователь который создал товар
        $product->author_id = \Auth::user()->id;

        // Множественная загрузка изображений
        /*$photos = $request->file('photos');
        $paths = [];*/

        $str_for_bd = '';
        if (request()->hasFile('photo1')) {
            $photo = $request->file('photo1');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= 'public/' . $filename;
        }

        if (request()->hasFile('photo2')) {
            $photo = $request->file('photo2');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||' . 'public/' . $filename;
        }

        if (request()->hasFile('photo3')) {
            $photo = $request->file('photo3');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo4')) {
            $photo = $request->file('photo4');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo5')) {
            $photo = $request->file('photo5');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo6')) {
            $photo = $request->file('photo6');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo7')) {
            $photo = $request->file('photo7');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo8')) {
            $photo = $request->file('photo8');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo9')) {
            $photo = $request->file('photo9');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo10')) {
            $photo = $request->file('photo10');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo11')) {
            $photo = $request->file('photo11');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo12')) {
            $photo = $request->file('photo12');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo13')) {
            $photo = $request->file('photo13');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo14')) {
            $photo = $request->file('photo14');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo15')) {
            $photo = $request->file('photo15');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo16')) {
            $photo = $request->file('photo16');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo17')) {
            $photo = $request->file('photo17');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo18')) {
            $photo = $request->file('photo18');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo19')) {
            $photo = $request->file('photo19');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        if (request()->hasFile('photo20')) {
            $photo = $request->file('photo20');
            $extension = $photo->getClientOriginalExtension();
            $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
            $photo->storeAs('/public', $filename);
            $str_for_bd .= '|||public/' . $filename;
        }

        $product->img = $str_for_bd;

        /*        if (isset($photos) && count($photos) > 0) {
                    foreach ($photos as $photo) {
                        $extension = $photo->getClientOriginalExtension();
                        $filename = 'product-photo-' . rand(0, 999999) . time() . '.' . $extension;
                        $paths[] = $photo->storeAs('/public', $filename);
                    }
                    $product->img = implode('|||', $paths);
                }*/

        // Пишем в бд
        $product->save();

        // Отправляю обратно в create
        $tag = $product->tag;

        $file = $request->file('img_product');
        // Отправляю обратно в create
        return redirect()->route('product', compact('tag', 'path'))->with('success', 'Продукт успешно создан!');
    }

    /* Корзина */
    public
    function basket()
    {
        //$basket_page = 'basket';
        //return view('basket', compact('orders', 'basket_page'));
        return view('basket');
    }

    /* Добавить в корзину */
    public
    function getAddToCart(ProductRequest $request, $id, $page = null)
    {
        $product = Product::find($id);
        //dd($product);

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);

        $cart->add($product, $product->product_id);

        $request->session()->put('cart', $cart);

        //dd($_GET['page_card']);
        $request->session()->put('cart', $cart);
        if (!isset($_GET['page'])) {
            return redirect()->route('index_catalog');
        } else {
            return redirect('shoppingCart');
        }
    }

    public
    function getAddToCart_view(ProductRequest $request, $id)
    {
        $product = Product::find($id);
        //dd($product);
        $id = $product->product_id;
        $tag = $product->tag;

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);

        $cart->add($product, $product->product_id);

        $request->session()->put('cart', $cart);

        return redirect()->route("product", compact('tag'));
    }

    /* Уменьшение количества одного товара */
    public
    function getReduceByOne($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);

        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }

        return redirect()->route('product.shoppingCart');
    }

    /* Удаление из корзины 1 товара */
    public
    function getRemoveItem($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);

        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }

        return redirect()->route('product.shoppingCart');
    }

    // Перейти в корзину
    public
    function getCart()
    {
        if (!Session::has('cart')) {
            return view('basket');
        }

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $products = $cart->items;
        $totalPrice = $cart->totalPrice;
        $title = 'Корзина Koenigsmann';
        return view('basket', compact('products', 'totalPrice', 'title'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function show($tag)
    {

        $product = Product::where('tag', '=', $tag)->first();
        $aks = Product::where('category', '=', 'aksessuary')->get();

        if (!$product) {
            return redirect()->route('index_catalog')->with('success', 'Данная страница не найдена!');
        }

        switch ($product->category) {
            case 'dlya-doma':
                $title = "Беговая дорожка " . $product->title . " - купить за " . number_format($product->price, 0, '', ' ') . " рублей";
                $meta_tag_description = 'Купите беговую дорожку ' . $product->title . ' за ' . number_format($product->price, 0, '', ' ') . ' рублей. Закажите беговую дорожку с доставкой на дом';
                $h1 = $product->compact_folding;
                break;
            case 'limited-edition':
                $title = "Беговая дорожка " . $product->title . " - купить за " . number_format($product->price, 0, '', ' ') . " рублей";
                $meta_tag_description = 'Купите беговую дорожку ' . $product->title . ' за ' . number_format($product->price, 0, '', ' ') . ' рублей. Закажите беговую дорожку с доставкой на дом';
                $h1 = $product->compact_folding;
                break;
            case 'aksessuary':
                $title = $product->type;
                $meta_tag_description = $product->level;
                $h1 = $product->compact_folding;
                break;
            case 'commerce':
                $title = "Беговая дорожка " . $product->title . " - купить за " . number_format($product->price, 0, '', ' ') . " рублей";
                $meta_tag_description = 'Купите беговую дорожку ' . $product->title . ' за ' . number_format($product->price, 0, '', ' ') . ' рублей. Закажите беговую дорожку с доставкой на дом';
                $h1 = $product->compact_folding;
                break;
            default:
                $title = "Купить" . $product->title;
                $meta_tag_description = "Купите" . $product->title;
                $h1 = $product->compact_folding;
        }

        $page_product_show = 'Карточка товара';

        return view('card_product', compact('product', 'aks', 'title', 'meta_tag_description', 'h1', 'page_product_show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($tag)
    {
        $product = Product::where('tag', '=', $tag)->first();

        if (\Auth::user()->id != 1) {
            return redirect()->route('index_catalog')->with('success', 'Вы не можете редактировать данный продукт!');
        }

        if (!$product) {
            return redirect()->route('index_catalog')->with('success', 'Вот хулюган!');
        }

        $product->data_img = explode('|||', $product->img);
        return view('edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(ProductRequest $request, $tag)
    {
        /*        $this->validate($request, [
                    'img' => 'reqired|dimensions:min_width=250,max_width=1000|mimes:jpeg,png,gif|max:2000'
                ]);*/

        $product = Product::where('tag', '=', $tag)->first();

        if (\Auth::user()->id != 1) {
            return redirect('index')->withErrors('danger', 'Вы не можете создавать продукт!');
        }

        $product->title = $request->title;
        $product->properties = $request->properties;
        $product->tag = $request->tag;
        $product->price = $request->price;
        $product->category = $request->category;
        $product->description = $request->description;

        // Большой список колонок
        $product->type = $request->type;
        $product->level = $request->level;
        $product->weight_trainer = $request->weight_trainer;
        $product->package_dimensions = $request->package_dimensions;
        $product->package_weight = $request->package_weight;
        $product->folding_design = $request->folding_design;
        $product->compact_folding = $request->compact_folding;
        $product->folded_dimensions = $request->folded_dimensions;
        $product->dimensions = $request->dimensions;
        $product->type_powered = $request->type_powered;
        $product->motor_warranty = $request->motor_warranty;
        $product->frame_warranty = $request->frame_warranty;
        $product->warranty_electronic_components = $request->warranty_electronic_components;
        $product->wear_parts_warranty = $request->wear_parts_warranty;
        $product->engine_power = $request->engine_power;
        $product->top_speed = $request->top_speed;
        $product->maximum_user_weight = $request->maximum_user_weight;
        $product->running_belt = $request->running_belt;
        $product->sizes_cloth = $request->sizes_cloth;
        $product->deck_thickness = $request->deck_thickness;
        $product->tilt_adjustment = $request->tilt_adjustment;
        $product->depreciation = $request->depreciation;
        $product->display = $request->display;
        $product->workout_time_display = $request->workout_time_display;
        $product->program_specifications = $request->program_specifications;
        $product->frame = $request->frame;
        $product->heart_rate_measurement = $request->heart_rate_measurement;
        $product->integration = $request->integration;
        $product->multimedia = $request->multimedia;
        $product->additionally1 = $request->additionally1;
        $product->additionally2 = $request->additionally2;
        $product->additionally3 = $request->additionally3;
        $product->front_shaft_diameter = $request->front_shaft_diameter;
        $product->rear_shaft_diameter = $request->rear_shaft_diameter;
        $product->distance_between_racks = $request->distance_between_racks;
        $product->type_of_plastic = $request->type_of_plastic;
        $product->energy_saving = $request->energy_saving;
        $product->noise_level = $request->noise_level;
        $product->equipment = $request->equipment;
        $product->speed_adjustment_step = $request->speed_adjustment_step;
        $product->availability = $request->availability; // Наличие на складе
        $product->instruction = $request->instruction;

        // Множественная загрузка изображений
        /*$photos = $request->file('photos');
        $paths = [];

        if (isset($photos) && count($photos) > 0) {
            foreach ($photos as $photo) {
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 999999) . time() . '.' . $extension;
                $paths[] = $photo->storeAs('/public', $filename);
            }
            $product->img = implode('|||', $paths);
        } else { // На случай если недобавили фото
            $product_for_img = Product::join('users', 'author_id', '=', 'users.id')->find($product->product_id);
            $product->img = $product_for_img->img;
        }*/

        if (isset($request->sorting) && mb_strlen($request->sorting) > 10) {
            $product->img = $request->sorting;
        } else {
            $str_for_bd = '';
            if (request()->hasFile('photo1')) {
                $photo = $request->file('photo1');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= 'public/' . $filename;
            }

            if (request()->hasFile('photo2')) {
                $photo = $request->file('photo2');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||' . 'public/' . $filename;
            }

            if (request()->hasFile('photo3')) {
                $photo = $request->file('photo3');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo4')) {
                $photo = $request->file('photo4');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo5')) {
                $photo = $request->file('photo5');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo6')) {
                $photo = $request->file('photo6');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo7')) {
                $photo = $request->file('photo7');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo8')) {
                $photo = $request->file('photo8');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo9')) {
                $photo = $request->file('photo9');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo10')) {
                $photo = $request->file('photo10');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo11')) {
                $photo = $request->file('photo11');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo12')) {
                $photo = $request->file('photo12');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo13')) {
                $photo = $request->file('photo13');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo14')) {
                $photo = $request->file('photo14');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo15')) {
                $photo = $request->file('photo15');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo16')) {
                $photo = $request->file('photo16');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo17')) {
                $photo = $request->file('photo17');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo18')) {
                $photo = $request->file('photo18');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo19')) {
                $photo = $request->file('photo19');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            if (request()->hasFile('photo20')) {
                $photo = $request->file('photo20');
                $extension = $photo->getClientOriginalExtension();
                $filename = 'product-photo-' . rand(0, 99999) . time() . '.' . $extension;
                $photo->storeAs('/public', $filename);
                $str_for_bd .= '|||public/' . $filename;
            }

            $str_for_bd = $product->img . '|||' . $str_for_bd;

            if (mb_substr($str_for_bd, 0, 3) == '|||') {
                $str_for_bd = substr($str_for_bd, 3);
            }

            if (mb_substr($str_for_bd, -3) == '|||') {
                $str_for_bd = substr($str_for_bd, 0, -3);
            }

            $str_for_bd = str_ireplace('||||||', '|||', $str_for_bd);

            $product->img = $str_for_bd;
        }

        // Обновляем продукт в бд
        $product->update();

        // Отправляю обратно в edit
        //$tag = $product->tag;
        return redirect()->back()->with('success', 'Продукт успешно обновлён!');
        //return view('edit', compact('product', 'scroll '));
        //return redirect()->route('product', compact('tag'))->with('success', 'Продукт успешно обновлён!');
    }

    public function destroy_photo($str, $id_product)
    {
        @unlink(public_path('/storage/' . $str));
        $product = Product::where('product_id', '=', $id_product)->first();
        $new_str = str_ireplace('public/' . $str, '', $product->img);
        $new_str = str_ireplace('||||||', '|||', $new_str);

        if (mb_substr($new_str, 0, 3) == '|||') {
            $new_str = substr($new_str, 3);
        }

        if (mb_substr($new_str, -3) == '|||') {
            $new_str = substr($new_str, 0, -3);
        }

        $product->img = isset($new_str) ? $new_str : NULL;
        $product->update();

        return redirect()->back()->with('success', 'Изображение удалено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($tag)
    {
        $product = Product::where('tag', '=', $tag)->first();

        if (\Auth::user()->id != 1) {
            return redirect('index')->withErrors('danger', 'Вы не можете создавать продукт!');
        }

        $product->delete();

        return redirect('index_catalog')->with('success', 'Продукт успешно удалён!');
    }

    /* Переход на страницу Оформление заказа */
    public
    function getCheckout()
    {
        if (!Session::has('cart')) {
            return view('basket');
        }

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice;

        $checkout_page = 'Для определения страницы, оформления';
        $meta_tag_description = 'Оформление заказа';
        return view('checkout', compact('total', 'checkout_page', 'meta_tag_description'));
    }

    /* Оформление заказа */
    public
    function postCheckout(Request $request)
    {
        if (!Session::has('cart')) {
            return view('basket');
        }

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        Session::forget('cart');
        //dd($cart->totalPrice, $request['email']);

        /* Запись в бд */
        $order = new Order();
        $order->cart = serialize($cart);
        $order->fio = $request->input('user__fio');
        $order->email = $request->input('email');
        $order->phone = $request->input('phone');
        $order->city_list = $request->input('city_list');
        $order->delivery_terms = $request->input('delivery_terms');
        $order->points_issue = $request->input('points_issue');
        $order->city_list_pickup = $request->input('city_list_pickup');
        $order->points_pickup = $request->input('points_pickup');
        $order->method_cash = $request->input('method_cash');
        $order->method_card = $request->input('method_card');

        $order->comment = 'Насёлённый пункт' . $request->input('inhabited_locality') . ' Улица: ' . $request->input('street') . ' Подъезд: ' . $request->input('porch') . ' Квартира: ' . $request->input('apartment') . ' Комментарий: ' . $request->input('comment');

        if (Auth::check()) {
            /**
             * После проверки уже можешь получать любое свойство модели
             * пользователя через фасад Auth, например id
             */
            $order->user_id = Auth::user()->id;
        }

        // Пишем в бд
        $order->save();

        /* Отправка email*/
        $this->order_sending($cart, $request);

        sleep(1);
        //dump($request, $cart);
        return redirect()->route('basket')->with('success', 'Заказ оформлен!');
    }

    /* Отправка заказа на email -ы */
    //public function order_sending($id_order, $email, $fio, $phone, $delivery_method, $address, $comment, $arr_product)
    public
    function order_sending($cart, $request)
    {

        $order = new Order();
        $order->cart = serialize($cart);
        $order->fio = $request->input('user__fio');
        $order->email = $request->input('email');
        $order->phone = $request->input('phone');
        $order->city_list = $request->input('city_list');
        $order->delivery_terms = $request->input('delivery_terms');
        $order->points_issue = $request->input('points_issue');
        $order->city_list_pickup = $request->input('city_list_pickup');
        $order->points_pickup = $request->input('points_pickup');
        $order->method_cash = $request->input('method_cash');
        $order->method_card = $request->input('method_card');

        $order->comment = 'Насёлённый пункт' . $request->input('inhabited_locality') . ' Улица: ' . $request->input('street') . ' Подъезд: ' . $request->input('porch') . ' Квартира: ' . $request->input('apartment') . ' Комментарий: ' . $request->input('comment');


        //dd($order, $request);

        if (Auth::check()) {
            $orders = (array)$cart; // Преобразую в массив
            /*            $orders = Auth::user()->orders;
                        $orders->transform(function ($order, $key) {
                            $order->cart = unserialize($order->cart);
                            return $order;
                        });*/
        } else {
            $orders = (array)$cart; // Преобразую в массив
        }

        // Отправка E-mail
        $name_label_email = 'Заказ на сайте: ' . $_SERVER['SERVER_NAME'];

        $mail = new PHPMailer(true);

        try {

            $mail->CharSet = 'utf-8';
            // Настройки SMTP
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPDebug = 0;
            $mail->Host = 'ssl://smtp.yandex.ru';
            $mail->Port = 465;
            $mail->Username = 'info@koenigsmann.ru';
            $mail->Password = 'W!tee6Fp45';

            // От кого письмо
            $mail->setFrom('info@koenigsmann.ru' /*, $_SERVER['SERVER_NAME']*/);
            //$mail->setFrom('belocerkovecden@mail.ru' /*, $_SERVER['SERVER_NAME']*/);
            // Устанавливаем формат сообщения в HTML
            $mail->isHTML(true);
            // Тема письма
            $mail->Subject = $name_label_email;
            // Тело письма

            $email_t = new TemplateEmail();

            $id_order = date('d.m.Y - h:i:s A');
            $body_letter = $email_t->order_body_letter($id_order, $request['email'], $order->fio, $order->phone, $order->delivery_terms, $order->city_list, $order->method_cash, $order->method_card, $order->comment, $orders);
            $mail->Body = $body_letter;

// Добавляем получателей
            /*            $mail->addAddress('belocerkovecden@gmail.com', 'Тест ');
                        $mail->send();

                        $mail->clearAddresses();*/

            $mail->addAddress('info@koenigsmann.ru', 'Дорогой Админ сайта ' . $_SERVER['SERVER_NAME']);
            $mail->send();

            $mail->clearAddresses();

            $mail->addAddress($request->input('email'), 'Уважаемый покупатель');
            $mail->send();

        } catch (Exception $e) {
            echo "Письмо не отправлено. PHPMailer ошибка: {$mail->ErrorInfo}";
        }
    }

}
