<?php

namespace App\Http\Controllers;


use App\Cart;
use App\Contact;
use App\Order;
use App\Partner;
use Auth;
use App\TemplateEmail;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Product;
use Session;

class UserController extends Controller
{

    public function __construct()
    {
        // Если пользователь не авторизован под админом, то ему запрещено использовать методы данного класса
        // Кроме index и show
        $this->middleware('auth')->except('index');

        //Устанавливаю часовой пояс
        date_default_timezone_set('Europe/Moscow');
    }

    public function getProfile()
    {
        $orders = Auth::user()->orders;
        $orders->transform(function ($order, $key) {
            $order->cart = unserialize($order->cart);
            return $order;
        });
        //$orders = (array)$orders;
        //dd($orders);
        $user_page = 'user_page';
        /*return view('userinfo', compact('orders', 'user_page'));*/
        return view('personal_area', compact('orders', 'user_page'));
    }

    public function getUserInfo()
    {
        $orders = Auth::user()->orders;
        $orders->transform(function ($order, $key) {
            $order->cart = unserialize($order->cart);
            return $order;
        });

        $partners = Partner::orderBy('id', 'desc')->get();
        $contacts = Contact::orderBy('id', 'desc')->get();

        //$orders = (array)$orders;
        //dd($orders);
        $user_page = 'user_page';
        /*return view('userinfo', compact('orders', 'user_page'));*/
        return view('userinfo', compact('orders', 'user_page', 'partners', 'contacts'));
    }
}
