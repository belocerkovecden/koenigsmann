<?php

namespace App\Http\Controllers;

use App\Partner;
use Illuminate\Http\Request;
use App\Http\Requests\PartnerRequest;
use App\Cart;
use App\Order;
use Auth;
use App\TemplateEmail;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Str;
use Illuminate\Routing\Controller;
//use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Product;
use Session;
use Illuminate\Support\Collection;
use Illuminate\Http\File;

class PartnerController extends Controller
{
    public function create(Request $request)
    {

        //dd($request);
        $partner = new Partner();
        $partner->contact_person_name = $request->contact_person_name;
        $partner->contact_person_phone = $request->contact_person_phone;
        $partner->contact_person_email = $request->contact_person_email;
        $partner->organization_address = $request->organization_address;
        $partner->organization_inn = $request->organization_inn;
        $partner->contract = $request->contract;
        $partner->sales_through = $request->sales_through;
        $partner->trading_region = $request->trading_region;
        $partner->showroom = $request->showroom;
        $partner->have_delivery_service = $request->have_delivery_service;
        $partner->location_city = $request->location_city;

        $files = $request->file('files');
        $paths = [];

        if (isset($files) && count($files) > 0) {
            foreach ($files as $photo) {
                $extension = $photo->getClientOriginalExtension();
                $filename = 'userfiles-' . rand(0, 999999) . time() . '.' . $extension;
                $paths[] = $photo->storeAs('/userfiles', $filename);
            }
            $partner->files = implode('|||', $paths);
        }

        // Вопросы
        $partner->save();

        // Отправка E-mail
        $name_label_email = 'Партнёр на сайте: ' . $_SERVER['SERVER_NAME'];

        $mail = new PHPMailer(true);

        try {
            $msg = "ok";
            $mail->CharSet = 'utf-8';
            // Настройки SMTP
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPDebug = 0;
            $mail->Host = 'smtp.yandex.ru';
            $mail->Port = 465;
            $mail->Username = 'info@koenigsmann.ru';
            $mail->Password = 'W!tee6Fp45';
            $mail->SMTPSecure = "ssl"; // or ssl

            // От кого письмо
            $mail->setFrom('info@koenigsmann.ru');
            // Устанавливаем формат сообщения в HTML
            $mail->isHTML(true);
            // Тема письма
            $mail->Subject = $name_label_email;

            $arr = explode('|||', $partner->files);

            $str = '';
            foreach ($arr as $value) {
                $str .= '<a href="https://koenigsmann.ru/'. $value .'"></a>';
            }
            //dd($arr, $str);
            // Тело письма
            $body_letter = 'ФИО: ' . $request->contact_person_name
                . ' | Телефон: ' . $request->contact_person_phone
                . ' | Email: ' . $request->contact_person_email
                . ' | Адресс: ' . $request->organization_address
                . ' | ИНН: ' . $request->organization_inn
                . ' | Тип договора: ' . $request->contract
                . ' | Регион осуществления торговли: ' . $request->trading_region
                . ' | Есть ли шоу-рум: ' . $request->showroom
                . ' | Есть ли у вас собственная служба доставки: ' . $request->have_delivery_service
                . ' | Город нахождения: ' . $request->location_city
                . ' | Осуществляют продажи через: ' . $request->sales_through;
            $mail->Body = $body_letter;

            // Прикрипление файлов к письму
            if (!empty($_FILES['files']['name'][0])) {
                for ($ct = 0; $ct < count($_FILES['files']['tmp_name']); $ct++) {
                    $uploadfile = tempnam(sys_get_temp_dir(), sha1($_FILES['files']['name'][$ct]));
                    $filename = $_FILES['files']['name'][$ct];
                    if (move_uploaded_file($_FILES['files']['tmp_name'][$ct], $uploadfile)) {
                        $mail->addAttachment($uploadfile, $filename);
                    } else {
                        $msg .= 'Неудалось прикрепить файл ' . $uploadfile;
                    }
                }
            }

            $mail->addAddress('info@koenigsmann.ru', 'Петрова Елизавета');
            $mail->send();

        } catch (Exception $e) {
            echo "Письмо не отправлено. PHPMailer ошибка: {$mail->ErrorInfo}";
        }
        return view('partnership', ['partner' => $partner])->with('partner_success', 'Заявка успешно отправлена');
        unset($partner_success);
//        return redirect()->route('partnership', ['partner' => $partner])->with('success', 'Заявка подана!');
    }

    
        public function service(Request $request)
    {
        // Отправка E-mail
        $name_label_email = 'Сервисное обращение: ' . $_SERVER['SERVER_NAME'];

        $mail = new PHPMailer(true);

        try {
            $msg = "ok";
            $mail->CharSet = 'utf-8';
            // Настройки SMTP
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPDebug = 0;
            $mail->Host = 'smtp.yandex.ru';
            $mail->Port = 465;
            $mail->Username = 'info@koenigsmann.ru';
            $mail->Password = 'W!tee6Fp45';
            $mail->SMTPSecure = "ssl"; // or ssl

            // От кого письмо
            $mail->setFrom('info@koenigsmann.ru');
            // Устанавливаем формат сообщения в HTML
            $mail->isHTML(true);
            // Тема письма
            $mail->Subject = $name_label_email;

            // Тело письма
            $body_letter = 'ФИО: ' . $request->fio
                . ' | Телефон: ' . $request->contact_person_phone
                . ' | Email: ' . $request->contact_person_email
                . ' | № Заказа: ' . $request->order_number
                . ' | Название модели / ссылка на сайте: ' . $request->model_name
                . ' | Дата покупки товара / дата получения в ТК: ' . $request->purchase_date
                . ' | Номер ганатийного талона: ' . $request->warranty_card_number
                . ' | Подробное описании рекламации: ' . $request->description
                . ' | Желаемое решение проблемы: ' . $request->solution_problem;
            $mail->Body = $body_letter;

            // Прикрипление файлов к письму
            if (!empty($_FILES['files']['name'][0])) {
                for ($ct = 0; $ct < count($_FILES['files']['tmp_name']); $ct++) {
                    $uploadfile = tempnam(sys_get_temp_dir(), sha1($_FILES['files']['name'][$ct]));
                    $filename = $_FILES['files']['name'][$ct];
                    if (move_uploaded_file($_FILES['files']['tmp_name'][$ct], $uploadfile)) {
                        $mail->addAttachment($uploadfile, $filename);
                    } else {
                        $msg .= 'Неудалось прикрепить файл ' . $uploadfile;
                    }
                }
            }

            //$mail->addAddress('belocerkovecden@mail.ru', 'Петрова Елизавета');
            $mail->addAddress('service@koenigsmann.ru', 'Петрова Елизавета');
            $mail->send();

        } catch (Exception $e) {
            echo "Письмо не отправлено. PHPMailer ошибка: {$mail->ErrorInfo}";
        }
        return redirect()->back()->with('success', 'Заявка успешно отправлена');
        //return view('service')->with('partner_success', 'Заявка успешно отправлена');
        //unset($partner_success);
//        return redirect()->route('partnership', ['partner' => $partner])->with('success', 'Заявка подана!');
    }
}