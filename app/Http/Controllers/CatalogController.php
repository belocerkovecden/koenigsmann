<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class CatalogController extends Controller
{
    public function __construct()
    {
        // Если пользователь не авторизован под админом, то ему запрещено использовать методы данного класса
        // Кроме index_catalog
        $this->middleware('auth')->except('index_catalog', 'getProduct');
        date_default_timezone_get('Europe/Moscow');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_catalog(Request $request)
    {
        if ($request->search) {
            $products = Product::join('users', 'author_id', '=', 'users.id')
                ->where('title', 'like', '%' . $request->search . '%')
                ->orWhere('description', 'like', '%' . $request->search . '%')
                ->orWhere('name', 'like', '%' . $request->search . '%')
                ->orderBy('products.product_id', 'asc')
                ->get();
            $menu_active = 'search';
            $title = "Каталог спортивных тренажеров Koenigsmann";
            $meta_tag_description = 'Купите спортивный тренажер Koenigsmann. Закажите спортивный тренажер с доставкой на дом';

            // dd($request);
            return view('catalog', compact('products', 'menu_active', 'title', 'meta_tag_description'));
        } else {

            //$products = Product::all(); // Получаем все продукты из таблицы продукты
            $products = Product::join('users', 'author_id', '=', 'users.id')
                ->orderBy('products.product_id', 'asc')// Отсортировать по id
                ->paginate(15); // Пагинация
            $menu_active = 'catalog';

            $title = "Каталог спортивных тренажеров Koenigsmann";
            $meta_tag_description = 'Купите спортивный тренажер Koenigsmann. Закажите спортивный тренажер с доставкой на дом';
            $h1 = '';

            if ($request->getRequestUri() === '/category/begovye-dorozhki/') {
                $title = "Каталог беговых дорожек Koenigsmann";
                $meta_tag_description = 'Купите беговую дорожку Koenigsmann. Закажите беговую дорожку с доставкой на дом';
                $h1 = 'Беговые дорожки';
            }

            return view('catalog', compact('products', 'menu_active', 'title', 'meta_tag_description', 'h1')); // Генерируем вьюху и передаём туда все полученные продукты
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProduct()
    {
        $products = Product::where('category', 'dlya-doma')
            ->orWhere('category', 'commerce')
            ->orWhere('category', 'limited-edition')
            ->orderBy('products.product_id', 'asc')
            ->get();
        $arr_res = [];
        foreach ($products as $product) {
            $arr = explode("|||", $product->img);
            for ($i = 0; $i < count($arr); $i++) {
                $clear_str = explode('/', $arr[$i]);
                $clear_str = 'https://koenigsmann.ru/storage/' . $clear_str[1];
                $arr_res[$product->product_id][$i] = $clear_str;
            }
        }

        /* Формирование листа YML */
        $str = '';
        foreach ($products as $product) {
            $str .= '<offer available="true" id="' . $product->product_id . '">';
            $str .= '<url>https://koenigsmann.ru/product/' . $product->tag . '/</url>';
            $str .= '<price>' . $product->price . '.00</price>';
            $str .= '<currencyId>RUB</currencyId>';
            $str .= '<categoryId>';
            if ($product->category == 'dlya-doma') {
                $str .= '1';
            } elseif ($product->category == 'commerce') {
                $str .= '2';
            } elseif ($product->category == 'limited-edition') {
                $str .= '3';
            } elseif ($product->category == 'aksessuary') {
                $str .= '4';
            }
            $str .= '</categoryId>';
            foreach ($arr_res[$product->product_id] as $item) {
                if (count($arr_res[$product->product_id]) == 2) {
                    $str .= '<picture>' . $arr_res[$product->product_id][0] . '</picture>';
                } else {
                    $str .= '<picture>' . $item . '</picture>';
                }
            }
            $str .= '<name>';

            if ($product->category == 'dlya-doma')
                $str .= 'Беговая дорожка' . $product->title;
            elseif ($product->category == 'commerce')
                $str .= 'Беговая дорожка' . $product->title;
            elseif ($product->category == 'limited-edition')
                $str .= 'Беговая дорожка' . $product->title;
            elseif ($product->category == 'aksessuary')
                $str .= $product->title;

            $str .= '</name>';
            $str .= '<description><![CDATA[' . $product->properties . ']]></description>';
            $str .= '<manufacturer_warranty>true</manufacturer_warranty>';
            $str .= '</offer>';
        }

        $new_date = date("Y-m-d H:i", time() + (60*60*3));

        $yml = '<?xml version="1.0" encoding="utf-8"?>
        <!DOCTYPE yml_catalog SYSTEM "shops.dtd">
        <yml_catalog date="' . $new_date . '">
            <shop>
        <name>koenigsmann.ru | Официальный сайт производителя спортивных тренажеров Koenigsmann</name>
        <company>Koenigsmann</company>
        <url>https://koenigsmann.ru/</url>
        <phone>+7 (499) 350-15-85</phone>
        <platform>Laravel</platform>
        <version>Laravel Framework 5.8.36</version>
        <currencies>
            <currency id="RUB" rate="1"/>
        </currencies>
        <categories>
            <category id="1">Беговые дорожки для дома</category>
            <category id="2">Беговые дорожки для зала</category>
            <category id="3">Беговые дорожки Limited Edition</category>
            <category id="4">Аксессуары для беговых дорожек</category>
        </categories>
        <delivery-options>
            <option cost="0" days="" order-before="24"/>
        </delivery-options>
        <offers>' . $str . '</offers></shop></yml_catalog>';
        /* END Формирование листа YML */

        return $yml;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
