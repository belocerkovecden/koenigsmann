<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Auth;
use PHPMailer\PHPMailer\Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

use App\Cart;
use App\Order;
use App\TemplateEmail;
use PHPMailer\PHPMailer\PHPMailer;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Product;
use Session;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class EndSlashProvider extends ServiceProvider
{

/*    public function __construct()
    {
        $this->middleware('DontEndSlashMiddleware')->except('category', 'page', 'remove', 'reduce', 'add-to-cart');
    }*/

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    /*
     * Для редиректа с урл без слэша на урл с слэшом
     * */
    public function handle($request, Closure $next)
    {
        //dd($request);
        /*
         Исключаю те страницы которым не нужны слэши
         */
        if($request->getRequestUri() !== '/'
            && $request->getRequestUri() !== 'add-to-cart'
            && $request->getRequestUri() !== 'reduce'
            && $request->getRequestUri() !== 'remove'
            && $request->getRequestUri() !== 'category'
            && $request->getRequestUri() !== 'logout'
            && $request->getRequestUri() !== 'catalog'
            && $request->getRequestUri() !== 'edit'
            && $request->getRequestUri() !== 'create'
            && $request->getRequestUri() !== '/search/'
            && $request->getRequestUri() !== '?page'
            && $request->getRequestUri() !== 'page'
            && $request->getRequestUri() !== 'category?page=2'
            && $request->getRequestUri() !== '/category?page=2/'
            && $request->getRequestUri() !== 'catalog'
            && $request->getPathInfo() !== '/search/'
        ){
            //dd($request->getRequestUri(), $request->getPathInfo());
            if (!preg_match('/.+\/$/', $request->getRequestUri()))
            {
                $base_url = Config::get('app.url');
                return Redirect::to($base_url.$request->getRequestUri().'/');
            }
        }
        return $next($request);
    }
}
