<?php

namespace App;



class TemplateEmail
{
    private $social;
    private $fb;
    private $vk;
    private $inst;

    public function __construct()
    {

        $this->fb = 'https://www.facebook.com/koenigsmann.ru/';
        $this->vk = 'https://vk.com/koenigsmann';
        $this->inst = 'https://www.instagram.com/koenigsmann.ru/';
    }

    public function order_body_letter($id_order, $email, $fio, $phone, $delivery_method, $address, $method_cash, $method_card, $comment, $orders=null)
    {
        $str = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body style="margin: 0 auto;padding: 20px 0;background: #ffffff;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="#eeeeee" class="wrap-table-email"
       style="max-width: 600px;min-width: 320px;width: 100%;border-collapse: collapse;border-spacing: 0;margin: 0 auto;padding: 0;
  mso-table-lspace: 0pt;mso-table-rspace: 0pt;background: #F5F5F5;">
    <tbody>

    <tr>
        <td class="wrap-td-header" style="margin: 0;padding: 0;">
            <table class="table-header" border="0" cellpadding="0" cellspacing="0" width="100%" align="center"
                   bgcolor="#256AA2"
                   style="max-width: 600px;min-width: 320px;width: 100%;border-collapse: collapse;border-spacing: 0;margin: 0 auto;padding: 0; mso-table-lspace: 0pt;mso-table-rspace: 0pt;background: #1c1c1c;color: #ffffff;">
                <tbody>
                <tr>
                <td>
                <img style="text-align: center; margin: 0 auto; display: block;" src="https://lh3.googleusercontent.com/aKcsdPwstgWpzBlsefUIuoqhza_RlNurQAg4VaolFaBtfgp4aAta4bXzzZlTcWIfeAlqRfko08_gFwOu6kPntCOKQ2ZAtAYJuzdBTgEPsLsf_iaJkXCPwcoWvBYpVbrpounOTMf8-jNoQ3tAqO7MnWLgABDph3oeCABfrtOzK3VapjH8S-2e5QoG9wfLS9hxeGjEhCVuSmQ9ME7QAG93gptR2kbUGe4ed8_bGDbBzxCACjUIZaXY1J2u5BEsZUyG8uk1cmwYtvVoEsHkpHCFqtvUk2HderSFoag4kET1YZXZk2PjoHD9G4B3QtziKwHMiJK4xptWaJwcyUGv_stOS5f0uhRqCOsh9EFFAKLNKv5BfKriFfH3fpW_V4aBUb1YVYOfZ0zQHxmFEJsc3Gupj6sM_bg2Q63jOaAgfnW5wqAIxdwkmVOpAzR5yVJFhdIxaHMY8xDztiM1HLb9h_gzQvuZJCmBZUoHFukFuNB79QQ8IjiQJes_m3Tz6DCvrcSZlkv2W-QgyEEdQd0HVoNOUXtMUJCbwbWgJkxRocoIHwV12bMDB7iCWf-zdkEUVC0Vc0MPNSlfC1xQQcoTNKgXHt9fQUCbV40JzJ7iB38S5t5UVuYQOUS_tkE0fNDAcyG5ECsB1vatLGZySwuRWBzrYMz3Yvht1_8XySYOzOexCoRTQjQWrlmwZw=w308-h48-no" alt="logo">
</td>
</tr>
                    <tr>
                        <td class="td-header" align="center" color="#ffffff"
                            style="margin: 0;padding: 15px 15px;font-family: Arial, Verdana, sans-serif;color: #ffffff;font-size: 16px;line-height: 22px;mso-line-height-rule: exactly;font-style: normal;font-weight: bold;letter-spacing: normal;text-align: center;">
                            Ваш заказ № ' . $id_order . '
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>

    <tr>
        <td class="wrap-td-content" style="margin: 0;padding: 0;">
            <table class="table-content" border="0" cellpadding="0" cellspacing="0" width="100%" align="center"
                   bgcolor="#eeeeee"
                   style="max-width: 600px;min-width: 320px;width: 100%;border-collapse: collapse;border-spacing: 0;margin: 0 auto;padding: 0; mso-table-lspace: 0pt;mso-table-rspace: 0pt;background: #F5F5F5;">
                <tbody>
                <tr>
                    <td class="td-content-text"
                        style="margin: 0;padding: 15px;font-family: Arial, Verdana, sans-serif;color: #333333;font-size: 15px;line-height: 20px;mso-line-height-rule: exactly;font-style: normal;font-weight: normal;letter-spacing: normal;text-align: center;text-indent: 10px;">
                        Здравствуйте!<br>
                        Вы оформили заказ на сайте <b>' . $_SERVER['SERVER_NAME'] . '</b>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table class="m_5885824213983040779MsoNormalTable" border="1" cellspacing="0" cellpadding="0"
                               width="100%" style="width:100.0%;border:outset #333333 1.0pt">
                            <tbody>
                            <tr>
                                <td colspan="2" style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">Заказ № ' . $id_order . '<u></u><u></u></p></td>
                            </tr>
                            <tr>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">Email<u></u><u></u></p></td>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal"><a href="mailto:' . $email . '" target="_blank">' . $email . '</a><u></u><u></u>
                                </p></td>
                            </tr>
                            <tr>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">ФИО<u></u><u></u></p></td>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">' . $fio . '<u></u><u></u></p></td>
                            </tr>
                            <tr>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">Телефон<u></u><u></u></p></td>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">' . $phone . '<u></u><u></u></p></td>
                            </tr>
                            <tr>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">Доставка<u></u><u></u></p></td>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">' . $delivery_method . '<u></u><u></u></p></td>
                            </tr>
                            <tr>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">Адрес<u></u><u></u></p></td>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">' . $address . '<u></u><u></u></p></td>
                            </tr>
                            <tr>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">Метод оплаты<u></u><u></u></p></td>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">Наличка: ' . $method_cash . ' | Картой: ' . $method_card . '<u></u><u></u></p></td>
                            </tr>
                            <tr>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">Примечание<u></u><u></u></p></td>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">' . $comment . '<u></u><u></u></p></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="height: 30pt">

                    </td>
                </tr>

                <tr>
                    <td>
                        <table class="m_5885824213983040779MsoNormalTable" border="1" cellspacing="0" cellpadding="0"
                               width="100%" style="width:100.0%;border:outset #333333 1.0pt">
                            <tbody>
                            <tr>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">Название товара<u></u><u></u></p></td>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">Количество<u></u><u></u></p></td>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">Цена за шт.<u></u><u></u></p></td>
                                <td style="border:inset #333333 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p
                                        class="MsoNormal">Стоимость учитывая количество.<u></u><u></u></p></td>
                            </tr>';
        $total_price = 0;
//dd($orders);
/*        if(gettype($orders) === 'object'){
            $ord = '';
            foreach($orders as $order) {
                $ord .='<tr class="list-group">';
                foreach($order->cart->items as $item) {
                    $ord .= '<td class="list-group-item">' . $item['item']['title'] . '</td>';
                    $ord .= '<td class="list-group-item">' . $item['item']['qty'] . '</td>';
                    $ord .= '<td class="list-group-item">' . $item['item']['price'] . '</td>';
                    $sum = $item['item']['price'] * $item['item']['qty'];
                    $ord .= '<td class="list-group-item">' . $sum . '</td>';
                }
                $ord .=  '</tr>';

                $total_price = $order->cart->totalPrice;
            }
            $str .= $ord;
        } else*/ if(gettype($orders) === 'array') {
            $ord = '';

            foreach ($orders['items'] as $i) {
                $ord .='<tr class="list-group">';
                    $ord .= '<td class="list-group-item" style="text-align:center;"><a href="https://'. $_SERVER['SERVER_NAME'] . '/' . $i['item']->product_id .'">' . $i['item']->title . '</td>';
                    $ord .= '<td class="list-group-item" style="text-align:center;">' . $i['qty'] . '</td>';
                    $ord .= '<td class="list-group-item" style="text-align:center;">' . number_format($i['item']->price, 0, '', ' ')  . '</td>';
                    $sum = $i['item']->price * $i['qty'];
                    $ord .= '<td class="list-group-item" style="text-align:center;">' . $sum . '</td>';
                $ord .=  '</tr>';
            }
            $str .= $ord;
            $total_price = $orders['totalPrice'];
        }

        $str .= '</tbody>
                        </table>
                    </td>
                </tr>
               <tr>
                    <td class="td-content-text" style="margin: 0;padding: 15px;font-family: Arial, Verdana, sans-serif;color: #333333;font-size: 15px;line-height: 20px;mso-line-height-rule: exactly;font-style: normal;font-weight: normal;letter-spacing: normal;text-align: center;text-indent: 10px;">
                        Итого общая сумма: ' . number_format($total_price, 0, '', ' ') . ' руб.
                    </td>
                </tr>
               <tr>
                    <td class="td-content-text" style="margin: 0;padding: 15px;font-family: Arial, Verdana, sans-serif;color: #333333;font-size: 15px;line-height: 20px;mso-line-height-rule: exactly;font-style: normal;font-weight: normal;letter-spacing: normal;text-align: center;text-indent: 10px;">
                        Благодарим за обращение в наш интернет – магазин.
                    </td>
                </tr>
                <tr>
                    <td align="center" class="td-content-btn" style="margin: 0;padding: 0;text-align: center;">
                        <a target="_blank" href="https://' . $_SERVER['SERVER_NAME'] . '/" class="content-button"
                           style="display: inline-block;background: #c70000;color: #ffffff;padding: 10px 25px;margin: 16px auto;border-radius: 5px;text-decoration: none;font-family: Arial, Verdana, sans-serif;mso-line-height-rule: exactly;font-style: normal;font-weight: normal;letter-spacing: normal;text-align: center;line-height: 20px;">Перейти
                            на сайт</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>';

        if (!empty($this->fb) && !empty($this->ok) && !empty($this->inst)) {
            $str .= '<tr>
        <td class="wrap-td-footer" style="margin: 0;padding: 0;">
            <table align="center" class="table-footer" border="0" cellpadding="0" cellspacing="0" width="100%"
                   bgcolor="#eeeeee"
                   style="max-width: 600px;min-width: 320px;width: 100%;border-collapse: collapse;border-spacing: 0;margin: 0 auto;padding: 0; mso-table-lspace: 0pt;mso-table-rspace: 0pt;background: #F5F5F5;">
                <tbody>
                <tr>
                    <td style="margin: 0;padding: 0;">
                        <table class="table-social-title" border="0" cellpadding="0" cellspacing="0" width="100%"
                               align="center" bgcolor="#F4F4F4" style="max-width: 600px;min-width: 320px;width: 100%;border-collapse: collapse;border-spacing: 0;margin: 0 auto;padding: 0;
  mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                            <tbody>
                            <tr>
                                <td class="td-social-title" align="center"
                                    style="margin: 0;padding: 20px 15px 5px;font-family: Arial, Verdana, sans-serif;color: #256AA2;font-size: 15px;line-height: 20px;mso-line-height-rule: exactly;font-style: normal;font-weight: normal;letter-spacing: normal;text-align: center;">
                                    Мы в социальных сетях:
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="margin: 0;padding: 0;">
                        <table class="table-social-buttons" border="0" cellpadding="0" cellspacing="0" width="210px"
                               align="center" bgcolor="#F4F4F4" style="max-width: 600px;min-width: 320px;width: 50px;border-collapse: collapse;border-spacing: 0;margin: 16px auto;padding: 0;
  mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                            <tbody>
                            <tr align="center">
                            <td></td>
                                <td width="50px" class="td-social-buttons" style="margin: 0;padding: 0;">
                                    <a target="_blank" href="' . $this->fb . '"
                                       class="social-buttons">
                                        <img alt="fb" width="30px" height="30px" class="social-img"
                                             src="https://lh3.googleusercontent.com/TaG9VS6TeYNNWIggia0fq4Qmv410n4yEftDUOlAZHH1bsnHJKpryNPBOMg2trP8i7zJxiM4PA5d0l2hnwHcKoVJMkT4TT45u7Rae21nWZPiTDWfL1r9mFfSyeCd9PmfjJ-9ejft6HQbP1sifApfjaU-F-Px6OWwFxW_5LCbQfjVElc3zCrxILJZLgVhzJcY0CJhFlMXbXkRe2D-33jGZuhbFiBod2zMlSj_P_k0Th2Gi9myvEt9yCzOIY8dEigABHaiuBdR2q5bMkNRsC2sHs9BFpADTuVyMVYtRLXHZgAV8vl4ISyY_BUVUrlvU8d52LVr0VzGXnPEX5f_Vu8OqKvEFUH6sxaRTCC6ccVto6Sro7zRMp0Fbts_D24-PwRSg5E-FDhkQxDi1mwpog9wtTCyQsmsmEfKsEO-4GWojifXyN-7WrHydgQEG85MFzDxPPurCjuGe33LbyKJ3GqdqE06dKsTjm8kfDyViWxWnwOwBVGRK9qg-Xnxz2Z5bBnZlfedKUB8Wr6qClRYNJOglaRy0j2HekbXWNPPADY2VDy381z5ezC6Bg3mZ5lCThzlQWdsv_sbajzq1FdxJ07MTGR_uP35JSF8kYo09Ogt8qvxe1gx66phbAtY_haI0w1vRG5WFIJn-KhLxhoAVUhXauxDsjaSZco4=s75-no"
                                             style="outline: none;-ms-interpolation-mode: bicubic;display: block;margin: 0 auto;padding: 0;">
                                    </a>
                                </td>
                                <td width="50px" class="td-social-buttons" style="margin: 0;padding: 0;">
                                    <a target="_blank" href="' . $this->vk . '"
                                       class="social-buttons">
                                        <img alt="vk" width="30px" height="30px" class="social-img"
                                             src="https://lh3.googleusercontent.com/P5yc1Gm_MaXWGtYNfamduEYUN2aPk7ImeztOkso8MAyZiJlfjxg2j5-Wt2WIZotgPyY-nQXmK3FMthF7rybF_gPRBVls6uFGI6woGS0gJfX2IbWY9rEJ5bcRemuQ0KXJqrLyeHQvsFxa678mQMwWUmRh3JVfKuiYMUOWWQdBrzu_DLmyOozZakg-eAHrXiOBz5JCkNlwFEaDBpzymd4pF-6r64ju7mALmTJMUheBAuOuN8dsaxu1EOky5z2SxYMOlI1oojb0xFGWwbfCNgACjbjJnLT7JjCEecfXn6Z3P3YquIQJ4PfFN7mBJRZpkYUIK3_cXuaS14bYseDMtNNxIPkuhZLiaIJJLjCdZ2giJYd_mNMMTlsIrgAMEjgOMtKp5tEfwQESRLCGAolGtCPTLTi23givI8Xlhx-ZpNBzm8xPR3f4QkkGL0JVZj7L4cw3AcxL2NxgJDPxC-uuvzlxmL6GrpaUES31TuOmUEGwvSUpy9kNkbt7jL3O89PK5a-NIgYUkazIcdV45YzUW1p5nEXvKTfhplNm1QgJEzvuv5haTnx7XVWr15q2f9XdWLPH4PgHIRluXhPiUQPPOGjXKWSfAOXfo836YLjcNBMipo5gqiK1Sl4nQk83JR2M9tW0xfmHRFCuhjlE4f6E5NbhaS7xH_RXG6k=s75-no"
                                             style="outline: none;-ms-interpolation-mode: bicubic;display: block;margin: 0 auto;padding: 0;">
                                    </a>
                                </td>
                                <td width="50px" class="td-social-buttons" style="margin: 0;padding: 0;">
                                    <a target="_blank" href="' . $this->inst . '" class="social-buttons">
                                        <img alt="ok" width="30px" height="30px" class="social-img"
                                             src="https://lh3.googleusercontent.com/6tpg6WRVAcHx8WuvsU4XZOQEzU2UCNYj-sM8G-mfemp6eKmIxaWsDjxxz4IDh9LN0XPDolCoYWDJY54zc_FRYYquXoWW3ObBN54i7SwGe47jALeAxYZiw_2xfUv9cLnZeA3Mi_FLYWqcD48dYOfw0WUr2kod6u57_oChrJG0gJGEe53V_M8lQHPzDJUBL0kUAKXE86jkSiN1uljjCcn-zJxVM7GFO7D5TA7ZvNHNPMjSLIUJWKv23Kqn8-gwdol-TwDFlswikrbyhe4rRmFHjyAlBd_nhqy9biwm_Lil1_6O3hyY_cwaPpZoVCd0Sud8sX0pu4R_9OD7ojhBHepnlK3XrxCROk-kmJaZXtSIvaX4QEhoWzMBeT6-8xBXEhulfOc_kSZBoAVkijFq1h5B8Nk1Pd1h2kIV4qxUjAoeqClObMX8HhG7iXh6vr5zHgOPWKi-R3m4svh4ERiUBcBc-RibxsjIqEspR2a3QfPuTzx9fBqr7CAlhVs0WrpniQgfzHY1IuiW8ybczfmsoqnj_slBmGPz6COovhTzkDQViVIQxVQA21kNcAlA-aCQDXWR5GuNCGBqjWsNsCVSvF43ftrNo-Se_Jz5QYadELbp1RzVlYNIgaK6gTFdeKr3DDfFzkZVnL3XDI4JMhRaeENNraVY3IhgA-8=s75-no"
                                             style="outline: none;-ms-interpolation-mode: bicubic;display: block;margin: 0 auto;padding: 0;">
                                    </a>
                                </td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>';
        }

        $str .= '</tbody></table></body></html>';
        return $str;
    }
}
