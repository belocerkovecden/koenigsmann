@extends('layouts.layout', ['title' => 'Официальный сайт производителя спортивных тренажеров Koenigsmann'])


@section('content')

    <!-- START Главная -->
    <div class="index">

        {{--        <section class="banner">
                    <div id="bgndVideo" class="player"
                         data-property="{videoURL:'https://www.youtube.com/watch?v=LL0xHZ_3n0U',containment:'self', mute:true, startAt:0, opacity:1, showControls: false, stopAt: 0, showYTLogo:false, optimizeDisplay: false, realfullscreen: false, stopMovieOnBlur: false}"></div>
                </section>--}}


        <div class="banner">
            <div class="relation video">
                <div class="relation__ratio"></div>
                <section
                        style="width: 100%; min-width: 100%; display: inline-block; text-align: center;"
                        id="sect" class="relation__content">
                    <div class="wrap_video"
                         style="position: absolute; z-index: 0; min-width: 100%; min-height: 100%; max-height: 500px; left: 0; top: 0; overflow: hidden; opacity: 1; background-image: none; transition-property: opacity; transition-duration: 1000ms;">
                        <video width="100%" height="auto" autoplay="autoplay" loop="loop" id="myVideo" muted>
                            {{--<source src="video/duel.ogv" type='video/ogg; codecs="theora, vorbis"'>--}}
                            <source src="{{ asset('/video/km.mp4') }}" type='video/mp4;'>
                            {{--<source src="video/duel.webm" type='video/webm; codecs="vp8, vorbis"'>--}}
                            Не поддерживается вашим браузером.
                            <a href="{{ asset('/video/km.mp4') }}" download>Скачайте видео</a>.
                        </video>
                    </div>
                </section>
            </div>
        </div>

        {{--        <section style="width: 100%; min-width: 100%; display: inline-block; position: relative; text-align: center;" --}}{{--class="banner"--}}{{-->
                    <div class="wrap_video" style="position: absolute; z-index: 0; min-width: 100%; min-height: 100%; max-height: 500px; left: 0; top: 0; overflow: hidden; opacity: 1; background-image: none; transition-property: opacity; transition-duration: 1000ms;">
                        <video width="100%" height="auto" autoplay="autoplay" loop="loop" id="myVideo" muted>
                            --}}{{--<source src="video/duel.ogv" type='video/ogg; codecs="theora, vorbis"'>--}}{{--
                            <source src="{{ asset('video/km.mp4') }}" type='video/mp4;'>
                            --}}{{--<source src="video/duel.webm" type='video/webm; codecs="vp8, vorbis"'>--}}{{--
                            Не поддерживается вашим браузером.
                            <a href="{{ asset('video/km.mp4') }}">Скачайте видео</a>.
                        </video>
                    </div>
                </section>--}}

        <section class="index_section_category">
            <h1 class="categories__title">Спортивные тренажеры</h1>
            <div class="categories wrapper">
                <a href="{{ url('/category/begovye-dorozhki/dlya-doma') }}" class="cat_block"><img
                            src="{{ asset('img/category/forhome.png') }}" alt="">
                    <p>Беговые дорожки для дома</p></a>
                <a href="{{ url('/category/begovye-dorozhki/commerce') }}" class="cat_block"><img
                            src="{{ asset('img/category/comerc.png') }}" alt="">
                    <p>Коммерческие дорожки</p></a>
                <a href="{{ url('/category/begovye-dorozhki/aksessuary') }}" class="cat_block"><img
                            src="{{ asset('img/category/aks.png') }}" alt="">
                    <p>Аксессуары</p></a>
                <a href="{{ url('/category/begovye-dorozhki/limited-edition') }}" class="cat_block"><img
                            src="{{ asset('img/category/limit.png') }}" alt="">
                    <p>Лимитированная коллекция</p></a>
            </div>
        </section>
        <section class="description">
            <div class="desc_block">
                <div class="info">
                    <h1 class="header">Невероятный результат</h1>
                    <p class="text">Компактный тренажёр E1.0 с выдвижным поручнем, идеально дополняющий интерьер
                        кабинета или спальни </p>
                    <a href="{{ url('/product/begovaya-dorozhka-koenigsmann-model-e10') }}" class="button">Узнать больше</a>
                </div>
                <img class="image" src="{{ asset('img/index-dorojka1.png') }}" alt="">
            </div>
            <div class="desc_block">
                <div class="info">
                    <h1 class="header">Профессиональная выправка</h1>
                    <p class="text">Высокотехнологичная и пройденный тысячами тестов беговая дорожка S1.0 заслужит
                        лояльность и доверие ваших клиентов
                    </p>
                    <a href="{{ url('/product/begovaya-dorozhka-koenigsmann-model-s10') }}" class="button">Узнать больше</a>
                </div>
                <img class="image" src="{{ asset('img/index-dorojka2.png') }}" alt="">
            </div>
        </section>
    </div>
    <!-- END Главная -->

    {{-- Скрипт запуска видео --}}
    <script>
      window.addEventListener('load', () => {
        //JS функции для работы с примером:
        //запуск воспроизведения видео примера
        function playClip (media) {
          media.play()
        }

        //стоп воспроизведения видео примера
        function stopClip (media) {
          media.pause()
        }

        //Использование и демонстрация:
        //получим медиа объект в переменную
        var myVideo = document.getElementById('myVideo')

        setTimeout(function () {
          playClip(myVideo)
        }, 2000)

        //запуск по onclick для кнопок:
        //playClip(myVideo); //для кнопки "Play"
        //stopClip(myVideo); //для кнопки "Stop"

        /* Высота видео */
        let banner = document.querySelector('.banner')
        let scr_width = window.innerWidth
          || document.documentElement.clientWidth
          || document.body.clientWidth

        let scr_height = window.innerHeight
          || document.documentElement.clientHeight
          || document.body.clientHeight

        if( scr_height > scr_width) {
          banner.style.height = 'auto'
        } else {
          banner.style.height = ''
        }
      })
    </script>
@endsection