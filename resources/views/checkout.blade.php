@extends('layouts.layout', ['title' => 'Оформление заказа'])


@section('content')

    <!-- START Оформление заказа -->

    <div class="head_checkout">
        <div class="wrapper h100">
            <a href="{{ route('product.shoppingCart') }}" class="link_go_basket">
                <span class="link_go_basket__icon"></span>
                <span class="link_go_basket__text">Корзина</span>
            </a>

            <h1 class="header_checkout wrapper">
                Оформление заказа
            </h1>
        </div>
    </div>

    <section class="wrapper">

        <form action="{{ route('checkout') }}" method="post" class="form_checkout">
        @csrf
        <!-- Аккардион -->
            <div class="wrap_accordion">

                <div class="line_vertical"></div>

                <!-- Первый разворот -->
                <h2 class="block__h2">Личные данные <span class="cube"></span></h2>
                <div class="first_block">
                    <div class="input-field user__fio">
                        <input name="user__fio" type="text" id="user__fio" required>
                        <label for="user__fio">ФИО</label>
                    </div>
                    <div class="input-field user__email">
                        <input name="email" type="text" id="user__email" required>
                        <label for="user__email">E-mail</label>
                    </div>
                    <div class="input-field user__phone">
                        <input name="phone" type="tel" id="user__phone" required>
                        <label for="user__phone">Телефон</label>
                    </div>
                    <a href="#" id="first_block__btn">Далее</a>
                </div>

                <!-- Второй разворот -->
                <h2 class="block__h2">Способ получения <span class="cube"></span></h2>
                <div class="second_block">
                    <div class="wrap_delivery">
                        <a href="#" class="delivery_btn" id="active_delivery_btn">Доставка</a>
                        <a href="#" class="delivery_btn" id="pickup_from_store">Самовывоз</a>
                    </div>

                    <!-- Условия доставки -->
                    <div class="wrapper_delivery_terms">

                        <div class="wrap_select_checkout_page">
                            <label for="city_list">Укажите регион доставки заказа</label>
                            <!-- <select name="city_list" id="city_list">
                                <option value="Москва">Москва</option>
                                <option value="Санкт-Петербург">Санкт-Петербург</option>
                                <option value="Владимир">Владимир</option>
                            </select> -->
                            <input type="text" name="city_list" id="city_list">
                            <!-- <span class="down"></span> -->
                            <!-- <input id="address" name="address" type="text" /> -->
                            

                        </div>

                        <div class="wrap_delivery_terms">
                            <ul id="result_address"></ul>
                            <h3 class="head_delivery_terms">Условия доставки</h3>
                            <div class="wrap_checkbox_terms">
                                <label class="checkbox">
                                    <input type="radio" name="delivery_terms" value="Доставка до пункта выдачи"
                                           checked="" id="point_delivery">
                                    <span class="custom-checkbox_radio_terms"></span>
                                    <span class="checkbox-text">Доставка до пункта выдачи</span>
                                </label>
                            </div>
                            <div class="wrap_checkbox_terms">
                                <label class="checkbox">
                                    <input type="radio" name="delivery_terms" value="Доставка до адреса"
                                           id="address_delivery">
                                    <span class="custom-checkbox_radio_terms"></span>
                                    <span class="checkbox-text">Доставка до адреса</span>
                                </label>
                            </div>
                        </div>

                        <!-- Форма адреса -->
                        <div class="form_address">
                            <h3 class="head_form_address">Адрес доставки</h3>
                            <div class="input-field inhabited_locality">
                                <input name="inhabited_locality" type="text" id="inhabited_locality" value="">
                                <label for="inhabited_locality">Населённый пункт</label>
                            </div>
                            <div class="input-field street">
                                <input name="street" type="text" id="street" value="">
                                <label for="street">Улица</label>
                            </div>
                            <div class="wrapper_apartment">
                                <div class="input-field porch">
                                    <input name="porch" type="text" id="porch" value="">
                                    <label for="porch">Подъезд</label>
                                </div>
                                <div class="input-field apartment">
                                    <input name="apartment" type="text" id="apartment" value="">
                                    <label for="apartment">Квартира</label>
                                </div>
                            </div>
                            <div class="input-field comment">
                                <input name="comment" type="text" id="comment" value="">
                                <label for="comment">Комментарий</label>
                            </div>
                        </div>

                        <div class="wrap_points_issue">
                            <h3 class="head_points_issue">Пункуты выдачи</h3>
                            <div class="wrap_checkbox_issue">
                                <label class="checkbox">
                                    <input type="radio" name="points_issue" value="Деловые линии" checked="">
                                    <span class="custom-checkbox_radio_issue"></span>
                                    <span class="checkbox-text"><img src="{{ asset('img/delivery/dellin.svg') }}"
                                                                     alt="Деловые линии"></span>
                                </label>
                            </div>
                            <div class="wrap_checkbox_issue">
                                <label class="checkbox">
                                    <input type="radio" name="points_issue" value="ПЭК">
                                    <span class="custom-checkbox_radio_issue"></span>
                                    <span class="checkbox-text"><img src="{{ asset('img/delivery/pak.svg') }}" alt="ПЭК"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <!-- Условия самовывоза -->
                    <div class="wrapper_pickup dn">
                        <div class="wrap_select_checkout_page">
                            <label for="city_list_pickup">Пункты выдачи</label>
                            <select name="city_list_pickup" id="city_list_pickup">
                                <option value="Москва">Москва</option>
                                <!-- <option value="Санкт-Петербург">Санкт-Петербург</option> -->
                                <option value="Владимир">Владимир</option>
                            </select>
                            <span class="down"></span>
                        </div>

                        <div class="wrap_points_pickup">
                            <div class="wrap_checkbox_pickup">
                                <label class="checkbox df_ac">
                                    <input type="radio" name="points_pickup" value="Проспект Вологоградский, д.93" checked id="msk">
                                    <span class="custom-checkbox_radio_pickup"></span>
                                    <div class="checkbox-text__pickup checkbox-text">
                                        <span class="first_str">Проспект Вологоградский, д.93</span>
                                        <span class="second_str">
                                        Будни <span class="grey_text"> 08:00 - 23:00</span>
                                        <br class="brbr">
                                        Выходные <span class="grey_text"> 09:00 - 21:00</span>
                                    </span>
                                    </div>
                                </label>
                            </div>
                            <div class="wrap_checkbox_pickup" style="display: none;">
                                <label class="checkbox df_ac">
                                    <input type="radio" name="points_pickup" value="Улица Большая Нижегородская, д.79" id="vdm">
                                    <span class="custom-checkbox_radio_pickup"></span>
                                    <div class="checkbox-text__pickup checkbox-text">
                                        <span class="first_str">Улица Большая Нижегородская, д.79</span>
                                        <span class="second_str">
                                        Будни <span class="grey_text"> 08:00 - 23:00</span>
                                        <br class="brbr">
                                        Выходные <span class="grey_text"> 09:00 - 21:00</span>
                                    </span>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>

                    <a href="#" id="second_block__btn">Далее</a>
                </div>


<style>
    .pay_sber {
        /*background: url({{ asset('/img/pay_sber.jpg') }}) center center / cover no-repeat no-repeat;*/
        width: 160px;
        padding: 0 10px !important;
        display: inline-block;
        background: #fff !important;
    }

    .wrap_accordion > div.third_block > .wrap_payment_methods {
        display: flex;
        flex-wrap: wrap;
    }

    .pay_sber > img {
        max-width: 135px !important;
    }

    @media (max-width: 1024px) { 
        .pay_sber > img {
            max-width: 112px !important;
        }
    }
</style>
                <!-- Третий разворот -->
                <h2 class="block__h2">Способ оплаты <span class="cube"></span></h2>
                <div class="third_block">
                    <div class="wrap_payment_methods">
                        <a href="#" class="payment_btn cash_btn" id="active_pay_btn" style="display: none;">Наличными
                            <input type="hidden" name="method_cash" value="true">
                        </a>
                        <a href="#" class="payment_btn card_upon_receipt" id="card_upon_receipt" style="display: none;">Картой при получении
                            <input type="hidden" name="method_card" value="false">
                        </a>
                        <a href="#" class="payment_btn" id="pay_site">Оплата на сайте
                            <input type="hidden" name="pay_site" value="false">
                        </a>
                        <!-- <a href="#" class="payment_btn pay_sber">
                            <img src="{{ asset('/img/pay_sber.jpg') }}" alt="Покупай со сбербанком">
                            <input type="hidden" name="method_sber" value="false">
                        </a> -->
                    </div>
                </div>

            </div>

            <!-- Блок оформления заказа -->
            <div class="checkout_wrap_page"> <!-- Вторая обертка для js -->

                <h2 class="head_checkout_page">
                    <p>Ваш заказ</p>
                    <a href="#" class="edit">
                        <span class="edit__icon"></span>
                        <span class="edit__text">Редактировать</span>
                    </a>
                </h2>

                <ul class="payment_list_page">
                    <li>
                        <div>Стоимость товаров</div>
                        <span class="dotted"></span>
                        <div>{{ number_format($total, 0, '', ' ') }} <span>₽</span></div>
                    </li>
                    <li>
                        <div>Скидка</div>
                        <span class="dotted"></span>
                        <div class="red">0 <span>₽</span></div>
                    </li>
                    <li>
                        <div>Доставка</div>
                        <span class="dotted"></span>
                        <div style="font-size: 12px;">Обговаривается с менеджером <span></span></div>
                    </li>
                </ul>


                <div class="total_checkout_page">
                    <h5 class="total_h5">Итого</h5>
                    <div class="total_price">{{ number_format($total, 0, '', ' ') }} ₽</div>
                </div>

                <button type="submit" class="go_to_registration_page">Оформить</button>

            </div>
        </form>

    </section>
    <!-- END Оформление заказа -->

    <!-- Контейнер не удалять!!! он для мобильной версии сайта, сюда переносится блок оформления -->
    <div class="container_for_checkout"></div>

    <!-- Показываем / Скрываем кнопки Оплаты -->
    <script>
      window.addEventListener('load', () => {
        const cash_btn = document.querySelector('.cash_btn');
        const card_upon_receipt = document.querySelector('.card_upon_receipt');
        const pay_site = document.getElementById('pay_site');

        const active_delivery_btn = document.getElementById('active_delivery_btn')
        const pickup_from_store = document.getElementById('pickup_from_store')
    
        pickup_from_store.addEventListener('click', show_btn_nal_site)
        function show_btn_nal_site () {
            cash_btn.style.display = 'inline-block'
            pay_site.style.display = 'inline-block'
            card_upon_receipt.style.display = 'none'
        }

        active_delivery_btn.addEventListener('click', show_btn_site)
        function show_btn_site () {
            pay_site.style.display = 'inline-block'
            cash_btn.style.display = 'none'
            card_upon_receipt.style.display = 'none'
        }

        const city_list = document.querySelector('#city_list')
        city_list.addEventListener('input', search_msk_vladimir)
        function search_msk_vladimir () {
            /*console.log(city_list.value.toLowerCase());
            console.log(city_list.value.toLowerCase().indexOf('москв'));*/
            if (city_list.value.toLowerCase().indexOf('москв') > -1 || city_list.value.toLowerCase().indexOf('владими') > -1) {
                cash_btn.style.display = 'inline-block'
                pay_site.style.display = 'inline-block'
                card_upon_receipt.style.display = 'inline-block' 
            } else {
                cash_btn.style.display = 'none'
                pay_site.style.display = 'inline-block'
                card_upon_receipt.style.display = 'none' 
            }
        }
      })
    </script>

    <!-- Скрыть показать Адрес доставки и Пункты выдачи -->
    <script>

      window.addEventListener('load', () => {
        // инпуты с событиями
        let point_delivery = document.getElementById('point_delivery')
        let address_delivery = document.getElementById('address_delivery')

        point_delivery.addEventListener('input', show_inp_address)

        address_delivery.addEventListener('input', points)

        function show_inp_address (e) {

          // Блоки для показа
          let form_address = document.querySelector('.form_address')
          let points_issue = document.querySelector('.wrap_points_issue')

          if (e.target.checked) {
            form_address.style.display = 'none'
            points_issue.style.display = 'block'
          }
        }

        function points (e) {

          // Блоки для показа
          let form_address = document.querySelector('.form_address')
          let points_issue = document.querySelector('.wrap_points_issue')

          if (e.target.checked) {
            form_address.style.display = 'block'
            points_issue.style.display = 'none'
          }
        }

      })
    </script>

    <!-- Перенос блока h100 -->
    <script>
      window.addEventListener('load', () => {

        move_block_h100()

        function move_block_h100 () {
          let scr_width = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth

          if (scr_width < 768) {
            let h100 = document.querySelector('.head_checkout .h100')
            if (h100) {

              h100.classList.remove('wrapper')
              let checkout_wrap_clone = h100.cloneNode(true)
              h100.remove()

              let logo = document.querySelector('.logo')
              logo.style.display = 'none'

              let wrap_mob = document.querySelector('.wrap_mob ')
              wrap_mob.style.width = '100%'

              let container_for_checkout = document.querySelector('.wrap_mob')
              let sandwich = document.querySelector('.sandwich')
              container_for_checkout.insertBefore(checkout_wrap_clone, sandwich)
            }
          } else if (scr_width > 767) {
            let h100 = document.querySelector('.wrap_mob .h100')
            if (h100) {
              h100.classList.add('wrapper')
              let checkout_wrap_clone = h100.cloneNode(true)
              h100.remove()

              let logo = document.querySelector('.logo')
              logo.style.display = ''

              let wrap_mob = document.querySelector('.wrap_mob ')
              wrap_mob.style.width = ''

              let head_checkout = document.querySelector('.head_checkout')
              head_checkout.appendChild(checkout_wrap_clone)
            }
          }
        }

        window.addEventListener('resize', () => { move_block_h100() })
      })
    </script>

    <!-- Перенос блока checkout_wrap -->
    <script>
      window.addEventListener('load', () => {

        move_block_checkout_wrap()

        function move_block_checkout_wrap () {
          let scr_width = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth

          if (scr_width < 768) {
            let checkout_wrap = document.querySelector('.form_checkout .checkout_wrap_page')
            if (checkout_wrap) {
              let checkout_wrap_clone = checkout_wrap.cloneNode(true)
              checkout_wrap.remove()

              let container_for_checkout = document.querySelector('.container_for_checkout')
              container_for_checkout.appendChild(checkout_wrap_clone)
            }
          } else if (scr_width > 767) {
            let checkout_wrap = document.querySelector('.container_for_checkout .checkout_wrap')
            if (checkout_wrap) {
              let checkout_wrap_clone = checkout_wrap.cloneNode(true)
              checkout_wrap.remove()

              let container_for_checkout = document.querySelector('.form_checkout')
              container_for_checkout.appendChild(checkout_wrap_clone)
            }
          }
        }

        window.addEventListener('resize', () => { move_block_checkout_wrap() })
      })
    </script>

    <!-- Аккардион -->
    <script>
      window.addEventListener('load', () => {
        // Кнопки
        let first_block__btn = document.getElementById('first_block__btn')
        let second_block__btn = document.getElementById('second_block__btn')

        // Развороты
        let first_block = document.querySelector('.first_block')
        let second_block = document.querySelector('.second_block')
        let third_block = document.querySelector('.third_block')

        // Заголовки H2
        let block__h2 = document.querySelectorAll('.block__h2')

        first_block__btn.addEventListener('click', show_second_block)

        function show_first_block () {
          first_block.style.height = 'auto'
          second_block.style.height = '0'
          third_block.style.height = '0'

          block__h2[0].style.textDecoration = 'none'
          block__h2[0].style.cursor = ''
          block__h2[0].firstElementChild.style.background = '#C4C4C4'
          block__h2[1].firstElementChild.style.background = '#3F3F3F'
          block__h2[2].firstElementChild.style.background = '#3F3F3F'

          let line_vertical = document.querySelector('.line_vertical')
          line_vertical.style.transition = 'all 0s'
          line_vertical.style.height = ''
        }

        function show_second_block () {
          //valid_mail ()
          // if (valid_fio()) {
          first_block.style.height = '0'
          second_block.style.height = 'auto'
          third_block.style.height = '0'

          block__h2[0].style.textDecoration = 'underline'
          block__h2[1].style.textDecoration = 'none'
          block__h2[1].firstElementChild.style.background = ''

          block__h2[0].style.cursor = 'pointer'
          block__h2[0].firstElementChild.style.background = '#3F3F3F'
          block__h2[1].firstElementChild.style.background = '#C4C4C4'
          block__h2[2].firstElementChild.style.background = '#3F3F3F'

          block__h2[0].addEventListener('click', show_first_block)

          let line_vertical = document.querySelector('.line_vertical')
          line_vertical.style.transition = 'all 0s'
          line_vertical.style.height = ''
          //  }
        }

        second_block__btn.addEventListener('click', show_third_block)

        function show_third_block () {
          first_block.style.height = '0'
          second_block.style.height = '0'
          third_block.style.height = 'auto'

          block__h2[1].style.textDecoration = 'underline'
          block__h2[1].style.cursor = 'pointer'

          block__h2[0].firstElementChild.style.background = '#3F3F3F'
          block__h2[1].firstElementChild.style.background = '#3F3F3F'
          block__h2[2].firstElementChild.style.background = '#C4C4C4'

          block__h2[1].addEventListener('click', show_second_block)

          let line_vertical = document.querySelector('.line_vertical')
          line_vertical.style.transition = 'all 0s'

          let scr_width = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth
          if (scr_width < 1024) {
            line_vertical.style.height = 'calc(100% - 177px)'
          } else {
            line_vertical.style.height = 'calc(100% - 150px)'
          }

          // Отодвигаю подвал
          let w = new ScreenWidth
          if (w.getScreenWidth() > 1024) {
            footer_down()
          }
        }

          /* Валидация input FIO */

        // Валидируем email
        let user__fio = document.getElementById('user__fio')
        user__fio.addEventListener('input', valid_fio)
        function valid_fio () {
          let user__fio = document.getElementById('user__fio')
          if (user__fio.value.length > 2) {
            user__fio.style.height = '54px'
            user__fio.style.borderBottom = '2px solid gold'
            return true
          } else {
            user__fio.style.height = '54px'
            user__fio.style.borderBottom = '2px solid red'
            return false
          }
        }

        // Валидируем email
        let inp_mail = document.getElementById('user__email')
        inp_mail.addEventListener('input', valid_mail)

        function valid_mail () {
          let inp_mail = document.getElementById('user__email')
          if (!inp_mail.value.match(/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/gi)) {
            inp_mail.style.height = '54px'
            inp_mail.style.borderBottom = '2px solid red'
          } else {
            inp_mail.style.height = '54px'
            inp_mail.style.borderBottom = '2px solid gold'
          }
        }

        // Валидация телефона
        let inp_tel = document.getElementById('user__phone')
        inp_tel.addEventListener('input', valid_phone)
        inp_tel.addEventListener('change', valid_phone)

        // Валидируем email
        function valid_phone () {
          let inp_phone = document.getElementById('user__phone')
          console.log(inp_phone.value.length)
          if (inp_phone.value.length !== 20) {
            inp_phone.style.height = '54px'
            inp_phone.style.borderBottom = '2px solid red'
            console.log('ВАлидацию не прошёл')
          } else {
            inp_phone.style.height = '54px'
            inp_phone.style.borderBottom = '2px solid gold'
            console.log('Валидацию Прошёл!')
          }
        }
      })

      // Отодвигаю подвал
      function footer_down () {
        let b = document.querySelector('body')
        if( b.offsetHeight < 1000) {
          let foot = document.querySelector('.footer')
          foot.style.transition = '0s'
          foot.style.marginTop = '215px'
        } else {
          let foot = document.querySelector('.footer')
          if(foot) {
            foot.style.transition = '0s'
            foot.style.marginTop = ''
          }
        }
      }
    </script>

    <!-- Переключатель способа оплаты -->
    <script>
      window.addEventListener('load', () => {
        let payment_btn = document.querySelectorAll('.payment_btn')

        for (let i = 0; i < payment_btn.length; i++) {
          payment_btn[i].addEventListener('click', toggle_btn_payment, false)
        }

        function toggle_btn_payment (e) {
          e.preventDefault()
          e.stopPropagation()

            /* Цвет кнопки */
          for (let i = 0; i < payment_btn.length; i++) {
            if (payment_btn[i].getAttribute('id') === 'active_pay_btn') {
              payment_btn[i].removeAttribute('id')
              payment_btn[i].firstElementChild.value = 'false'
            }
         }

       if (e.currentTarget.tagName === 'A' ) {
            e.currentTarget.setAttribute('id', 'active_pay_btn')
            e.currentTarget.firstElementChild.value = 'true'
        }

        }
      })
    </script>

    <!-- Переключатель, - Доставка <-> Самовывоз -->
    <script>
      window.addEventListener('load', () => {
        let delivery_btn = document.querySelectorAll('.delivery_btn')

        for (let i = 0; i < delivery_btn.length; i++) {
          delivery_btn[i].addEventListener('click', toggle_btn_delivery)
        }

        function toggle_btn_delivery (e) {
          e.preventDefault()

            /* Цвет кнопки */
          for (let i = 0; i < delivery_btn.length; i++) {
            if (delivery_btn[i].getAttribute('id') === 'active_delivery_btn') {
              delivery_btn[i].removeAttribute('id')
            }
          }

          e.target.setAttribute('id', 'active_delivery_btn')

            /* Скрываю и показываю элементы */
          let wrapper_delivery_terms = document.querySelector('.wrapper_delivery_terms')
          let wrapper_pickup = document.querySelector('.wrapper_pickup')

          if (wrapper_delivery_terms.getAttribute('class') === 'wrapper_delivery_terms'
            && e.target.textContent === 'Самовывоз') {
            wrapper_delivery_terms.classList.add('dn')
            wrapper_pickup.classList.remove('dn')
          }

          if (wrapper_pickup.getAttribute('class') === 'wrapper_pickup'
            && e.target.textContent === 'Доставка') {
            wrapper_delivery_terms.classList.remove('dn')
            wrapper_pickup.classList.add('dn')
          }
        }
      })
    </script>

    <!-- Выбор города и показ определённого адреса -->
    <script>
        window.addEventListener('load', () => {
            const city_list_pickup = document.getElementById('city_list_pickup')
            city_list_pickup.addEventListener('change', show_address)

            function show_address (e) {
                const wrap_points_pickup = document.querySelector('.wrap_points_pickup')
                console.log(document.getElementById('msk'));
                console.log(document.getElementById('vdm'));
                if (e.target.value == 'Москва') {
                    wrap_points_pickup.firstElementChild.style.display = 'block'
                    wrap_points_pickup.lastElementChild.style.display = 'none'
                    document.getElementById('msk').checked = true;
                    document.getElementById('vdm').checked = false;
                } else {
                    wrap_points_pickup.firstElementChild.style.display = 'none'
                    wrap_points_pickup.lastElementChild.style.display = 'block'
                    document.getElementById('msk').checked = false;
                    document.getElementById('vdm').checked = true;
                }
            }
        })
    </script>

    <!-- Вычисляю высоту вертикальной линии -->
    <script>
      window.addEventListener('load', () => {

        let wrap_accordion = document.querySelector('.wrap_accordion')
        let third_block = document.querySelector('.third_block')
        let height_line = third_block.offsetTop + 16
          /*    console.log(wrap_accordion.offsetHeight);
           console.log(third_block.offsetTop);*/
      })
    </script>


    <!--  Подсказка по городу  -->
    <style>
        .wrap_delivery_terms {
            position: relative;
        }

        #result_address {
            position: absolute;
            border-bottom: 1px solid #7A7A7A;
            border-left: 1px solid #7A7A7A;
            border-right: 1px solid #7A7A7A;
            top: -25px;
            padding-left: 5px;
            padding-right: 5px;
            left: 0;
            background: #323232;
            display: none;
        }
        #result_address li {
            margin-bottom: 8px;
        }

        #result_address li:hover {
            cursor: pointer;
            background: #1A1A1A;
        }
    </style>

    <script>
        window.addEventListener('load', () => {
            const city_list = document.querySelector('#city_list')
            city_list.addEventListener('input', show_hint_sity)
            function show_hint_sity (e) {
                if (e.target.value.length > 3) {
                    let url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address";
                    let token = "f03eaf67f130604830ff3da5dcb30b0123bc0225";
                    let query = e.target.value;

                    let options = {
                        method: "POST",
                        mode: "cors",
                        type: "ADDRESS",
                        hint: false,
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json",
                            "Authorization": "Token " + token
                        },
                        body: JSON.stringify({query: query})
                    }

                    fetch(url, options)
                    .then(response => response.text())
                    .then(result => {
                        let str_obj = JSON.parse(result)
                        const result_address = document.getElementById('result_address')
                        result_address.style.display = 'block'
                        result_address.innerHTML = ''
                            for (let i of str_obj.suggestions) {
                                let li = document.createElement('LI')
                                li.textContent = i.value
                                result_address.appendChild(li)
                            }
                            for (let i of result_address.children) {
                                i.addEventListener('click', add_address)
                            }
                        })
                    .catch(error => console.log("error", error));
                }
            }

            function add_address (e) {
                city_list.value = e.target.textContent
                const result_address = document.getElementById('result_address')
                result_address.style.display = ''
                result_address.innerHTML = ''
            }
        })
    </script>

@endsection