@extends('layouts.layout', ['title' => 'Создание товара'])


@section('content')

    <form action="{{ route('product.store') }}" method="post" class="container" style="margin-top: 150px;"
          enctype="multipart/form-data">
        {{-- Безопасность от Laravel --}}
        @csrf

        <h1 class="h1">Создать продукт</h1>

        <div class="form-group">
            Название товара, которое будет в каталоге
            <span style="color:red;"> *</span>
            <input name="title" type="text" class="form-control" required placeholder="Название товара"
                   value="{{ old('title') ?? $product->title ?? '' }}">
        </div>

        <div class="form-group">
            H1 Название которое будет в подробной карточке товара!
            <span style="color: red;">*</span>
            <input name="compact_folding" type="text" class="form-control" placeholder="H1 Название которое будет в подробной карточке товара"
                   value="{{ old('compact_folding') ?? $product->compact_folding ?? '' }}">
        </div>

        <div class="form-group">
            ЧПУ без пробелов это ссылка на товар <span style="color:red;"> *</span>
            <input name="tag" type="text" class="form-control" placeholder="ЧПУ" required
                   value="{{ old('tag') ?? $product->tag ?? '' }}">
        </div>

        <div class="form-group">
            Стоимость товара <span style="color:red;"> *</span>
            <input name="price" type="number" class="form-control" required placeholder="Цена"
                   value="{{ old('price') ?? $product->price ?? '' }}">
        </div>

        <div class="form-group">
            Подробное описание товара <span style="color:red;"> *</span>
            <textarea name="description" rows="10" class="form-control"
                      placeholder="Описание товара">{{ old('description') ?? $product->description ?? '' }}</textarea>
        </div>

        <div class="form-group">
            Дополнительные свойства
            <input name="properties" type="text" class="form-control" placeholder="Дополнительные свойства"
                   value="{{ old('properties') ?? $product->properties ?? '' }}">
        </div>

        <div class="form-group">
            Категория, нужно выбрать из: <b>aksessuary</b> <b>limited-edition</b> <b>dlya-doma</b> <b>commerce</b>
            <span style="color:red;"> *</span>
            <input name="category" type="text" class="form-control" placeholder="Категория" required
                   value="{{ old('category') ?? $product->category ?? '' }}">
        </div>

        <div class="form-group">
            Тип (Для акссесуаров, это title <span style="color:red;">*</span>)
            <input name="type" type="text" class="form-control" placeholder="Тип"
                   value="{{ old('type') ?? $product->type ?? '' }}">
        </div>

        <div class="form-group">
            Уровень (Для акссесуаров, это description <span style="color:red;">*</span>)
            <input name="level" type="text" class="form-control" placeholder="Уровень"
                   value="{{ old('level') ?? $product->level ?? '' }}">
        </div>
        <div class="form-group" id="create_list_photos">
            <span>
                1&nbsp;<input name="photo1" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                         value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                2&nbsp;<input name="photo2" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                3&nbsp;<input name="photo3" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                4&nbsp;<input name="photo4" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                5&nbsp;<input name="photo5" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                6&nbsp;<input name="photo6" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                7&nbsp;<input name="photo7" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                8&nbsp;<input name="photo8" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                9&nbsp;<input name="photo9" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                10&nbsp;<input name="photo10" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                11&nbsp;<input name="photo11" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                12&nbsp;<input name="photo12" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                13&nbsp;<input name="photo13" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                14&nbsp;<input name="photo14" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                15&nbsp;<input name="photo15" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                16&nbsp;<input name="photo16" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                17&nbsp;<input name="photo17" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                18&nbsp;<input name="photo18" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                19&nbsp;<input name="photo19" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
            <span>
                20&nbsp;<input name="photo20" type="file" class="form-control" accept="image/*,image/jpeg,image/png"
                       value="{{ old('img_product') ?? $product->img_product ?? '' }}">
            </span>
        </div>

        {{--                                   Большой список                                --}}
        <div class="form-group">
            <input name="dimensions" type="text" class="form-control" placeholder="Габариты (ДхШхВ)"
                   value="{{ old('dimensions') ?? $product->dimensions ?? '' }}">
        </div>
        <div class="form-group">
            <input name="weight_trainer" type="text" class="form-control" placeholder="Вес тренажера"
                   value="{{ old('weight_trainer') ?? $product->weight_trainer ?? '' }}">
        </div>
        <div class="form-group">
            <input name="package_dimensions" type="text" class="form-control" placeholder="Размеры упаковки Габариты в упаковке"
                   value="{{ old('package_dimensions') ?? $product->package_dimensions ?? '' }}">
        </div>
        <div class="form-group">
            <input name="package_weight" type="text" class="form-control" placeholder="Вес в упаковке"
                   value="{{ old('package_weight') ?? $product->package_weight ?? '' }}">
        </div>
        <div class="form-group">
            <input name="folding_design" type="text" class="form-control" placeholder="Складная конструкция"
                   value="{{ old('folding_design') ?? $product->folding_design ?? '' }}">
        </div>
        <div class="form-group">
            <input name="folded_dimensions" type="text" class="form-control" placeholder="Габариты в сложенном виде"
                   value="{{ old('folded_dimensions') ?? $product->folded_dimensions ?? '' }}">
        </div>
        <div class="form-group">
            <input name="type_powered" type="text" class="form-control" placeholder="Тип питания"
                   value="{{ old('type_powered') ?? $product->type_powered ?? '' }}">
        </div>
        <div class="form-group">
            <input name="motor_warranty" type="text" class="form-control" placeholder="Гарантия на мотор"
                   value="{{ old('motor_warranty') ?? $product->motor_warranty ?? '' }}">
        </div>
        <div class="form-group">
            <input name="frame_warranty" type="text" class="form-control" placeholder="Гарантия на раму"
                   value="{{ old('frame_warranty') ?? $product->frame_warranty ?? '' }}">
        </div>
        <div class="form-group">
            <input name="warranty_electronic_components" type="text" class="form-control" placeholder="Гарантия на узлы движения и электронные блоки"
                   value="{{ old('warranty_electronic_components') ?? $product->warranty_electronic_components ?? '' }}">
        </div>
        <div class="form-group">
            <input name="wear_parts_warranty" type="text" class="form-control" placeholder="Гарантия на детали износа"
                   value="{{ old('wear_parts_warranty') ?? $product->wear_parts_warranty ?? '' }}">
        </div>
        <div class="form-group">
            <input name="warranty" type="text" class="form-control" placeholder="Гарантия"
                   value="{{ old('warranty') ?? $product->warranty ?? '' }}">
        </div>
        <div class="form-group">
            <input name="engine_power" type="text" class="form-control" placeholder="Мощность двигателя, л.с."
                   value="{{ old('engine_power') ?? $product->engine_power ?? '' }}">
        </div>
        <div class="form-group">
            <input name="top_speed" type="text" class="form-control" placeholder="Максимальная скорость, км/ч"
                   value="{{ old('top_speed') ?? $product->top_speed ?? '' }}">
        </div>
        <div class="form-group">
            <input name="maximum_user_weight" type="text" class="form-control" placeholder="Максимальный вес пользователя"
                   value="{{ old('maximum_user_weight') ?? $product->maximum_user_weight ?? '' }}">
        </div>
        <div class="form-group">
            <input name="running_belt" type="text" class="form-control" placeholder="Беговое полотно"
                   value="{{ old('running_belt') ?? $product->running_belt ?? '' }}">
        </div>
        <div class="form-group">
            <input name="sizes_cloth" type="text" class="form-control" placeholder="Размеры полотна (ДхШ)"
                   value="{{ old('sizes_cloth') ?? $product->sizes_cloth ?? '' }}">
        </div>
        <div class="form-group">
            <input name="deck_thickness" type="text" class="form-control" placeholder="Толщина деки"
                   value="{{ old('deck_thickness') ?? $product->deck_thickness ?? '' }}">
        </div>
        <div class="form-group">
            <input name="tilt_adjustment" type="text" class="form-control" placeholder="Регулировка наклона"
                   value="{{ old('tilt_adjustment') ?? $product->tilt_adjustment ?? '' }}">
        </div>
        <div class="form-group">
            <input name="depreciation" type="text" class="form-control" placeholder="Амортизация"
                   value="{{ old('depreciation') ?? $product->depreciation ?? '' }}">
        </div>
        <div class="form-group">
            <input name="display" type="text" class="form-control" placeholder="Дисплей"
                   value="{{ old('display') ?? $product->display ?? '' }}">
        </div>
        <div class="form-group">
            <input name="workout_time_display" type="text" class="form-control" placeholder="Отображение времени тренировки"
                   value="{{ old('workout_time_display') ?? $product->workout_time_display ?? '' }}">
        </div>
        <div class="form-group">
            <input name="user_programs" type="text" class="form-control" placeholder="Пользовательские программы"
                   value="{{ old('user_programs') ?? $product->user_programs ?? '' }}">
        </div>
        <div class="form-group">
            <input name="program_specifications" type="text" class="form-control" placeholder="Спецификации программ"
                   value="{{ old('program_specifications') ?? $product->program_specifications ?? '' }}">
        </div>
        <div class="form-group">
            <input name="frame" type="text" class="form-control" placeholder="Рама"
                   value="{{ old('frame') ?? $product->frame ?? '' }}">
        </div>
        <div class="form-group">
            <input name="heart_rate_measurement" type="text" class="form-control" placeholder="Измерение пульса"
                   value="{{ old('heart_rate_measurement') ?? $product->heart_rate_measurement ?? '' }}">
        </div>
        <div class="form-group">
            <input name="integration" type="text" class="form-control" placeholder="Интеграция"
                   value="{{ old('integration') ?? $product->integration ?? '' }}">
        </div>
        <div class="form-group">
            <input name="multimedia" type="text" class="form-control" placeholder="Мультимедиа"
                   value="{{ old('multimedia') ?? $product->multimedia ?? '' }}">
        </div>
        <div class="form-group">
            <input name="additionally1" type="text" class="form-control" placeholder="Дополнительно 1"
                   value="{{ old('additionally1') ?? $product->additionally1 ?? '' }}">
        </div>
        <div class="form-group">
            <input name="additionally2" type="text" class="form-control" placeholder="Дополнительно 2"
                   value="{{ old('additionally2') ?? $product->additionally2 ?? '' }}">
        </div>
        <div class="form-group">
            <input name="additionally3" type="text" class="form-control" placeholder="Дополнительно 3"
                   value="{{ old('additionally3') ?? $product->additionally3 ?? '' }}">
        </div>
        <div class="form-group">
            <input name="front_shaft_diameter" type="text" class="form-control" placeholder="Диаметр переднего вала"
                   value="{{ old('front_shaft_diameter') ?? $product->front_shaft_diameter ?? '' }}">
        </div>
        <div class="form-group">
            <input name="rear_shaft_diameter" type="text" class="form-control" placeholder="Диаметр заднего вала"
                   value="{{ old('rear_shaft_diameter') ?? $product->rear_shaft_diameter ?? '' }}">
        </div>
        <div class="form-group">
            <input name="distance_between_racks" type="text" class="form-control" placeholder="Расстояние между стойками"
                   value="{{ old('distance_between_racks') ?? $product->distance_between_racks ?? '' }}">
        </div>
        <div class="form-group">
            <input name="type_of_plastic" type="text" class="form-control" placeholder="Тип пластика"
                   value="{{ old('type_of_plastic') ?? $product->type_of_plastic ?? '' }}">
        </div>
        <div class="form-group">
            <input name="energy_saving" type="text" class="form-control" placeholder="Энергосбережение"
                   value="{{ old('energy_saving') ?? $product->energy_saving ?? '' }}">
        </div>
        <div class="form-group">
            <input name="noise_level" type="text" class="form-control" placeholder="Уровень шума"
                   value="{{ old('noise_level') ?? $product->noise_level ?? '' }}">
        </div>
        <div class="form-group">
            <input name="equipment" type="text" class="form-control" placeholder="Комплектация"
                   value="{{ old('equipment') ?? $product->equipment ?? '' }}">
        </div>

        <div class="form-group">
            <input name="availability" type="text" class="form-control" placeholder="Наличие, либо 'есть' либо 'нет'"
                   value="{{ old('availability') ?? $product->availability ?? '' }}">
        </div>

        <div class="form-group">Путь до инструкции
            <input name="instruction" type="text" class="form-control" placeholder="Путь до инструкции"
                   value="{{ old('instruction') ?? $product->instruction ?? '' }}">
        </div>

        <input type="submit" value="Создать продукт" class="btn btn-outline-success">
    </form>

@endsection