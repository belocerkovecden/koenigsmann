@extends('layouts.layout', ['title' => 'Покупателям'])


@section('content')

<!-- START Покупателям -->
<div class="to-buyers">
    

        <ul class="breadcrumbs wrapper">
            <li><a href="/">Главная</a></li>
            <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
            <li><a href="#">Покупателям</a></li>
        </ul>
        <div class="banner">
            <h1>Стань участником клуба</h1>
            <p class="info">Члены клуба имеют приватный доступ к акциям и получают скидку 3 % на покупку продукции</p>
            <a href="#" class="button">Стать участником</a>
        </div>
        <div class="steps wrapper">
            <div class="wrap"><div class="icon icon1"></div>
            <p class="text">Купи нашу продукцию </p></div>
            <div class="wrap"><div class="icon icon2"></div>
            <p class="text">Зарегистрируй штрих-код</p></div>
            <div class="wrap"><div class="icon icon3"></div>
            <p class="text">Выигрывай подарки</p></div>
        </div>
        <h1 class="with-line">Экслюзивные акции для членов клуба</h1>
        <div class="stock-list wrapper">
            <div class="wrap">
                <div class="info">
                    <h1 class="header">ВЫИГРАЙ МЕДОВЫЙ МЕСЯЦ!</h1>
                    <p class="text">Купи украшение из новой коллекции Wedding, зарегистрируй штрихкод и выиграй свадебное путешествие! Акция проводится с 18 марта по 9 сентября 2019 г.</p>
                    <a href="" class="button">Подробнее</a>
                </div>
                <div class="image">
                    <img src="{{ asset('/img/stock1.png') }}" alt="">
                </div>
            </div>
            <div class="wrap">
                <div class="info">
                    <h1 class="header">ПУТЕШЕСТВИЕ В ЛЕТО</h1>
                    <p class="text">С 1 сентября по 14 ноября регистрируй штрихкоды украшений и часов и получи шанс выиграть фантастические подарки: набор украшений, часы для двоих, акустическую систему или главный приз – путешествие для всей семьи!</p>
                    <a href="" class="button">Подробнее</a>
                </div>
                <div class="image">
                    <img src="{{ asset('/img/stock2.png') }}" alt="">
                </div>
            </div>
            <a href="#" class="more">Посмотреть больше</a>
        </div>

    <div class="wrapper" style="margin-top: 50px; margin-bottom: 230px; display: block;">
        <div class="banner">
            <div class="relation video">
                <div class="relation__ratio"></div>
                <section style="width: 100%; min-width: 100%; display: inline-block; text-align: center; margin-top: 50px;"
                         id="sect" class="relation__content">
                    <div class="wrap_video"
                         style="position: absolute; z-index: 0; min-width: 100%; min-height: 100%; max-height: 500px; left: 0; top: 0; overflow: hidden; opacity: 1; background-image: none; transition-property: opacity; transition-duration: 1000ms;">
                        <video width="100%" height="auto" autoplay="autoplay" loop="loop" id="myVideo" muted>
                            {{--<source src="video/duel.ogv" type='video/ogg; codecs="theora, vorbis"'>--}}
                            <source src="{{ asset('/video/video_for_product/1.mp4') }}" type='video/mp4;'>
                            {{--<source src="video/duel.webm" type='video/webm; codecs="vp8, vorbis"'>--}}
                            Не поддерживается вашим браузером.
                            <a href="{{ asset('/video/video_for_product/1.mp4') }}" download>Скачайте видео</a>.
                        </video>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<!-- END Покупателям -->

@endsection