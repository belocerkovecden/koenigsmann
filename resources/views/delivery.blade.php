@extends('layouts.layout', ['title' => 'Условия доставки'])


@section('content')
<div class="delivery_content">
	<div class="wrapper">
       <!--  <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
            <li><a href="#">Контакты</a></li>
        </ul> -->
		<h1>Условия доставки</h1>
		<div class="description">
			<div class="description__child">
				<div class="head">Доставка по Москве и Московской обл. </div>
				<div class="text">
					<span>Любой тренажёр:</span>
					<a>1000 руб</a>
				</div>
				<div class="text">
					<span>За МКАД:</span>
					<a>30 руб/км</a>
				</div>
				<div class="text">
					<span>Подъём(без лифта):</span>
					<a>200 руб/этаж</a>
				</div>
			</div>
			<div class="description__child">
				<div class="head">Доставка по РФ</div>
				<div class="text">
					<span>ПЭК / Деловые линии / Байкал сервис / ГТД: </span>
					<a>500 руб.</a>
				</div>
				<div class="text">
					<span>Наложенный платёж:</span>
					<a>500 руб.</a>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection