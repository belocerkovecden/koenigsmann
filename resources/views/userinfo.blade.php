@extends('layouts.layout', ['title' => 'Данные аккаунта'])


@section('content')

<!-- START Аккаунт -->
{{--<div class="wrapper">
    <ul id="breadcrumbs_lk">
        <li><a href="/">Главная</a></li>
        <li>/</li>
        <li><a href="catalog">Личный кабинет</a></li>
        <li>/</li>
        <li><a href="#">Данные аккаунта</a></li>
    </ul>
</div>--}}

<section class="wrapper">
    <div class="userinfo-content">
        <div class="block-avatar">
            <div class="wrap-img">
                <img src="img/avatar.jpg" alt="">
            </div>
            <!-- <div class="wrap-button">
                <span class="contract-number">Договор № 240024 
                    <br> id: {{ Auth::user()->id }}
                </span>
                <a href="#" class="desktop">Редактировать данные</a>
                <a href="#" class="tablet">Редактировать</a> 
            </div> -->
        </div>
        <div class="block-info">
            <div class="block-info__company">
                <span class="company-name">ООО "ВЕ ГРУПП"</span>
                <span class="contract-number">Договор № 240024 
                    <!-- <br> id: {{ Auth::user()->id }} -->
                </span>
            </div>
            <div class="block-info__fio">Евглевский Иван Павлович</div>
            <div class="block-info__position">Генеральный директор</div>
            <hr>
            <div class="block-info__contacts">
                <div class="block-contact">
                    <span class="block-contact__head">Логин:</span>
                    <span class="block-contact__info">director@mir-sporta.com</span>
                </div>
                <div class="block-contact">
                    <span class="block-contact__head">E-mail <span>(основной):</span></span>
                    <span class="block-contact__info">director@mir-sporta.com</span>
                </div>
                <div class="block-contact">
                        <span class="block-contact__head">E-mail <span class="var1">(дополнительный):</span><span
                                    class="var2">(доп.):</span></span>
                    <span class="block-contact__info">director@mir-sporta.com</span>
                </div>
                <div class="block-contact">
                        <span class="block-contact__head">E-mail <span class="var1">(дополнительный 2):</span><span
                                    class="var2">(доп. 2):</span></span>
                    <span class="block-contact__info">director@mir-sporta.com</span>
                </div>
            </div>
            <div class="subscribe">Вы подписаны на рассылку об изменениях в наличии товара. <a href="#">Отписаться</a></div>

            <a class="logout_link_mob" style="margin-top: 20px;">
                <span class="logout_link_mob__icon"></span>
                <span class="logout_link_mob__text">Выйти</span>
            </a>
        </div>
    </div>
</section>


{{-- Список заказов --}}
@auth
@if(Auth::user()->id == 1)
    <div class="container wrapper">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3 style="font-size: 32px; font-weight: bold; margin-top: 30px;">Заказы:</h3>
                <ul class="list-group">
                    @foreach($orders as $order)
                        <br>
                        <li class="list-group-item"
                            style="background: #1c1c1c; border: 1px solid rgba(71,66,70,0.84);font-weight: bold;color: #bcbcbc;">
                            Заказ № {{ $order->id }}</li>
                        @foreach($order->cart->items as $item)
                            <li class="list-group-item"
                                style="background: #1c1c1c; border: 1px solid rgba(71,66,70,0.84); padding: 10px;">
                                {{ $item['item']['title'] }} - {{ $item['item']['qty'] }}
                                <span class="badge" style="background: #1c1c1c; font-size: 16px;">{{ $item['price'] }}
                                    <span>₽</span></span>
                            </li>
                        @endforeach

                        <strong style="font-size: 18px; font-weight: bold; margin-top: 15px;margin-bottom: 30px;">Итого: {{ $order->cart->totalPrice }}
                            <span>₽</span></strong>

                    @endforeach
                </ul>

                <h3 style="font-size: 32px; font-weight: bold; margin-top: 30px;">Партнёры:</h3>
                <ul class="list-group">
                    @foreach($partners as $partner)
                        <br>
                        <li class="list-group-item"
                            style="background: #1c1c1c; border: 1px solid rgba(71,66,70,0.84);font-weight: bold;color: #bcbcbc;">
                            ID № {{ $partner->id }}</li>
                        <li class="list-group-item"
                            style="background: #1c1c1c; border: 1px solid rgba(71,66,70,0.84); padding: 10px;">
                            {{ $partner['created_at'] }}
                            <br>
                            <span class="badge"
                                  style="background: #1c1c1c; font-size: 16px;"><b>Проявленный интерес:</b> {{ $partner['interest'] }}</span>
                            <br>
                            <span class="badge"
                                  style="background: #1c1c1c; font-size: 16px;"><b>Название организации:</b> {{ $partner['organization_name'] }}</span>
                            <br>
                            <span class="badge"
                                  style="background: #1c1c1c; font-size: 16px;"><b>ИНН:</b> {{ $partner['organization_inn'] }}</span>
                            <br>
                            <span class="badge"
                                  style="background: #1c1c1c; font-size: 16px;"><b>Адрес:</b> {{ $partner['organization_address'] }}</span>
                            <br>
                            <span class="badge"
                                  style="background: #1c1c1c; font-size: 16px;"><b>Персона:</b> {{ $partner['contact_person_name'] }}</span>
                            <br>
                            <span class="badge"
                                  style="background: #1c1c1c; font-size: 16px;"><b>Телефон:</b> {{ $partner['contact_person_phone'] }}</span>
                            <br>
                            <span class="badge"
                                  style="background: #1c1c1c; font-size: 16px;"><b>E-mail:</b> {{ $partner['contact_person_email'] }}</span>
                            <br>

                        </li>

                    @endforeach
                </ul>

                <h3 style="font-size: 32px; font-weight: bold; margin-top: 30px;">Обращения:</h3>
                <ul class="list-group">
                    @foreach($contacts as $contact)
                        <br>
                        <li class="list-group-item"
                            style="background: #1c1c1c; border: 1px solid rgba(71,66,70,0.84);font-weight: bold;color: #bcbcbc;">
                            ID № {{ $contact->id }}</li>
                        <li class="list-group-item"
                            style="background: #1c1c1c; border: 1px solid rgba(71,66,70,0.84); padding: 10px;">
                            {{ $contact['created_at'] }}
                            <br>
                            <span class="badge"
                                  style="background: #1c1c1c; font-size: 16px;"><b>Имя пользователя:</b> {{ $contact['contact_name'] }}</span>
                            <br>
                            <span class="badge"
                                  style="background: #1c1c1c; font-size: 16px;"><b>E-mail:</b> {{ $contact['contact_email'] }}</span>
                            <br>
                            <span class="badge"
                                  style="background: #1c1c1c; font-size: 16px;"><b>Обращение:</b> {{ $contact['contact_description'] }}</span>
                            <br>

                        </li>

                    @endforeach
                </ul>

                {{--                {{ dd($partners, $contacts) }}--}}

                @if(count($orders) < 1)
                    <h2 style="font-size: 20px; color: #f7b806;">Заказов нет!</h2>
                @endif
            </div>
        </div>
    </div>
@endif
@endauth
<!-- END Аккаунт -->

@endsection 
