@extends('layouts.layout', ['title' => 'Прайс-лист'])
{{--@extends('layouts.private_office', ['title' => 'Прайс лист'])--}}

@section('content')
    <div class="price_list">

        <div class="wrapper" id="full_width">

            <h1 class="price_list__h1">Прайс-лист и остатки на 2019 год</h1>

            {{-- Таблица --}}
            <div class="wrap_table">


                <table class="table table-dark">
                    <thead id="t_head">
                    <tr id="tr_head">
                        <th scope="col" class="bg3f3f3f min_width137">Модель фото</th>
                        <th scope="col" class="bg3f3f3f hide_for_tablet_product_name min_width140">Наименование</th>
                        <th scope="col" class="bg3f3f3f min_width300">Характеристики</th>
                        <th scope="col" class="bg3f3f3f min_width110">РРЦ</th>
                        <th scope="col" class="min_width217 bg3f3f3f min_width217">Оптовая цена до <br> 330 000 скидка
                            35.49%
                        </th>
                        <th scope="col" class="min_width217 bg3f3f3f min_width217">Оптовая цена до <br> 330 000 скидка
                            35.49%
                        </th>
                        <th scope="col" class="min_width217 bg3f3f3f min_width217">Оптовая цена до <br> 330 000 скидка
                            35.49%
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">
                            <img class="table__img" src="{{ asset('img/run.jpg') }}"
                                 alt="{{ asset('img/run.jpg') }}">
                            <span class="product_name_for_tablet">Koenigsmann C1.0</span>
                        </th>
                        <td class="product_name_for_desktop">Koenigsmann C1.0</td>
                        <td class="td_for_p min_width300">
                            <p>Габариты в сборе: 1720*772*1405mm</p>
                            <p>Габариты в упаковке: 1760*850*360mm</p>
                            <p>Вес нетто/брутто: 76/85 кг</p>
                        </td>
                        <td class="table__price min_width110">42 990,00 Р</td>
                        <td class="table__price min_width217">42 990,00 Р</td>
                        <td class="table__price min_width217">42 990,00 Р</td>
                        <td class="table__price min_width217">42 990,00 Р</td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <img class="table__img" src="{{ asset('img/run.jpg') }}"
                                 alt="{{ asset('img/run.png') }}">
                            <span class="product_name_for_tablet">Koenigsmann C2.0</span>
                        </th>
                        <td class="product_name_for_desktop">Koenigsmann C2.0</td>
                        <td class="td_for_p">
                            <p>Габариты в сборе: 1720*772*1405mm</p>
                            <p>Габариты в упаковке: 1760*850*360mm</p>
                            <p>Вес нетто/брутто: 76/85 кг</p>
                        </td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <img class="table__img" src="{{ asset('img/run.jpg') }}"
                                 alt="{{ asset('img/run.png') }}">
                            <span class="product_name_for_tablet">Koenigsmann C3.0</span>
                        </th>
                        <td class="product_name_for_desktop">Koenigsmann C3.0</td>
                        <td class="td_for_p">
                            <p>Габариты в сборе: 1720*772*1405mm</p>
                            <p>Габариты в упаковке: 1760*850*360mm</p>
                            <p>Вес нетто/брутто: 76/85 кг</p>
                        </td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <img class="table__img" src="{{ asset('img/run.jpg') }}"
                                 alt="{{ asset('img/run.png') }}">
                            <span class="product_name_for_tablet">Koenigsmann C4.0</span>
                        </th>
                        <td class="product_name_for_desktop">Koenigsmann C4.0</td>
                        <td class="td_for_p">
                            <p>Габариты в сборе: 1720*772*1405mm</p>
                            <p>Габариты в упаковке: 1760*850*360mm</p>
                            <p>Вес нетто/брутто: 76/85 кг</p>
                        </td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                    </tr>
                    </tbody>
                </table>

                <h2 class="price_list__h2">Комплектующие</h2>

                <table class="table table-dark">
                    <thead class="table_hide_thead">
                    <tr class="table_hide_tr">
                        <th scope="col" class="bg3f3f3f th_hide min_width137">Модель фото</th>
                        <th scope="col" class="bg3f3f3f th_hide hide_for_tablet_product_name">Наименование</th>
                        <th scope="col" class="bg3f3f3f th_hide">Характеристики</th>
                        <th scope="col" class="bg3f3f3f th_hide">РРЦ</th>
                        <th scope="col" class="min_width217 bg3f3f3f th_hide">Оптовая цена до <br> 330 000 скидка
                            35.49%
                        </th>
                        <th scope="col" class="min_width217 bg3f3f3f th_hide">Оптовая цена до <br> 330 000 скидка
                            35.49%
                        </th>
                        <th scope="col" class="min_width217 bg3f3f3f th_hide">Оптовая цена до <br> 330 000 скидка
                            35.49%
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">
                            <img class="table__img" src="{{ asset('img/run.jpg') }}"
                                 alt="{{ asset('img/run.png') }}">
                            <span class="product_name_for_tablet">Koenigsmann C5.0</span>
                        </th>
                        <td class="product_name_for_desktop">Koenigsmann C5.0</td>
                        <td class="td_for_p">
                            <p>Габариты в сборе: 1720*772*1405mm</p>
                            <p>Габариты в упаковке: 1760*850*360mm</p>
                            <p>Вес нетто/брутто: 76/85 кг</p>
                        </td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <img class="table__img" src="{{ asset('img/run.jpg') }}"
                                 alt="{{ asset('img/run.png') }}">
                            <span class="product_name_for_tablet">Koenigsmann C6.0</span>
                        </th>
                        <td class="product_name_for_desktop">Koenigsmann C6.0</td>
                        <td class="td_for_p">
                            <p>Габариты в сборе: 1720*772*1405mm</p>
                            <p>Габариты в упаковке: 1760*850*360mm</p>
                            <p>Вес нетто/брутто: 76/85 кг</p>
                        </td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <img class="table__img" src="{{ asset('img/run.jpg') }}"
                                 alt="{{ asset('img/run.png') }}">
                            <span class="product_name_for_tablet">Koenigsmann C5.0</span>
                        </th>
                        <td class="product_name_for_desktop">Koenigsmann C5.0</td>
                        <td class="td_for_p">
                            <p>Габариты в сборе: 1720*772*1405mm</p>
                            <p>Габариты в упаковке: 1760*850*360mm</p>
                            <p>Вес нетто/брутто: 76/85 кг</p>
                        </td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <img class="table__img" src="{{ asset('img/run.jpg') }}"
                                 alt="{{ asset('img/run.png') }}">
                            <span class="product_name_for_tablet">Koenigsmann C6.0</span>
                        </th>
                        <td class="product_name_for_desktop">Koenigsmann C6.0</td>
                        <td class="td_for_p">
                            <p>Габариты в сборе: 1720*772*1405mm</p>
                            <p>Габариты в упаковке: 1760*850*360mm</p>
                            <p>Вес нетто/брутто: 76/85 кг</p>
                        </td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                        <td class="table__price">42 990,00 Р</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    {{-- Скролл --}}
    <script>
      window.addEventListener('load', () => {

        /* Задаю высоту обёртке таблицы и добавляю скролл */
        let wrap_table = document.querySelector('.wrap_table')
        let w = new ScreenWidth

        /* Задаю высоту странице что бы подвал не прыгал вверх */
        let height_screen = w.getViewportHeight()
        let height_wrap_table = height_screen - 135
        let price_list = document.querySelector('.price_list')
        //price_list.style.height = (height_wrap_table + 180) + 'px'

        window.addEventListener('scroll', () => {
          let scroll_top = window.pageYOffset || document.documentElement.scrollTop
          let scroll_height = document.body.scrollHeight

          if (scroll_top >= 100) {
            let tr_head = document.getElementById('tr_head')
            tr_head.classList.add('wrapper')
            tr_head.style.position = 'fixed'
            tr_head.style.top = '134px'
            tr_head.style.overflowX = 'scroll'

            let height_screen = w.getViewportHeight()
            let height_wrap_table = height_screen - 135
            wrap_table.style.height = height_wrap_table + 'px'
            wrap_table.style.overflowY = 'scroll'
            wrap_table.style.marginTop = '219px'
          } else {
            let tr_head = document.getElementById('tr_head')
            tr_head.style.position = 'static'
            tr_head.style.top = 'none'
            tr_head.classList.remove('wrapper')


            wrap_table.style.height = 'auto'
            wrap_table.style.overflowY = 'visible'
            wrap_table.style.marginTop = '0'
          }
        })

        /*let wrap_table = document.querySelector('.wrap_table')*/
        wrap_table.addEventListener('scroll', scroll_head)

        let tr_head = document.getElementById('tr_head')
        tr_head.addEventListener('scroll', scroll_body)

        function scroll_head (e) {
          let tr_head = document.getElementById('tr_head')
          tr_head.scrollLeft = e.target.scrollLeft
        }

        function scroll_body (e) {
          let wrap_table = document.querySelector('.wrap_table')
          wrap_table.scrollLeft = e.target.scrollLeft
        }



      })
    </script>

    {{-- Скрипт для полной ширины таблицы на планшетах и мобильных --}}
    <script>
      window.addEventListener('load', () => {
        let window_width = window.innerWidth
          || document.documentElement.clientWidth
          || document.body.clientWidth

        if (window_width < 1024) {

          let full_width = document.getElementById('full_width')

          if (full_width.classList.contains('wrapper')) {
            full_width.classList.remove('wrapper')
          }
        } else {

          let full_width = document.getElementById('full_width')

          if (!full_width.classList.contains('wrapper')) {
            full_width.classList.add('wrapper')
          }
        }
      })
    </script>

    <!-- Перелинковка -->
    <script>
      window.addEventListener('load', () => {

          let wi = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth

          if (wi < 768) {
            let all_views = []

            function collect_items () {
              let breadcrumbs_UC = document.querySelector('.breadcrumbs_UC')
              let list_result_UC = document.querySelector('.list_result_UC')

              let breadcrumbs_PL = document.querySelector('.breadcrumbs_PL')
              let list_result_PL = document.querySelector('.list_result_PL')
              let wrap_btn_PL = document.querySelector('.wrap_btn_PL')

              let marketing_support = document.querySelector('.marketing_support')
              let list_result_MS = document.querySelector('.list_result_MS')
              let wrap_btn_MS = document.querySelector('.wrap_btn_MS')

              return [breadcrumbs_UC, list_result_UC, breadcrumbs_PL, list_result_PL, list_result_MS, wrap_btn_PL, wrap_btn_MS, marketing_support]
            }

            collect_items()

            function hide_all () {
              let all_views = collect_items()
              if (Array.isArray(all_views)) {
                for (let i = 0; i < all_views.length; i++) {
                  all_views[i].style.display = 'none'
                }
              }

              let return_to_menu = document.getElementById('return_to_menu')
              return_to_menu.style.display = 'none'

              let wrap_mob_LK = document.querySelector('.wrap_mob_LK')
              wrap_mob_LK.style.width = '100%'

              return all_views
            }

            // Показать Выгрузку каталога
            let UC = document.getElementById('UC')
            UC.addEventListener('click', show_UC)

            function show_UC () {
              let arr = hide_all()
              arr[0].style.display = 'block'
              arr[1].style.display = 'block'
              auxiliary()
            }

            // Показать Прайс-листы
            let PL = document.getElementById('PL')
            PL.addEventListener('click', show_PL)

            function show_PL () {
              let arr = hide_all()
              arr[2].style.display = 'block'
              arr[3].style.display = 'block'
              arr[5].style.display = 'flex'
              auxiliary()
            }

            // Маркетинговая поддержка
            let MS = document.getElementById('MS')
            MS.addEventListener('click', show_MS)

            function show_MS () {
              let arr = hide_all()
              arr[4].style.display = 'block'
              arr[7].style.display = 'block'
              arr[6].style.display = 'flex'
              auxiliary()
            }

            // Вспомогательная функция, которая работает с шапкой страницы и убирает меню
            function auxiliary () {
              let wrap_data_basket = document.querySelector('.wrap_data_basket')
              wrap_data_basket.style.display = 'none'

              let wrap_data_user_line_LK = document.querySelector('.wrap_data_user_line_LK')
              wrap_data_user_line_LK.style.display = 'none'

              let return_to_menu = document.getElementById('return_to_menu')
              return_to_menu.style.display = 'inline-block'

              let wrap_mob_LK = document.querySelector('.wrap_mob_LK')
              wrap_mob_LK.style.width = 'auto'

              let wrap_scroll_for_md = document.querySelector('.wrap_scroll_for_md')
              wrap_scroll_for_md.style.display = 'none'

              return_to_menu.addEventListener('click', go_to_menu)
            }

            // Собираю все элементы у которых есть класс go_menu, и по клику отправляю всех в меню
            let go_menu = document.querySelectorAll('.go_menu')
            for (let i = 0; i < go_menu.length; i++) {
              go_menu[i].addEventListener('click', go_to_menu)
            }

            // Возвращаем всё на место, показываем меню
            function go_to_menu () {
              let wrap_data_basket = document.querySelector('.wrap_data_basket')
              wrap_data_basket.style.display = 'flex'

              let wrap_data_user_line_LK = document.querySelector('.wrap_data_user_line_LK')
              wrap_data_user_line_LK.style.display = 'block'

              let return_to_menu = document.getElementById('return_to_menu')
              return_to_menu.style.display = 'none'

              let wrap_mob_LK = document.querySelector('.wrap_mob_LK')
              wrap_mob_LK.style.width = '100%'

              let wrap_scroll_for_md = document.querySelector('.wrap_scroll_for_md')
              wrap_scroll_for_md.style.display = 'block'

              hide_all()
            }
          }
        }
      )
    </script>
@endsection