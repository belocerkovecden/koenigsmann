@extends('layouts.layout', ['title' => 'Станьте партнером компании Koenigsmann'])


@section('content')

    <!-- START BreadCrumbs -->
    <ul class="breadcrumbs wrapper">
        <li><a href="/">Главная</a></li>
        <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
        <li><a href="#">Стать партнером</a></li>
    </ul>
    <!-- END BreadCrumbs -->


    <!-- START Стать партнером -->
    <section class="wrapper">
        <!-- Лев Leo -->
        <div class="bg"></div>

            <form action="{{ route('partner') }}" class="form" method="post">

                {{-- Безопасность от Laravel --}}
                @csrf

                <div class="i-interested">
                    <h3 class="i-interested__head">Меня интересует</h3>
                    <div class="input-field i-interested__input">
                        <input type="text" id="interested" name="interest" required {{ old('interest') ?? $partner->interest ?? '' }}>
                        <label for="interested">Введите Ваши интересы</label>
                    </div>
                </div>

                <div class="organization">
                    <h3 class="organization__head">Организация</h3>
                    <div class="input-field organization__name">
                        <input type="text" id="organization__name" name="organization_name" required {{ old('organization_name') ?? $partner->organization_name ?? '' }}>
                        <label for="organization__name">Название</label>
                    </div>
                    <div class="input-field organization__inn">
                        <input type="text" id="organization__inn" name="organization_inn" {{ old('organization_inn') ?? $partner->organization_inn ?? '' }}>
                        <label for="organization__inn">ИНН</label>
                    </div>
                    <div class="input-field organization__address">
                        <input type="text" id="organization__address" name="organization_address" {{ old('organization_address') ?? $partner->organization_address ?? '' }}>
                        <label for="organization__address">Адрес регистрации</label>
                    </div>
                </div>

                <div class="contact-face">
                    <h3 class="contact-face__head">Контактное лицо</h3>
                    <div class="input-field contact-face__name">
                        <input type="text" id="contact-face__name" name="contact_person_name" {{ old('contact_person_name') ?? $partner->contact_person_name ?? '' }}>
                        <label for="contact-face__name">Имя</label>
                    </div>
                    <div class="input-field contact-face__phone">
                        <input type="tel" id="contact-face__phone" name="contact_person_phone" required {{ old('contact_person_phone') ?? $partner->contact_person_phone ?? '' }}>
                        <label for="contact-face__phone">Телефон</label>
                    </div>
                    <div class="input-field contact-face__email">
                        <input type="text" id="contact-face__email" name="contact_person_email" required {{ old('contact_person_email') ?? $partner->contact_person_email ?? '' }}>
                        <label for="contact-face__email">E-mail</label>
                    </div>
                </div>

                <div class="wrap_btn-form_info">
                    {{--<button type="submit" class="btn-form btn_submit_partner">Отправить заявку</button>--}}
                    <input type="submit" value="Отправить заявку" class="btn-form btn_submit_partner">
                    <p class="info">Нажимая на кнопку, вы соглашаетесь на обработку персональных данных в соответствии с
                        Политикой конфиденциальности</p>
                </div>
            </form>

            <div class="service">
                <h2 class="head_step">Сервис и преференции</h2>

                <ul class="wrap_cards">
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Экспертный подбор оборудования</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Система скидок</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Гарантийное обслуживание</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
            <span class="wrap_img">
                <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
            </span>
                            <div class="header">
                                <p class="angle_right_white">Персональный менеджер</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="escort">
                <h2 class="head_step">Сопровождение продаж</h2>

                <ul class="wrap_cards">
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Экспертный подбор оборудования</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Система скидок</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Гарантийное обслуживание</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Персональный менеджер</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="partner">
                <h2 class="head_step">Как стать партнёром?</h2>

                <ul class="list_step">
                    <li>
                        <img src="{{ asset('img/circle/circle1.svg') }}" alt="1">
                        <p>Заполнить заявку выше</p>
                    </li>
                    <li>
                        <img src="{{ asset('img/circle/circle3.svg') }}" alt="2">
                        <p>Дождаться звонка менеджера</p>
                    </li>
                    <li>
                        <img src="{{ asset('img/circle/circle4.svg') }}" alt="3">
                        <p>Согласовать коммерческие условия</p>
                    </li>
                    <li>
                        <img src="{{ asset('img/circle/circle2.svg') }}" alt="4">
                        <p>Начать работу</p>
                    </li>
                </ul>

            </div>



    </section>
    <!-- END Стать партнером -->

@endsection