@extends('layouts.layout', ['title' => 'Корзина Koenigsmann'])

@section('content')

<!-- START Корзина -->
<div class="head_basket">
    <div class="wrapper wrap_head">
        <div class="header_basket">
            @if(Session::has('cart'))
            <h1>Корзина <span>
                @if(isset(Session::get('cart')->totalQty) && Session::get('cart')->totalQty > 0)
                    {{  Session::get('cart')->totalQty ??  Session::has('cart') }}
                @endif
                    товар.</span></h1>
            @else
            <h1>Корзина</h1>
            @endif
        </div>
    </div>
</div>

<section class="wrapper">

    <div class="wrap_cards_checkout">

        @if(Session::has('cart'))
        <ul class="basket_cards">
            @foreach($products as $product)

                {{--{{ dd($product) }}--}}
            <li>
<!-- Дублирование названия товара -->
                <a href="/product/{{ $product['item']->tag }}" class="description__name-mobile">{{ $product['item']['short_title'] }}</a>

                <div class="wrap_img">
                    <img src="{{ $product['item']['img'] ? $product['item']['img'] : asset('img/run.jpg') }}" alt="run">
                </div>

                <div class="description">
                    <a href="/product/{{ $product['item']->tag }}" class="description__name">{{ $product['item']['title'] }}</a>
                    <div class="description__code mob_hide"><span class="mob_hide">Код товара: #id</span> {{ $product['item']['product_id'] }}</div>
                    @if($product['item']['display']) <div class="description__color">Дисплей: {{ $product['item']['display'] }} </div>@endif
                    <div class="description__remove_from_basket">
                        <span class="icon_remove"></span>
                        <a href="{{ route('product.remove', ['id' => $product['item']['product_id']]) }}" class="text_remove">Удалить <span class="mob_hide">из корзины</span></a>
                    </div>
                </div>

                <div class="price_block">

                    <!-- Дублирую id для мобильной версии -->
                    <span class="id_product">id: {{ $product['item']['product_id'] }}</span>
                    <form method="get" action="#" class="price_block__form">
                        @csrf
                        <a href="{{ route('product.reduceByOne', ['id' => $product['item']['product_id']]) }}" class="minus">−</a>
                        <input class="form_inp" type="text" min="0" value="{{ $product['qty'] }}">
                        <a href="{{ route('product.addToCart', ['id' => $product['item']['product_id'], 'page' => 'basket']) }}" class="plus">+</a>
                    </form>

                    <div class="price">
                        {{--<span class="price__before">{{ $product['item']['price'] }} ₽</span>--}}
                        <span class="price__after">{{ number_format($product['item']['price'], 0, '', ' ') }}₽</span>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>


        <div class="checkout_wrap"> <!-- Вторая обертка для js -->

            <h2 class="head_checkout_basket">Оформление</h2>
            <form action="#" class="promo_code">
                <input type="text" placeholder="Ввести промокод">
                <button type="submit">Применить</button>
            </form>

            <ul class="payment_list">
                <li>
                    <div>Стоимость товаров</div>
                    <span class="dotted"></span>
                    <div>{{ number_format($totalPrice, 0, '', ' ') }} <span>₽</span></div>
                </li>
                <li>
                    <div>Скидка</div>
                    <span class="dotted"></span>
                    <div class="red">0 <span>₽</span></div>
                </li>
                <li>
                    <div>Доставка</div>
                    <span class="dotted"></span>
                    <div style="font-size: 10px;">Обговаривается с менеджером <span>{{--₽--}}</span></div>
                </li>
            </ul>

            <div class="total">
                <h5>Итого</h5>
                <div class="total_price">{{ number_format($totalPrice, 0, '', ' ') }} ₽</div>
            </div>

            <a href="{{ route('checkout') }}" class="go_to_registration">Перейти к оформлению</a>

        </div>
        @else

                <div class="empty_basket">
                    <p>В корзине пока ничего нету</p>
                    <a href="{{ url('category') }}" class="go_to_catalog">Перейти в каталог</a>
                </div>

        @endif

    </div>
</section>

<!-- Контейнер не удалять!!! он для мобильной версии сайта, сюда переносится блок оформления -->
<div class="container_for_checkout"></div>

<!-- END Корзина -->


{{-- Скрипт выборка изображения из списка --}}
<script>
  window.addEventListener('load', () => {
    let image_str = document.querySelectorAll('.wrap_img img');
    for (let i = 0; i < image_str.length; i++) {
      let arr = image_str[i].src.split('%7C%7C%7C');
      let name_img = arr[0].split('/');
      image_str[i].src = '/storage/' + name_img[name_img.length - 1];
    }
  })
</script>

<!-- Перенос блока checkout_wrap -->
<script>

  window.addEventListener('load', () => {

    move_block_checkout_wrap();

    function move_block_checkout_wrap () {
      let scr_width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth

      if(scr_width < 768) {
        let checkout_wrap = document.querySelector('.wrap_cards_checkout .checkout_wrap')
        if(checkout_wrap) {
          let checkout_wrap_clone = checkout_wrap.cloneNode(true)
          checkout_wrap.remove()

          let container_for_checkout = document.querySelector('.container_for_checkout')
          container_for_checkout.appendChild(checkout_wrap_clone);
        }
      } else if(scr_width > 767) {
        let checkout_wrap = document.querySelector('.container_for_checkout .checkout_wrap')
        if(checkout_wrap) {
          let checkout_wrap_clone = checkout_wrap.cloneNode(true)
          checkout_wrap.remove()

          let container_for_checkout = document.querySelector('.wrap_cards_checkout')
          container_for_checkout.appendChild(checkout_wrap_clone);
        }
      }
    }


    window.addEventListener('resize', () => { move_block_checkout_wrap(); })
  });

</script>

@endsection