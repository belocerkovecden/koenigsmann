@extends('layouts.layout', ['title' => 'Где купить?'])


@section('content')

    <!-- START BreadCrumbs -->
    <ul class="breadcrumbs wrapper">
        <li><a href="/">Главная</a></li>
        <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
        <li><a href="">Где купить?</a></li>
    </ul>
    <!-- END BreadCrumbs -->

    <!-- START Где купить -->
    <section class="wrapper">
        <!-- Лев Leo -->
        <div class="bg"></div>

        <h2 class="header_select_city">Мы на карте:</h2>
       {{-- <h2 class="header_select_city">Выбрать город</h2>--}}

{{--        <div class="wrap_select">
            <select name="city_list" id="city_list">
                <option value="Москва">Москва</option>
                <option value="Санкт-Петербург">Санкт-Петербург</option>
                <option value="Владимир">Владимир</option>
            </select>
            <span class="down"></span>
        </div>--}}


        <div class="wrap_map_list_city">
            <div class="wrap_map">
{{--                <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Ae845208e855f583afb8ac4b0ce45d6d504ffb5c2c7cbbda9ffc299d8fd1e1713&amp;source=constructor"
                        width="100%" height="444" frameborder="0"></iframe>--}}

                <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Ae845208e855f583afb8ac4b0ce45d6d504ffb5c2c7cbbda9ffc299d8fd1e1713&amp;source=constructor"
                        width="100%" height="444" frameborder="0"></iframe>
            </div>
            <div class="city_list">
                <h2 class="city_list__header">Адреса магазинов</h2>
                <ul class="city_list__ul-list">
                    <li>
                        <h3>Фирменный магазин KOENIGSMANN</h3>
                        <div class="address">
                            <span class="address__icon"></span>
                            <p>Москва, Старокалужское шоссе, д.62</p>
                        </div>
                        <div class="phone">
                            <span class="phone__icon"></span>
                            <a href="tel:+74993501585">+7 499 350 15 85</a>
                        </div>
                    </li>
{{--                    <li>
                        <h3>Спортамастер</h3>
                        <div class="address">
                            <span class="address__icon"></span>
                            <p>Москва, Киевское шоссе, 22-й километр, дв4</p>
                        </div>
                        <div class="phone">
                            <span class="phone__icon"></span>
                            <p>32 44 56</p>
                        </div>
                    </li>
                    <li>
                        <h3>Спортивный мир</h3>
                        <div class="address">
                            <span class="address__icon"></span>
                            <p>Москва, Киевское шоссе, 22-й километр, дв4</p>
                        </div>
                        <div class="phone">
                            <span class="phone__icon"></span>
                            <p>8 800 111 11 22</p>
                        </div>
                    </li>
                    <li>
                        <h3>Мир спорта</h3>
                        <div class="address">
                            <span class="address__icon"></span>
                            <p>Москва, Киевское шоссе, 22-й километр, дв4</p>
                        </div>
                        <div class="phone">
                            <span class="phone__icon"></span>
                            <p>32 44 56</p>
                        </div>
                    </li>
                    <li>
                        <h3>Спортивный мир</h3>
                        <div class="address">
                            <span class="address__icon"></span>
                            <p>Москва, Киевское шоссе, 22-й километр, дв4</p>
                        </div>
                        <div class="phone">
                            <span class="phone__icon"></span>
                            <p>32 44 56</p>
                        </div>
                    </li>
                    <li>
                        <h3>Мир спорта</h3>
                        <div class="address">
                            <span class="address__icon"></span>
                            <p>Москва, Киевское шоссе, 22-й километр, дв4</p>
                        </div>
                        <div class="phone">
                            <span class="phone__icon"></span>
                            <p>8 800 111 11 22</p>
                        </div>
                    </li>--}}
                </ul>

                <a href="#" id="show_all">Посмотреть ещё</a>
            </div>
        </div>

{{--        <h2 class="buy_in_shop">Купить в интернет-магазине</h2>

        <div class="wrap_for_arrow">
            <!-- Слайд влево -->
            <span id="btn_left"></span>

            <div class="wrap_overflow">
                <div class="wrap-brand"> <!-- Обёртка для брэндов -->

                    <a href="#"><img src="img/brand/brand_gipersport.svg" alt="gipersport"></a>
                    <a href="#"><img src="img/brand/brand_mirsporta.svg" alt="mirsporta"></a>
                    <a href="#"><img src="img/brand/brand_sportmaster.svg" alt="sportmaster"></a>
                    <a href="#"><img src="img/brand/brand_gipersport.svg" alt="gipersport"></a>
                    <a href="#"><img src="img/brand/brand_mirsporta.svg" alt="mirsporta"></a>
                    <a href="#"><img src="img/brand/brand_sportmaster.svg" alt="sportmaster"></a>
                    <a href="#"><img src="img/brand/brand_gipersport.svg" alt="gipersport"></a>
                    <a href="#"><img src="img/brand/brand_mirsporta.svg" alt="mirsporta"></a>
                    <a href="#"><img src="img/brand/brand_sportmaster.svg" alt="sportmaster"></a>
                    <a href="#"><img src="img/brand/brand_gipersport.svg" alt="gipersport"></a>
                    <a href="#"><img src="img/brand/brand_mirsporta.svg" alt="mirsporta"></a>
                    <a href="#"><img src="img/brand/brand_sportmaster.svg" alt="sportmaster"></a>

                </div>
            </div>

            <!-- Слайд вправо -->
            <span id="btn_right"></span>
        </div>--}}
    </section>
    <!-- END Где купить -->

@endsection