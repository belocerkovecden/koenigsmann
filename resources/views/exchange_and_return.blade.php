@extends('layouts.layout', ['title' => 'Горячая линия Koenigsmann'])


@section('content')

<style>
    .wrap_gold_clip .label_gold_clip {
        max-width: 380px;
    }
</style>
<!-- START Заявка на сервисное обслуживание -->
<section class="partner_page">
    <div class="wrapper">

        {{-- Обмен и возврат --}}

        <div id="exchange_and_return">
            <h2>Обмен и возврат</h2>

            <h3>Получение заказа</h3>

            <p>При получении покупки, обращайте внимание на целостность упаковки и на наличие всех сопроводительных
                документов, а именно: платежный документ, товарная накладная, гарантийный талон, инструкция, она же
                технический паспорт. После этого подпишите документы у курьера или в отделении ТК, если забираете
                заказ из отделения. </p>

            <p>Правила интернет торговли регулируются статьей и «Правилами продажи товаров дистанционным способом»,
                утвержденных Постановлением Правительства РФ от 27.09.2007 г. № 612. С момента получения товара
                покупатель имеет право в течение 7 дней вернуть товар без объяснения причин и отказаться от него в
                любой момент до доставки. При отказе от товара надлежащего качества, по ГК РФ Вы обязаны оплатить
                доставку. Срок возврата денежных средств составляет не более 10 дней с даты поступления
                возвращенного товара.</p>

            <h3>Обмен и возврат товара надлежащего качества</h3>

            <p>Возврат товара надлежащего качества возможен в случае, если сохранены его товарный вид,
                потребительские свойства, а также документ, подтверждающий факт и условия покупки указанного товара.
                Если Вам по какой-либо причине не понравился товар и Вы хотите его обменять на другой, товар не был
                в эксплуатации, сохранилась упаковка, все сопроводительные документы и, самое главное, не прошло 7
                дней со дня покупки, заполните форму на странице <a href="https://www.koenigsmann.ru/service/">https://www.koenigsmann.ru/service/</a>,
                и мы произведем обмен товара на другой.</p>

            <h3>Возврат товара ненадлежащего качества</h3>

            <p>Если вы обнаружили скрытые дефекты после покупки, в течение 7 календарных дней запоните форму на
                странице <a href="https://www.koenigsmann.ru/service/">https://www.koenigsmann.ru/service/</a>.
                Отправьте товар в сервисный центр для установления причины поломки. В случае выявления заводского
                дефекта мы бесплатно производим замену на новый товар и компенсируем все расходы.</p>

            <h3>Гарантийный срок</h3>

            <p>Если товар сломался во время гарантийного срока, запоните форму на странице
                <a href="https://www.koenigsmann.ru/service/">https://www.koenigsmann.ru/service/</a>. Сервис
                диагностирует проблему по вашему описанию. Если проблему удастся решить удаленно, вам бесплатно
                отправят деталь на замену. Если удаленно не удастся починить товар, отправьте его в сервисный центр.
                Все расходы будут компенсированы, а ремонт займет не более 14 дней.</p>

            <h3>Постгарантийное обслуживание</h3>

            <p>Если техника сломалась после истечения гарантийного срока, заполните форму на странице
                <a href="https://www.koenigsmann.ru/service/">https://www.koenigsmann.ru/service/</a>. Сервис
                диагностирует проблему и предложит варианты решения: удаленный ремонт или ремонт в сервисном центре.
            </p>

            <h3>Ответы на вопросы</h3>

            <p>Для получения дополнительной информации вы всегда можете обратиться в службу поддержки на странице
                <a href="https://www.koenigsmann.ru/service/">https://www.koenigsmann.ru/service/</a> или по номеру
                <br>
                +7 499 350 15 85. Эксперты Koenigsmann готовы оперативно решить возникшие вопросы ежедневно с 9:00
                до 21:00 по московскому времени. </p>

            <div class="attention">
                <span class="attention__icon"></span>
                <p class="attention__text">Внимание! Все заявки на рекламации или возврат средств принимаются только
                    через форму внизу этой страницы. По телефону, чату или другим способам связи заявки не
                    расцениваются как официальные.</p>
            </div>

        </div>

    </div>
</section>
<!-- END Заявка на сервисное обслуживание -->

{{-- Валидация --}}
<script>
  window.addEventListener('load', () => {

    let form_partner = document.querySelector('.form_partner')
    form_partner.addEventListener('submit', valid_inp)

    function valid_inp (e) {

      let data_partner_inp = document.querySelectorAll('.data-partner input')
      let error = []
      for (let i = 0; i < data_partner_inp.length; i++) {
        if (data_partner_inp[i].value.trim().length < 2) {
          error.push(data_partner_inp[i].nextElementSibling.textContent)
        }
      }

      if (location_city.value.trim().length < 2) {
        error.push(location_city.nextElementSibling.textContent)
      }
      if (error.length > 0) {
        e.preventDefault()
        e.stopPropagation()
        let form_partner__red = document.querySelector('.form_partner__red-text')
        form_partner__red.style.display = 'inline-block'
        return false
      }
    }

  })
</script>

{{-- Показать загружаемые файлы --}}
<script>
  window.addEventListener('load', () => {
    let gold_clip_inp = document.getElementById('gold_clip_inp')
    gold_clip_inp.addEventListener('input', show_name_upload_files)

    function show_name_upload_files (e) {
      let name_files = document.querySelector('.name_files')
      let obj = e.target.files
      for (let key in obj) {
        if ((typeof obj[key].name === 'string') && obj[key].name.indexOf('.') > 0) {
          let li = document.createElement('LI')
          li.textContent = obj[key].name
          name_files.appendChild(li)
        }
      }
    }
  })
</script>
@endsection
