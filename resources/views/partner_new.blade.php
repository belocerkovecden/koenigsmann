@extends('layouts.layout', ['title' => 'Станьте партнером компании Koenigsmann'])


@section('content')

    <!-- START Стать партнером -->
    <section class="partner_page">
        <div class="wrapper">
            <h1 class="partner_page__h1">Стать партнёром компании</h1>

            <p class="partner_page__first-p">
                <span class="span_fz_24px">Добрый день</span>, уважаемый партнер, Вас приветсвует компания KOENIGSMANN!
                Просим заполнить небольшую анкету, что бы наше взаимодействие стало еще удобнее.
            </p>

            <form action="{{ route('partner') }}" class="form_partner" method="post" enctype="multipart/form-data" multiple>

                {{-- Безопасность от Laravel --}}
                @csrf

                <div class="data-partner">
                    <h3 class="data-partner__h3">Данные</h3>
                    <div class="input-field data-partner__fio">
                        <input type="text" id="data-partner__fio" name="contact_person_name"
                               required {{ old('contact_person_name') ?? $partner->organization_name ?? '' }}>
                        <label for="data-partner__fio">ФИО контактного лица <span class="red_star">*</span> </label>
                    </div>
                    <div class="input-field data-partner__phone">
                        <input type="tel" id="data-partner__phone" name="contact_person_phone"
                               required {{ old('contact_person_phone') ?? $partner->contact_person_phone ?? '' }}>
                        <label for="data-partner__phone">Телефон <span class="red_star">*</span> </label>
                    </div>
                    <div class="input-field data-partner__email">
                        <input type="text" id="data-partner__email" name="contact_person_email"
                               required {{ old('contact_person_email') ?? $partner->contact_person_email ?? '' }}>
                        <label for="data-partner__email">E-mail <span class="red_star">*</span> </label>
                    </div>
                    <div class="input-field data-partner__website">
                        <input type="text" id="data-partner__website" name="organization__address"
                               required {{ old('organization__address') ?? $partner->organization_address ?? '' }}>
                        <label for="data-partner__website">Сайт <span class="red_star">*</span> </label>
                    </div>
                    <div class="input-field data-partner__inn">
                        <input type="text" id="data-partner__inn" name="organization_inn"
                               required {{ old('organization_inn') ?? $partner->organization_inn ?? '' }}>
                        <label for="data-partner__inn">ИНН организации <span class="red_star">*</span> </label>
                    </div>
                </div>

                <div class="questions">
                    <h3 class="questions__h3">Вопросы</h3>


                    <h4 class="list_quest">1) Как вы планируете работать с нами? <span class="red_star">*</span></h4>
                    <div class="wrap_checkbox_question">
                        <label class="checkbox">
                            <input type="radio" name="points_issue" value="Деловые линии" checked="">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">По дилерскому договору</span>
                        </label>
                    </div>
                    <div class="wrap_checkbox_question mb60">
                        <label class="checkbox">
                            <input type="radio" name="points_issue" value="ПЭК">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">По агентскому договору</span>
                        </label>
                    </div>

                    <h4 class="list_quest">2) Осуществляете продажи через...  <span class="red_star">*</span></h4>
                    <div class="wrap_checkbox_question">
                        <label class="checkbox">
                            <input type="radio" name="points_issue" value="Деловые линии" checked="">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">Интернет-магазин</span>
                        </label>
                    </div>
                    <div class="wrap_checkbox_question mb60">
                        <label class="checkbox">
                            <input type="radio" name="points_issue" value="ПЭК">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">Розничный магазин</span>
                        </label>
                    </div>

                    <h4 class="list_quest">3) Регион осуществления торговли <span class="red_star">*</span></h4>
                    <div class="wrap_checkbox_question">
                        <label class="checkbox">
                            <input type="radio" name="points_issue" value="Деловые линии" checked="">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">По РФ</span>
                        </label>
                    </div>
                    <div class="wrap_checkbox_question">
                        <label class="checkbox">
                            <input type="radio" name="points_issue" value="ПЭК">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">По РФ + СНГ</span>
                        </label>
                    </div>
                    <div class="wrap_checkbox_question mb60">
                        <label class="checkbox">
                            <input type="radio" name="points_issue" value="ПЭК">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">Только в домашнем регионе/городе</span>
                        </label>
                    </div>

                    <hr class="hr_partner_new">

                    <div class="wrap_checkbox_question">
                        <label class="checkbox">
                            <input type="radio" name="points_issue" value="ПЭК">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">Есть ли у вас шоу-рум? <span class="red_star">*</span> </span>
                        </label>
                    </div>
                    <div class="wrap_checkbox_question">
                        <label class="checkbox">
                            <input type="radio" name="points_issue" value="ПЭК">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">Есть ли у вас собственная служба доставки? <span class="red_star">*</span> </span>
                        </label>
                    </div>
                </div>

                <div class="input-field location_city">
                    <input type="text" id="location_city" name="location_city"
                           required {{ old('location_city') ?? $partner->location_city ?? '' }}>
                    <label for="location_city">В каком городе вы находитесь <span class="red_star">*</span> </label>
                </div>

                <div class="form-group wrap_gold_clip">
                    <span class="head_gold_clip">Карточка предприятия <span class="red_star">*</span> </span>
                    <label class="label_gold_clip">
                        <span class="gold_clip_icon"></span>
                        <span class="upload_file_text">Загрузить</span>
                    <input name="photos[]" type="file" class="form-control" multiple id="gold_clip_inp"
                           accept="image/*,image/jpeg,image/png"
                           value="{{ old('img_product') ?? $product->img_product ?? '' }}">
                    </label>
                </div>

                <div class="wrap_btn-form_info">
                    {{--<button type="submit" class="btn-form btn_submit_partner">Отправить заявку</button>--}}
                    <input type="submit" value="Отправить заявку" class="btn-form btn_submit_partner">
                    <p class="info">Нажимая на кнопку, вы соглашаетесь на обработку персональных данных в соответствии с
                        Политикой конфиденциальности</p>

                    <div class="form_partner__red-text">Заполните все обязательные поля!</div>
                </div>
            </form>

            <div class="service">
                <h2 class="head_step">Сервис и преференции</h2>

                <ul class="wrap_cards">
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/partner/expert.jpg') }}" alt="expert.jpg">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Экспертный подбор оборудования</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/partner/sails.jpg') }}" alt="sails.jpg">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Система скидок</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/partner/waranty.jpg') }}" alt="waranty.jpg">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Гарантийное обслуживание</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
            <span class="wrap_img">
                <img src="{{ asset('img/partner/manager.jpg') }}" alt="manager.jpg">
            </span>
                            <div class="header">
                                <p class="angle_right_white">Персональный менеджер</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>

           {{-- <div class="escort">
                <h2 class="head_step">Сопровождение продаж</h2>

                <ul class="wrap_cards">
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Экспертный подбор оборудования</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Система скидок</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Гарантийное обслуживание</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Персональный менеджер</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>--}}

            <div class="partner">
                <h2 class="head_step">Как стать партнёром?</h2>

                <ul class="list_step">
                    <li>
                        <img src="{{ asset('img/circle/circle1.svg') }}" alt="1">
                        <p>Заполнить заявку выше</p>
                    </li>
                    <li>
                        <img src="{{ asset('img/circle/circle3.svg') }}" alt="2">
                        <p>Дождаться звонка менеджера</p>
                    </li>
                    <li>
                        <img src="{{ asset('img/circle/circle4.svg') }}" alt="3">
                        <p>Согласовать коммерческие условия</p>
                    </li>
                    <li>
                        <img src="{{ asset('img/circle/circle2.svg') }}" alt="4">
                        <p>Начать работу</p>
                    </li>
                </ul>

            </div>
        </div>
    </section>

    <!-- END Стать партнером -->

@endsection