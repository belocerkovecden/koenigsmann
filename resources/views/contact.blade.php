@extends('layouts.layout', ['title' => 'Обратная связь Koenigsmann'])


@section('content')

<!-- START Контакты -->
<div class="contacts"><!-- Лев Leo -->
    <div class="bg"></div>
    <div class="wrapper">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
            <li><a href="#">Контакты</a></li>
        </ul>
        <h1>Обратная связь</h1>
        <div class="wrap-contacts_info-form_request">
            <div class="contacts_info">
                <div class="email contacts_info__child">
                    <div class="head">E-mail</div>
                    <div class="text">
                        <span></span>
                        <a href="mailto:info@koenigsmann.ru">info@koenigsmann.ru</a>
                    </div>
                </div>
                <div class="phone contacts_info__child">
                    <div class="head">Телефон</div>
                    <div class="text">
                        <span></span>
                        <a href="tel:+74993501585">+7 499 350 15 85</a>
                    </div>
                </div>
                <div class="address contacts_info__child">
                    <div class="head">Фактический/Юридический адрес</div>
                    <div class="text">
                        <a>Россия,600016, г. Владимир, ул. Большая Нижегородская , дом 79, этаж/офис 2/16</a>
                    </div>
                </div>
                <div class="requisites contacts_info__child">
                    <div class="head">Реквизиты компании</div>
                    <div class="text">
                        <span>ИНН</span>
                        <a>3329096750</a>
                    </div>
                    <div class="text">
                        <span>ОГРН</span>
                        <a>1193328009093</a>
                    </div>
<!--                     <div class="text">
                        <span>ОГРИП</span>
                        <a>119332</a>
                    </div>  -->                    
                </div>
                <div class="socials contacts_info__child">
                    <div class="head">Мы в соц. сетях</div>
                    <div class="text">
                        <a href="https://vk.com/koenigsmann" class="vk" target="_blank"></a>
                        {{--<a href="#" class="ok"></a>--}}
                        <a href="https://www.instagram.com/koenigsmann.ru/" class="instagram" target="_blank"></a>
                        <a href="https://www.facebook.com/koenigsmann.ru/" class="facebook" target="_blank"></a>
                    </div>
                </div>

            </div>
            <div class="form_request form_desc"><h1>Оставьте заявку</h1>
                <form action="{{ route('contacts') }}" method="post">

                    {{-- Безопасность от Laravel --}}
                    @csrf

                    <div class="input-field contacts_input">
                        <input type="text" id="name" name="contact_name" required value="{{ old('contact_name') ?? $product->contact_name ?? '' }}">
                        <label for="name">Имя</label>
                    </div>
                    <div class="input-field contacts_input">
                        <input type="email" id="email" name="contact_email" required value="{{ old('contact_email') ?? $product->contact_email ?? '' }}">
                        <label for="signin_password">E-mail</label>
                    </div>
                    <div class="input-field contacts_input">
                        <input type="text" id="description" name="contact_description" required value="{{ old('contact_description') ?? $product->contact_description ?? '' }}">
                        <label for="description">Описание</label>
                    </div>
                    <input type="submit" class="send_request" value="Оставить заявку">
                </form>
            </div>
        </div>
        <div class="wrap-delivery">
            <div class="shipping wrap-delivery__child">
                <div class="head">Доставка транспортными компаниями</div>
                <div class="text">
                    <a class="delovielinii"></a>
                    <a class="pek"></a>
                </div>
            </div>
            <div class="payment wrap-delivery__child">
                <div class="head">Оплата</div>
                <div class="text">
                    <span class="bank_card wrap-delivery__child__icon"></span>
                    <a>Картой на сайте</a>
                    <span class="cards">
                        <span class="mastercard"></span>
                        <span class="mir"></span>
                        <span class="visa"></span>
                    </span>
                </div>
                <div class="text wrap-delivery__child">
                    <span class="invoice wrap-delivery__child__icon"></span>
                    <a>По выставленному счёту</a>
                </div>
            </div>

            {{-- По примеру от Сбербанка --}}

<style>

.wrapper_sber {
    position: relative;
    z-index: 100;
    margin-top: 40px;
    margin-bottom: 40px;
    overflow: hidden;
}

.wrapper_sber > .wrap_img {
    background: #fff;
    margin-bottom: 30px;
    max-width: 240px;
    text-align: center;
    height: 100px;
    display: flex;
    justify-content: center;
    align-items: center;
}

.wrapper_sber > .wrap_img > img {
    max-width: 190px;
}

.wrapper_sber p {
    margin-top: 20px;
    margin-bottom: 20px;
}

.wrapper_sber ul li {
    list-style: disc;
    margin-left: 20px;
}

</style>

            <div class="wrapper_sber">
                <div class="wrap_img">
                    <img src="{{ asset('/img/pay_sber.jpg') }}" alt="Покупай со сбербанком">
                </div>

                <p>
                    Владельцы дебетовой банковской карты ПАО Сбербанк, подключившиеся к услуге «Мобильный банк» и системе «Сбербанк Онлайн», могут совершать покупки <b>в кредит</b>, не выходя из дома!
                </p>

                <h3>Кредит по программе "Покупай со Сбербанком"</h3>

                <p>Основные требования:</p>

                <ul>
                   <li>Гражданство РФ</li>
                   <li>Возраст покупателя - от 21 до 65 лет на момент возврата кредита по договору</li>
                   <li>Наличие постоянной (временной) регистрации по месту жительства/пребывания на территории Российской Федерации</li>
                </ul>          
                
                <p>Преимущества Сервиса:</p>

                <ul>
                    <li>Без первоначального взноса по кредиту</li>
                    <li>Срок действия кредита без переплаты: 6 - 24 месяцев</li>
                    <li>Срок действия кредита от 3 до 36 месяцев</li>
                    <li>Сумма кредита от 3 000 до 300 000 рублей</li>
                </ul>
                
                <p>Клиентский сценарий:</p>
                            
                <ul>
                    <li>Выберите товар на сайте, добавьте его в корзину</li>
                    <li>При оформлении товара, выберите способ оплаты « В кредит от покупай со Сбербанком»</li>
                    <li>После оформления заказа в корзине, необходимо нажать на кнопку «в кредит от Покупай со Сбербанком»</li>
                    <li>Подать заявку в Сбербанк Онлайн.</li>
                    <li>После одобрения Банком заявки подтвердите согласие с его условиями в Сбербанк Онлайн и ожидать доставку</li>
                </ul>

                <p>Подробнее с условиями кредитования можно ознакомиться на странице <a href="https://www.pokupay.ru/" target="_blank">https://www.pokupay.ru/</a> </p>
                <!--<p>Условия кредитования <a href="https://www.pokupay.ru/credit_terms" target="_blank">https://www.pokupay.ru/credit_terms</a> </p>-->
            </div>


            <div class="for_buyers wrap-delivery__child">
                <div class="head">Покупателю</div>
                <div class="text">
                    <a href="{{ url('policy') }}">Политика конфиденциальности</a>
                </div>
                <div class="text">
                    <a href="{{ url('agreement') }}">Пользовательское соглашение</a>
                </div>
            </div>
            <div class="description wrap-delivery__child">
                <div class="head">Описание процессов передачи данных</div>
                <p class="text">
                    Для оплаты (ввода реквизитов Вашей карты) Вы будете перенаправлены на платёжный шлюз ПАО СБЕРБАНК. Соединение с платёжным шлюзом и передача информации осуществляется в защищённом режиме с использованием протокола шифрования SSL. В случае если Ваш банк поддерживает технологию безопасного проведения интернет-платежей Verified By Visa, MasterCard SecureCode, MIR Accept, J-Secure для проведения платежа также может потребоваться ввод специального пароля.
                    <br><br>
                    Настоящий сайт поддерживает 256-битное шифрование. Конфиденциальность сообщаемой персональной информации обеспечивается ПАО СБЕРБАНК. Введённая информация не будет предоставлена третьим лицам за исключением случаев, предусмотренных законодательством РФ. Проведение платежей по банковским картам осуществляется в строгом соответствии с требованиями платёжных систем МИР, Visa Int., MasterCard Europe Sprl, JCB.
                </p>
            </div>
        </div>
        <div class="form_request form_mob"><h1>Оставьте заявку</h1>
            <form action="{{ route('contacts') }}" method="post">

                {{-- Безопасность от Laravel --}}
                @csrf

                <div class="input-field contacts_input">
                    <input type="text" id="name" name="contact_name" required value="{{ old('contact_name') ?? $product->contact_name ?? '' }}">
                    <label for="name">Имя</label>
                </div>
                <div class="input-field contacts_input">
                    <input type="email" id="email" name="contact_email" required value="{{ old('contact_email') ?? $product->contact_email ?? '' }}">
                    <label for="signin_password">E-mail</label>
                </div>
                <div class="input-field contacts_input">
                    <input type="text" id="description" name="contact_description" required value="{{ old('contact_description') ?? $product->contact_description ?? '' }}">
                    <label for="description">Описание</label>
                </div>
                <input type="submit" class="send_request" value="Оставить заявку">
            </form>
        </div>
        <div class="description_mob">
            <div class="head">Описание процессов передачи данных</div>
            <p class="text">
                Для оплаты (ввода реквизитов Вашей карты) Вы будете перенаправлены на платёжный шлюз ПАО СБЕРБАНК. Соединение с платёжным шлюзом и передача информации осуществляется в защищённом режиме с использованием протокола шифрования SSL. В случае если Ваш банк поддерживает технологию безопасного проведения интернет-платежей Verified By Visa, MasterCard SecureCode, MIR Accept, J-Secure для проведения платежа также может потребоваться ввод специального пароля.
                <br><br>
                Настоящий сайт поддерживает 256-битное шифрование. Конфиденциальность сообщаемой персональной информации обеспечивается ПАО СБЕРБАНК. Введённая информация не будет предоставлена третьим лицам за исключением случаев, предусмотренных законодательством РФ. Проведение платежей по банковским картам осуществляется в строгом соответствии с требованиями платёжных систем МИР, Visa Int., MasterCard Europe Sprl, JCB.
            </p>
        </div>
    </div>
</div>
<!-- END Контакты -->


@endsection