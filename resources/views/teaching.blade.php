<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Обучение</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>

<!-- START POPUP Обратный звонок -->
<div class="back_g">
    <div class="wrap_popup">

        <div class="wrap_mob_head">
            <a href="/" class="logo_popup">
                <img src="img/logo.svg" alt="">
            </a>

            <div class="close_popup">
                <span class="close_popup__icon"></span>
            </div>
        </div>

        <div class="head">
            <h4>Обратный звонок</h4>
            <div class="close_popup">
                <span class="close_popup__text">Закрыть</span>
                <span class="close_popup__icon"></span>
            </div>
        </div>

        <form method="post" class="body_popup">
            <a href="tel:88001111122" class="popup_call_myself">Позвонить</a>
            <p>Введите номер телефона и мы перезвоним вам за 30 минут!</p>
            <label>
                <span class="label_span"><span class="plus7">+7</span> 999 383-43-24</span>
                <input type="tel" id="phone" name="phone" minlength="5" required>
            </label>
            <input type="submit" value="Перезвоните мне">

            <p class="policy">
                Нажимая на кнопку, вы соглашаетесь на обработку персональных данных в соответствии с Политикой
                конфиденциальности
            </p>
        </form>
    </div>
</div>
<!-- END POPUP Обратный звонок -->

<!-- START  HEADER -->
<header class="header">

    <div class="header-menuLK">

        <div class="wrapper">

            <!-- Первая линия личный кабинет-->
            <div class="wrap_first_line_LK">

                <div class="wrap_link_home">
                    <span class="link_home_icon"></span>
                    <a class="link_home" href="/">На главную</a>
                </div>

                <div class="wrap_phone_email">
                    <div class="block-phone">
                        <span class="block-phone__icon-phone"></span>
                        <div class="block-phone__wrap-head-number">
                            <div class="block-phone__head">Контактный номер</div>
                            <a href="tel:+74993501585" class="block-phone__number">+7 499 350 15 85</a>
                        </div>
                    </div>
                    <div class="block-email">
                        <span class="block-email__icon-email"></span>
                        <div class="block-email__wrap-head-email">
                            <div class="block-email__head">E-mail</div>
                            <a href="mailto:info@koenigsmann.ru" class="block-email__number">info@koenigsmann.ru</a>
                        </div>
                    </div>

                    <a href="/" class="logo">
                        <img src="img/logo.svg" alt="">
                    </a>

                    <a href="#" id="return_to_menu"></a>

                    <div class="wrap_mob_LK">
                        <a href="tel:88001111122" class="mobile768"></a> <a href="#" class="sandwich"></a>
                    </div>
                </div>
            </div>

            <!-- Вторая линия -->
            <ul class="second-line_LK">
                <li><a href="#">О бренде</a></li>
                <li><a href="http://cj34342.tmweb.ru/km/catalog.html">Каталог продукции</a></li>
                <li><a href="http://cj34342.tmweb.ru/km/personal_area-stock.html">Акции</a></li>
                <li><a href="#">Покупателям</a></li>
                <li class="active"><a href="http://cj34342.tmweb.ru/km/">Стать партнёром</a></li>
                <li><a href="http://cj34342.tmweb.ru/km/where_buy.html">Где купить</a></li>
                <li><a href="http://cj34342.tmweb.ru/km/contact.html">Контакты</a></li>
            </ul>
        </div>
    </div>
</header>

<!-- Вторая линия -->
<div class="wrap_data_user_line_LK">
    <div class="wrapper">
        <div class="user_data_link">
            <h1 class="wrap_data_user_head">Личный кабинет</h1>
            <div class="wrap_data_basket">
                <div class="wrap_data_account">
                    <span class="account_icon"></span>
                    <h2>Данные аккаунта</h2>
                </div>
                <div class="wrap_basket">
                    <span class="basket_icon"></span>
                    <h2>Корзина</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END HEADER -->

<!-- START Стать партнером -->
<section class="wrapper">

    <div class="wrap_scroll_for_md">
        <div class="wrap_tab_btn_my teaching_menu">
            <div class="wrap_tab_teaching">
                <h3 class="show_md" id="UC"><a href="#">Выгрузка каталога</a></h3>
                <h3 id="PL"><a href="#">Прайс-листы</a></h3>
                <h3><a href="http://cj34342.tmweb.ru/km/personal_area-stock.html">Акции</a></h3>
                <h3 class="active_tab"><a href="#">Обучение</a></h3>
                <h3 id="MS"><a href="#">Маркетинговая поддержка</a></h3>
            </div>

            <a href="#" id="btn_upload_katalog">Выгрузка каталога</a>
        </div>
    </div>

    <div class="training_cards">
        <a href="#" class="training_card">
            <div class="wrap_img"><img src="img/eli.png" alt="eli"></div>
            <h2>Как заработать на KOENIGSMANN</h2>
        </a>
        <a href="#" class="training_card">
            <div class="wrap_img"><img src="img/eli.png" alt="eli"></div>
            <h2>Убеждение. Работа с возражениями</h2>
        </a>
        <a href="#" class="training_card">
            <div class="wrap_img"><img src="img/eli.png" alt="eli"></div>
            <h2>Как заработать на KOENIGSMANN</h2>
        </a>
        <a href="#" class="training_card">
            <div class="wrap_img"><img src="img/eli.png" alt="eli"></div>
            <h2>Как заработать на KOENIGSMANN</h2>
        </a>
        <a href="#" class="training_card">
            <div class="wrap_img"><img src="img/eli.png" alt="eli"></div>
            <h2>Убеждение. Работа с возражениями</h2>
        </a>
        <a href="#" class="training_card">
            <div class="wrap_img"><img src="img/eli.png" alt="eli"></div>
            <h2>Как заработать на KOENIGSMANN</h2>
        </a>
    </div>

</section>

<script src="js/lib/jquery-3.4.1.min.js"></script>
<script src="js/lib/jquery.maskedinput.min.js"></script>
<script src="js/main.min.js"></script>
<script>
  window.addEventListener('load', () => {
    /*** Маска телефона для всплывающей формы ***/
    $('#phone').mask('+7 ( 999 ) 999-99-99')
  })
</script>
</body>
</html>