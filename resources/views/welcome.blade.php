<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Партнерам</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>

<!-- START POPUP Обратный звонок -->
<div class="back_g">
    <div class="wrap_popup">

        <div class="wrap_mob_head">
            <a href="/" class="logo_popup">
                <img src="img/logo.svg" alt="">
            </a>

            <div class="close_popup">
                <span class="close_popup__icon"></span>
            </div>
        </div>

        <div class="head">
            <h4 class="show_lg">Обратный звонок</h4>
            <h4 class="show_md">Звонок</h4>
            <div class="close_popup">
                <span class="close_popup__text">Закрыть</span>
                <span class="close_popup__icon"></span>
            </div>
        </div>
        <form action="#" class="body_popup">
            <a class="popup_call_site" href="tel:+74993501585">8 800 111 12 22</a>
            <a class="popup_call_myself" href="tel:+79283048594">Позвонить</a>
            <p class="min_30">Введите номер телефона и мы перезвоним вам за 30 минут!</p>
            <h4 class="body_popup__show_md">Обратный звонок</h4>

            <div class="input-field call_back_user_name">
                <input type="text" id="call_back_user_name" required>
                <label for="call_back_user_name">Имя</label>
            </div>

            <div class="input-field call_back_user_phone">
                <input type="tel" id="phone" name="phone" minlength="5" required>
                <label for="phone">Телефон</label>
            </div>

            <span class="call_back_after">Перезвонить через <input type="text" class="call_back_after__input" value="20"> минут</span>
            <!--
                        <label>
                            <span class="label_span"><span class="plus7">+7</span> 999 383-43-24</span>
                            <input type="tel" id="phone" name="phone" minlength="5" required>
                        </label>-->
            <div class="wrap_btn_policy">
                <input type="submit" value="Перезвоните мне">

                <p class="policy">
                    Нажимая на кнопку, вы соглашаетесь на обработку персональных данных в соответствии с Политикой
                    конфиденциальности
                </p>
            </div>
        </form>
    </div>
</div>
<!-- END POPUP Обратный звонок -->


<!-- START  HEADER -->
<header class="header">

    <div class="wrap_first_line wrapper">

        <div class="wrap_search_phone">
            <div class="block-search">
                <span class="block-search__icon-search"></span>
                <input class="block-search__input" type="text" placeholder="Поиск">
                <span class="block-search__icon-close"></span>
            </div>
            <div class="block-phone">
                <span class="block-phone__icon-phone"></span>
                <div class="block-phone__wrap-head-number">
                    <div class="block-phone__head">Обратный звонок</div>
                    <a href="tel:88001111122" class="block-phone__number">8 800 111 11 22</a>
                </div>
            </div>
        </div>

        <a href="/" class="logo">
            <img src="img/logo.svg" alt="">
        </a>

        <a href="#" class="login">Вход в личный кабинет</a>

        <!-- Видно только на планшете и меньше -->
        <div class="wrap_mob">
            <a href="tel:88001111122" class="mobile768"></a>
            <a href="#" class="sandwich"></a>
        </div>

    </div>

    <div class="header-menu">

        <div class="wrapper"><!-- Первая линия -->

            <!-- Вторая линия -->
            <ul class="second-line">
                <li><a href="#">О бренде</a></li>
                <li><a href="http://cj34342.tmweb.ru/km/catalog.html">Каталог продукции</a></li>
                <li><a href="http://cj34342.tmweb.ru/km/personal_area-stock.html">Акции</a></li>
                <li><a href="http://cj34342.tmweb.ru/km/">Покупателям</a></li>
                <li class="active"><a href="http://cj34342.tmweb.ru/km/">Стать партнёром</a></li>
                <li><a href="http://cj34342.tmweb.ru/km/where_buy.html">Где купить</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </div>
    </div>
</header>
<!-- END HEADER -->

<!-- START BreadCrumbs -->
<ul class="breadcrumbs wrapper">
    <li><a href="/">Главная</a></li>
    <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
    <li><a href="#">Стать партнером</a></li>
</ul>
<!-- END BreadCrumbs -->

<!-- START Стать партнером -->
<section class="wrapper">
    <!-- Лев Leo -->
    <div class="bg"></div>

    <form action="#" class="form">
        <div class="i-interested">
            <h3 class="i-interested__head">Меня интересует</h3>
            <div class="input-field i-interested__input">
                <input type="text" id="interested">
                <label for="interested">Введите Ваши интересы</label>
            </div>
        </div>

        <div class="organization">
            <h3 class="organization__head">Организация</h3>
            <div class="input-field organization__name">
                <input type="text" id="organization__name">
                <label for="organization__name">Название</label>
            </div>
            <div class="input-field organization__inn">
                <input type="text" id="organization__inn">
                <label for="organization__inn">ИНН</label>
            </div>
            <div class="input-field organization__address">
                <input type="text" id="organization__address">
                <label for="organization__address">Адрес регистрации</label>
            </div>
        </div>

        <div class="contact-face">
            <h3 class="contact-face__head">Контактное лицо</h3>
            <div class="input-field contact-face__name">
                <input type="text" id="contact-face__name">
                <label for="contact-face__name">Имя</label>
            </div>
            <div class="input-field contact-face__phone">
                <input type="tel" id="contact-face__phone">
                <label for="contact-face__phone">Телефон</label>
            </div>
            <div class="input-field contact-face__email">
                <input type="text" id="contact-face__email">
                <label for="contact-face__email">E-mail</label>
            </div>
        </div>

        <div class="wrap_btn-form_info">
            <a href="#" class="btn-form">Отправить заявку</a>
            <p class="info">Нажимая на кнопку, вы соглашаетесь на обработку персональных данных в соответствии с
                Политикой конфиденциальности</p>
        </div>
    </form>

    <div class="service">
        <h2 class="head_step">Сервис и преференции</h2>

        <ul class="wrap_cards">
            <li>
                <a href="#">
                    <span class="wrap_img">
                        <img src="img/tetya_bg.png" alt="tetya_bg.png">
                    </span>
                    <div class="header">
                        <p class="angle_right_white">Экспертный подбор оборудования</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="wrap_img">
                        <img src="img/tetya_bg.png" alt="tetya_bg.png">
                    </span>
                    <div class="header">
                        <p class="angle_right_white">Система скидок</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="wrap_img">
                        <img src="img/tetya_bg.png" alt="tetya_bg.png">
                    </span>
                    <div class="header">
                        <p class="angle_right_white">Гарантийное обслуживание</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
            <span class="wrap_img">
                <img src="img/tetya_bg.png" alt="tetya_bg.png">
            </span>
                    <div class="header">
                        <p class="angle_right_white">Персональный менеджер</p>
                    </div>
                </a>
            </li>
        </ul>
    </div>

    <div class="escort">
        <h2 class="head_step">Сопровождение продаж</h2>

        <ul class="wrap_cards">
            <li>
                <a href="#">
                    <span class="wrap_img">
                        <img src="img/tetya_bg.png" alt="tetya_bg.png">
                    </span>
                    <div class="header">
                        <p class="angle_right_white">Экспертный подбор оборудования</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="wrap_img">
                        <img src="img/tetya_bg.png" alt="tetya_bg.png">
                    </span>
                    <div class="header">
                        <p class="angle_right_white">Система скидок</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="wrap_img">
                        <img src="img/tetya_bg.png" alt="tetya_bg.png">
                    </span>
                    <div class="header">
                        <p class="angle_right_white">Гарантийное обслуживание</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="wrap_img">
                        <img src="img/tetya_bg.png" alt="tetya_bg.png">
                    </span>
                    <div class="header">
                        <p class="angle_right_white">Персональный менеджер</p>
                    </div>
                </a>
            </li>
        </ul>
    </div>

    <div class="partner">
        <h2 class="head_step">Как стать партнёром?</h2>

        <ul class="list_step">
            <li>
                <img src="img/circle/circle1.svg" alt="1">
                <p>Заполнить заявку выше</p>
            </li>
            <li>
                <img src="img/circle/circle3.svg" alt="2">
                <p>Дождаться звонка менеджера</p>
            </li>
            <li>
                <img src="img/circle/circle4.svg" alt="3">
                <p>Согласовать коммерческие условия</p>
            </li>
            <li>
                <img src="img/circle/circle2.svg" alt="4">
                <p>Начать работу</p>
            </li>
        </ul>

    </div>
</section>
<!-- END Стать партнером -->


<!-- Заказать обратный звонок -->
<script>

</script>
<script src="js/lib/jquery-3.4.1.min.js"></script>
<script src="js/lib/jquery.maskedinput.min.js"></script>
<script src="js/main.min.js"></script>
<script>
  window.addEventListener('load', () => {
    /*** Маска телефона для всплывающей формы ***/
    $('#phone').mask('+7 ( 999 ) 999-99-99')
  })
</script>
</body>
</html>