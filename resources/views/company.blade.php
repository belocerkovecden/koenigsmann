@extends('layouts.layout', ['title' => 'О компании Koenigsmann'])


@section('content')

<div class="company">
    <h1>О компании</h1>
    <p class="foreword">История компании началась не так давно, компания основана лишь в 2013 году, но бренд Koenigsmann уже завоевал всемирное признание и ассоциируется с терминами качество и надежность. Свой путь от небольшого конструкторского бюро из пригорода Роттердама где работали талантливые инженеры, всего за семь лет превратились в крупную корпорацию, продукций которой пользуются в более чем в двадцати странах по всему миру. Что же способствовало такому росту?</p>
    <!-- <a href="#" class="pcffs">Регламенты стандарта PCFFS (PDF)</a> -->
    <div class="big_block">
        <div class="text_in_big_block">
            <span class="small_title">Инновации</span>
            <h2 class="big_title">Вектор развития компании</h2>
            <p class="top_text">Первая линейка кардиотренажеров Koenigsmann разрабатывалась в течение трех лет совместно c ведущими специалистами Нидерландов в области кардиологии и ортопедии специально для участия в конкурсе на поставку оборудования для подготовки Вооруженных сил Нидерландов. В рамках исследований были проведены ряд технологических и медицинских испытаний. Проверялись такие характеристики, как ударная прочность конструкции, усталостная прочность конструкции, устойчивость к окружающему воздействию, а также влияние оборудования на опорно-двигательный аппарат человека. Все это позволило создать практически совершенное спортивное оборудование, и все эти разработки были внедрены и в серию гражданских беговых дорожек Koenigsmann.</p>
            
            <div class="wrap">
                <div class="left_block">
                    <img src="{{ asset('img/company_image.png') }}" alt="" class="left_block__img">
                    <p class="left_block__text">На данный момент для создания спортивного оборудования компания привлекает ведущих мировых дизайнеров и экспертов в области медицины и спорта для совместного создания оригинальных спортивных тренажеров и гаджетов.
                    Так же неоспоримым преимуществом дорожек нидерландского производителя является его стремление следовать тренду фитнесс индустрии. Каждый год редизайну подвергается не только внешний вид дорожек, что позволяет им выглядеть всегда современно, но и технологическая начинка.
                    </p>
                </div>
                <div class="right_block">
                    <div class="right_block__item">
                        <div class="right_block__item__indicator">
                            <span class="right_block__item__indicator__icon arrow"></span>
                            <span class="right_block__item__indicator__text">500</span>
                        </div>
                        <p class="right_block__item__text">Каждый прототип тренажера проходит более 500 видов испытаний прежде чем поступить в продажу. Продукция Koenigsmann отвечает всем требованиям безопасности и износостойкости.</p>
                    </div>
                    <div class="right_block__item">
                       <div class="right_block__item__indicator">
                            <span class="right_block__item__indicator__icon refresh"></span>
                            <span class="right_block__item__indicator__text">80%</span>
                        </div>
                        <p class="right_block__item__text">В производстве используются только качественные и экологически чистые материалы. Более 80% используемых материалов доступны к переработке, тем самым компания заботится о сохранении ресурсов планеты.</p>
                    </div>
                </div>
            </div>                
        </div>
    </div>
    <div class="big_block">
        <div class="text_in_big_block">
            <span class="small_title">Здоровая планета</span>
            <h2 class="big_title">Каждый клиент бренда Koenigsmann вносит огромный вклад в развитие и сохранение нашей планеты.</h2>
            <p class="top_text">Техника Koenigsmann создается людьми и для людей! Но также компания не забывает и об окружающей среде и братьях наших меньших. И подтверждает это действиями. В каждой стране где работает дилерская сеть, так же работает специально организованный благотворительный фонд «Koenigsmann, clean the world». С каждой проданной беговой дорожки 1% отчисляется в этот фонд, и идет на благо природы и человека, именно в той стране, где эта дорожка была приобретена.</p>
            <div class="wrap">
                <div class="left_block">
                    <img src="{{ asset('img/company_health.jpg') }}" alt="" class="left_block__img">
                    <p class="left_block__text">В России так же действует благотворительный фонд, который только недавно начал свою работу, но уже успел помочь нескольким лесным хозяйствам. На средства фонда закупались корма для животных и техника для предотвращения браконьерства.
                    </p>
                </div>
                <div class="right_block">
                    <h3 class="right_block__header">Главные достижения</h3>
                    <div class="right_block__item">
                       <div class="right_block__item__indicator">
                            <span class="right_block__item__indicator__icon paw"></span>
                            <span class="right_block__item__indicator__text">10</span>
                        </div>
                        <p class="right_block__item__text">В Нидерландах на средства этого фонда было открыто более десяти питомников для бездомных животных, которые и содержатся на средства фонда. </p>
                    </div>
                    <div class="right_block__item">
                       <div class="right_block__item__indicator">
                            <span class="right_block__item__indicator__icon flower"></span>
                            <span class="right_block__item__indicator__text">>3000</span>
                        </div>
                        <p class="right_block__item__text">Так же фонд сотрудничает с лесными хозяйствами, к примеру, в Польше было высажено более трех тысяч единиц хвойных деревьев.</p>
                    </div>
                    <div class="right_block__item">
                       <div class="right_block__item__indicator">
                            <span class="right_block__item__indicator__icon cricket"></span>
                            <span class="right_block__item__indicator__text">60</span>
                        </div>
                        <p class="right_block__item__text">В странах Восточной Европы было установлено более шестидесяти детских спортивных комплексов в малоразвитых районах. </p>
                    </div>
                </div>
            </div>                
        </div>
    </div>
    <div class="tagline">Be fit <br> be koenigsmann</div>
</div>  
@endsection