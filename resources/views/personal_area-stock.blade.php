@extends('layouts.layout', ['title' => 'Акции'])


@section('content')

{{--<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Личный кабинет Акции</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
<body>

<!-- START  HEADER -->
<header class="header-menu-profile">
    <div class="wrapper">
        
        <!-- Первая линия -->
        <div class="wrap_first_line">
            <a class="comeback" href="/">
                <span class="comeback__icon"></span>
                <span class="comeback__text">На главную</span>
            </a>
            <div class="wrap_phone_mail">
                <div class="block-phone">
                    <span class="block-phone__icon-phone"></span>
                    <div class="block-phone__wrap-head-number">
                        <div class="block-phone__head">Контактный номер</div>
                        <a href="tel:88001111122" class="block-phone__number">8 800 111 11 22</a>
                    </div>
                </div>
                <div class="block-mail">
                    <span class="block-mail__icon-mail"></span>
                    <div class="block-mail__wrap-head-mail">
                        <div class="block-mail__head">E-mail</div>
                        <a  href="mailto:info@koenigsmann.nl" class="block-mail__mail">info@koenigsmann.nl</a>
                    </div>
                </div>
            </div>
            <a href="/" class="logo">
                <img src="{{ asset('img/logo.svg') }}" alt="">
            </a>            
      
        <!-- Видно только на планшете и меньше -->
                <div class="wrap_mob">
                    <div class="wrap-userinfo_card">
                        <div class="block-userinfo">
                            <span class="block-userinfo__icon"></span>
                            <a href="" class="block-userinfo__head">Аккаунт</a>
                        </div>
                        <div class="block-card">
                            <span class="block-card__icon"></span>
                            <a href="" class="block-card__head">Корзина</a>
                        </div>
                    </div>
                    <a href="tel:88001111122" class="mobile768"></a>
                    <a href="#" class="sandwich"></a>
                </div>
          </div>
    </div>
    <!-- Вторая линия -->
        <ul class="second-line">
            <li><a href="#">О бренде</a></li>
            <li><a href="#">Каталог продукции</a></li>
            <li><a href="#">Акции</a></li>
            <li><a href="#">Покупателям</a></li>
            <li class="active"><a href="#">Стать партнёром</a></li>
            <li><a href="#">Где купить</a></li>
            <li><a href="#">Контакты</a></li>
        </ul>
</header>
<!-- END HEADER -->--}}

<!-- START Акции -->
<div class="top-line-account">
    <div class="wrapper">
        <h1>Личный кабинет</h1>
        <div class="wrap_account-userinfo_card">
            <div class="block-account-userinfo">
                <span class="block-account-userinfo__icon"></span>
                <a href="" class="block-account-userinfo__head">Данные аккаунта</a>
            </div>
            <div class="block-card">
                <span class="block-card__icon"></span>
                <a href="" class="block-card__head">Корзина</a>
            </div>
        </div>
    </div>
</div>

<section class="wrapper" id="section_not_twice_scroll">
    <!-- Лев Leo -->
    <div class="bg"></div>

    <div class="wrap_tab_btn">
        <div class="wrap_tab">
            <h3>Прайс-листы</h3>
            <h3 class="active_tab">Акции</h3>
            <h3>Обучение</h3>
            <h3>Маркетинговая поддержка</h3>
        </div>

        <a href="#" id="btn_upload_katalog">Выгрузка каталога</a>
    </div>
    <div class="stock">
        <div class="grey_block"></div>
        <div class="accordion">
            <div class="trigger">
                <input type="checkbox" id="checkbox-1" name="checkbox-1" checked="checked" />
                <label for="checkbox-1" class="checkbox">
                    Скидка 6% на весы <i></i>
                </label>
                <div class="content">
                    <p>Наши специалисты соберут ваше спортивное оборудование в течении 15 минут и проконсультируют Вас по использованию и уходу за вашей покупкой!</p>
                    <p>Мы считаем, что наши клиенты должны знать, кто приезжает к ним домой для осуществления сборки. </p>
                    <p>В нашем магазине работают профессиональные сборщики, которые ответственно выполняют свою работу.</p>
                    <p>Возникли вопросы? Обращайтесь к нашим специалистам. Мы рады каждому Вашему звонку! </p>
                    <p>Бесплатная сборка распространяется на товары, приобретенные без скидки, стоимость товара должна быть свыше 30000 руб.</p>
                    <br><p>Наши специалисты соберут ваше спортивное оборудование в течении 15 минут и проконсультируют Вас по использованию и уходу за вашей покупкой!</p>
                    <p>Мы считаем, что наши клиенты должны знать, кто приезжает к ним домой для осуществления сборки. </p>
                    <p>В нашем магазине работают профессиональные сборщики, которые ответственно выполняют свою работу.</p>
                    <p>Возникли вопросы? Обращайтесь к нашим специалистам. Мы рады каждому Вашему звонку! </p>
                    <p>Бесплатная сборка распространяется на товары, приобретенные без скидки, стоимость товара должна быть свыше 30000 руб.</p>
                    <br><p>Наши специалисты соберут ваше спортивное оборудование в течении 15 минут и проконсультируют Вас по использованию и уходу за вашей покупкой!</p>
                    <p>Мы считаем, что наши клиенты должны знать, кто приезжает к ним домой для осуществления сборки. </p>
                    <p>В нашем магазине работают профессиональные сборщики, которые ответственно выполняют свою работу.</p>
                    <p>Возникли вопросы? Обращайтесь к нашим специалистам. Мы рады каждому Вашему звонку! </p>
                    <p>Бесплатная сборка распространяется на товары, приобретенные без скидки, стоимость товара должна быть свыше 30000 руб.</p>
                    <br><p>Наши специалисты соберут ваше спортивное оборудование в течении 15 минут и проконсультируют Вас по использованию и уходу за вашей покупкой!</p>
                    <p>Мы считаем, что наши клиенты должны знать, кто приезжает к ним домой для осуществления сборки. </p>
                    <p>В нашем магазине работают профессиональные сборщики, которые ответственно выполняют свою работу.</p>
                    <p>Возникли вопросы? Обращайтесь к нашим специалистам. Мы рады каждому Вашему звонку! </p>
                    <p>Бесплатная сборка распространяется на товары, приобретенные без скидки, стоимость товара должна быть свыше 30000 руб.</p>
                    <br><p>Наши специалисты соберут ваше спортивное оборудование в течении 15 минут и проконсультируют Вас по использованию и уходу за вашей покупкой!</p>
                    <p>Мы считаем, что наши клиенты должны знать, кто приезжает к ним домой для осуществления сборки. </p>
                    <p>В нашем магазине работают профессиональные сборщики, которые ответственно выполняют свою работу.</p>
                    <p>Возникли вопросы? Обращайтесь к нашим специалистам. Мы рады каждому Вашему звонку! </p>
                    <p>Бесплатная сборка распространяется на товары, приобретенные без скидки, стоимость товара должна быть свыше 30000 руб.</p>
                    <br><p>Наши специалисты соберут ваше спортивное оборудование в течении 15 минут и проконсультируют Вас по использованию и уходу за вашей покупкой!</p>
                    <p>Мы считаем, что наши клиенты должны знать, кто приезжает к ним домой для осуществления сборки. </p>
                    <p>В нашем магазине работают профессиональные сборщики, которые ответственно выполняют свою работу.</p>
                    <p>Возникли вопросы? Обращайтесь к нашим специалистам. Мы рады каждому Вашему звонку! </p>
                    <p>Бесплатная сборка распространяется на товары, приобретенные без скидки, стоимость товара должна быть свыше 30000 руб.</p>
                    <br><p>Наши специалисты соберут ваше спортивное оборудование в течении 15 минут и проконсультируют Вас по использованию и уходу за вашей покупкой!</p>
                    <p>Мы считаем, что наши клиенты должны знать, кто приезжает к ним домой для осуществления сборки. </p>
                    <p>В нашем магазине работают профессиональные сборщики, которые ответственно выполняют свою работу.</p>
                    <p>Возникли вопросы? Обращайтесь к нашим специалистам. Мы рады каждому Вашему звонку! </p>
                    <p>Бесплатная сборка распространяется на товары, приобретенные без скидки, стоимость товара должна быть свыше 30000 руб.</p>
                    <br><p>Наши специалисты соберут ваше спортивное оборудование в течении 15 минут и проконсультируют Вас по использованию и уходу за вашей покупкой!</p>
                    <p>Мы считаем, что наши клиенты должны знать, кто приезжает к ним домой для осуществления сборки. </p>
                    <p>В нашем магазине работают профессиональные сборщики, которые ответственно выполняют свою работу.</p>
                    <p>Возникли вопросы? Обращайтесь к нашим специалистам. Мы рады каждому Вашему звонку! </p>
                    <p>Бесплатная сборка распространяется на товары, приобретенные без скидки, стоимость товара должна быть свыше 30000 руб.</p>
                    <br><p>Наши специалисты соберут ваше спортивное оборудование в течении 15 минут и проконсультируют Вас по использованию и уходу за вашей покупкой!</p>
                    <p>Мы считаем, что наши клиенты должны знать, кто приезжает к ним домой для осуществления сборки. </p>
                    <p>В нашем магазине работают профессиональные сборщики, которые ответственно выполняют свою работу.</p>
                    <p>Возникли вопросы? Обращайтесь к нашим специалистам. Мы рады каждому Вашему звонку! </p>
                    <p>Бесплатная сборка распространяется на товары, приобретенные без скидки, стоимость товара должна быть свыше 30000 руб.</p>
                    <br><p>Наши специалисты соберут ваше спортивное оборудование в течении 15 минут и проконсультируют Вас по использованию и уходу за вашей покупкой!</p>
                    <p>Мы считаем, что наши клиенты должны знать, кто приезжает к ним домой для осуществления сборки. </p>
                    <p>В нашем магазине работают профессиональные сборщики, которые ответственно выполняют свою работу.</p>
                    <p>Возникли вопросы? Обращайтесь к нашим специалистам. Мы рады каждому Вашему звонку! </p>
                    <p>Бесплатная сборка распространяется на товары, приобретенные без скидки, стоимость товара должна быть свыше 30000 руб.</p>
                    <br><p>Наши специалисты соберут ваше спортивное оборудование в течении 15 минут и проконсультируют Вас по использованию и уходу за вашей покупкой!</p>
                    <p>Мы считаем, что наши клиенты должны знать, кто приезжает к ним домой для осуществления сборки. </p>
                    <p>В нашем магазине работают профессиональные сборщики, которые ответственно выполняют свою работу.</p>
                    <p>Возникли вопросы? Обращайтесь к нашим специалистам. Мы рады каждому Вашему звонку! </p>
                    <p>Бесплатная сборка распространяется на товары, приобретенные без скидки, стоимость товара должна быть свыше 30000 руб.</p>
                    <br>
                    

                </div>
            </div>
            
            <div class="trigger">
                <input type="checkbox" id="checkbox-2" name="checkbox-2" />
                <label for="checkbox-2" class="checkbox">
                    Сезонная акция <i></i>
                </label>
                <div class="content">
                    <p>The magic Indian is a mysterious spiritual force, and we're going to Cathedral Rock, and that's the vortex of the heart.You ever roasted doughnuts?These kind of things only happen for the first time once.Did you feel that? Look at me - I'm not out of breath anymore! 

                    <p>When you get lost in your imaginatory vagueness, your foresight will become a nimble vagrant.You ever roasted doughnuts?These kind of things only happen for the first time once.You gotta go through it to see there ain't nothing to it.It's good to yell at people and tell people that you're from Tennesee, so that way you'll be safe.</p>

                    <p>The best way to communicate is compatible. Compatible communication is listening with open ears and an open mind, and not being fearful or judgemental about what you're hearing.Did you feel that? Look at me - I'm not out of breath anymore!These kind of things only happen for the first time once.</p>
                </div>
            </div>
            
            <div class="trigger">
                <input type="checkbox" id="checkbox-3" name="checkbox-3" />
                <label for="checkbox-3" class="checkbox">
                    Скидка 6% на весы <i></i>
                </label>
                <div class="content">
                    <p>The magic Indian is a mysterious spiritual force, and we're going to Cathedral Rock, and that's the vortex of the heart.You ever roasted doughnuts?These kind of things only happen for the first time once.Did you feel that? Look at me - I'm not out of breath anymore! 

                    <p>When you get lost in your imaginatory vagueness, your foresight will become a nimble vagrant.You ever roasted doughnuts?These kind of things only happen for the first time once.You gotta go through it to see there ain't nothing to it.It's good to yell at people and tell people that you're from Tennesee, so that way you'll be safe.</p>

                    <p>The best way to communicate is compatible. Compatible communication is listening with open ears and an open mind, and not being fearful or judgemental about what you're hearing.Did you feel that? Look at me - I'm not out of breath anymore!These kind of things only happen for the first time once.</p>
                </div>
            </div>
            <div class="trigger">
                <input type="checkbox" id="checkbox-4" name="checkbox-4" />
                <label for="checkbox-4" class="checkbox">
                    Сезонная акция <i></i>
                </label>
                <div class="content">
                    <p>The magic Indian is a mysterious spiritual force, and we're going to Cathedral Rock, and that's the vortex of the heart.You ever roasted doughnuts?These kind of things only happen for the first time once.Did you feel that? Look at me - I'm not out of breath anymore! 

                    <p>When you get lost in your imaginatory vagueness, your foresight will become a nimble vagrant.You ever roasted doughnuts?These kind of things only happen for the first time once.You gotta go through it to see there ain't nothing to it.It's good to yell at people and tell people that you're from Tennesee, so that way you'll be safe.</p>

                    <p>The best way to communicate is compatible. Compatible communication is listening with open ears and an open mind, and not being fearful or judgemental about what you're hearing.Did you feel that? Look at me - I'm not out of breath anymore!These kind of things only happen for the first time once.</p>
                </div>
            </div>
        </div>
    </div>
    



</section>
<!-- END Акции -->
@endsection
{{-- Закрываем флэшку --}}
{{--
<script>
  let close_btn = document.querySelector('.close')

  if (close_btn) {
    close_btn.addEventListener('click', () => {
      document.getElementById('flash_message').style.display = 'none'
    })
  }

</script>
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ asset('js/lib/jquery-3.4.1.min.js') }}" defer></script>
<script src="{{ asset('js/lib/jquery.maskedinput.min.js') }}" defer></script>
--}}
{{--<script src="{{ asset('src/js/vendor/tinymce/js/tinymce/tinymce.min.js') }}" defer></script>--}}{{--

<script src="{{ asset('js/main.min.js') }}" defer></script>


<script>
  window.addEventListener('load', () => {
    /*** Маска телефона для всплывающей формы ***/
    $('#phone').mask('+7 ( 999 ) 999-99-99')

  })
</script>

</body>
</html>--}}
