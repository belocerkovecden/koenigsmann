<li class="nav-item @if(isset($title) && $title == 'О компании Koenigsmann') {{ 'active' }} @endif"><a
            href="{{ url('company') }}">О бренде</a></li>

<li class="nav-item @if(isset($title) && $title == 'Каталог спортивных тренажеров Koenigsmann') {{ 'active' }} @endif">
    <a
            href="{{ url('category') }}">Каталог продукции</a></li>

{{--<li class="nav-item @if(isset($title) && $title == 'Покупателям') {{ 'active' }} @endif"><a
            href="{{ url('to-buyers') }}">Покупателям</a></li>--}}
{{--<li class="nav-item @if(isset($title) && $title == 'Прайс-лист') {{ 'active' }} @endif"><a
            href="{{ url('price_list') }}">Прайс-лист</a></li>--}}
{{--<li class="nav-item @if(isset($title) && $title == 'Акции') {{ 'active' }} @endif"><a
            href="{{ url('public_stock') }}">Акции</a></li>--}}

<li class="nav-item @if(isset($title) && $title == 'Станьте партнером компании Koenigsmann') {{ 'active' }} @endif"><a
            href="{{ url('partnership') }}">Стать партнёром</a></li>

{{--<li class="nav-item @if(isset($title) && $title == 'Где купить?') {{ 'active' }} @endif"><a
            href="{{ url('where_buy') }}">Где купить</a></li>--}}

<li class="nav-item @if(isset($title) && $title == 'Обратная связь Koenigsmann') {{ 'active' }} @endif"><a
            href="{{ url('contacts') }}">Контакты</a></li>
