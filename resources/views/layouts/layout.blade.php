<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="yandex-verification" content="76f5a40fa46f4cdd" />
    {{--<meta name="robots" content="noindex,nofollow">--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title }}</title>
    @if(isset($meta_tag_description))
    <meta name="description" content="{{ $meta_tag_description }}">
    @endif
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/css/fileinput-rtl.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">--}}
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
{{--    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('css/style.css?id=e0a1d7ed9b7e554065671') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css?id=e0a1d7ed9654644065671') }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <link rel="icon" href="{{ asset('32х32.png') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('32х32.png') }}" type="image/x-icon">

    <script src="{{ asset('js/ScreenWidth.js') }}"></script>
{{--    <script>
        let w = new ScreenWidth;
        console.log(w.getScreenWidth());
    </script>--}}
    <style id="style_for_inp_search">

    </style>
</head>
<body>
<!-- START POPUP Обратный звонок -->
<div class="back_g">
    <div class="wrap_popup">

        <div class="wrap_mob_head">
            <a href="/" class="logo_popup">
                <img src="{{ asset('img/logo.svg') }}" alt="">
            </a>

            <div class="close_popup">
                <span class="close_popup__icon"></span>
            </div>
        </div>

        <div class="head">
            <h4 class="show_lg">Обратный звонок</h4>
            <h4 class="show_md">Звонок</h4>
            <div class="close_popup">
                <span class="close_popup__text">Закрыть</span>
                <span class="close_popup__icon"></span>
            </div>
        </div>
        <form action="#" class="body_popup">
            <a class="popup_call_site" href="tel:+74993501585">+7 499 350 15 85</a>
            <a class="popup_call_myself" href="tel:+74993501585">Позвонить</a>
            <p class="min_30">Введите номер телефона и мы перезвоним вам за 30 минут!</p>
            <h4 class="body_popup__show_md">Обратный звонок</h4>

            <div class="input-field call_back_user_name">
                <input type="text" id="call_back_user_name" required>
                <label for="call_back_user_name">Имя</label>
            </div>

            <div class="input-field call_back_user_phone">
                <input type="tel" id="phone" name="phone" minlength="5" required>
                <label for="phone">Телефон</label>
            </div>

            <span class="call_back_after">Перезвонить через <input type="text" class="call_back_after__input"
                                                                   value="20"> минут</span>
            <!--
                        <label>
                            <span class="label_span"><span class="plus7">+7</span>4993501585</span>
                            <input type="tel" id="phone" name="phone" minlength="5" required>
                        </label>-->
            <div class="wrap_btn_policy">
                <input type="submit" value="Перезвоните мне">

                <p class="policy">
                    Нажимая на кнопку, вы соглашаетесь на обработку персональных данных в соответствии с Политикой
                    конфиденциальности
                </p>
            </div>
        </form>
    </div>
</div>
<!-- END POPUP Обратный звонок -->

<!-- START POPUP Авторизация -->
<div class="back_g2">
    <div class="wrap_popup">
        <div class="head">
            <h2 class="h1">Вход в аккаунт</h2>
            <a href="#" class="close_popup">
                <span class="close_popup__text">Закрыть</span>
                <span class="close_popup__icon"></span>
            </a>
        </div>

        <form method="POST" action="{{ route('login') }}" class="body_popup">
            @csrf

            <div class="input-field signin_input">
                <input class="@error('email') is-invalid @enderror" type="text" id="login" name="email"
                       value="{{ old('email') }}" required autocomplete="email" autofocus>
                <label for="login">E-mail или логин</label>
            </div>
            @error('email')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
            <div class="input-field signin_input">
                <input class="form-control @error('password') is-invalid @enderror" type="password" id="password"
                       minlength="6" name="password" required autocomplete="current-password">
                <label for="password">{{ __('Пароль') }}</label>
            </div>

            @if (Route::has('password.request'))
                <a class="forgot" href="{{ route('password.request') }}">
                    {{ __('Забыли пароль?') }}
                </a>
            @endif

            <button type="submit" class="signin_button ">{{ __('Войти в аккаунт') }}</button>

            @if (Route::has('register'))
                <p class="registration desc">
                    <a href="{{ route('register') }}">{{ __('Зарегистрироваться') }},</a> если нет аккаунта
                </p>
            @endif

            @if (Route::has('register'))
                <p class="registration mob">
                    <a href="{{ route('register') }}/">{{ __('Зарегистрироваться') }},</a> если нет аккаунта
                </p>
            @endif

        </form>
    </div>
</div>
<!-- END POPUP Авторизация -->

<!-- START  HEADER -->
<header class="header">

    <div class="wrap_first_line wrapper">

        <div class="wrap_search_phone">
            <form action="{{ route('search.index') }}" class="block-search">
                @csrf
                <span class="block-search__icon-search"></span>
                <input class="block-search__input" type="text" placeholder="Поиск" name="search" required>
                <span class="block-search__icon-close"></span>
                <input type="submit" class="search_submit" value="Найти">
            </form>
            <div class="block-phone" id="hint">
                <span class="block-phone__icon-phone"></span>
                <div class="block-phone__wrap-head-number">
                    <span class="hint">Нажмите для обратного звонка</span>
                    {{--<div class="block-phone__head">Обратный звонок</div>--}}
                    <a href="tel:+74993501585" class="block-phone__number">+7 499 350 15 85</a>
                </div>
            </div>
        </div>

        @if(isset($menu_active) || (isset($title) && ($title == 'Личный кабинет' || $title == 'Данные аккаунта')))
        <!-- Только для страницы Каталог, Кабинет, Аккаунт -->
            <script>
              window.addEventListener('load', () => {
                let w = new ScreenWidth;
                let screen_width = w.getScreenWidth()
                if (screen_width < 768) {
                  let logo = document.querySelector('.logo')
                  logo.style.display = 'none'
                }
              })
            </script>
        @endif

        <a href="{{ url('/') }}" class="logo" id="logo">
            @if(isset($url))
                <img src="{{ asset('img/logo_with_angular_fill.png') }}" alt="Koenigsmann">
            @else
                <img src="{{ asset('img/logo.png') }}" alt="Koenigsmann">
            @endif
        </a> 

        @if(isset($menu_active))
        <!-- Кнопка только для страницы Каталог -->
        <a href="#" class="btn_filter_mobile">Фильтр</a>
        @endif

        {{--На случай если пользователь НЕ аваторизован--}}
        @guest

        <div class="wrap_authorize">
            <a href="#" class="not_login">Войти</a>
            <a href="{{ route('product.shoppingCart') }}" class="basket_link" id="basket_link1"

                @if (Route::current() !== null && Route::current()->getName() == 'product.shoppingCart') style="color:#E0AA5A; font-family: MontserratBold;" @endif>Корзина

                <div id="basket_qty1">
                    @if(isset(Session::get('cart')->totalQty) && Session::get('cart')->totalQty > 0)
                        <span class="qty_product"> {{  Session::get('cart')->totalQty ??  Session::has('cart') }}</span>
                    @endif
                </div>
            </a>
        </div>

        @else

        {{--На случай если пользователь авторизован--}}
        <div class="wrap_authorize">
            <a href="{{ route('personal_area') }}" class="login"
               {{--<a href="{{ route('userinfo') }}" class="login"--}}
               @if(isset($user_page)) style="color:#E0AA5A; font-family: MontserratBold;" @endif>{{ Auth::user()->name }}</a>
            <a href="{{ route('product.shoppingCart') }}" class="basket_link" id="basket_link2"

               @if (Route::current() !== null && Route::current()->getName() == 'product.shoppingCart') style="color:#E0AA5A; font-family: MontserratBold;" @endif>Корзина
                <div id="basket_qty2">
                    @if(isset(Session::get('cart')->totalQty) && Session::get('cart')->totalQty > 0)
                        <span class="qty_product"> {{  Session::get('cart')->totalQty ??  Session::has('cart') }}</span>
                    @endif
                </div>
            </a>
            {{--            <a class="dropdown-item" href="{{ route('logout') }}" id="logout_icon"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>--}}
        </div>
        @endguest

        <!-- Видно только на планшете и меньше на странице, - Оформления -->
        {{--@if(isset($checkout_page))--}}
        <div class="wrap_mob  @if(isset($title) && ($title == 'Личный кабинет' || $title == 'Данные аккаунта')) office @endif">
            <a href="tel:+74993501585" class="mobile768"></a>
            <a href="#" class="sandwich"></a>
        </div>
        {{--@endif--}}
    </div>

    <div class="header-menu">

        <div class="wrapper">
            @guest
                <div class="wrap_authorize_mobile">
                    {{--<a href="#" class="login">Войти</a>--}}
                    <a href="{{ route('product.shoppingCart') }}" class="basket_link" id="basket_link3"

                       @if(Route::current() !== null && Route::current()->getName() == 'product.shoppingCart') style="color:#E0AA5A; font-family: MontserratBold;" @endif>Корзина
                        <div id="basket_qty3">
                            @if(isset(Session::get('cart')->totalQty) && Session::get('cart')->totalQty > 0)
                                <span class="qty_product"> {{  Session::get('cart')->totalQty ??  Session::has('cart') }}</span>
                            @endif
                        </div>
                    </a>
                </div>

            @else

                <!-- На случай если пользователь авторизован -->
                <div class="wrap_authorize_mobile">
                    <a href="{{ route('userinfo') }}" class="login">Аккаунт</a>
                    <a href="{{ route('product.shoppingCart') }}" class="basket_link" id="basket_link4"
                        @if(Route::current() !== null)
                            @if (Route::current()->getName() == 'product.shoppingCart') style="color:#E0AA5A; font-family: MontserratBold;" @endif>Корзина
                        @endif
                        <div id="basket_qty4">
                            @if(isset(Session::get('cart')->totalQty) && Session::get('cart')->totalQty > 0)
                                <span class="qty_product"> {{  Session::get('cart')->totalQty ??  Session::has('cart') }}</span>
                            @endif
                        </div>
                    </a>
                    {{--            <a class="dropdown-item" href="{{ route('logout') }}" id="logout_icon"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{ __('') }}
                                </a>--}}

                    <form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            @endguest

            <!-- Вторая линия -->
            <ul class="second-line">
                <li id="show_tablet">
                    <div class="wrap_search_phone">
                        <form action="{{ route('search.index') }}" class="block-search_tablet">
                            @csrf
                            <span class="block-search__icon-search"></span>
                            <input class="block-search__input_menu" type="text" placeholder="Поиск" name="search"
                                   required>
                            <span class="block-search__icon-close_menu"></span>
                            <input type="submit" class="search_submit" value="Найти">
                        </form>
                    </div>
                </li>
                @include('layouts.nav')  
                <!-- Ссылки на возврат товара и условия доставки-->
                <li class="delivery_links">
                    <a href="{{ url('delivery') }}" class="">Условия доставки</a>
                    <a href="{{ url('purchase_returns') }}" class="">Возврат товара</a>      
                    <a href="{{ url('service') }}" class="">Заявка на сервисное обслуживание</a>  
                </li>      
                <!-- Ссылки на возврат товара и условия доставки-->        
            </ul>

            

        </div>

        <!-- Контэйнер для левого контента в меню на планшете и мобиле -->
        <div class="container_menu_form">
            <h4>E-mail</h4>
            <a href="mailto:info@koenigsmann.ru" class="email_our">info@koenigsmann.ru</a>

            <h4>Телефон</h4>
            <a href="tel:+74993501585" class="smaller768">Позвонить</a>
            <a href="tel:+74993501585" class="phone_our">+7 499 350 15 85</a>

            <form action="#" class="form_tablet">
                <h3>Оставьте заявку</h3>

                <div class="input-field user_name">
                    <input type="text" id="user_name">
                    <label for="user_name">Имя</label>
                </div>

                <div class="input-field user_email">
                    <input type="text" id="user_email">
                    <label for="user_email">E-mail</label>
                </div>

                <div class="input-field user_description">
                    <input type="text" id="user_description">
                    <label for="user_description">Описание</label>
                </div>

                <input type="submit" class="submit_menu_mobile" value="Оставить заявку">
            </form>
        </div>
    </div>
</header>

 @if(isset($title) && ($title == 'Личный кабинет' || $title == 'Данные аккаунта'))
<!-- Вторая линия для аккаунта и кабинета-->
<div class="wrap_data_user_line_LK">
    <div class="wrapper">
        <div class="user_data_link">
            <h1 class="wrap_data_user_head"> @if(isset($title) && $title == 'Данные аккаунта') {{ 'Данные аккаунта' }} @elseif(isset($title) && $title == 'Личный кабинет') {{ 'Личный кабинет' }} @endif</h1>
            <div class="wrap_data_basket">
                <a href="{{ url('personal_area') }}" class="wrap_private_office 
                @if(isset($title) && $title == 'Личный кабинет') {{ 'active' }} @endif
                ">
                    <span class="private_office_icon"></span>
                    <h2>Личный кабинет</h2>
                </a>
                <a href="{{ url('userinfo') }}" class="wrap_data_account
                @if(isset($title) && $title == 'Данные аккаунта') {{ 'active' }} @endif">
                    {{--@if(isset($title) && $title !== 'Данные аккаунта')--}}
                        <span class="data_account_icon"></span>
                        <h2>Данные аккаунта</h2>
                    {{--@endif--}}
                </a>
                {{-- <div class="wrap_basket">
                    <span class="basket_icon"></span>
                    <a href="{{ route('product.shoppingCart') }}" class="basket_link"
                       @if (Route::current()->getName() == 'product.shoppingCart') style="color:#E0AA5A; font-family: MontserratBold;" @endif>Корзина
                        @if(isset(Session::get('cart')->totalQty) && Session::get('cart')->totalQty > 0)
                            <span class="qty_product"> {{  Session::get('cart')->totalQty ??  Session::has('cart') }}</span>
                        @endif
                    </a>
                </div> --}}
                <a href="{{ route('logout') }}" class="logout_link" style="margin-left: 58px;" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <span class="logout_link__icon"></span>
                    <span class="logout_link__text">
                        {{ __('Выйти') }}
                    </span>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
@endif
<!-- END HEADER -->

{{-- Здесь вывожу Флэш сообщения --}}
@if($errors->any())
<div class="container wrapper" id="flash_message">
    @foreach($errors->all() as $error)
    <div class="alert alert-danger alert-dismissible fade show" style="margin-top: 150px; margin-bottom: -75px;"
         role="alert">
        {{ $error }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: #000;">
            <span aria-hidden="true" style="color: #000;">&times;</span>
        </button>
    </div>
    @endforeach
</div>
@endif

@if(Session::has('success'))
<div class="container wrapper" id="flash_message">
    <div class="alert alert-success alert-dismissible fade show" style="margin-top: 150px; margin-bottom: -75px;"
         role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: #000;">
            <span aria-hidden="true" style="color: #000;">&times;</span>
        </button>
    </div>
</div>
@endif

@yield('content')

@if(!isset($contact) && !isset($page_price_list))
<!-- START Footer -->
<footer class="footer">
    <div class="footer-menu">
        <div class="wrapper"><!-- Первая линия -->

            <ul class="first-line">@include('layouts.nav')</ul>

            <div class="second-line">
                <a href="/" class="logo">
                    <img src="{{ asset('/img/logo.svg') }}" alt="Лого сайта Koenigsmann">
                </a>
                <div class="block-phone"><span class="block-phone__icon-phone"></span>
                    <div class="block-phone__wrap-head-number">
                        <div class="block-phone__head">Контактный номер</div>
                        <a href="tel:+74993501585" class="block-phone__number">+7 499 350 15 85</a></div>
                </div>
                <div class="block-email"><span class="block-email__icon-email"></span>
                    <div class="block-email__wrap-head-email">
                        <div class="block-email__head">E-mail</div>
                        <a href="mailto:info@koenigsmann.ru" class="block-email__number">info@koenigsmann.ru</a></div>
                </div>
                <div class="social">
                    <a href="https://vk.com/koenigsmann" class="vk" target="_blank"></a>
                    {{--<a href="#" class="ok"></a>--}}
                    <a href="https://www.instagram.com/koenigsmann.ru/" class="instagram" target="_blank"></a>
                    <a href="https://www.facebook.com/koenigsmann.ru/" class="facebook" target="_blank"></a>
                </div>
            </div>
            <div class="third-line">
                <a href="{{ url('exchange_and_return') }}" class="">Обмен и возврат</a>
                <a href="{{ url('delivery') }}" class="">Условия доставки</a>
                <a href="{{ url('service') }}" class="">Сервисное обслуживание</a>
                
            </div>
            <div class="fourth-line">© 2020 KOENIGSMANN — официальный интернет-магазин производителя спортивных товаров на территории РФ. Все права защищены. <a href="{{ url('agreement') }}">Пользовательское соглашение</a> и <a href="{{ url('policy') }}">Политика конфиденциальности</a></div>
        </div>
    </div>
</footer>
<!-- END Footer -->
@endif
{{-- Подвал для планшета и уже --}}
<div class="header-menu footer_for_tablet">

    <div class="wrapper">

        {{--На случай если пользователь не аваторизован--}}
        {{--        @guest

        <div class="wrap_authorize_mobile">
            <a href="#" class="login" id="login_footer">Войти</a>
            <a href="{{ route('product.shoppingCart') }}" class="basket_link"
               @if (Route::current()->getName() == 'product.shoppingCart') style="color:#E0AA5A; font-family: MontserratBold;" @endif>Корзина

                @if(isset(Session::get('cart')->totalQty) && Session::get('cart')->totalQty > 0)
                    <span class="qty_product"> {{  Session::get('cart')->totalQty ??  Session::has('cart') }}</span>
                @endif
            </a>
        </div>

        @else

        --}}{{--На случай если пользователь авторизован--}}{{--
            <div class="wrap_authorize_mobile">
                <a href="{{ route('userinfo') }}" class="login">Аккаунт</a>
                <a href="{{ route('product.shoppingCart') }}" class="basket_link"
                   @if (Route::current()->getName() == 'product.shoppingCart') style="color:#E0AA5A; font-family: MontserratBold;" @endif>Корзина
                    @if(isset(Session::get('cart')->totalQty) && Session::get('cart')->totalQty > 0)
                        <span class="qty_product"> {{  Session::get('cart')->totalQty ??  Session::has('cart') }}</span>
                    @endif
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        @endguest--}}

        <!-- Вторая линия -->
        {{--            <ul class="second-line" id="menu_tablet_footer">
                <li id="show_tablet_footer" style="display: flex;">
                    <div class="wrap_search_phone">
                        <form action="{{ route('search.index') }}" class="block-search_tablet">
                            @csrf
                            <span class="block-search__icon-search"></span>
                            <input class="block-search__input_menu" id="block-search__input_menu" type="text"
                                   placeholder="Поиск" name="search"
                                   required>
                            <span class="block-search__icon-close_menu"></span>
                            <input type="submit" class="search_submit" value="Найти">
                        </form>
                    </div>
                </li>
                <li class="@if(isset($title) && $title == 'О брэнде') {{ 'active' }} @endif"><a
                            href="{{ url('company') }}">О бренде</a></li>
                <li class="@if(isset($title) && $title == 'Каталог') {{ 'active' }} @endif"><a
                            href="{{ url('catalog') }}">Каталог продукции</a></li>
                <li class="@if(isset($title) && $title == 'Акции') {{ 'active' }} @endif"><a
                            href="{{ url('personal_area-stock') }}">Акции</a></li>
                                    <li class="@if(isset($title) && $title == 'Покупателям') {{ 'active' }} @endif"><a
                                                href="{{ url('to-buyers') }}">Покупателям</a></li>
                <li class="@if(isset($title) && $title == 'Партнерство') {{ 'active' }} @endif"><a
                            href="{{ url('partner') }}">Стать партнёром</a></li>
                <li class="@if(isset($title) && $title == 'Где купить?') {{ 'active' }} @endif"><a
                            href="{{ url('where_buy') }}">Где купить</a></li>
                <li class="@if(isset($title) && $title == 'Контакты') {{ 'active' }} @endif"><a
                            href="{{ url('contacts') }}">Контакты</a></li>
            </ul>--}}
    </div>


    @if(!isset($contact))
    <!-- Контэйнер для левого контента в меню на планшете и мобиле -->
    <div class="container_menu_form" id="container_menu_form">
        <div class="container_menu_form_left">
            <h4 class="container_menu_form_left__h4">E-mail</h4>
            <a href="mailto:info@koenigsmann.ru" class="email_our">info@koenigsmann.ru</a>

            <h4 class="container_menu_form_left__h4">Телефон</h4>
            <a href="tel:+74993501585" class="phone_our">+7 499 350 15 85</a>

            <div class="social">

                <h4 class="social_h4">Социальные сети</h4>

                <a href="https://vk.com/koenigsmann" class="vk" target="_blank"></a>
                {{--<a href="#" class="ok"></a>--}}
                <a href="https://www.instagram.com/koenigsmann.ru/" class="instagram" target="_blank"></a>
                <a href="https://www.facebook.com/koenigsmann.ru/" class="facebook" target="_blank"></a>
            </div>
        </div>

        <form action="#" class="form_tablet">
            <h3 class="form_tablet__h3">Оставьте заявку</h3>

            <div class="input-field user_name">
                <input type="text" id="user_name_footer">
                <label for="user_name_footer">Имя</label>
            </div>

            <div class="input-field user_email">
                <input type="text" id="user_email_footer">
                <label for="user_email_footer">E-mail</label>
            </div>

            <div class="input-field user_description">
                <input type="text" id="user_description_footer">
                <label for="user_description_footer">Описание</label>
            </div>

            <input type="submit" class="submit_menu_mobile" value="Оставить заявку">
        </form>
        <!-- Авторское право -->
        <div class="copyright">© 2020 KOENIGSMANN — официальный интернет-магазин производителя спортивных товаров на территории РФ. Все права защищены. <a href="{{ url('agreement') }}">Пользовательское соглашение</a> и <a href="{{ url('policy') }}">Политика конфиденциальности</a></div>
        <!-- Авторское право -->
    </div>

    @endif
</div>


{{-- Подсказка --}}
<script>
  window.addEventListener('load', () => {
    let hint_block = document.getElementById('hint')
    if (hint_block) {
      hint_block.addEventListener('mouseover', show_hint)
    }

    function show_hint (e) {
      let hint = document.querySelector('.hint')
      hint.style.display = 'inline-block'
    }

    if (hint_block) {
      hint_block.addEventListener('mouseout', hide_hint)
    }

    function hide_hint (e) {
      let hint = document.querySelector('.hint')
      hint.style.display = 'none'
    }
  })
</script>

{{-- Закрываем в меню поиск на планшете и меньше --}}
<script>
  window.addEventListener('load', () => {

    let block_search__input_menu = document.querySelector('.block-search__input_menu')
   // let block_search__input_menu_footer = document.querySelector('#block-search__input_menu')
    block_search__input_menu.addEventListener('focus', width_inp)
   // block_search__input_menu_footer.addEventListener('focus', width_inp)

    function width_inp () {
      let scr_width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth
      if (scr_width > 767) {
        let block_search_tablet = document.querySelectorAll('.block-search_tablet')
        for (let i = 0; i < block_search_tablet.length; i++) {
          block_search_tablet[i].style.maxWidth = '226px'
          block_search_tablet[i].style.minWidth = '226px'
          block_search_tablet[i].style.borderBottom = '2px solid rgb(144, 0, 32)'
        }
      }
    }

    let close_search = document.querySelectorAll('.block-search__icon-close_menu')
    for (let i = 0; i < close_search.length; i++) {
        close_search[i].addEventListener('click', close_inp)
    }

    function close_inp () {
      let scr_width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth
      if (scr_width > 767) {
        let block_search_tablet = document.querySelectorAll('.block-search_tablet')
        for (let i = 0; i < block_search_tablet.length; i++) {
          block_search_tablet[i].style.maxWidth = ''
          block_search_tablet[i].style.minWidth = ''
          block_search_tablet[i].style.borderBottom = ''
        }
      }
    }

  })
</script>

{{-- Закрываем флэшку --}}
<script>
  let close_btn = document.querySelector('.close')

  if (close_btn) {
    close_btn.addEventListener('click', () => {
      document.getElementById('flash_message').style.display = 'none'
    })
  }

</script>
<script src="{{ asset('js/app.js?id=9114149da394e9a03723') }}" defer></script>
<script src="{{ asset('js/lib/jquery-3.4.1.min.js') }}" defer></script>
<script src="{{ asset('js/lib/jquery.mb.YTPlayer.min.js') }}" defer></script>
<script src="{{ asset('js/lib/jquery.maskedinput.min.js') }}" defer></script>
{{--<script src="{{ asset('src/js/vendor/tinymce/js/tinymce/tinymce.min.js') }}" defer></script>--}}
<script src="{{ asset('js/main.min.js?id=9114149da394e9a03723') }}" defer></script>

@if(!isset($url))
    <!-- Скрипт для лого -->
    <script>
      window.addEventListener('load', () => {
        let logo = document.getElementById('logo')
        logo.addEventListener('mouseover', show_back_logo)

        function show_back_logo (e) {
          logo.firstElementChild.setAttribute('src', '/img/logo_with_angular_fill.png')
          logo.style.transition = 'none'
          logo.style.top = '-9.5px'
        }

        logo.addEventListener('mouseout', show_back_nlogo)

        function show_back_nlogo (e) {
          logo.firstElementChild.setAttribute('src', '/img/logo.png')
          logo.style.transition = 'none'
          logo.style.top = ''
        }
      })
    </script>
@endif

<script>
  window.addEventListener('load', () => {
    /*** Маска телефона для всплывающей формы ***/
    $('#phone').mask('+7 ( 999 ) 999-99-99')
  })
</script>


{{-- Обновляем в том случае если, изменение окна больше или меньше на 100px --}}
<script>
  // Обновляем в том случае если, изменение окна больше или меньше на 100px
  let scr_width = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth
  window.addEventListener('resize', (e) => {
    let res = scr_width - e.target.innerWidth
    if (res > 200) {
      location.reload()
    }
    if (res < -200) {
      location.reload()
    }
  })
</script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
    m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
    ym(57585364, "init", {
    clickmap:true,
    trackLinks:true,
    accurateTrackBounce:true,
    webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/57585364(43 B)
https://mc.yandex.ru/watch/57585364
" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158707469-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-158707469-1');
</script>
</body>
</html>