@extends('layouts.layout', ['title' => $title, 'meta_tag_description' => $meta_tag_description, ])


@section('content')

    <!-- START BreadCrumbs -->
    <ul class="breadcrumbs_CP wrapper">
        <li><a href="/">Главная</a></li>
        <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
        <li><a href="{{ url('category') }}">Каталог</a></li>
        <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>

        @if($product->category === 'dlya-doma')
            <li><a href="/category/begovye-dorozhki/{{ $product->category }}/">{{ __('Беговая дорожка для дома') }}</a></li>
        @elseif($product->category === 'limited-edition')
            <li><a href="/category/begovye-dorozhki/{{ $product->category }}/">{{ __('Лимитированная коллекция') }}</a></li>
        @elseif($product->category === 'commerce')
            <li><a href="/category/begovye-dorozhki/{{ $product->category }}/">{{ __('Коммерческий продукт') }}</a></li>
        @elseif($product->category === 'aksessuary')
            <li><a href="/category/begovye-dorozhki/{{ $product->category }}/">{{ __('Категория акссесуары') }}</a></li>
        @endif

    </ul>
    <!-- END BreadCrumbs -->

    <!-- START Карточка товара -->
    <section class="wrapper cont">
        <!-- Лев Leo -->
        <div class="bg"></div>

        <h1 class="main_header">{{ $h1 }}</h1>

        <div class="wrap_img_description">
            <!-- Вертикальный слайдер скролл -->
            <div class="wrap_scroll_img">
                <span class="angle_up"></span>
                <div class="wrap_for_angle">
                    <ul class="list_img">

                    </ul>
                </div>
                <span class="angle_down"></span>
            </div>

            <!-- Большое фото -->
            <div class="big_img">
                <img src="{{ asset('img/3preload.gif') }}"
                     data-imglist="{{ $product->img ? $product->img : asset('img/5preload.gif') }}"
                     loading="lazy"
                     alt="Изображение товара">
            </div>

            <div class="block_price_description">

                <div class="price">
                    <span class="full_price">{{ number_format($product->price, 0, '', ' ') }}&nbsp;&#8381;</span>
                    {{--                    <span class="discount_price">422&nbsp;444&nbsp;&#8381;</span>
                                        <span class="discount_amount">Скидка&nbsp;10&nbsp;000&nbsp;руб.</span>--}}
                </div>

                <div class="wrap_btn_buy">
                	@if($product->availability === "есть")
                    <a href="{{ route('product.addToCart_view', ['id' => $product->product_id]) }}"
                       id="buy_btn">Купить</a>
                       @elseif($product->availability === "нет")
                       <a href="{{ route('product.addToCart_view', ['id' => $product->product_id]) }}"
                       id="buy_btn">Предзаказ</a>
                        @endif
                    {{--<a href="#" id="by_installments">Купить в рассрочку</a>--}}
                </div>

                @if(isset($product->availability))
                        <span id="product_availability_information">
                        @if($product->availability === "есть")
                            <span class="availability__icon"></span>
                            <span class="availability__text">
                                В наличии
                            </span>
                        @elseif($product->availability === "нет")
                            <span id="cross"></span>
                            <span id="not_available">Нет в наличии!</span>
                        @endif
                        </span>
                @endif

                <a href="#" id="info_service">
                    <span class="service__icon"></span>
                    <span class="service__text">Сервисный центр</span>
                </a>
                <a href="#" id="info_delivery">
                    <span class="info_delivery__icon"></span>
                    <span class="info_delivery__text">Доставка до <span
                                class="color_white">Вашего города</span>{{-- 400р., до 18.17.20--}}</span>
                </a>
                @if($product->instruction)
                    <a href="https://koenigsmann.ru/{{ $product->instruction }}" download id="link-downLoad">
                        <span class="link-downLoad__icon"></span>
                        <span class="link-downLoad__text">Скачать инструкцию</span>
                    </a>
                @endif
            <!-- Текст описания -->
                <div class="wrap_text_description">

                    <p class="text_description">{{ $product->properties }}</p>
                </div>

            </div>

        </div>

        @if(isset($product->description))
        <div class="text_block_product" id="specification_block">
            <h2 class="head_step">Описание</h2>

            <p class="specification_text">
                {!! $product->description !!}
            </p>
        </div>
        @endif

        <script>
            window.addEventListener('load', () => {
              let arr_video = [];
            })
        </script>

        @if($product->category !== "aksessuary")
            <div class="text_block_product" id="characteristic">
                <h2 class="head_step">Характеристики</h2>

                {{--<h3 class="h3">Динамические свойства</h3>--}}

                <ul class="specifications">
                    @if($product->engine_power)
                        <li><span>Двигатель:</span><span class="line"></span><span>{{ $product->engine_power }}</span>
                        </li>
                    @endif
                    @if($product->sizes_cloth)
                        <li><span>Размеры бегового полотна (ДхШ):</span><span
                                    class="line"></span><span>{{ $product->sizes_cloth }}</span></li>
                    @endif
                    @if($product->maximum_user_weight)
                        <li><span>Максимальный вес пользователя:</span><span
                                    class="line"></span><span>{{ $product->maximum_user_weight }}</span></li>
                    @endif
                    @if($product->type_powered)
                        <li><span>Тип питания:</span><span class="line"></span><span>{{ $product->type_powered }}</span>
                        </li>
                    @endif
                    @if($product->energy_saving)
                        <li><span>Энергосбережение:</span><span
                                    class="line"></span><span>{{ $product->energy_saving }}</span></li>
                    @endif
                    @if($product->top_speed)
                        <li><span>Скорость:</span><span class="line"></span><span>{{ $product->top_speed }}</span></li>
                    @endif
                    @if($product->speed_adjustment_step)
                        <li><span>Шаг регулировки скорости:</span><span
                                    class="line"></span><span>{{ $product->speed_adjustment_step }}</span></li>
                    @endif
                    @if($product->folding_design)
                        <li><span>Система складывания:</span><span
                                    class="line"></span><span>{{ $product->folding_design }}</span></li>
                    @endif
                    @if($product->tilt_adjustment)
                        <li><span>Регулировка наклона:</span><span
                                    class="line"></span><span>{{ $product->tilt_adjustment }}</span></li>
                    @endif
                    @if($product->depreciation)
                        <li><span>Амортизация:</span><span class="line"></span><span>{{ $product->depreciation }}</span>
                        </li>
                    @endif
                    @if($product->display)
                        <li><span>Дисплей:</span><span class="line"></span><span>{{ $product->display }}</span></li>
                    @endif
                    @if($product->workout_time_display)
                        <li><span>Показания дисплея:</span><span
                                    class="line"></span><span>{{ $product->workout_time_display }}</span></li>
                    @endif
                    @if($product->program_specifications)
                        <li><span>Спецификации программ:</span><span
                                    class="line"></span><span>{{ $product->program_specifications }}</span></li>
                    @endif
                    @if($product->integration)
                        <li><span>Интеграция:</span><span class="line"></span><span>{{ $product->integration }}</span>
                        </li>
                    @endif
                    @if($product->multimedia)
                        <li><span>Мультимедиа:</span><span class="line"></span><span>{{ $product->multimedia }}</span>
                        </li>
                    @endif
                    @if($product->heart_rate_measurement)
                        <li><span>Измерение пульса:</span><span
                                    class="line"></span><span>{{ $product->heart_rate_measurement }}</span></li>
                    @endif
                    @if($product->equipment)
                        <li><span>Комплектация:</span><span
                                    class="line"></span><span>{{ $product->equipment }}</span></li>
                    @endif
                    @if($product->additionally1)
                        <li><span>Дополнительно:</span><span
                                    class="line"></span><span>{{ $product->additionally1 }}</span></li>
                    @endif
                    @if($product->running_belt)
                        <li><span>Беговое полотно:</span><span
                                    class="line"></span><span>{{ $product->running_belt }}</span></li>
                    @endif
                    @if($product->deck_thickness)
                        <li><span>Дека:</span><span
                                    class="line"></span><span>{{ $product->deck_thickness }}</span></li>
                    @endif
                    @if($product->frame)
                        <li><span>Рама:</span><span class="line"></span><span>{{ $product->frame }}</span></li>
                    @endif
                    @if($product->front_shaft_diameter)
                        <li><span>Диаметр переднего вала:</span><span
                                    class="line"></span><span>{{ $product->front_shaft_diameter }}</span></li>
                    @endif
                    @if($product->rear_shaft_diameter)
                        <li><span>Диаметр заднего вала:</span><span
                                    class="line"></span><span>{{ $product->rear_shaft_diameter }}</span></li>
                    @endif
                    @if($product->type_of_plastic)
                        <li><span>Тип пластика:</span><span
                                    class="line"></span><span>{{ $product->type_of_plastic }}</span></li>
                    @endif
                    @if($product->noise_level)
                        <li><span>Уровень шума:</span><span
                                    class="line"></span><span>{{ $product->noise_level }}</span></li>
                    @endif
                    @if($product->motor_warranty)
                        <li><span>Гарантия на мотор:</span><span
                                    class="line"></span><span>{{ $product->motor_warranty }}</span></li>
                    @endif
                    @if($product->frame_warranty)
                        <li><span>Гарантия на раму:</span><span
                                    class="line"></span><span>{{ $product->frame_warranty }}</span></li>
                    @endif
                    @if($product->warranty_electronic_components)
                        <li><span>Гарантия на узлы движения и электронные блоки:</span><span
                                    class="line"></span><span>{{ $product->warranty_electronic_components }}</span></li>
                    @endif
                    @if($product->wear_parts_warranty)
                        <li><span>Гарантия на детали износа:</span><span
                                    class="line"></span><span>{{ $product->wear_parts_warranty }}</span></li>
                    @endif
                    @if($product->dimensions)
                        <li><span>Габариты (ДхШхВ):</span><span
                                    class="line"></span><span>{{ $product->dimensions }}</span></li>
                    @endif
                    @if($product->weight_trainer)
                        <li><span>Вес тренажера:</span><span
                                    class="line"></span><span>{{ $product->weight_trainer }}</span></li>
                    @endif
                    @if($product->package_dimensions)
                        <li><span>Габариты в упаковке:</span><span
                                    class="line"></span><span>{{ $product->package_dimensions }}</span></li>
                    @endif
                    @if($product->package_weight)
                        <li><span>Вес в упаковке:</span><span
                                    class="line"></span><span>{{ $product->package_weight }}</span></li>
                    @endif
                    @if($product->folded_dimensions)
                        <li><span>Габариты в сложенном виде:</span><span
                                    class="line"></span><span>{{ $product->folded_dimensions }}</span></li>
                    @endif
                    @if($product->distance_between_racks)
                        <li><span>Расстояние между стойками:</span><span
                                    class="line"></span><span>{{ $product->distance_between_racks }}</span></li>
                    @endif
                </ul>
            </div>
            <div class="show-all">
                <span class="show-all__icon"></span>
                <span class="show-all__text">Смотреть полностью</span>
            </div>
        @endif

        @if($product->category === 'aksessuary')
            <div class="text_block_product" id="characteristic">
                <h2 class="head_step">Характеристики:</h2>

                <ul class="specifications">
                    @if($product->category === 'aksessuary')
                        @if($product->additionally1)
                            {{--<span class="title">Максимальный вес пользователя:  </span>--}}
                            <li><span class="text">{{ $product->additionally1 }}</span></li>

                        @endif
                        @if($product->additionally2)
                            {{--<span class="title">Максимальный вес пользователя:  </span>--}}
                            <li><span class="text">{{ $product->additionally2 }}</span></li>

                        @endif
                        @if($product->additionally3)
                            {{--<span class="title">Вес тренажера: </span>--}}
                            <li><span class="text">{{ $product->additionally3 }}</span></li>
                        @endif
                    @endif
                </ul>
            </div>
        @endif


        @auth
        @if(Auth::user()->id == 1)
            <div class="card-btn">
                <a href="{{ route('edit', ['tag'=>$product->tag]) }}" class="btn btn-outline-warning">Редактировать</a>

                <form action="{{ route('product.destroy', ['id'=>$product->tag]) }}" method="post"
                      onsubmit="if(confirm('Вы уверены?')){return true}else{return false}">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-outline-danger" value="Удалить">
                </form>
            </div>
        @endif
        @endauth

        @if($product->category !== "limited-edition")
        <!-- С этим товаром покупают -->
            <div class="additionally">
                <h2 class="head_step">С этим товаром покупают</h2>
                <ul class="additionally__ul">
                    @foreach($aks as $product)
                        <li>
                            <a href="/product/{{ $product->tag }}">
                                <span class="wrap_img">
                                    <?php
                                    if ($product->img) {
                                       $arr = explode('|||', $product->img);
                                       $arr = explode('/', $arr[0]);
                                    } ?>
                                    <img src="/storage/{{ $arr[1] ? $arr[1] : asset('img/4preload.gif') }}" alt="">
                                </span>
                                <span class="additionally_descr">{{ mb_strlen($product->title) > 21 ?  mb_strimwidth($product->title, 0, 22, "") : $product->title }}</span>
                                <span class="additionally_price">{{ number_format($product->price, 0, '', ' ') }} р</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{--        <div class="additional_materials">
                    <h2 class="head_step">Дополнительные материалы</h2>
                    <a href="#" class="additional_materials__link">
                        <span class="additional_materials__icon"></span>
                        <span class="additional_materials__text">Документ о правилах интернет торговли</span>
                    </a>
                </div>--}}

    </section>
    <!-- END Карточка товара -->

    {{-- Добавление в корзину без перезагрузки --}}
    <script>
      window.addEventListener('load', () => {
        const buy = document.getElementById('buy_btn')
        const basket_qty1 = document.getElementById('basket_qty1')
        const basket_qty2 = document.getElementById('basket_qty2')
        const basket_qty3 = document.getElementById('basket_qty3')
        const basket_qty4 = document.getElementById('basket_qty4')

        buy.addEventListener('click', add_qty_basket)

        function add_qty_basket (e) {
          e.preventDefault()

          let id_product = e.target.getAttribute('href').split('/')
          id_product = id_product[id_product.length - 2]

          fetch("{{ 'http://'.$_SERVER['HTTP_HOST'] . '/' }}add-to-cart/" + id_product,
            {
              method: "GET",
              headers:{"content-type":"application/x-www-form-urlencoded"}
            })
            .then( response => {
              if (response.status !== 200) {
                return Promise.reject();
              }
              return response.text()
            });
          //.then(i => console.log(i))
          //.catch(() => console.log('ошибка'));

          // Добавляю количество в корзину
          const qty_product = document.querySelectorAll('.qty_product')
          const basket_link = document.querySelectorAll('.basket_link')

          if(
            (basket_qty1 && basket_qty1.firstElementChild)
            || (basket_qty2 && basket_qty2.firstElementChild )
            || (basket_qty3 && basket_qty3.firstElementChild)
            || (basket_qty4 && basket_qty4.firstElementChild)
          ) {
            for (let i of qty_product) {
              let qty = i.textContent.trim()
              i.textContent = 1 + +qty
              i.style.transition = 'all .3s'
              i.style.color = '#990018'
              i.style.background = '#ffffff'

              setTimeout(() => {
                i.style.transition = 'all .3s'
                i.style.color = ''
                i.style.background = ''
              }, 1000)
            }
          } else {

            if(basket_qty1 && !basket_qty1.firstElementChild) {
              let span1 = document.createElement('SPAN')
              span1.classList.add('qty_product')
              span1.textContent = 1
              basket_qty1.appendChild(span1)

              let span3 = document.createElement('SPAN')
              span3.classList.add('qty_product')
              span3.textContent = 1
              basket_qty3.appendChild(span3)
            }
            if(basket_qty2 && !basket_qty2.firstElementChild) {
              let span2 = document.createElement('SPAN')
              span2.classList.add('qty_product')
              span2.textContent = 1
              basket_qty2.appendChild(span2)

              let span4 = document.createElement('SPAN')
              span4.classList.add('qty_product')
              span4.textContent = 1
              basket_qty4.appendChild(span4)
            }

            const qty_product = document.querySelectorAll('.qty_product')
            for (let i of qty_product) {
              i.style.transition = 'all .3s'
              i.style.color = '#990018'
              i.style.background = '#ffffff'

              setTimeout(() => {
                i.style.transition = 'all .3s'
                i.style.color = ''
                i.style.background = ''
              }, 1000)
            }
          }
        }
      })
    </script>

    <!-- Подтверждение удаления Товара -->
    <script>
      let inp_btn_del = document.querySelector('#form_del')
      if (inp_btn_del) {
        inp_btn_del.addEventListener('submit', (e) => {
          //e.preventDefault()
          if (confirm('Точно удалить продукт?')) {
            return true
          } else {
            return false
          }
        })
      }
    </script>

    {{-- Скрываю сэндвич на ширинах меньше 1024 --}}
    <script>
        window.addEventListener('load', () => {
          let wi = new ScreenWidth();
          if(wi.getScreenWidth() < 1024) {
            let sandwich = document.querySelector('.sandwich')
            if (sandwich) sandwich.style.display = 'none'
            let for_sm = document.getElementById('for_sm')

	        if (for_sm) {

	            for_sm.style.display = 'inline-block'
	            if (wi.getScreenWidth() < 768)  {
	              for_sm.style.top = ''
	              for_sm.style.marginRight = '-17px'
	            } else {
	              for_sm.style.top = '-18px'
	            }
	            //for_sm.style.marginRight = '0'

	        }

          } else {
            let sandwich = document.querySelector('.sandwich')
            if (sandwich) sandwich.style.display = 'inline-block'
            let for_sm = document.getElementById('for_sm')

	        if (for_sm) {
	        	for_sm.style.display = ''
	            for_sm.style.top = ''
	            for_sm.style.marginRight = '0'
	        }
            
          }
        })
    </script>

    <!-- Переношу текстовый блок -->
    <script>
      let wi = new ScreenWidth();
      /*      if (wi < 1280) {
       let wrap_text_description = document.querySelectorAll('.wrap_text_description')

       if (wrap_text_description) {
       let wrap_text_description_clone = wrap_text_description[1].cloneNode(true)
       wrap_text_description[1].remove()
       let container = document.querySelector('.cont')
       let elem_before = document.querySelector('.head_step')
       container.insertBefore(wrap_text_description_clone, elem_before)
       }
       }

       if (wi < 1024) {
       let wrap_text_description = document.querySelectorAll('.wrap_text_description')

       if (wrap_text_description) {
       let wrap_text_description_clone = wrap_text_description[0].cloneNode(true)
       wrap_text_description[0].remove()
       let container = document.querySelector('.cont')
       let elem_before = document.querySelector('.head_step')
       container.insertBefore(wrap_text_description_clone, elem_before)
       }
       }*/

    </script>

    <!-- Подставляем фото в блок с большим фото -->
    <script>
      window.addEventListener('load', () => {

          /* Скрипт выборка изображения из списка */
        let image_str = document.querySelector('.big_img img')
        image_str = image_str.dataset.imglist

        if (image_str.indexOf('|||') >= 0) {
          let arr = image_str.split('|||')
          for_view(arr)

          let big_img = document.querySelector('.big_img img')
          let name_img = big_img.getAttribute('src').split('|||')
          name_img = arr[0].split('/')
          big_img.setAttribute('src', '/storage/' + name_img[name_img.length - 1])
        } else {
			let arr = image_str.split('|||')
			let big_img = document.querySelector('.big_img img')
			let name_img = big_img.getAttribute('src').split('|||')
			name_img = arr[0].split('/')
			big_img.setAttribute('src', '/storage/' + name_img[name_img.length - 1])
        }

        if (image_str.indexOf('%7C%7C%7C') >= 0) {
          let arr = image_str.split('%7C%7C%7C')
          for_view(arr)

          let big_img = document.querySelector('.big_img img')
          let name_img = big_img.getAttribute('src').split('%7C%7C%7C')
          name_img = arr[0].split('/')
          big_img.setAttribute('src', '/storage/' + name_img[name_img.length - 1])
        } else {
			let arr = image_str.split('%7C%7C%7C')
			let big_img = document.querySelector('.big_img img')
			let name_img = big_img.getAttribute('src').split('%7C%7C%7C')
			name_img = arr[0].split('/')
			big_img.setAttribute('src', '/storage/' + name_img[name_img.length - 1])
        }

          /* Небольшие фото для просмотра li */
        function for_view (arr) {
          let list_img = document.querySelector('.list_img')

          //console.log(arr);

          if (arr.length !== 2) {
            for (let i = 0; i < arr.length; i++) {
              let name_img = arr[i].split('/')
              let str = '/storage/' + name_img[name_img.length - 1]
              let li = document.createElement('li')

              if (i > 0) {
                li.setAttribute('class', 'dark')
              }
              let img_product = document.createElement('img')
              img_product.setAttribute('src', str)
              img_product.setAttribute('alt', 'Фото товара')
              li.appendChild(img_product)
              list_img.appendChild(li)
            }
          } else if(arr.length === 2) {
            let name_img = arr[0].split('/')
            let str = '/storage/' + name_img[name_img.length - 1]
            let li = document.createElement('li')
            let img_product = document.createElement('img')
            img_product.setAttribute('src', str)
            img_product.setAttribute('alt', 'Фото товара')
            li.appendChild(img_product)
            list_img.appendChild(li)
          }

          picture_highlighting()
        }

        let list_img = document.querySelectorAll('.list_img li img')
        for (let i = 0; i < list_img.length; i++) {
          list_img[i].addEventListener('mouseover', substitute_image)
        }
        function substitute_image (e) {
          let big_img = document.querySelector('.big_img img')
          big_img.setAttribute('src', e.target.getAttribute('src'))
        }

        //подсветка картинок
        function picture_highlighting () {
          let li_img = document.querySelectorAll('.list_img li')
          for (let i = 0; i < li_img.length; i++) {
            li_img[i].addEventListener('mouseover', dark_list_img)
          }
          function dark_list_img (e) {

            let wi = window.innerWidth
              || document.documentElement.clientWidth
              || document.body.clientWidth

            if (wi > 767) {
              let li_img = document.querySelectorAll('.list_img li')
              for (let i = 0; i < li_img.length; i++) {
                if (li_img[i].querySelector('img') !== e.target) {
                  if (!li_img[i].classList.contains('dark')) {
                    li_img[i].classList.add('dark')
                  }
                } else {
                  if (li_img[i].classList.contains('dark')) {
                    li_img[i].classList.remove('dark')
                  }
                }
              }
            }
          }
        }

        /*** Скрипт карусели "фото товара" ***/
        let chevron_left = document.querySelector('.angle_up')
        let chevron_right = document.querySelector('.angle_down')
        let container_img = document.querySelector('.list_img')

        chevron_right ? chevron_right.addEventListener('click', move_right) : ''
        chevron_right ? chevron_right.addEventListener('touch', move_right) : ''
        chevron_left ? chevron_left.addEventListener('click', move_left) : ''
        chevron_left ? chevron_left.addEventListener('touch', move_left) : ''

        function move_right (e) {
          e.preventDefault()

          let wi = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth
          if (wi > 1280) {
            container_img.scrollTop += 492
            setTimeout(function () {
              show_angle767()
            }, 300, true)
          } else if (wi > 1023) {
            container_img.scrollTop += 320
            setTimeout(function () {
              show_angle767()
            }, 300, true)
          } else if (wi > 767) {
            container_img.scrollTop += 230
            setTimeout(function () {
              show_angle767()
            }, 300, true)
          } else if (wi <= 768) {
            container_img.scrollLeft -= 308

            setTimeout(function () {
              show_angle()
            }, 1000, true)
          }
        }

        function move_left (e) {
          e.preventDefault()

          let wi = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth
          if (wi > 1280) {
            container_img.scrollTop -= 495

            setTimeout(function () {
              show_angle767()
            }, 300, true)
          } else if (wi > 1023) {
            container_img.scrollTop -= 320
            setTimeout(function () {
              show_angle767()
            }, 300, true)
          } else if (wi > 767) {
            container_img.scrollTop -= 308
            setTimeout(function () {
              show_angle767()
            }, 300, true)
          } else if (wi <= 768) {
            container_img.scrollLeft += 308

            setTimeout(function () {
              show_angle()
            }, 300, true)

          }
        }

        function show_angle () {
          let res = container_img.children.length * ( container_img.children[0].offsetWidth + 20 )
          if (container_img.scrollLeft > 0) {
            chevron_right.style.display = 'inline-block'
          } else {
            chevron_right.style.display = 'none'
          }

          if (container_img.scrollLeft < (res - 350)) {
            chevron_left.style.display = 'inline-block'
          } else {
            chevron_left.style.display = 'none'
          }
        }

          /* 767 и больше */
        function show_angle767 () {
          let wi = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth

          let list_img = document.querySelector('.list_img')
          let angle_down = document.querySelector('.angle_down')
          let angle_up = document.querySelector('.angle_up')

          if (wi > 767) {
            if (list_img.children.length > 5) {
              angle_down.style.display = 'inline-block'
            } else if (list_img.scrollHeight >= list_img.scrollHeight) {
              angle_up.style.display = 'none'
              angle_down.style.display = 'none'
              list_img.style.height = '425px'
            }

            if (list_img.scrollTop > 0) {
              angle_up.style.display = 'inline-block'
              list_img.style.height = '377px'
            }

            if (list_img.scrollTop > 0 && (list_img.scrollTop + list_img.offsetHeight) >= list_img.scrollHeight) {
              angle_up.style.display = 'inline-block'
              angle_down.style.display = 'none'
              list_img.style.height = '377px'
            }

            if (list_img.scrollTop === 0 && list_img.children.length < 7) {
              angle_up.style.display = 'none'
              list_img.style.height = '425px'
            } else if (list_img.scrollTop === 0) {
              angle_up.style.display = 'none'
              list_img.style.height = '377px'

              let w = new ScreenWidth();
              if(w.getScreenWidth() < 1024) {
                list_img.style.height = '300px'
                list_img.style.maxHeight = '305px'
              }
            }
          }

          if (wi > 1280) {
            if (list_img.children.length > 5) {
              angle_down.style.display = 'inline-block'
            } else if (list_img.scrollHeight >= list_img.scrollHeight) {
              angle_up.style.display = 'none'
              angle_down.style.display = 'none'
              list_img.style.height = '525px'
            }

            if (list_img.scrollTop > 0) {
              angle_up.style.display = 'inline-block'
              list_img.style.height = '495px'
            }

            if (list_img.scrollTop > 0 && (list_img.scrollTop + list_img.offsetHeight) >= list_img.scrollHeight) {
              angle_up.style.display = 'inline-block'
              angle_down.style.display = 'none'
              list_img.style.height = '495px'
            }

            if (list_img.scrollTop === 0 && list_img.children.length < 7) {
              angle_up.style.display = 'none'
              list_img.style.height = '525px'
            } else if (list_img.scrollTop === 0) {
              angle_up.style.display = 'none'
              list_img.style.height = '495px'
            }
          }

        }

        show_angle767()

      })
    </script>

    <!-- Показать подробности весь список -->
    <script>
      let show_all = document.querySelector('.show-all')
      if (show_all) {
      	show_all.addEventListener('click', show_list_description)	
      }      

      function show_list_description () {
        let specifications = document.querySelectorAll('.specifications li')

        let wi = window.innerWidth
          || document.documentElement.clientWidth
          || document.body.clientWidth

        if (wi < 768) {
          for (let i = 0; i < specifications.length; i++) {
            specifications[i].style.display = 'block'
          }
        } else {
          for (let i = 0; i < specifications.length; i++) {
            specifications[i].style.display = 'flex'
          }
        }

        show_all.style.opacity = '0'
      }
    </script>

    <!-- Заказать обратный звонок -->
    <script>
      let block_phone__head = document.querySelector('.block-phone__head')
      let close_popup = document.querySelector('.close_popup')
      let label_span = document.querySelector('.label_span')
      let back_g = document.querySelector('.back_g')
      let phone = document.getElementById('phone')

      if (block_phone__head) {
        block_phone__head.addEventListener('click', show_popup_call_me)
      }
      if (close_popup) {
        close_popup.addEventListener('click', hide_popup_call_me)
      }
      if (back_g) {
        back_g.addEventListener('click', hide_popup_call_me)
      }

      function show_popup_call_me () {
        back_g.style.display = 'block'
      }

      function hide_popup_call_me (e) {
        e.preventDefault()

        let close_popup = document.querySelector('.close_popup')
        let back_g = document.querySelector('.back_g')
        if (e.target.className === 'back_g'
          || e.target.className === 'close_popup'
          || e.target.className === 'close_popup__text'
          || e.target.className === 'close_popup__icon'
        ) {
          back_g.style.display = 'none'
        }
      }

      // по фокусу убираю span
      phone.addEventListener('focus', () => label_span.style.display = 'none')
      phone.addEventListener('mouseenter', () => label_span.style.display = 'none')
      phone.addEventListener('mouseover', () => label_span.style.display = 'none')
    </script>

    <script>
      //показать-скрыть стрелки прокрутки
      /*      window.addEventListener('load', show_hide_arrows)

       let list_img = document.querySelector('.list_img')
       list_img.addEventListener('scroll', show_hide_arrows)
       function show_hide_arrows () {
       let container_img = 			document.querySelector('.list_img')
       let array_img = 		document.querySelectorAll('.list_img li')
       let angle_up = 			document.querySelector('.angle_up')
       let angle_down = 		document.querySelector('.angle_down')
       let inner_img =			document.querySelectorAll('.list_img li img')[0]
       let first_img = 		array_img[0]
       let last_img = 			array_img[array_img.length - 1]

       let container_img_top = 		Math.round(container_img.getBoundingClientRect().top)
       let container_img_bottom = 	Math.round(container_img.getBoundingClientRect().bottom)
       let first_img_top = 	Math.round(first_img.getBoundingClientRect().top)
       let last_img_bottom = 	Math.round(last_img.getBoundingClientRect().bottom)

       var margin_bottom_li = parseInt(window.getComputedStyle(first_img).marginBottom, 10)
       var height_for_container_img_6 = 6*inner_img.height+5*margin_bottom_li //для 6 картинок
       var height_for_container_img_5 = 5*inner_img.height+4*margin_bottom_li //для 5 картинок

       if (container_img_top == first_img_top) {//начало
       angle_up.style.display = 'none';
       angle_down.style.display = 'inline-block';
       container_img.style.height = height_for_container_img_6 +'px';
       container_img.style.margin = '0';
       }else if (container_img_bottom == last_img_bottom){ //конец
       angle_up.style.display = 'inline-block';
       angle_down.style.display = 'none';
       container_img.style.height = height_for_container_img_6 +'px';
       container_img.style.margin = '0';
       }else{ //в промежутке
       angle_up.style.display = 'inline-block';
       angle_down.style.display = 'inline-block';
       container_img.style.height = height_for_container_img_5 +'px';
       container_img.style.margin = '14px 0';
       }
       }*/


    </script>
    <script>
      // Обновляем в том случае если, изменение окна больше или меньше на 200px
      let scr_widths = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth
      window.addEventListener('resize', (e) => {
        let res = scr_widths - e.target.innerWidth
        if (res > 250) {
          location.reload()
        }
        if (res < -250) {
          location.reload()
        }
      })
    </script>
@endsection