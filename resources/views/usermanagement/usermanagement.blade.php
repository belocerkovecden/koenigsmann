@extends('layouts.layout', ['title' => 'Создание пользователя'])


@section('content')



        <form action="{{ route('user_manage') }}" method="post" class="container" style="margin-top: 150px;">
            @csrf

            <h1 class="h1">Создать пользователя</h1>

            <div class="form-group">
                <input name="name" type="text" class="form-control" required placeholder="Логин"
                       value="{{ old('name') ?? $user->name ?? '' }}">
            </div>

            <div class="form-group">
                <input name="email" type="text" class="form-control" required placeholder="E-mail"
                       value="{{ old('email') ?? $user->email ?? '' }}">
            </div>

            <div class="form-group">
                <input name="password" type="text" class="form-control" required placeholder="Пароль"
                       value="{{ old('password') ?? $user->password ?? '' }}">
            </div>

            <input type="submit" value="Создать пользователя" class="btn btn-outline-success">
        </form>

@endsection