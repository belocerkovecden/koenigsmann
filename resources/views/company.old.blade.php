@extends('layouts.layout', ['title' => 'О компании Koenigsmann'])


@section('content')

    {{--<!-- START О компании -->
    <div class="first_bg_city"></div>

    <div class="" id="incomprehensible_site_width">
        <div class="wrapper text_block">
            <h1 class="text_block__header">О компании</h1>
            <p class="text_block__p">Для создания спортивного оборудования компания привлекает ведущих мировых дизайнеров и
                экспертов в области медицины и спорта для совместного создания оригинальных спортивных тренажеров и
                гаджетов</p>
        </div>
    </div>

    <div class="wrap_tablo_runman">
        <div class="wrapper">
            <div class="dop_wrap">
                <div class="wrap_tablo">
                    <div class="wrap_img_tablo"><img src="{{ asset('img/tablo.png') }}" alt="tablo"></div>
                    <p class="wrap_tablo_p">Беговые дорожки км с уникальной системой софтфит сочетают в себе самые лучшие
                        комплектующие и
                        материалы,
                        передовые технологии, совершенное качество и великолепный дизайн.</p>
                </div>

                <div class="wrap_runman">
                    <div class="wrap_img"><img src="{{ asset('img/runman.jpg') }}" alt="runman"></div>
                    <p> Более 1000 различных тестов и инсталляций на стадии внедрения каждой дорожки из модельного ряда.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <div class="wrap_status_building">
            <div class="wrap_img_build">
                <img src="{{ asset('img/build.png') }}" alt="building">
            </div>

            <p class="build_text">Компания завоевала статус и признание постоянного поставщика для европейских сетевых
                клубов и гостиничных сетей,
                государственных служб Нидерландов, рекреационных центров, корпоративных офисов и университетов. </p></div>
    </div>

    <div class="wrap_run_girl">

    </div>

    <div class="dop_wrap_last_block">
        <div class="wrap_text wrapper" id="another_incomprehensible_site_width">
            <h2 class="wrap_text__h2">Инновации</h2>

            <h3 class="wrap_text__h3">Дизайн</h3>
            <p class="wrap_text__p">Органичность, эстетика, вместе с этим некоторая строгость и, возможно, даже брутальность
                форм всегда выделяли
                эти тренажеры среди оборудования прочих производителей. Команда дизайнеров во главе с Этиком Вальтером этой
                компании завоевала репутацию одной из лучших в Европе, которая подкрепляется множеством различных престижных
                наград. </p>

            <h3 class="wrap_text__h3">Движение</h3>
            <p class="wrap_text__p">Сердцем дорожки является ее приводная система. Эти дорожка оснащена сверхнадежной в
                эксплуатации, проверенной
                временем связкой мотора и контроллера (ПРИДУМАТ НАДО!), который производится компанией. Основная особенность
                ПРИДУМАТЬ НАДО- это адаптивный отклик на вес каждого пользователя и скорость тренировки. Это значит, что для
                всех пользователей с разным весом полотно движется как по траве ,равномерно, мягко и соответствует заданному
                значению скорости.</p>
            <p class="wrap_text__p">Отдельно следует отметить амортизационную платформу SoftFIT которая стала одной из
                лучших в мире среди
                аналогичных систем с точки зрения надежности, качества и демпфирующих свойств. Ее характерной особенностью
                является то, что она поддерживает одинаковый уровень амортизации на всем скоростном диапазоне. Беговые
                дорожки других фирм часто страдают повышенной «прыгучестью» при использовании на высоких скоростях, а это не
                комфортно и небезопасно. </p>

            <h3 class="wrap_text__h3">Программные решения</h3>
            <p class="wrap_text__p">КМ т занимает одно из лидирующих мест касательно внедрения современных софтверных
                решений, которые относятся
                к различным сторонам эксплуатации тренажера. Это и различные специализированные тренировочные профили
                (PEB-тест Службы Федеральной гвардии Нидерландов, "Gerkin-протокол" Пожарной Службы Нидерландов, тренировки
                по стандартам армии нидерландов и прочие), и выгрузка результатов тренировки в on-line сервис КМ, и
                развлекательная составляющая с применением iPod/iPhone/Nike+iPod, и фитнес ориентированный мультимедийный
                on-line сервис КМ, а также ринтеграция с различными мобильными устройствами на базе iOS и Android и доступ в
                интернет. </p>

        </div>
    </div>
    <!-- END О компании -->--}}

    <!-- START О компании -->


    <div class="company">
        <!-- START BreadCrumbs -->
        <ul class="breadcrumbs wrapper">
            <li>
                <a href="/">Главная</a>
            </li>
            <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
            <li>
                <a href="#">О компании</a>
            </li>
        </ul>
        <!-- END BreadCrumbs -->
        <div class="wrap_content">
            <img src="{{ asset('img/logo.svg') }}" class="logo">
            <p class="top_text">Для создания спортивного оборудования компания привлекает ведущих мировых дизайнеров и экспертов в области медицины и спорта для совместного создания оригинальных спортивных тренажеров и гаджетов</p>
            <img src="{{ asset('img/company_top_image.png') }}" alt="" class="top_image">
            <div class="additionally">
                <div class="additionally__wrap">
                    <div class="additionally__wrap__text">
                        <p>Беговые дорожки км с уникальной системой софтфит сочетают в себе самые лучшие комплектующие и материалы, передовые технологии, совершенное качество и великолепный дизайн.</p>
                    </div>
                    <div class="additionally__wrap__image">
                        <img src="{{ asset('img/additionally_img1.png') }}" alt="">
                    </div>
                </div>
                <div class="additionally__wrap">
                    <div class="additionally__wrap__text">
                        <p>Более 1000 различных тестов и инсталляций на стадии внедрения каждой дорожки из модельного ряда.</p>
                    </div>
                    <div class="additionally__wrap__image">
                        <img src="{{ asset('img/additionally_img2.png') }}" alt="">
                    </div>
                </div>
            </div>
            <hr>
            <div class="map">
                <p class="map__text">Компания завоевала статус и признание постоянного поставщика для европейских сетевых клубов и гостиничных сетей, государственных служб Нидерландов, рекреационных центров, корпоративных офисов и университетов.</p>
                <img class="map__image" src="{{ asset('img/nederlands_map.png') }}">
            </div>
            <div class="dop_wrap_last_block">
                <div class="wrap_text" id="another_incomprehensible_site_width">
                    <h2 class="wrap_text__h2">Инновации</h2>
                    <h3 class="wrap_text__h3">Дизайн</h3>
                    <p class="wrap_text__p">Органичность, эстетика, вместе с этим некоторая строгость и, возможно, даже брутальность форм всегда выделяли эти тренажеры среди оборудования прочих производителей. Команда дизайнеров во главе с Этиком Вальтером этой компании завоевала репутацию одной из лучших в Европе, которая подкрепляется множеством различных престижных наград.</p>
                    <h3 class="wrap_text__h3">Движение</h3>
                    <p class="wrap_text__p">Эта дорожка оснащена сверхнадежной в эксплуатации, проверенной временем связкой мотора и контроллера (Dynamic Engine - Динамик Энджин), который производится компанией. Основная особенность (Adaptive move - Адаптив Мув) - это адаптивный отклик на вес каждого пользователя и скорость тренировки. Это значит, что для всех пользователей с разным весом полотно движется как по траве ,равномерно, мягко и соответствует заданному значению скорости.</p>
                    <div class="note">
                        <div class="note__image">
                            <img src="{{ asset('img/index-dorojka1.png') }}">
                        </div>
                        <p class="note__text">Отдельно следует отметить амортизационную платформу SoftFIT которая стала одной из лучших в мире среди аналогичных систем с точки зрения надежности, качества и демпфирующих свойств. Ее характерной особенностью является то, что она поддерживает одинаковый уровень амортизации на всем скоростном диапазоне. Беговые дорожки других фирм часто страдают повышенной «прыгучестью» при использовании на высоких скоростях, а это не комфортно и небезопасно.</p>
                    </div>
                    <h3 class="wrap_text__h3">Программные решения</h3>
                    <p class="wrap_text__p">КМ т занимает одно из лидирующих мест касательно внедрения современных софтверных решений, которые относятся к различным сторонам эксплуатации тренажера. Это и различные специализированные тренировочные профили (PEB-тест Службы Федеральной гвардии Нидерландов, "Gerkin-протокол" Пожарной Службы Нидерландов, тренировки по стандартам армии нидерландов и прочие), и выгрузка результатов тренировки в on-line сервис КМ, и развлекательная составляющая с применением iPod/iPhone/Nike+iPod, и фитнес ориентированный мультимедийный on-line сервис КМ, а также ринтеграция с различными мобильными устройствами на базе iOS и Android и доступ в интернет.</p>
                </div>
            </div>
        </div>
    </div>


    <!-- END О компании -->

    <script>
      let scr_width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth

      if (scr_width < 1024 && scr_width > 767) {
        let wrap_tablo_runman = document.querySelector('.wrap_tablo_runman')
        if (wrap_tablo_runman) {
          wrap_tablo_runman.firstElementChild.classList.remove('wrapper')
        }

        let wrap_status_building = document.querySelector('.wrap_status_building')
        if (wrap_status_building) {
          wrap_status_building.parentNode.classList.remove('wrapper')
        }
      } else {
        let wrap_tablo_runman = document.querySelector('.wrap_tablo_runman')
        if (wrap_tablo_runman) {
          wrap_tablo_runman.firstElementChild.classList.add('wrapper')
        }
      }

      if (scr_width < 767) {
        let wrap_status_building = document.querySelector('.wrap_status_building')
        if (wrap_status_building) {
          wrap_status_building.parentNode.classList.remove('wrapper')
        }
      }
    </script>
    {{-- Обновляем в том случае если, изменение окна больше или меньше на 100px --}}
    <script>
      // Обновляем в том случае если, изменение окна больше или меньше на 100px
      let scr_width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth
      window.addEventListener('resize', (e) => {
        let res = scr_width - e.target.innerWidth
        if (res > 200) {
          location.reload()
        }
        if (res < -200) {
          location.reload()
        }
      })
    </script>

@endsection