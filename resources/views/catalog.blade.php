@extends('layouts.layout', ['title' => $title])


@section('content')
    <!-- START BreadCrumbs -->
    <!-- <ul class="breadcrumbs wrapper">
        <li><a href="/">Главная</a></li>
        <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
        <li><a href="#">Стать партнером</a></li>
    </ul> -->
    <!-- END BreadCrumbs -->

    <!-- START Каталог-->
    <section class="catalog-content">
        <div class="filter">
            <div class="wrapper">
                <div class="wrap_btn_list">
                    <ul class="property_ul">
                        <li class="property_ul__li">
                            <div class="big_btn_filter @if(isset($category_name) && $category_name == 'dlya-doma') {{ 'active_filter' }} @endif">
                                <div class="property_icon"><img
                                            src="{{ $product->img ?? asset('img/category/forhome-mini.png') }}"
                                            alt="filter"></div>
                                @if(isset($category_name) && $category_name == 'dlya-doma')
                                    <h1 class="property_text_h1"><a href="{{ '/category/' }}" class="property_text">Беговые
                                            дорожки для дома</a></h1>
                                @else
                                    <a href="{{ '/category/begovye-dorozhki/dlya-doma/' }}"
                                       class="property_text">Беговые дорожки для дома</a>
                                @endif
                            </div>
                            <ul class="property_angle active_property">
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form">
                                        <span>Длина и ширина бегового полотна (см)</span>
                                        <span class="angle_down"></span>
                                    </a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Длина</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_from__length">
                                                <label for="filter_from__length">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_before__length">
                                                <label for="filter_before__length">До</label>
                                            </div>

                                            <div class="head_filter mt35">Ширина</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_from__width">
                                                <label for="filter_from__width">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_before__width">
                                                <label for="filter_before__width">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form"><span>Макс. вес (кг)</span><span
                                                class="angle_down"></span></a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Вес</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_from__weight">
                                                <label for="filter_from__weight">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_before__weight">
                                                <label for="filter_before__weight">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form">
                                        <span>Мощность (л.с)</span>
                                        <span class="angle_down"></span>
                                    </a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Л.С.</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_from__ls">
                                                <label for="filter_from__ls">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_before__ls">
                                                <label for="filter_before__ls">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                            </ul>

                        </li>

                        <li class="property_ul__li">
                            <div class="big_btn_filter @if(isset($category_name) && $category_name == 'commerce') {{ 'active_filter_commerce' }} @endif">
                                <div class="property_icon"><img
                                            src="{{ $product->img ?? asset('img/category/comerc-mini.png') }}"
                                            alt="filter"></div>
                                @if(isset($category_name) && $category_name == 'commerce')
                                    <h1 class="property_text_h1"><a href="{{ '/category/' }}" class="property_text">Беговые
                                            дорожки для зала</a></h1>
                                @else
                                    <a href="{{ '/category/begovye-dorozhki/commerce/' }}"
                                       class="property_text">Беговые дорожки для зала</a>
                                @endif
                            </div>

                            <ul class="property_angle">
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form">
                                        <span>Длина и ширина бегового полотна (см)</span>
                                        <span class="angle_down"></span>
                                    </a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Длина</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_comerc_from__length">
                                                <label for="filter_from__length">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_comerc_before__length">
                                                <label for="filter_before__length">До</label>
                                            </div>

                                            <div class="head_filter mt35">Ширина</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_comerc_from__width">
                                                <label for="filter_from__width">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_comerc_before__width">
                                                <label for="filter_before__width">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form">
                                        <span>Макс. вес (кг)</span>
                                        <span class="angle_down"></span>
                                    </a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Вес</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_comerc_from__weight">
                                                <label for="filter_from__weight">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filte_comercr_before__weight">
                                                <label for="filter_before__weight">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form">
                                        <span>Мощность (л.с)</span>
                                        <span class="angle_down"></span>
                                    </a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Л.С.</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_comerc_from__ls">
                                                <label for="filter_from__ls">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filte_comercr_before__ls">
                                                <label for="filter_before__ls">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form"><span>Гарантия (лет)</span><span
                                                class="angle_down"></span></a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Лет</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_comerc_from__years">
                                                <label for="filter_comerc_from__years">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_comerc_before__years">
                                                <label for="filter_comerc_before__years">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                            </ul>
                        </li>

                        <li class="property_ul__li">
                            <div class="big_btn_filter @if(isset($category_name) && $category_name == 'limited-edition') {{ 'active_filter_white' }} @endif">
                                <div class="property_icon"><img
                                            src="{{ $product->img ?? asset('img/category/limit-mini.png') }}"
                                            alt="filter"></div>
                                @if(isset($category_name) && $category_name == 'limited-edition')
                                    <h1 class="property_text_h1"><a href="{{ '/category/' }}"
                                                                    class="property_text">Беговые дорожки Limited
                                            Edition</a></h1>
                                @else
                                    <a href="{{ '/category/begovye-dorozhki/limited-edition/' }}"
                                       class="property_text">Беговые дорожки Limited Edition</a>
                                @endif
                            </div>

                            <ul class="property_angle">
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form">
                                        <span>Длина и ширина бегового полотна (см)</span>
                                        <span class="angle_down"></span>
                                    </a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Длина</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_limit_from__length">
                                                <label for="filter_from__length">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_limit_before__length">
                                                <label for="filter_before__length">До</label>
                                            </div>

                                            <div class="head_filter mt35">Ширина</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_limit_from__width">
                                                <label for="filter_from__width">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_limit_before__width">
                                                <label for="filter_before__width">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form"><span>Макс. вес (кг)</span><span
                                                class="angle_down"></span></a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Вес</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_limit_from__weight">
                                                <label for="filter_from__weight">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_limit_before__weight">
                                                <label for="filter_before__weight">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form">
                                        <span>Мощность (л.с)</span>
                                        <span class="angle_down"></span>
                                    </a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Л.С.</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_limit_from__ls">
                                                <label for="filter_from__ls">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_limit_before__ls">
                                                <label for="filter_before__ls">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                            </ul>
                        </li>

                        <li class="property_ul__li">
                            <div class="big_btn_filter @if(isset($category_name) && $category_name == 'aksessuary') {{ 'active_filter_white' }} @endif">
                                <div class="property_icon"><img
                                            src="{{ $product->img ?? asset('img/category/aks-mini.png') }}"
                                            alt="filter"></div>
                                @if(isset($category_name) && $category_name == 'aksessuary')
                                    <h1 class="property_text_h1"><a href="{{ '/category/' }}" class="property_text">Аксессуары
                                            для беговых дорожек</a></h1>
                                @else
                                    <a href="{{ '/category/begovye-dorozhki/aksessuary/' }}"
                                       class="property_text">Аксессуары для беговых дорожек</a>
                                @endif
                            </div>

                            <ul class="property_angle">
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form">
                                        <span>Длина и ширина бегового полотна (см)</span>
                                        <span class="angle_down"></span>
                                    </a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Длина</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_aks_from__length">
                                                <label for="filter_from__length">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_aks_before__length">
                                                <label for="filter_before__length">До</label>
                                            </div>

                                            <div class="head_filter mt35">Ширина</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_aks_from__width">
                                                <label for="filter_from__width">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_aks_before__width">
                                                <label for="filter_before__width">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form"><span>Макс. вес (кг)</span><span
                                                class="angle_down"></span></a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Вес</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_aks_from__weight">
                                                <label for="filter_from__weight">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_aks_before__weight">
                                                <label for="filter_before__weight">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                                <li class="property_angle__li">
                                    <a href="#" class="link_show_form">
                                        <span>Мощность (л.с)</span>
                                        <span class="angle_down"></span>
                                    </a>
                                    <form action="#" class="form_filter_popup">
                                        <div class="popup_filter">
                                            <div class="head_filter">Л.С.</div>
                                            <div class="input-field filter_from">
                                                <input type="text" id="filter_aks_from__ls">
                                                <label for="filter_from__ls">От</label>
                                            </div>
                                            <div class="input-field filter_before">
                                                <input type="text" id="filter_aks_before__ls">
                                                <label for="filter_before__ls">До</label>
                                            </div>
                                        </div>

                                        <button class="btn_submit" type="submit">Применить</button>
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        @if(isset($_GET['search']))

            @if(count($products) > 0 )

                <div class="wrapper"
                     style="margin-top: 15px; margin-bottom: 15px; color: #bcbcbc; font-size: 12px; text-align: right;">
                    <h2>Результаты поиска по запросу, - "<?=$_GET['search']?>" </h2>
                    <p style="margin-top: 15px; margin-bottom: 15px; color: #bcbcbc; font-size: 12px;">Всего
                        найдено {{ count($products) }}</p>
                </div>

                @foreach($products as $product)

                    <div class="product">
                        <div class="wrapper">
                            <a href="{{ route('product', ['tag' => $product->tag]) }}" class="product_image">
                                <img src="{{ asset('/img/3preload.gif') }}"
                                     loading="lazy"
                                     alt="Изображение товара"
                                     data-img="{{ $product->img ? $product->img : asset('img/4preload.gif') }}">
                            </a>

                            <div class="wrap-name-price-buy">
                                <div class="name-price">
                                    <div class="wrap-name">
                                        <a href="{{ route('product', ['tag' => $product->tag]) }}" class="name">
                                            {{ $product->title }}
                                        </a>
                                        <span class="category">
                                            @if($product->category === 'dlya-doma')
                                                {{ __('Беговая дорожка для дома') }}
                                            @elseif($product->category === 'limited-edition')
                                                {{ __('Лимитированная коллекция') }}
                                            @elseif($product->category === 'commerce')
                                                {{ __('Коммерческий продукт') }}
                                            @elseif($product->category === 'aksessuary')
                                                {{ __('Категория акссесуары') }}
                                            @endif
                                        </span>
                                    </div>
                                    <div class="price">
                                        <div>{{ number_format($product->price, 0, '', ' ') }}</div>
                                    </div>
                                </div>

                                @if($product->availability === "есть")
                                    <a href="{{ route('product.addToCart', ['id' => $product->product_id]) }}"
                                       class="buy">Купить</a>
                                @elseif($product->availability === "нет")
                                    <a href="{{ route('product.addToCart', ['id' => $product->product_id]) }}"
                                       class="buy">Предзаказ</a>
                                @endif
                            </div>
                            <div class="description">
                                <p class="description__text">
                                    {{ $product->description }}
                                </p>
                            </div>
                            {{--<a href="#" class="file"></a>--}}
                            <hr class="hr_checkout">
                            <div class="characteristics">
                                <div class="column col1">
                                    <span class="title">Двигатель: </span>
                                    <span class="text">3,25 л.с. Mitsubishi (постоянный ток)</span>
                                    <br>
                                    <span class="title">Скорость: </span>
                                    <span class="text">1-18 км/ч</span>
                                    <br>
                                    <span class="title">Регулировка угла наклона:  </span>
                                    <span class="text">электрическая</span>
                                    <br>
                                    <span class="title">Скорость: </span>
                                    <span class="text">1-18 км/ч</span>
                                </div>
                                <div class="column col2">
                                    <span class="title">Размер бегового полотна:  </span>
                                    <span class="text">138*46 см</span>
                                    <br>
                                    <span class="title">Консоль:  </span>
                                    <span class="text">голубой LCD-дисплей 18 см</span>
                                    <br>
                                    <span class="title">Макс. вес пользователя: </span>
                                    <span class="text">150 кг</span>
                                    <br>
                                    <span class="title">Гарантия: </span>
                                    <span class="text">4 года (стандартная) / 8 лет (расширенная)</span>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            @else
                <div class="wrapper"
                     style="margin-top: 15px; margin-bottom: 15px; color: #bcbcbc; font-size: 12px; text-align: right;">
                    <h2 style="margin-top: 15px; margin-bottom: 15px; color: #bcbcbc; font-size: 12px; text-align: right;">
                        По запросу "<?=htmlspecialchars($_GET['search'])?>" ничего не найдено! </h2>
                    <a href="{{ url('catalog') }}"
                       style="margin-top: 15px; margin-bottom: 15px; color: #bcbcbc; font-size: 12px; text-align: right;">Отобразить
                        все продукты!</a>
                </div>
            @endif

        @else

            @foreach($products as $product)
                <div class="product">
                    <div class="wrapper">
                        <a href="{{ route('product', ['tag' => $product->tag]) }}" class="product_image">
                            <img src="{{ asset('img/3preload.gif') }}"
                                 loading="lazy"
                                 alt="Изображение товара"
                                 style="background: #1c1c1c;"
                                 data-img="{{ $product->img ? $product->img : asset('img/4preload.gif') }}">
                        </a>

                        <div class="wrap-name-price-buy">
                            <div class="name-price">
                                <div class="wrap-name">
                                    <a href="{{ route('product', ['tag' => $product->tag]) }}" class="name">
                                        {{ $product->title }}
                                    </a>
                                    <span class="category">
                                        @if($product->category === 'dlya-doma')
                                            {{ __('Беговая дорожка для дома') }}
                                        @elseif($product->category === 'limited-edition')
                                            {{ __('Лимитированная коллекция') }}
                                        @elseif($product->category === 'commerce')
                                            {{ __('Коммерческий продукт') }}
                                        @elseif($product->category === 'aksessuary')
                                            {{ __('Категория акссесуары') }}
                                        @endif
                            </span>
                                </div>
                                <div class="price">
                                    <div>{{ number_format($product->price, 0, '', ' ') }}&nbsp;₽</div>
                                </div>
                            </div>
                            @if($product->availability === "есть")
                                <a href="{{ route('product.addToCart', ['id' => $product->product_id]) }}"
                                   class="buy">Купить</a>
                            @elseif($product->availability === "нет")
                                <a href="{{ route('product.addToCart', ['id' => $product->product_id]) }}"
                                   class="buy">Предзаказ</a>
                        @endif
                        <!-- <a href="{{ route('product.addToCart', ['id' => $product->product_id]) }}" class="buy">Купить</a> -->
                        </div>
                        <div class="description">
                            <p class="description__text">
                                {{ $product->properties }}
                            </p>

                        </div>
                        {{--<a href="#" class="file"></a>--}}
                        <hr>
                        <div class="characteristics">
                            <div class="column col1">
                                @if($product->category !== 'aksessuary')
                                    @if($product->engine_power)
                                        <span class="title">Двигатель: </span>
                                        <span class="text">{{ $product->engine_power }} </span>
                                        <br>
                                    @endif

                                    @if($product->sizes_cloth)
                                        <span class="title">Размеры полотна (ДхШ): </span>
                                        <span class="text">{{ $product->sizes_cloth }}</span>
                                        <br>
                                    @endif
                                    @if($product->maximum_user_weight)
                                        <span class="title">Максимальный вес пользователя:  </span>
                                        <span class="text">{{ $product->maximum_user_weight }}</span>
                                        <br>
                                    @endif
                                    @if($product->energy_saving)
                                        <span class="title">Энергосбережение: </span>
                                        <span class="text">{{ $product->energy_saving }}</span>
                                    @endif
                                @elseif($product->category === 'aksessuary')
                                    @if($product->additionally1)
                                        <span class="text">{{ $product->additionally1 }}</span>
                                        <br>
                                    @endif
                                    @if($product->additionally2)
                                        <span class="text">{{ $product->additionally2 }}</span>
                                        <br>
                                    @endif
                                    @if($product->additionally3)
                                        <span class="text">{{ $product->additionally3 }}</span>
                                    @endif
                                @endif
                            </div>
                            <div class="column col2">
                                @if($product->category !== 'aksessuary')
                                    @if($product->top_speed)
                                        <span class="title">Скорость:  </span>
                                        <span class="text">{{ $product->top_speed }}</span>
                                        <br>
                                    @endif
                                    @if($product->tilt_adjustment)
                                        <span class="title">Регулировка наклона:  </span>
                                        <span class="text">{{ $product->tilt_adjustment }}</span>
                                        <br>
                                    @endif
                                    @if($product->depreciation)
                                        <span class="title">Амортизация: </span>
                                        @if(strripos($product->depreciation, '/'))
                                            <span class="text">{{ is_array($short = explode('/', $product->depreciation)) ? $short[0] : '' }}</span>
                                        @else
                                            <span class="text">{{ $product->depreciation }}</span>
                                        @endif
                                        <br>
                                    @endif
                                    @if($product->display)
                                        <span class="title">Дисплей: </span>
                                        <span class="text">{{ $product->display }}</span>
                                    @endif
                                    @if($product->additionally3)
                                        <span class="title">Складная конструкция: </span>
                                        <span class="text">{{ $product->additionally3 }}</span>
                                        <br>
                                    @endif
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            @endforeach

        @endif
    </section>

    <div class="wrap_pagination wrapper">

        {{-- Пагинация --}}
        {{--@if(!isset($_GET['search']))--}} {{-- В случае если есть поиск, показать пагинацию. --}}
        {{--{{ $products->links() }}--}}
        {{-- @endif--}}

        @if(!isset($menu_active))
            @if($menu_active == 'search')
                {{ $products->links() }}
            @endif
        @endif
        {{--@if(!isset($menu_active))--}}
        @if($menu_active == 'catalog')
            {{ $products->links() }}
        @endif
        {{--@endif--}}
    </div>
    <!-- END Каталог -->

    {{-- Добавление в корзину --}}
    <script>
      window.addEventListener('load', () => {
        const buy = document.querySelectorAll('.buy')
        const basket_qty1 = document.getElementById('basket_qty1')
        const basket_qty2 = document.getElementById('basket_qty2')
        const basket_qty3 = document.getElementById('basket_qty3')
        const basket_qty4 = document.getElementById('basket_qty4')

        for (let i of buy) {
          i.addEventListener('click', add_qty_basket)
        }

        function add_qty_basket (e) {
          e.preventDefault()

          let id_product = e.target.getAttribute('href').split('/')
          id_product = id_product[id_product.length - 2]

            fetch("{{ 'http://'.$_SERVER['HTTP_HOST'] . '/' }}add-to-cart/" + id_product,
             {
                method: "GET",
                headers:{"content-type":"application/x-www-form-urlencoded"}
             })
             .then( response => {
             if (response.status !== 200) {
                return Promise.reject();
             }
                return response.text()
             });
             //.then(i => console.log(i))
             //.catch(() => console.log('ошибка'));

          // Добавляю количество в корзину
          const qty_product = document.querySelectorAll('.qty_product')
          const basket_link = document.querySelectorAll('.basket_link')

          if(
            (basket_qty1 && basket_qty1.firstElementChild)
            || (basket_qty2 && basket_qty2.firstElementChild )
            || (basket_qty3 && basket_qty3.firstElementChild)
            || (basket_qty4 && basket_qty4.firstElementChild)
          ) {
            for (let i of qty_product) {
              let qty = i.textContent.trim()
              i.textContent = 1 + +qty
              i.style.transition = 'all .3s'
              i.style.color = '#990018'
              i.style.background = '#ffffff'

              setTimeout(() => {
                i.style.transition = 'all .3s'
                i.style.color = ''
                i.style.background = ''
              }, 1000)
            }
          } else {

            if(basket_qty1 && !basket_qty1.firstElementChild) {
              let span1 = document.createElement('SPAN')
              span1.classList.add('qty_product')
              span1.textContent = 1
              basket_qty1.appendChild(span1)

              let span3 = document.createElement('SPAN')
              span3.classList.add('qty_product')
              span3.textContent = 1
              basket_qty3.appendChild(span3)
            }
            if(basket_qty2 && !basket_qty2.firstElementChild) {
              let span2 = document.createElement('SPAN')
              span2.classList.add('qty_product')
              span2.textContent = 1
              basket_qty2.appendChild(span2)

              let span4 = document.createElement('SPAN')
              span4.classList.add('qty_product')
              span4.textContent = 1
              basket_qty4.appendChild(span4)
            }

            const qty_product = document.querySelectorAll('.qty_product')
            for (let i of qty_product) {
              i.style.transition = 'all .3s'
              i.style.color = '#990018'
              i.style.background = '#ffffff'

              setTimeout(() => {
                i.style.transition = 'all .3s'
                i.style.color = ''
                i.style.background = ''
              }, 1000)
            }
          }
        }
      })
    </script>


    <!-- Фильтр для мобильника -->
    <div class="filter_mobile">
        <div class="filter_mobile__header">
            <span class="arrow_left"></span>
            <div class="h3">Фильтр</div>
        </div>
        <form action="#" class="form_filter_popup_mobile">
            <div class="popup_filter_inp">
                <div class="head_filter">Длина полотна</div>
                <div class="input-field filter_from">
                    <input type="text" id="filter_from__length_mobile">
                    <label for="filter_from__length_mobile">От</label>
                </div>
                <div class="input-field filter_before">
                    <input type="text" id="filter_before__length_mobile">
                    <label for="filter_before__length_mobile">До</label>
                </div>

                <div class="head_filter">Ширина полотна</div>
                <div class="input-field filter_from">
                    <input type="text" id="filter_from__width_mobile">
                    <label for="filter_from__width_mobile">От</label>
                </div>
                <div class="input-field filter_before">
                    <input type="text" id="filter_before__width_mobile">
                    <label for="filter_before__width_mobile">До</label>
                </div>
                <div class="head_filter">Максимальный вес</div>
                <div class="input-field filter_from">
                    <input type="text" id="filter_from__weight_mobile">
                    <label for="filter_from__weight_mobile">От</label>
                </div>
                <div class="input-field filter_before">
                    <input type="text" id="filter_before__weight_mobile">
                    <label for="filter_before__weight_mobile">До</label>
                </div>

                <div class="head_filter">Мощность (л.с.)</div>
                <div class="input-field filter_from">
                    <input type="text" id="filter_from__power_mobile">
                    <label for="filter_from__power_mobile">От</label>
                </div>
                <div class="input-field filter_before">
                    <input type="text" id="filter_before__power_mobile">
                    <label for="filter_before__power_mobile">До</label>
                </div>
            </div>

            <input type="reset" value="Сбросить" class="reset">

            <button class="btn_submit_filter" type="submit">Применить</button>
        </form>
    </div>

    <!-- Фильтр для моб -->
    <script>
      let btn_filter_mobile = document.querySelector('.btn_filter_mobile')

      if (btn_filter_mobile) {
        btn_filter_mobile.addEventListener('click', () => {
          let filter_mobile = document.querySelector('.filter_mobile')
          filter_mobile.style.display = 'block'
        })
      }

      let arrow_left = document.querySelector('.arrow_left')
      arrow_left.addEventListener('click', () => {
        let filter_mobile = document.querySelector('.filter_mobile')
        filter_mobile.style.display = ''
      })
    </script>

    {{-- Скрипт выборка изображения из списка --}}
    <script>
      window.addEventListener('load', () => {
        let image_str = document.querySelectorAll('.product_image img')
        for (let i = 0; i < image_str.length; i++) {
          let arr = image_str[i].dataset.img.split('|||')
          let name_img = arr[0].split('/')
          image_str[i].src = '/storage/' + name_img[1]
        }
      })
    </script>

    <script>
      window.addEventListener('load', () => {
        let wi = window.innerWidth
          || document.documentElement.clientWidth
          || document.body.clientWidth
        if (wi > 767) {
          let big_btn_filter = document.querySelectorAll('.big_btn_filter')
          let property_angle = document.querySelectorAll('.property_angle')
          for (let i = 0; i < big_btn_filter.length; i++) {
            big_btn_filter[i].addEventListener('click', show_filter)
          }
          //отобразить блок маленьких фильтров
          function show_filter (e) {
            for (let i = 0; i < big_btn_filter.length; i++) { //каждый большой фильтр
              if (big_btn_filter[i].classList.value === 'big_btn_filter active_filter') { //если большой фильтр активный
                big_btn_filter[i].classList.remove('active_filter') //сделать его неактивным
              }
            }
            e.currentTarget.classList.add('active_filter')//текущий большой фильтр сделать активным
            //скрыть все блоки маленьких фильтров
            for (let i = 0; i < property_angle.length; i++) { //каждый блок маленьких фильтров
              property_angle[i].style.display = 'none'//скрыть
            }
            e.currentTarget.nextElementSibling.style.display = 'flex' //отобразить соответствующий блок маленьких фильтров
          }

            /* Скрыть  показать     формы */
          document.querySelector('body').addEventListener('click', show_hide_form_filter)
          function show_hide_form_filter (e) {
            var link_show_form = e.target.closest('.link_show_form')
            if (link_show_form != null)
              var visible_form_filter_popup = link_show_form.nextElementSibling.style.display
            hide_forms_filter()
            if ((link_show_form != null) && (visible_form_filter_popup == ''))
              show_form_filter(link_show_form)
          }

          function hide_forms_filter () {
            let form_filter_popup = document.querySelectorAll('.form_filter_popup')
            for (let i = 0; i < form_filter_popup.length; i++) {
              form_filter_popup[i].style.display = ''
            }
            let angle_down = document.querySelectorAll('.angle_down')
            for (let i = 0; i < angle_down.length; i++) {
              angle_down[i].style.transform = ''
            }
          }

          function show_form_filter (e) {
            e.lastElementChild.style.transform = 'rotate(-180deg)'
            e.nextElementSibling.style.display = 'block'
          }
        } else {
          let big_btn_filter = document.querySelectorAll('.big_btn_filter')
          let property_angle = document.querySelectorAll('.property_angle')
          for (let i = 0; i < big_btn_filter.length; i++) {
            big_btn_filter[i].addEventListener('click', show_filter_mob)
          }
          function show_filter_mob (e) {
            //e.preventDefault()
            for (let i = 0; i < big_btn_filter.length; i++) {
              if (big_btn_filter[i].classList.value === 'big_btn_filter active_filter') {
                big_btn_filter[i].classList.remove('active_filter')
              }
            }
            e.currentTarget.classList.add('active_filter')
          }
        }
        //дизайн
        if (wi < 768) {
          let el_filter = document.querySelector('.filter')
          el_filter.firstElementChild.classList.remove('wrapper')
        } else {
          let el_filter = document.querySelector('.filter')
          el_filter.firstElementChild.classList.add('wrapper')
        }
      })
    </script>

    {{-- Обновляем в том случае если, изменение окна больше или меньше на 100px --}}
    <script>
      // Обновляем в том случае если, изменение окна больше или меньше на 100px
      scr_width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth
      window.addEventListener('resize', (e) => {
        let res = scr_width - e.target.innerWidth
        if (res > 200) {
          location.reload()
        }
        if (res < -200) {
          location.reload()
        }
      })
    </script>

    {{-- Подсветка категорий на мобиле --}}
    <script>
      window.addEventListener('load', () => {

        let w = new ScreenWidth
        if (w.getScreenWidth() < 768) {

          let isMarked = false
          if (!isMarked_fn(isMarked)) {
            let big_btn_filter = document.querySelectorAll('.big_btn_filter')
            big_btn_filter[0].classList.add('active_filter')
          } else {
            let big_btn_filter = document.querySelectorAll('.big_btn_filter')
            let property_ul = document.querySelector('.property_ul')
            for (let i = 0; i < big_btn_filter.length; i++) {
              if (big_btn_filter[i].classList.contains('active_filter')
                || big_btn_filter[i].classList.contains('active_filter_commerce')
                || big_btn_filter[i].classList.contains('active_filter_white')) {
                return isMarked = true
              }
            }
          }
        }

        function isMarked_fn (isMarked) {

          let big_btn_filter = document.querySelectorAll('.big_btn_filter')

          for (let i = 0; i < big_btn_filter.length; i++) {
            if (big_btn_filter[i].classList.contains('active_filter')
              || big_btn_filter[i].classList.contains('active_filter_commerce')
              || big_btn_filter[i].classList.contains('active_filter_white')) {
              return isMarked = true
            }
          }

          return isMarked = false
        }
      })
    </script>
@endsection
