@extends('layouts.layout', ['title' => 'Ошибка 404'])


@section('content')


{{--    <div class="wrapper">
        <h1 style="margin-top: 200px;">Страница не найдена(404 ошибка)</h1>

        <img src="{{ asset('img/404.gif') }}" alt="404" style="display: block;margin-top: 50px;">

        <a href="/" style="display: block;margin-top: 50px;margin-bottom: 75px; font-size: 40px;text-decoration: underline; color: #fff;"><h2>Перейти на главную ></h2></a>
    </div>--}}
<div class="head_basket">
    <div class="wrapper wrap_head">
        <div class="header_basket">
            <h1>Страница не найдена</h1>
        </div>
    </div>
</div>

<section class="wrapper">
    <div class="not_found">
        <p>Она была удалена, либо вовсе не существовала на сайте!</p>
    </div>
</section>

@endsection