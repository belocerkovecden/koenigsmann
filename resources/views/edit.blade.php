@extends('layouts.layout', ['title' => 'Редактирование товара', 'meta_tag_description' => 'Koenigsmann'])


@section('content')

    <form method="post" action="{{ route('product.edit', ['tag'=>$product->tag]) }}" class="container" id="main_form_edit"
          style="margin-top: 150px;" enctype="multipart/form-data" accept-charset='UTF-8'>
        @csrf
        @method('PATCH')

        <h1 class="h1">Редактировать продукт</h1>

        <div class="form-group">
            Название товара, которое будет в каталоге<span style="color:red;">*</span>
            <input name="title" type="text" class="form-control" required placeholder="Название товара"
                   value="{{ $product->title}}">
        </div>

        <div class="form-group">
            H1 Название которое будет в подробной карточке товара!
            <span style="color:red;">*</span>
            <input name="compact_folding" type="text" class="form-control" placeholder="H1 Название которое будет в подробной карточке товара"
                   value="{{ $product->compact_folding }}">
        </div>

        <div class="form-group">
            ЧПУ без пробелов это ссылка на товар <span style="color:red;">*</span>
            <input name="tag" type="text" class="form-control" placeholder="ЧПУ" required
                   value="{{ $product->tag }}">
        </div>

        <div class="form-group">
            Цена (без пробелов, и без символа Р) <span style="color:red;">*</span>
            <input name="price" type="number" class="form-control" required placeholder="Цена"
                   value="{{ $product->price }}">
        </div>

        <div class="form-group">
            Описание  <span style="color:red;">*</span>
            <textarea name="description" rows="10" class="form-control"
                      placeholder="Описание товара">{{ $product->description }}</textarea>
        </div>

        <div class="form-group">
            Дополнительные свойства
            <input name="properties" type="text" class="form-control" placeholder="Дополнительные свойства"
                   value="{{ $product->properties }}">
        </div>

        <div class="form-group">
            Категория, нужно выбрать из: <b>aksessuary</b> <b>limited-edition</b> <b>dlya-doma</b> <b>commerce</b>  <span style="color:red;">*</span>
            <input name="category" type="text" class="form-control" placeholder="Категория"
                   value="{{ $product->category }}" required>
        </div>

        <div class="form-group">
            Тип (Для акссесуаров, это title) <span style="color:red;">*</span>
            <input name="type" type="text" class="form-control" placeholder="Тип"
                   value="{{ $product->type }}">
        </div>

        <div class="form-group">
            Уровень (Для акссесуаров, это description) <span style="color:red;">*</span>
            <input name="level" type="text" class="form-control" placeholder="Уровень"
                   value="{{ $product->level }}">
        </div>

        <div class="form-group container_d" id="create_list_photos">
            @for($i = 1; $i < 21; $i++)
                <div class="block_img_data draggable">
                    @if(isset($product->data_img) && count($product->data_img) > 0)
                        <b>{{ $i }}</b>&nbsp;
                        @for($j = 0; $j < count($product->data_img); $j++)
                            @if(($j + 1) == $i)
                                <img src="{{ str_ireplace('public/', '/storage/', $product->data_img[$j]) }}" alt="#"
                                     style="margin-left: 20px; max-width: 150px;">
                                <a href="{{ route('destroy_photo', ['str' => str_ireplace('public/', '', $product->data_img[$j]), 'id_product' => $product->product_id]) }}"
                                   class="del_block_photo" title="Удалить фото">&times;</a>
                                <span>
                                <input name="photo{{ $i }}" type="hidden" class="form-control inp_sort"
                                       accept="image/*,image/jpeg,image/png"
                                       value="{{ $product->data_img[$j] }}">
                                </span>
                            @endif
                        @endfor
                    @endif

                    @if(isset($product->data_img) && count($product->data_img) < $i)
                        <span>
                            <input name="photo{{ $i }}" type="file" class="form-control empty_inp"
                                   accept="image/*,image/jpeg,image/png" value="">
                        </span>
                    @endif

                </div>
            @endfor

            <input type="hidden" name="sorting" id="sorting">
        </div>

        {{--                                   Большой список                                --}}
        <div class="form-group">
            Габариты (ДхШхВ)
            <input name="dimensions" type="text" class="form-control" placeholder="Габариты (ДхШхВ)"
                   value="{{ $product->dimensions }}">
        </div>
        <div class="form-group">
            Вес тренажера
            <input name="weight_trainer" type="text" class="form-control" placeholder="Вес тренажера"
                   value="{{ $product->weight_trainer }}">
        </div>
        <div class="form-group">
            Размеры упаковки Габариты в упаковке
            <input name="package_dimensions" type="text" class="form-control"
                   placeholder="Размеры упаковки Габариты в упаковке"
                   value="{{ $product->package_dimensions }}">
        </div>
        <div class="form-group">
            Вес в упаковке
            <input name="package_weight" type="text" class="form-control" placeholder="Вес в упаковке"
                   value="{{ $product->package_weight }}">
        </div>
        <div class="form-group">
            Складная конструкция
            <input name="folding_design" type="text" class="form-control" placeholder="Складная конструкция"
                   value="{{ $product->folding_design }}">
        </div>
        <div class="form-group">
            Габариты в сложенном виде
            <input name="folded_dimensions" type="text" class="form-control" placeholder="Габариты в сложенном виде"
                   value="{{ $product->folded_dimensions }}">
        </div>
        <div class="form-group">
            Тип питания
            <input name="type_powered" type="text" class="form-control" placeholder="Тип питания"
                   value="{{ $product->type_powered }}">
        </div>
        <div class="form-group">
            Гарантия на мотор
            <input name="motor_warranty" type="text" class="form-control" placeholder="Гарантия на мотор"
                   value="{{ $product->motor_warranty }}">
        </div>
        <div class="form-group">
            Гарантия на раму
            <input name="frame_warranty" type="text" class="form-control" placeholder="Гарантия на раму"
                   value="{{ $product->frame_warranty }}">
        </div>
        <div class="form-group">
            Гарантия на узлы движения и электронные блоки
            <input name="warranty_electronic_components" type="text" class="form-control"
                   placeholder="Гарантия на узлы движения и электронные блоки"
                   value="{{ $product->warranty_electronic_components }}">
        </div>
        <div class="form-group">
            Гарантия на детали износа
            <input name="wear_parts_warranty" type="text" class="form-control" placeholder="Гарантия на детали износа"
                   value="{{ $product->wear_parts_warranty }}">
        </div>
        <div class="form-group">
            Гарантия
            <input name="warranty" type="text" class="form-control" placeholder="Гарантия"
                   value="{{ $product->warranty }}">
        </div>
        <div class="form-group">
            Мощность двигателя, л.с.
            <input name="engine_power" type="text" class="form-control" placeholder="Мощность двигателя, л.с."
                   value="{{ $product->engine_power }}">
        </div>
        <div class="form-group">
            Максимальная скорость, км/ч
            <input name="top_speed" type="text" class="form-control" placeholder="Максимальная скорость, км/ч"
                   value="{{ $product->top_speed }}">
        </div>
        <div class="form-group">
            Максимальный вес пользователя
            <input name="maximum_user_weight" type="text" class="form-control"
                   placeholder="Максимальный вес пользователя"
                   value="{{ $product->maximum_user_weight }}">
        </div>
        <div class="form-group">
            Беговое полотно
            <input name="running_belt" type="text" class="form-control" placeholder="Беговое полотно"
                   value="{{ $product->running_belt }}">
        </div>
        <div class="form-group">
            Размеры полотна (ДхШ)
            <input name="sizes_cloth" type="text" class="form-control" placeholder="Размеры полотна (ДхШ)"
                   value="{{ $product->sizes_cloth }}">
        </div>
        <div class="form-group">
            Толщина деки
            <input name="deck_thickness" type="text" class="form-control" placeholder="Толщина деки"
                   value="{{ $product->deck_thickness }}">
        </div>
        <div class="form-group">
            Регулировка наклона
            <input name="tilt_adjustment" type="text" class="form-control" placeholder="Регулировка наклона"
                   value="{{ $product->tilt_adjustment }}">
        </div>
        <div class="form-group">
            Амортизация
            <input name="depreciation" type="text" class="form-control" placeholder="Амортизация"
                   value="{{ $product->depreciation }}">
        </div>
        <div class="form-group">
            Дисплей
            <input name="display" type="text" class="form-control" placeholder="Дисплей"
                   value="{{ $product->display }}">
        </div>
        <div class="form-group">
            Отображение времени тренировки
            <input name="workout_time_display" type="text" class="form-control"
                   placeholder="Отображение времени тренировки"
                   value="{{ $product->workout_time_display }}">
        </div>
        <div class="form-group">
            Предустановленные программы
            <input name="preset_programs" type="text" class="form-control" placeholder="Предустановленные программы"
                   value="{{ $product->preset_programs }}">
        </div>
        <div class="form-group">
            Пользовательские программы
            <input name="user_programs" type="text" class="form-control" placeholder="Пользовательские программы"
                   value="{{ $product->user_programs }}">
        </div>
        <div class="form-group">
            Спецификации программ
            <input name="program_specifications" type="text" class="form-control" placeholder="Спецификации программ"
                   value="{{ $product->program_specifications }}">
        </div>
        <div class="form-group">
            Рама
            <input name="frame" type="text" class="form-control" placeholder="Рама"
                   value="{{ $product->frame }}">
        </div>
        <div class="form-group">
            Измерение пульса
            <input name="heart_rate_measurement" type="text" class="form-control" placeholder="Измерение пульса"
                   value="{{ $product->heart_rate_measurement }}">
        </div>
        <div class="form-group">
            Интеграция
            <input name="integration" type="text" class="form-control" placeholder="Интеграция"
                   value="{{ $product->integration }}">
        </div>
        <div class="form-group">
            Мультимедиа
            <input name="multimedia" type="text" class="form-control" placeholder="Мультимедиа"
                   value="{{ $product->multimedia }}">
        </div>
        <div class="form-group">
            Дополнительно 1
            <input name="additionally1" type="text" class="form-control" placeholder="Дополнительно 1"
                   value="{{ $product->additionally1 }}">
        </div>
        <div class="form-group">
            Дополнительно 2
            <input name="additionally2" type="text" class="form-control" placeholder="Дополнительно 2"
                   value="{{ $product->additionally2 }}">
        </div>
        <div class="form-group">
            Дополнительно 3
            <input name="additionally3" type="text" class="form-control" placeholder="Дополнительно 3"
                   value="{{ $product->additionally3 }}">
        </div>
        <div class="form-group">
            Диаметр переднего вала
            <input name="front_shaft_diameter" type="text" class="form-control" placeholder="Диаметр переднего вала"
                   value="{{ $product->front_shaft_diameter }}">
        </div>
        <div class="form-group">
            Диаметр заднего вала
            <input name="rear_shaft_diameter" type="text" class="form-control" placeholder="Диаметр заднего вала"
                   value="{{ $product->rear_shaft_diameter }}">
        </div>
        <div class="form-group">
            Расстояние между стойками
            <input name="distance_between_racks" type="text" class="form-control"
                   placeholder="Расстояние между стойками"
                   value="{{ $product->distance_between_racks }}">
        </div>
        <div class="form-group">
            Тип пластика
            <input name="type_of_plastic" type="text" class="form-control" placeholder="Тип пластика"
                   value="{{ $product->type_of_plastic }}">
        </div>
        <div class="form-group">
            Энергосбережение
            <input name="energy_saving" type="text" class="form-control" placeholder="Энергосбережение"
                   value="{{ $product->energy_saving }}">
        </div>
        <div class="form-group">
            Уровень шума
            <input name="noise_level" type="text" class="form-control" placeholder="Уровень шума"
                   value="{{ $product->noise_level }}">
        </div>
        <div class="form-group">
            Комплектация
            <input name="equipment" type="text" class="form-control" placeholder="Комплектация"
                   value="{{ $product->equipment }}">
        </div>
        <div class="form-group">
            Шаг регулировки скорости, км/ч
            <input name="speed_adjustment_step" type="text" class="form-control"
                   placeholder="Шаг регулировки скорости, км/ч"
                   value="{{ $product->speed_adjustment_step }}">
        </div>
        <div class="form-group">
            Наличие товара на складе либо 'есть' либо 'нет'
            <input name="availability" type="text" class="form-control"
                   placeholder="Наличие товара на складе либо 'есть' либо 'нет'"
                   value="{{ $product->availability }}">
        </div>


        <div class="form-group">
            Путь до инструкции
            <input name="instruction" type="text" class="form-control" placeholder="Путь до инструкции"
                   value="{{ $product->instruction }}">
        </div>


        <input type="submit" value="Обновить продукт" class="btn btn-outline-success" id="imitation_event_click">
    </form>
    <script>
      window.addEventListener('load', () => {

        let stop_sort = false

        const draggables = document.querySelectorAll('.draggable')
        const containers = document.querySelectorAll('.container_d')

        draggables.forEach(draggable => {
          draggable.addEventListener('dragstart', () => {
            draggable.classList.add('dragging')
            //console.log('Взлёт')
          })
          draggable.addEventListener('dragend', () => {
            draggable.classList.remove('dragging')
            //console.log('Посадка')
            numbering()
          })
        })

        containers.forEach(container => {
          container.addEventListener('dragover', e => {
            e.preventDefault()
            const afterElement = getDragAfterElement(container, e.clientY)
            const draggable = document.querySelector('.dragging')
            if (afterElement == null) {
              container.appendChild(draggable)
            } else {
              container.insertBefore(draggable, afterElement)
            }
            //console.log('В полёте');
          })
        })

        function getDragAfterElement (container, y) {
          if(!stop_sort) {
            const draggableElements = [...container.querySelectorAll('.draggable:not(.dragging)')]

            return draggableElements.reduce((closest, child) => {
              const box = child.getBoundingClientRect()
              const offset = y - box.top - box.height / 2
              if (offset < 0 && offset > closest.offset) {
                return {offset: offset, element: child}
              } else {
                return closest
              }
            }, {offset: Number.NEGATIVE_INFINITY}).element
          }
        }

        function numbering () {
          const block_img_data = document.querySelectorAll('.block_img_data')

          // Нумеруем
          for (let i = 0; i < block_img_data.length; i++) {
            block_img_data[i].lastElementChild.lastElementChild.setAttribute('name', 'photo' + (i + 1))
            block_img_data[i].firstElementChild.textContent = i + 1
          }

          // Составляем строку после сортировки
          const inp_sort = document.querySelectorAll('.inp_sort')
          let str = ''
          for (let s = 0; s < inp_sort.length; s++) {
            if (s === 0) {
              str += inp_sort[s].defaultValue
            } else if (s === inp_sort.length + 1) {
              str += inp_sort[s].defaultValue
            } else {
              str += '|||' + inp_sort[s].defaultValue
            }
          }

          if(!stop_sort) {
            document.getElementById('sorting').value = str
          }

          if(!stop_sort) {
            const main_form_edit = document.getElementById('main_form_edit')
            main_form_edit.submit()
          }
          console.log('stop_sort = ' + stop_sort);
        }

        // Останавливаем сортировку
        stop_sorting()
        function stop_sorting () {
          const empty_inp = document.querySelectorAll('.empty_inp')
          for (let i of empty_inp) {
            i.addEventListener('input', del_drag)
          }
        }

        function del_drag () {
          const block_img_data = document.querySelectorAll('.block_img_data')
          const container_d = document.querySelectorAll('.container_d')
          for (let i of block_img_data) {
            i.classList.remove('draggable')
          }

          for (let i of container_d) {
            i.classList.remove('container_d')
          }
          stop_sort = true
          console.log('stop_sort = ' + stop_sort);
        }

          /* Скролим до фото */
        function scrollToElement(theElement) {
          if (typeof theElement === "string") theElement = document.getElementById(theElement);

          let selectedPosX = 0;
          let selectedPosY = 0;

          while (theElement != null) {
            selectedPosX += theElement.offsetLeft;
            selectedPosY += theElement.offsetTop - 70;
            theElement = theElement.offsetParent;
          }

          window.scrollTo(selectedPosX, selectedPosY);
        }

        @if(Session::has('success'))
          scrollToElement('create_list_photos');
          @endif

      })
    </script>

@endsection