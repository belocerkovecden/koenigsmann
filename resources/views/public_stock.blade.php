@extends('layouts.layout', ['title' => 'Акции'])


@section('content')

<div class="to-buyers" style="padding-top: 40px">

    <div class="stock-list wrapper">
        <div class="wrap">
            <div class="info">
                <h1 class="header">ВЫИГРАЙ МЕДОВЫЙ МЕСЯЦ!</h1>
                <p class="text">Купи украшение из новой коллекции Wedding, зарегистрируй штрихкод и выиграй свадебное путешествие! Акция проводится с 18 марта по 9 сентября 2019 г.</p>
                <a href="" class="button">Подробнее</a>
            </div>
            <div class="image">
                <img src="{{ asset('/img/stock1.png') }}" alt="">
            </div>
        </div>
        <div class="wrap">
            <div class="info">
                <h1 class="header">ПУТЕШЕСТВИЕ В ЛЕТО</h1>
                <p class="text">С 1 сентября по 14 ноября регистрируй штрихкоды украшений и часов и получи шанс выиграть фантастические подарки: набор украшений, часы для двоих, акустическую систему или главный приз – путешествие для всей семьи!</p>
                <a href="" class="button">Подробнее</a>
            </div>
            <div class="image">
                <img src="{{ asset('/img/stock2.png') }}" alt="">
            </div>
        </div>
        <div class="wrap">
            <div class="info">
                <h1 class="header">ПУТЕШЕСТВИЕ В ЛЕТО</h1>
                <p class="text">С 1 сентября по 14 ноября регистрируй штрихкоды украшений и часов и получи шанс выиграть фантастические подарки: набор украшений, часы для двоих, акустическую систему или главный приз – путешествие для всей семьи!</p>
                <a href="" class="button">Подробнее</a>
            </div>
            <div class="image">
                <img src="{{ asset('/img/stock2.png') }}" alt="">
            </div>
        </div>
        <div class="wrap">
            <div class="info">
                <h1 class="header">ПУТЕШЕСТВИЕ В ЛЕТО</h1>
                <p class="text">С 1 сентября по 14 ноября регистрируй штрихкоды украшений и часов и получи шанс выиграть фантастические подарки: набор украшений, часы для двоих, акустическую систему или главный приз – путешествие для всей семьи!</p>
                <a href="" class="button">Подробнее</a>
            </div>
            <div class="image">
                <img src="{{ asset('/img/stock1.png') }}" alt="">
            </div>
        </div>
        <div class="wrap">
            <div class="info">
                <h1 class="header">ПУТЕШЕСТВИЕ В ЛЕТО</h1>
                <p class="text">С 1 сентября по 14 ноября регистрируй штрихкоды украшений и часов и получи шанс выиграть фантастические подарки: набор украшений, часы для двоих, акустическую систему или главный приз – путешествие для всей семьи!</p>
                <a href="" class="button">Подробнее</a>
            </div>
            <div class="image">
                <img src="{{ asset('/img/stock2.png') }}" alt="">
            </div>
        </div>
        <a href="#" class="more">Посмотреть больше</a>
    </div>



</div>

@endsection