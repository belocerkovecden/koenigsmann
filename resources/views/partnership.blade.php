@extends('layouts.layout', ['title' => 'Станьте партнером компании Koenigsmann'])


@section('content')

    <!-- START Стать партнером -->
    <section class="partner_page">
        <div class="wrapper">

            <h1 class="partner_page__h1">Стать партнёром компании</h1>

            <p class="partner_page__first-p">
                <span class="span_fz_24px">Добрый день</span>, уважаемый партнер, Вас приветсвует компания KOENIGSMANN!
                Просим заполнить небольшую анкету, что бы наше взаимодействие стало еще удобнее.
            </p>

            <form action="{{ route('partner') }}" class="form_partner" method="post" enctype="multipart/form-data"
                  multiple>

                {{-- Безопасность от Laravel --}}
                @csrf

                <div class="data-partner">
                    <h3 class="data-partner__h3">Данные</h3>
                    <div class="input-field data-partner__fio">
                        <input type="text" id="data-partner__fio" name="contact_person_name"
                               required {{ old('contact_person_name') ?? $partner->organization_name ?? '' }}>
                        <label for="data-partner__fio">ФИО контактного лица <span class="red_star">*</span> </label>
                    </div>
                    <div class="input-field data-partner__phone">
                        <input type="tel" id="data-partner__phone" name="contact_person_phone"
                               required {{ old('contact_person_phone') ?? $partner->contact_person_phone ?? '' }}>
                        <label for="data-partner__phone">Телефон <span class="red_star">*</span> </label>
                    </div>
                    <div class="input-field data-partner__email">
                        <input type="text" id="data-partner__email" name="contact_person_email"
                               required {{ old('contact_person_email') ?? $partner->contact_person_email ?? '' }}>
                        <label for="data-partner__email">E-mail <span class="red_star">*</span> </label>
                    </div>
                    <div class="input-field data-partner__website">
                        <input type="text" id="data-partner__website" name="organization_address"
                               required {{ old('organization__address') ?? $partner->organization_address ?? '' }}>
                        <label for="data-partner__website">Сайт <span class="red_star">*</span> </label>
                    </div>
                    <div class="input-field data-partner__inn">
                        <input type="text" id="data-partner__inn" name="organization_inn"
                               required {{ old('organization_inn') ?? $partner->organization_inn ?? '' }}>
                        <label for="data-partner__inn">ИНН организации <span class="red_star">*</span> </label>
                    </div>
                </div>

                <div class="questions">
                    <h3 class="questions__h3">Вопросы</h3>


                    <h4 class="list_quest">1) Как вы планируете работать с нами? <span class="red_star">*</span></h4>
                    <div class="wrap_checkbox_question">
                        <label class="checkbox">
                            <input type="radio" name="contract" value="По дилерскому договору" checked="">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">По дилерскому договору</span>
                        </label>
                    </div>
                    <div class="wrap_checkbox_question mb60">
                        <label class="checkbox">
                            <input type="radio" name="contract" value="По агентскому договору">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">По агентскому договору</span>
                        </label>
                    </div>

                    <h4 class="list_quest">2) Осуществляете продажи через... <span class="red_star">*</span></h4>
                    <div class="wrap_checkbox_question">
                        <label class="checkbox">
                            <input type="radio" name="sales_through" value="Интернет-магазин" checked="">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">Интернет-магазин</span>
                        </label>
                    </div>
                    <div class="wrap_checkbox_question mb60">
                        <label class="checkbox">
                            <input type="radio" name="sales_through" value="Розничный магазин">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">Розничный магазин</span>
                        </label>
                    </div>

                    <h4 class="list_quest">3) Регион осуществления торговли <span class="red_star">*</span></h4>
                    <div class="wrap_checkbox_question">
                        <label class="checkbox">
                            <input type="radio" name="trading_region" value="По РФ" checked="">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">По РФ</span>
                        </label>
                    </div>
                    <div class="wrap_checkbox_question">
                        <label class="checkbox">
                            <input type="radio" name="trading_region" value="По РФ + СНГ">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">По РФ + СНГ</span>
                        </label>
                    </div>
                    <div class="wrap_checkbox_question mb60">
                        <label class="checkbox">
                            <input type="radio" name="trading_region" value="Только в домашнем регионе/городе">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">Только в домашнем регионе/городе</span>
                        </label>
                    </div>

                    <hr class="hr_partner_new">

                    <div class="wrap_checkbox_question">
                        <label class="checkbox">
                            <input type="checkbox" name="showroom" value="Есть шоу-рум">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">Есть ли у вас шоу-рум? </span>
                        </label>
                    </div>
                    <div class="wrap_checkbox_question">
                        <label class="checkbox">
                            <input type="checkbox" name="have_delivery_service" value="Есть служба доставки">
                            <span class="custom-checkbox_radio_quest"></span>
                            <span class="checkbox-text">Есть ли у вас собственная служба доставки? </span>
                        </label>
                    </div>
                </div>

                <div class="input-field location_city">
                    <input type="text" id="location_city" name="location_city"
                           required {{ old('location_city') ?? $partner->location_city ?? '' }}>
                    <label for="location_city">В каком городе вы находитесь <span class="red_star">*</span> </label>
                </div>

                <div class="form-group wrap_gold_clip" title="Файл для загрузки не больше 300 КилоБайт">
                    <span class="head_gold_clip">Карточка предприятия <span class="red_star">*</span> </span>
                    <ul class="name_files"></ul>
                    <label class="label_gold_clip">
                        <span class="gold_clip_icon"></span>
                        <span class="upload_file_text">Загрузить</span>
                        <input name="files[]" type="file" class="form-control" multiple id="gold_clip_inp"
                               {{--accept="text/plain,application/vnd.oasis.opendocument.spreadsheet,application/vnd.oasis.opendocument.presentation,image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"--}}
                               value="{{ old('img_product') ?? $product->img_product ?? '' }}">
                    </label>
                </div>
                @if(isset($partner_success))
                    <div class="wrap_success">
                        <span class="availability__icon"></span>
                        <div class="success">{{ $partner_success }}</div>
                    </div>
                    @else
                    <div class="wrap_btn-form_info">
                        {{--<button type="submit" class="btn-form btn_submit_partner">Отправить заявку</button>--}}
                        <input type="submit" value="Отправить заявку" class="btn-form btn_submit_partner">
                        <p class="info">Нажимая на кнопку, вы соглашаетесь на обработку персональных данных в соответствии с
                            Политикой конфиденциальности</p>

                        <div class="form_partner__red-text">Заполните все обязательные поля!</div>
                    </div>
                @endif

            </form>

            <div class="service">
                <h2 class="head_step">Сервис и преференции</h2>

                <ul class="wrap_cards">
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/partner/expert.jpg') }}" alt="expert.jpg">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Экспертный подбор оборудования</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/partner/sails.jpg') }}" alt="sails.jpg">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Система скидок</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                    <span class="wrap_img">
                        <img src="{{ asset('img/partner/waranty.jpg') }}" alt="waranty.jpg">
                    </span>
                            <div class="header">
                                <p class="angle_right_white">Гарантийное обслуживание</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
            <span class="wrap_img">
                <img src="{{ asset('img/partner/manager.jpg') }}" alt="manager.jpg">
            </span>
                            <div class="header">
                                <p class="angle_right_white">Персональный менеджер</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>

            {{-- <div class="escort">
                 <h2 class="head_step">Сопровождение продаж</h2>

                 <ul class="wrap_cards">
                     <li>
                         <a href="#">
                     <span class="wrap_img">
                         <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                     </span>
                             <div class="header">
                                 <p class="angle_right_white">Экспертный подбор оборудования</p>
                             </div>
                         </a>
                     </li>
                     <li>
                         <a href="#">
                     <span class="wrap_img">
                         <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                     </span>
                             <div class="header">
                                 <p class="angle_right_white">Система скидок</p>
                             </div>
                         </a>
                     </li>
                     <li>
                         <a href="#">
                     <span class="wrap_img">
                         <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                     </span>
                             <div class="header">
                                 <p class="angle_right_white">Гарантийное обслуживание</p>
                             </div>
                         </a>
                     </li>
                     <li>
                         <a href="#">
                     <span class="wrap_img">
                         <img src="{{ asset('img/tetya_bg.png') }}" alt="tetya_bg.png">
                     </span>
                             <div class="header">
                                 <p class="angle_right_white">Персональный менеджер</p>
                             </div>
                         </a>
                     </li>
                 </ul>
             </div>--}}

            <div class="partner">
                <h2 class="head_step">Как стать партнёром?</h2>

                <ul class="list_step">
                    <li>
                        <img src="{{ asset('img/circle/circle1.svg') }}" alt="1">
                        <p>Заполнить заявку выше</p>
                    </li>
                    <li>
                        <img src="{{ asset('img/circle/circle3.svg') }}" alt="2">
                        <p>Дождаться звонка менеджера</p>
                    </li>
                    <li>
                        <img src="{{ asset('img/circle/circle4.svg') }}" alt="3">
                        <p>Согласовать коммерческие условия</p>
                    </li>
                    <li>
                        <img src="{{ asset('img/circle/circle2.svg') }}" alt="4">
                        <p>Начать работу</p>
                    </li>
                </ul>

            </div>
        </div>
    </section>
    <!-- END Стать партнером -->

    {{-- Валидация --}}
    <script>
      window.addEventListener('load', () => {

        let form_partner = document.querySelector('.form_partner')
        form_partner.addEventListener('submit', valid_inp)

        function valid_inp (e) {

          let data_partner_inp = document.querySelectorAll('.data-partner input')
          let error = []
          for (let i = 0; i < data_partner_inp.length; i++) {
            if (data_partner_inp[i].value.trim().length < 2) {
              error.push(data_partner_inp[i].nextElementSibling.textContent)
            }
          }

          if (location_city.value.trim().length < 2) {
            error.push(location_city.nextElementSibling.textContent)
          }
          if (error.length > 0) {
            e.preventDefault()
            e.stopPropagation()
            let form_partner__red = document.querySelector('.form_partner__red-text')
            form_partner__red.style.display = 'inline-block'
            return false
          }
        }

      })
    </script>

    {{-- Показать загружаемые файлы --}}
    <script>
      window.addEventListener('load', () => {
        let gold_clip_inp = document.getElementById('gold_clip_inp')
        gold_clip_inp.addEventListener('input', show_name_upload_files)

        function show_name_upload_files (e) {
          let name_files = document.querySelector('.name_files')
          let obj = e.target.files
          for (let key in obj) {
            if ((typeof obj[key].name === 'string') && obj[key].name.indexOf('.') > 0) {
              let li = document.createElement('LI')
              li.textContent = obj[key].name
              name_files.appendChild(li)
            }
          }
        }
      })
    </script>
@endsection