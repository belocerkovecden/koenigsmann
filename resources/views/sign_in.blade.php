<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Покупателям</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body><!-- START POPUP Обратный звонок -->
<div class="back_g">
    <div class="wrap_popup">
        <div class="wrap_mob_head"><a href="/" class="logo_popup"><img src="img/logo.svg" alt=""></a>
            <div class="close_popup"><span class="close_popup__icon"></span></div>
        </div>
        <div class="head"><h4 class="show_lg">Обратный звонок</h4><h4 class="show_md">Звонок</h4>
            <div class="close_popup"><span class="close_popup__text">Закрыть</span> <span
                        class="close_popup__icon"></span></div>
        </div>
        <form action="#" class="body_popup"><a class="popup_call_site" href="tel:+78001111222">8 800 111 12 22</a> <a
                    class="popup_call_myself" href="tel:+79283048594">Позвонить</a>
            <p class="min_30">Введите номер телефона и мы перезвоним вам за 30 минут!</p>
            <div class="input-field call_back_user_name"><input type="text" id="call_back_user_name" required> <label
                        for="call_back_user_name">Имя</label></div>
            <div class="input-field call_back_user_phone"><input type="tel" id="phone" name="phone" minlength="5"
                                                                 required> <label for="phone">Телефон</label></div>
            <span class="call_back_after">Перезвонить через <input type="text" class="call_back_after__input"
                                                                   value="20"> минут</span><!--
            <label>
                <span class="label_span"><span class="plus7">+7</span> 999 383-43-24</span>
                <input type="tel" id="phone" name="phone" minlength="5" required>
            </label>-->
            <div class="wrap_btn_policy"><input type="submit" value="Перезвоните мне">
                <p class="policy">Нажимая на кнопку, вы соглашаетесь на обработку персональных данных в соответствии с
                    Политикой конфиденциальности</p></div>
        </form>
    </div>
</div><!-- END POPUP Обратный звонок --><!-- START POPUP Вход в личный кабинет -->
<div class="back_g2">
    <div class="wrap_popup">
        <div class="head"><h1 class="h1">Вход в аккаунт</h1>
            <div class="close_popup"><span class="close_popup__text">Закрыть</span> <span
                        class="close_popup__icon"></span></div>
        </div>
        <form action="#" class="body_popup">
            <div class="input-field signin_input"><input type="text" id="login" name="login" required> <label
                        for="login">E-mail или телефон</label></div>
            <div class="input-field signin_input"><input type="password" id="password" name="password" minlength="6"
                                                         required> <label for="password">Пароль</label></div>
            <a href="#" class="forgot">Забыли пароль?</a> <input type="submit" class="signin_button"
                                                                 value="Войти в аккаунт">
            <p class="registration desc"><a href="#">Зарегистрироваться,</a> если нет аккаунта</p>
            <p class="registration mob"><a href="#">Регистрация,</a> если нет аккаунта</p></form>
    </div>
</div>
<!-- END POPUP Вход в личный кабинет --><!-- START  HEADER -->
<header class="header">
    <div class="wrap_first_line wrapper">
        <div class="wrap_search_phone">
            <div class="block-search"><span class="block-search__icon-search"></span> <input class="block-search__input"
                                                                                             type="text"
                                                                                             placeholder="Поиск"> <span
                        class="block-search__icon-close"></span></div>
            <div class="block-phone"><span class="block-phone__icon-phone"></span>
                <div class="block-phone__wrap-head-number">
                    <div class="block-phone__head">Обратный звонок</div>
                    <a href="tel:88001111122" class="block-phone__number">8 800 111 11 22</a></div>
            </div>
        </div>
        <a href="/" class="logo"><img src="img/logo.svg" alt=""> </a><a href="#" class="login">Вход в личный кабинет</a>
        <!-- Видно только на планшете и меньше -->
        <div class="wrap_mob"><a href="tel:88001111122" class="mobile768"></a> <a href="#" class="sandwich"></a></div>
    </div>
    <div class="header-menu">
        <div class="wrapper"><!-- Первая линия --><!-- Вторая линия -->
            <ul class="second-line">
                <li><a href="#">О бренде</a></li>
                <li><a href="http://cj34342.tmweb.ru/km/catalog.html">Каталог продукции</a></li>
                <li><a href="http://cj34342.tmweb.ru/km/personal_area-stock.html">Акции</a></li>
                <li><a href="http://cj34342.tmweb.ru/km/">Покупателям</a></li>
                <li class="active"><a href="http://cj34342.tmweb.ru/km/">Стать партнёром</a></li>
                <li><a href="http://cj34342.tmweb.ru/km/where_buy.html">Где купить</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </div>
    </div>
</header><!-- END HEADER -->
<div class="wrapper"><h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1>
    <h1>Нажми, - "Вход в личный кабинет"!</h1></div>
<script src="js/main.min.js"></script>
</body>
</html>