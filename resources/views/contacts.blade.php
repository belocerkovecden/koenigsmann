@extends('layouts.layout', ['title' => 'Обратная связь Koenigsmann'])


@section('content')

<!-- START Контакты -->
<div class="contacts"><!-- Лев Leo -->
    <div class="bg"></div>
    <div class="wrapper">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
            <li><a href="#">Контакты</a></li>
        </ul>
        <h1>Контакты</h1>
        <div class="wrap-contacts_info-form_request">
            <div class="contacts_info">
                <div class="email">
                    <div class="head">E-mail</div>
                    <div class="text"><span></span>
                        <a href="mailto:info@koenigsmann.ru">info@koenigsmann.ru</a></div>
                </div>
                <div class="phone">
                    <div class="head">Телефон</div>
                    <div class="text"><span></span>
                        <a href="tel:+74993501585">+7 499 350 15 85</a></div>
                </div>
            </div>
            <div class="form_request"><h1>Оставьте заявку</h1>
                <form action="{{ route('contacts') }}" method="post">

                    {{-- Безопасность от Laravel --}}
                    @csrf

                    <div class="input-field contacts_input">
                        <input type="text" id="name" name="contact_name" required value="{{ old('contact_name') ?? $product->contact_name ?? '' }}">
                        <label for="name">Имя</label>
                    </div>
                    <div class="input-field contacts_input">
                        <input type="email" id="email" name="contact_email" required value="{{ old('contact_email') ?? $product->contact_email ?? '' }}">
                        <label for="signin_password">E-mail</label>
                    </div>
                    <div class="input-field contacts_input">
                        <input type="text" id="description" name="contact_description" required value="{{ old('contact_description') ?? $product->contact_description ?? '' }}">
                        <label for="description">Описание</label>
                    </div>
                    <input type="submit" class="send_request" value="Оставить заявку">
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Контакты -->


@endsection