@extends('layouts.layout', ['title' => 'Личный кабинет'])


@section('content')

<!-- START Аккаунт -->
<div class="top-line-account">
    <div class="wrapper">
        <h1>Личный кабинет</h1>
        <div class="wrap_account-userinfo_card">
            <div class="block-account-userinfo">
                <span class="block-account-userinfo__icon"></span>
                <a href="" class="block-account-userinfo__head">Данные аккаунта</a>
            </div>
            <div class="block-card">
                <span class="block-card__icon"></span>
                <a href="" class="block-card__head">Корзина</a>
            </div>
        </div>
    </div>
</div>
<section class="account-content wrapper">
    
</section>
<!-- END Аккаунт -->

@endsection