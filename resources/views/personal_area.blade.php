@extends('layouts.layout', ['title' => 'Личный кабинет'])


@section('content')

    <!-- START Личный кабинет -->
    <section class="wrapper" id="section_not_twice_scroll">
        <!-- Лев Leo -->
        <div class="bg_LK"></div>

        <div class="wrap_scroll_for_md">
            <div class="wrap_tab_btn_my">
                <div class="wrap_tab">
                    {{--<h3 class="show_md" id="UC"><a href="#">Выгрузка каталога</a></h3>--}}
                    <h3 class="active_tab" id="PL"><a href="#">{{--Прайс-листы/--}}Наличие</a></h3>
                    {{--<h3><a href="{{ url('public_stock') }}">Акции</a></h3>
                    <h3><a href="#">Обучение</a></h3>--}}
                    <h3 id="MS"><a href="#">Маркетинговая поддержка</a></h3>
                </div>

                {{--<a href="#" id="btn_upload_katalog">Выгрузка каталога</a>--}}
            </div>
        </div>

        {{-- Маркетинговые материалы --}}
        <div class="wrap_select_search">
            <div class="wrap_select">
                <select name="materials" id="materials">
                    <option value="all_files">Все файлы</option>
                    <option value="logo">Логотип</option>
                    <option value="instruction">Инструкции</option>
                    {{--                    <option value="Обучающие материалы">Обучающие материалы2</option>
                                        <option value="Обучающие материалы">Обучающие материалы3</option>
                                        <option value="Обучающие материалы">Обучающие материалы4</option>--}}
                </select>
                <span class="down"></span>
            </div>

            {{--            <div class="wrap_search">
                            <span class="icon_search"></span>
                            <input class="input_search" type="text" placeholder="Поиск внутри раздела">
                        </div>--}}

            <a class="wrap_download_all" download
               href="https://koenigsmann.ru/download/instruction/all_files_koenigsmann.zip">
                <div class="wrap_download_all__header">Загрузить всё</div>
                <span class="icon_download"></span>
            </a>

            {{--            <div class="wrap_download_selected">
                            <label class="download_selected__label">
                                <div class="wrap_download_selected__header">Загрузить выбранное</div>
                                <input type="checkbox" class="hidden">
                                <span class="custom_checkbox_pa"></span>
                            </label>
                        </div>--}}
        </div>
        <ul class="list_result">
            <li data-li="logo">
                <div class="wrap_icon_description">
                    <span class="icon_file"><img src="{{ asset('img/img_file.svg') }}" alt="img_file_logo"></span>
                    <div class="wrap_description">
                        <div class="description_name">Изображение с логотипом</div>
                        <div class="description_data">4.5 MB, 05.08.2019 11:48</div>
                    </div>
                </div>

                <div class="functional_right">
                    <a href="https://koenigsmann.ru/download/instruction/logo.png" class="wrap_download_all" download>
                        <div class="wrap_download_all__header">Загрузить</div>
                        <span class="icon_download"></span>
                    </a>

                    {{--                    <div class="wrap_download_selected">
                                            <label class="download_selected__label">
                                                <input type="checkbox" class="hidden">
                                                <span class="custom_checkbox_pa"></span>
                                            </label>
                                        </div>--}}
                </div>
            </li>
            <li data-li="instruction">
                <div class="wrap_icon_description">
                    <span class="icon_file"><img src="{{ asset('img/pdf_file/pdf_file.svg') }}" alt="pdf_file"></span>
                    <div class="wrap_description">
                        <div class="description_name">Инструкция - Koenigsmann MODEL C1.0</div>
                        <div class="description_data">65.9 MB, 05.08.2019 11:48</div>
                    </div>
                </div>

                <div class="functional_right">
                    <a class="wrap_download_all" download href="https://koenigsmann.ru/download/instruction/C1.0.pdf">
                        <div class="wrap_download_all__header">Загрузить</div>
                        <span class="icon_download"></span>
                    </a>
                    {{--                    <div class="wrap_download_selected">
                                            <label class="download_selected__label">
                                                <input type="checkbox" class="hidden">
                                                <span class="custom_checkbox_pa"></span>
                                            </label>
                                        </div>--}}
                </div>
            </li>
            <li data-li="instruction">
                <div class="wrap_icon_description">
                    <span class="icon_file"><img src="{{ asset('img/pdf_file/pdf_file.svg') }}" alt="pdf_file"></span>
                    <div class="wrap_description">
                        <div class="description_name">Инструкция - Koenigsmann MODEL C1.0 LIMITED EDITION</div>
                        <div class="description_data">66.0 MB, 05.08.2019 11:48</div>
                    </div>
                </div>

                <div class="functional_right">
                    <a class="wrap_download_all" download
                       href="https://koenigsmann.ru/download/instruction/C1.0 le.pdf">
                        <div class="wrap_download_all__header">Загрузить</div>
                        <span class="icon_download"></span>
                    </a>
                    {{--                    <div class="wrap_download_selected">
                                            <label class="download_selected__label">
                                                <input type="checkbox" class="hidden">
                                                <span class="custom_checkbox_pa"></span>
                                            </label>
                                        </div>--}}
                </div>
            </li>
            <li data-li="instruction">
                <div class="wrap_icon_description">
                    <span class="icon_file"><img src="{{ asset('img/pdf_file/pdf_file.svg') }}" alt="pdf_file"></span>
                    <div class="wrap_description">
                        <div class="description_name">Инструкция - Koenigsmann MODEL E1.0</div>
                        <div class="description_data">69.4 MB, 05.08.2019 11:48</div>
                    </div>
                </div>

                <div class="functional_right">
                    <a class="wrap_download_all" download href="https://koenigsmann.ru/download/instruction/E1.0.pdf">
                        <div class="wrap_download_all__header">Загрузить</div>
                        <span class="icon_download"></span>
                    </a>
                    {{--                    <div class="wrap_download_selected">
                                            <label class="download_selected__label">
                                                <input type="checkbox" class="hidden">
                                                <span class="custom_checkbox_pa"></span>
                                            </label>
                                        </div>--}}
                </div>
            </li>
            <li data-li="instruction">
                <div class="wrap_icon_description">
                    <span class="icon_file"><img src="{{ asset('img/pdf_file/pdf_file.svg') }}" alt="pdf_file"></span>
                    <div class="wrap_description">
                        <div class="description_name">Инструкция - Koenigsmann MODEL E1.0 LIMITED EDITION</div>
                        <div class="description_data">73.6 MB, 05.08.2019 11:48</div>
                    </div>
                </div>

                <div class="functional_right">
                    <a class="wrap_download_all" download
                       href="https://koenigsmann.ru/download/instruction/E1.0 le.pdf">
                        <div class="wrap_download_all__header">Загрузить</div>
                        <span class="icon_download"></span>
                    </a>
                    {{--                    <div class="wrap_download_selected">
                                            <label class="download_selected__label">
                                                <input type="checkbox" class="hidden">
                                                <span class="custom_checkbox_pa"></span>
                                            </label>
                                        </div>--}}
                </div>
            </li>
            <li data-li="instruction">
                <div class="wrap_icon_description">
                    <span class="icon_file"><img src="{{ asset('img/pdf_file/pdf_file.svg') }}" alt="pdf_file"></span>
                    <div class="wrap_description">
                        <div class="description_name">Инструкция - Koenigsmann MODEL S1.0</div>
                        <div class="description_data">72.2 MB, 05.08.2019 11:48</div>
                    </div>
                </div>

                <div class="functional_right">
                    <a class="wrap_download_all" download href="https://koenigsmann.ru/download/instruction/S1.0.pdf">
                        <div class="wrap_download_all__header">Загрузить</div>
                        <span class="icon_download"></span>
                    </a>
                    {{--                    <div class="wrap_download_selected">
                                            <label class="download_selected__label">
                                                <input type="checkbox" class="hidden">
                                                <span class="custom_checkbox_pa"></span>
                                            </label>
                                        </div>--}}
                </div>
            </li>
            <li data-li="instruction">
                <div class="wrap_icon_description">
                    <span class="icon_file"><img src="{{ asset('img/pdf_file/pdf_file.svg') }}" alt="pdf_file"></span>
                    <div class="wrap_description">
                        <div class="description_name">Инструкция - Koenigsmann MODEL S1.0 LIMITED EDITION</div>
                        <div class="description_data">73.6 MB, 05.08.2019 11:48</div>
                    </div>
                </div>

                <div class="functional_right">
                    <a class="wrap_download_all" download
                       href="https://koenigsmann.ru/download/instruction/S1.0 le.pdf">
                        <div class="wrap_download_all__header">Загрузить</div>
                        <span class="icon_download"></span>
                    </a>
                    {{--                    <div class="wrap_download_selected">
                                            <label class="download_selected__label">
                                                <input type="checkbox" class="hidden">
                                                <span class="custom_checkbox_pa"></span>
                                            </label>
                                        </div>--}}
                </div>
            </li>
            <li data-li="instruction">
                <div class="wrap_icon_description">
                    <span class="icon_file"><img src="{{ asset('img/pdf_file/pdf_file.svg') }}" alt="pdf_file"></span>
                    <div class="wrap_description">
                        <div class="description_name">Инструкция - Кoenigsmann MODEL XR1.0 COMMERCIAL</div>
                        <div class="description_data">72.2 MB, 05.08.2019 11:48</div>
                    </div>
                </div>

                <div class="functional_right">
                    <a class="wrap_download_all" download href="https://koenigsmann.ru/download/instruction/XR1.0.pdf">
                        <div class="wrap_download_all__header">Загрузить</div>
                        <span class="icon_download"></span>
                    </a>
                    {{--                    <div class="wrap_download_selected">
                                            <label class="download_selected__label">
                                                <input type="checkbox" class="hidden">
                                                <span class="custom_checkbox_pa"></span>
                                            </label>
                                        </div>--}}
                </div>
            </li>
            <li data-li="instruction">
                <div class="wrap_icon_description">
                    <span class="icon_file"><img src="{{ asset('img/pdf_file/pdf_file.svg') }}" alt="pdf_file"></span>
                    <div class="wrap_description">
                        <div class="description_name">Инструкция - Умные весы Koenigsmann Smart Scale</div>
                        <div class="description_data">10.4 MB, 05.08.2019 11:48</div>
                    </div>
                </div>

                <div class="functional_right">
                    <a class="wrap_download_all" download
                       href="https://koenigsmann.ru/download/instruction/smart scale.pdf">
                        <div class="wrap_download_all__header">Загрузить</div>
                        <span class="icon_download"></span>
                    </a>
                    {{--                    <div class="wrap_download_selected">
                                            <label class="download_selected__label">
                                                <input type="checkbox" class="hidden">
                                                <span class="custom_checkbox_pa"></span>
                                            </label>
                                        </div>--}}
                </div>
            </li>
        </ul>
        <!-- Наличие -->
        <div class="price_lists">
            <ul class="list_result_PL">
                <li>
                    <div class="wrap_icon_description_PL">
                        <div class="wrap_icon_download">
                            <span class="icon_file">
                                <img src="{{ asset('img/x_file.svg') }}" alt="x_file">
                            </span>
                            <a class="wrap_download_all">
                                <div class="wrap_download_all__header">Загрузить</div>
                                <span class="icon_download_PL"></span>
                            </a>
                        </div>
                        <div class="functional_right">
                            <div class="wrap_download_selected">
                                <label class="download_selected__label">
                                    <input type="checkbox" class="hidden">
                                    <span class="custom_checkbox_pa"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="wrap_description_PL">
                        <div class="description_name">Файл с логотипом</div>
                        <div class="description_data">4.5 MB, 05.08.2019 11:48</div>
                    </div>
                </li>
                <li>
                    <div class="wrap_icon_description_PL">
                        <div class="wrap_icon_download">
                            <span class="icon_file">
                                <img src="{{ asset('img/fl_file.svg') }}" alt="x_file">
                            </span>
                            <a class="wrap_download_all">
                                <div class="wrap_download_all__header">Загрузить</div>
                                <span class="icon_download_PL"></span>
                            </a>
                        </div>
                        <div class="functional_right">
                            <div class="wrap_download_selected">
                                <label class="download_selected__label">
                                    <input type="checkbox" class="hidden">
                                    <span class="custom_checkbox_pa"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="wrap_description_PL">
                        <div class="description_name">Файл с логотипом</div>
                        <div class="description_data">4.5 MB, 05.08.2019 11:48</div>
                    </div>
                </li>
                <li>
                    <div class="wrap_icon_description_PL">
                        <div class="wrap_icon_download">
                            <span class="icon_file">
                                <img src="{{ asset('img/x_file.svg') }}" alt="x_file">
                            </span>
                            <a class="wrap_download_all">
                                <div class="wrap_download_all__header">Загрузить</div>
                                <span class="icon_download_PL"></span>
                            </a>
                        </div>
                        <div class="functional_right">
                            <div class="wrap_download_selected">
                                <label class="download_selected__label">
                                    <input type="checkbox" class="hidden">
                                    <span class="custom_checkbox_pa"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="wrap_description_PL">
                        <div class="description_name">Файл с логотипом</div>
                        <div class="description_data">4.5 MB, 05.08.2019 11:48</div>
                    </div>
                </li>
                <li>
                    <div class="wrap_icon_description_PL">
                        <div class="wrap_icon_download">
                            <span class="icon_file">
                                <img src="{{ asset('img/psd_file.svg') }}" alt="x_file">
                            </span>
                            <a class="wrap_download_all">
                                <div class="wrap_download_all__header">Загрузить</div>
                                <span class="icon_download_PL"></span>
                            </a>
                        </div>
                        <div class="functional_right">
                            <div class="wrap_download_selected">
                                <label class="download_selected__label">
                                    <input type="checkbox" class="hidden">
                                    <span class="custom_checkbox_pa"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="wrap_description_PL">
                        <div class="description_name">Файл с логотипом</div>
                        <div class="description_data">4.5 MB, 05.08.2019 11:48</div>
                    </div>
                </li>
            </ul>

            <!-- Две кноки, - Загрузить всё и Загрузить выбранное -->
            <div class="wrap_btn_PL">
                <a href="#">
                    <span class="icon_btn_downLoad_all"></span>
                    <span class="text_btn">Загрузить всё</span>
                </a>
                <a href="#">
                    <span class="icon_btn_download_select"></span>
                    <span class="text_btn">Загрузить выбранное</span>
                </a>
            </div>
        </div>

        <!-- START BreadCrumbs Upload Catalog -->
        <ul class="breadcrumbs_UC">
            <li><a href="/">Главная</a></li>
            <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
            <li><a href="#" class="go_menu">Личный кабинет</a></li>
            <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
            <li><a href="#">Выгрузка каталога</a></li>
        </ul>
        <!-- END BreadCrumbs -->

        <!-- Выгрузка каталога -->
        <div class="catalog_upload_for_mob">

            <!-- Список для выгрузки каталога  -->
            {{--<ul class="list_result_UC">
                <li>
                    <div class="wrap_icon_description">
                        <div class="wrap_description">
                            <div class="description_name">Формат XML</div>
                        </div>
                    </div>

                    <div class="functional_right_UC">
                        <a class="wrap_download_all_UC">
                            <span class="icon_download"></span>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="wrap_icon_description">
                        <div class="wrap_description">
                            <div class="description_name">Формат YML</div>
                        </div>
                    </div>

                    <div class="functional_right_UC">
                        <a class="wrap_download_all_UC">
                            <span class="icon_download"></span>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="wrap_icon_description">
                        <div class="wrap_description">
                            <div class="description_name">Формат CSV</div>
                        </div>
                    </div>

                    <div class="functional_right_UC">
                        <a class="wrap_download_all_UC">
                            <span class="icon_download"></span>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="wrap_icon_description">
                        <div class="wrap_description">
                            <div class="description_name">Формат SHORTSCRIPT</div>
                        </div>
                    </div>

                    <div class="functional_right_UC">
                        <a class="wrap_download_all_UC">
                            <span class="icon_download"></span>
                        </a>
                    </div>
                </li>
            </ul>--}}
        </div>

        <!-- START BreadCrumbs Прайс листы -->
        <ul class="breadcrumbs_PL">
            <li><a href="/">Главная</a></li>
            <li>&nbsp;/&nbsp;</li>
            <li><a href="#" class="go_menu">Личный кабинет</a></li>
            <li>&nbsp;/&nbsp;&nbsp;</li>
            <li><a href="#">Прайс листы / Наличие</a></li>
        </ul>
        <!-- END BreadCrumbs -->


    </section>

    <!-- Прайс листы -->
    <div class="wrapper price_lists" id="full_width">

        <!-- START BreadCrumbs Прайс листы -->
        <ul class="breadcrumbs_PL">
            <li><a href="/">Главная</a></li>
            <li>&nbsp;/&nbsp;</li>
            <li><a href="#" class="go_menu">Личный кабинет</a></li>
            <li>&nbsp;/&nbsp;&nbsp;</li>
            <li><a href="#">Прайс листы / Наличие</a></li>
        </ul>
        <!-- END BreadCrumbs -->

        {{--Таблица--}}
        <div class="wrap_table">
            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col" class="bg3f3f3f">Модель фото</th>
                    <th scope="col" class="bg3f3f3f">Артикул</th>
                    <th scope="col" class="bg3f3f3f hide_for_tablet_product_name min_width217">Наименование</th>
                    {{--<th scope="col" class="bg3f3f3f">Сейчас в наличии</th>--}}
                    <th scope="col" class="bg3f3f3f">Ед. изм.</th>
                    {{--<th scope="col" class="bg3f3f3f">В резерве</th>--}}
                    {{--<th scope="col" class="bg3f3f3f">Доступно</th>--}}
                    {{--<th scope="col" class="bg3f3f3f">Расход</th>--}}
                    <th scope="col" class="bg3f3f3f">Остаток</th>
                    {{--<th scope="col" class="min_width217 bg3f3f3f">Оптовая цена до <br> 330 000 скидка 35.49%</th>
                    <th scope="col" class="min_width217 bg3f3f3f">Оптовая цена до <br> 330 000 скидка 35.49%</th>
                    <th scope="col" class="min_width217 bg3f3f3f">Оптовая цена до <br> 330 000 скидка 35.49%</th>--}}
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">
                        <img class="table__img" src="https://koenigsmann.ru/storage/product-photo-4199661582205293.jpg"
                             alt="{{ asset('img/run.jpg') }}">
                        <span class="product_name_for_tablet">Беговая дорожка Koenigsmann MODEL E1.0 KG</span>
                    </th>
                    <td class="table__price">TMKGME1</td>
                    <td class="product_name_for_desktop">Беговая дорожка Koenigsmann MODEL E1.0 KG</td>
                    {{--<td class="table__price">1,000</td>--}}
                    <td class="table__price">штук</td>
                    {{--<td class="table__price">1,000</td>--}}
                    {{--<td class="table__price"></td>--}}
                    {{--<td class="table__price">1,000</td>--}}
                    <td class="table__price"></td>
                </tr>
                <tr>
                    <th scope="row">
                        <img class="table__img" src="https://koenigsmann.ru/storage/product-photo-5265811582544685.jpg"
                             alt="{{ asset('img/run.jpg') }}">
                        <span class="product_name_for_tablet">Беговая дорожка Koenigsmann MODEL E1.0 LIMITED EDITION KG</span>
                    </th>
                    <td class="table__price">TMKGME1LE</td>
                    <td class="product_name_for_desktop">Беговая дорожка Koenigsmann MODEL E1.0 LIMITED EDITION KG</td>
                    {{--<td class="table__price">5,000</td>--}}
                    <td class="table__price">штук</td>
                    {{--<td class="table__price">1,000</td>--}}
                    {{--<td class="table__price">4,000</td>--}}
                    {{--<td class="table__price">1,000</td>--}}
                    <td class="table__price">4,000</td>
                </tr>
                <tr>
                    <th scope="row">
                        <img class="table__img" src="https://koenigsmann.ru/storage/product-photo-9086641582205366.jpg"
                             alt="{{ asset('img/run.jpg') }}">
                        <span class="product_name_for_tablet">Беговая дорожка Koenigsmann MODEL S1.0 KG</span>
                    </th>
                    <td class="table__price">TMKGMS1</td>
                    <td class="product_name_for_desktop">Беговая дорожка Koenigsmann MODEL S1.0 KG</td>
                    {{--<td class="table__price">10,000</td>--}}
                    <td class="table__price">штук</td>
                    {{--<td class="table__price">1,000</td>--}}
                    {{--<td class="table__price">9,000</td>--}}
                    {{--<td class="table__price">1,000</td>--}}
                    <td class="table__price">9,000</td>
                </tr>
                <tr>
                    <th scope="row">
                        <img class="table__img" src="https://koenigsmann.ru/storage/product-photo-1155841582544718.jpg"
                             alt="{{ asset('img/run.jpg') }}">
                        <span class="product_name_for_tablet">Беговая дорожка Koenigsmann MODEL S1.0 LIMITED EDITION KG</span>
                    </th>
                    <td class="table__price">TMKGMS1LE</td>
                    <td class="product_name_for_desktop">Беговая дорожка Koenigsmann MODEL S1.0 LIMITED EDITION KG</td>
                    {{-- <td class="table__price">12,000</td>--}}
                    <td class="table__price">штук</td>
                    {{--<td class="table__price">12,000</td>--}}
                    {{--<td class="table__price"></td>--}}
                    {{--<td class="table__price"></td>--}}
                    <td class="table__price">12,000</td>
                </tr>
                <tr>
                    <th scope="row">
                        <img class="table__img" src="https://koenigsmann.ru/storage/product-photo-8381891582205436.jpg"
                             alt="{{ asset('img/run.jpg') }}">
                        <span class="product_name_for_tablet">Беговая дорожка Koenigsmann MODEL XR1.0 COMMERCIAL KG</span>
                    </th>
                    <td class="table__price">TMKGMXR1C</td>
                    <td class="product_name_for_desktop">Беговая дорожка Koenigsmann MODEL XR1.0 COMMERCIAL KG</td>
                    {{--<td class="table__price">12,000</td>--}}
                    <td class="table__price">штук</td>
                    {{--<td class="table__price">12,000</td>--}}
                    {{--<td class="table__price"></td>--}}
                    {{--<td class="table__price"></td>--}}
                    <td class="table__price">12,000</td>
                </tr>


                </tbody>
            </table>

            <h2 class="price_list__h2">Комплектующие</h2>

            <table class="table table-dark">
                <thead class="table_hide_thead">
                <tr class="table_hide_tr">
                    {{--                        <th scope="col" class="bg3f3f3f th_hide">Модель фото</th>
                                            <th scope="col" class="bg3f3f3f th_hide hide_for_tablet_product_name">Наименование</th>
                                            <th scope="col" class="bg3f3f3f th_hide">Характеристики</th>
                                            <th scope="col" class="bg3f3f3f th_hide">РРЦ</th>
                                            <th scope="col" class="min_width217 bg3f3f3f th_hide">Оптовая цена до <br> 330 000 скидка 35.49%
                                            </th>
                                            <th scope="col" class="min_width217 bg3f3f3f th_hide">Оптовая цена до <br> 330 000 скидка 35.49%
                                            </th>
                                            <th scope="col" class="min_width217 bg3f3f3f th_hide">Оптовая цена до <br> 330 000 скидка 35.49%
                                            </th>--}}
                    <th scope="col" class="bg3f3f3f">Модель фото</th>
                    <th scope="col" class="bg3f3f3f">Артикул</th>
                    <th scope="col" class="bg3f3f3f hide_for_tablet_product_name">Наименование</th>
                    {{--<th scope="col" class="bg3f3f3f">Сейчас в наличии</th>--}}
                    <th scope="col" class="bg3f3f3f">Ед. изм.</th>
                    {{--<th scope="col" class="bg3f3f3f">В резерве</th>--}}
                    {{--<th scope="col" class="bg3f3f3f">Доступно</th>--}}
                    {{--<th scope="col" class="bg3f3f3f">Расход</th>--}}
                    <th scope="col" class="bg3f3f3f">Остаток</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">
                        <img class="table__img" src="https://koenigsmann.ru/storage/product-photo-7555231582205591.jpg"
                             alt="{{ asset('img/run.jpg') }}">
                        <span class="product_name_for_tablet">Кардиопояс KOENIGSMANN</span>
                    </th>
                    <td class="table__price">ACSKGMCB</td>
                    <td class="product_name_for_desktop">Кардиопояс KOENIGSMANN</td>
                    {{--<td class="table__price">87,000</td>--}}
                    <td class="table__price">штук</td>
                    {{--<td class="table__price"></td>--}}
                    {{--<td class="table__price">87,000</td>--}}
                    {{--<td class="table__price"></td>--}}
                    <td class="table__price">87,000</td>
                </tr>
                <tr>
                    <th scope="row">
                        <img class="table__img" src="https://koenigsmann.ru/storage/product-photo-5119181582205550.png"
                             alt="{{ asset('img/run.jpg') }}">
                        <span class="product_name_for_tablet">Коврик для беговой дорожки KOENIGSMANN</span>
                    </th>
                    <td class="table__price">ACSKGMCP</td>
                    <td class="product_name_for_desktop">Коврик для беговой дорожки KOENIGSMANN</td>
                    {{--<td class="table__price">282,000</td>--}}
                    <td class="table__price">штук</td>
                    {{--<td class="table__price"></td>--}}
                    {{--<td class="table__price">282,000</td>--}}
                    {{--<td class="table__price"></td>--}}
                    <td class="table__price">282,000</td>
                </tr>
                <tr>
                    <th scope="row">
                        <img class="table__img" src="https://koenigsmann.ru/storage/product-photo-3544871582544757.jpg"
                             alt="{{ asset('img/run.jpg') }}">
                        <span class="product_name_for_tablet">Умные весы KOENIGSMANN</span>
                    </th>
                    <td class="table__price">ACSKGMSC</td>
                    <td class="product_name_for_desktop">Умные весы KOENIGSMANN</td>
                    {{--<td class="table__price">127,000</td>--}}
                    <td class="table__price">штук</td>
                    {{--<td class="table__price"></td>--}}
                    {{--<td class="table__price">127,000</td>--}}
                    {{--<td class="table__price"></td>--}}
                    <td class="table__price">127,000</td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>

    <!-- Маркетинговая поддержка (Обучающие материалы) -->
    <div class="marketing_support">

        <div class="wrap_scroll_for_md_MS">
            <div class="wrap_tab_btn_MS">
                <div class="wrap_tab_MS">
                    <h3 class="show_md"><a href="#">Обучающие материалы</a></h3>
                    <h3><a href="#">Акции</a></h3>
                    <h3><a href="#">Обучение</a></h3>
                    <h3 class="active_tab"><a href="#">Маркетинговая поддержка</a></h3>
                </div>

                <a href="#" id="btn_upload_katalog">Выгрузка каталога</a>
            </div>
        </div>

        <ul class="list_result_MS wrapper">
            <li>
                <div class="wrap_icon_description_PL">
                    <div class="wrap_icon_download">
                        <span class="icon_file">
                            <img src="{{ asset('img/x_file.svg') }}" alt="x_file">
                        </span>
                        <a class="wrap_download_all">
                            <div class="wrap_download_all__header">Загрузить</div>
                            <span class="icon_download_PL"></span>
                        </a>
                    </div>
                    <div class="functional_right">
                        <div class="wrap_download_selected">
                            <label class="download_selected__label">
                                <input type="checkbox" class="hidden">
                                <span class="custom_checkbox_pa"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="wrap_description_PL">
                    <div class="description_name">Файл с логотипом</div>
                    <div class="description_data">4.5 MB, 05.08.2019 11:48</div>
                </div>
            </li>
            <li>
                <div class="wrap_icon_description_PL">
                    <div class="wrap_icon_download">
                        <span class="icon_file">
                            <img src="{{ asset('img/fl_file.svg') }}" alt="x_file">
                        </span>
                        <a class="wrap_download_all">
                            <div class="wrap_download_all__header">Загрузить</div>
                            <span class="icon_download_PL"></span>
                        </a>
                    </div>
                    <div class="functional_right">
                        <div class="wrap_download_selected">
                            <label class="download_selected__label">
                                <input type="checkbox" class="hidden">
                                <span class="custom_checkbox_pa"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="wrap_description_PL">
                    <div class="description_name">Файл с логотипом</div>
                    <div class="description_data">4.5 MB, 05.08.2019 11:48</div>
                </div>
            </li>
            <li>
                <div class="wrap_icon_description_PL">
                    <div class="wrap_icon_download">
                        <span class="icon_file">
                            <img src="{{ asset('img/x_file.svg') }}" alt="x_file">
                        </span>
                        <a class="wrap_download_all">
                            <div class="wrap_download_all__header">Загрузить</div>
                            <span class="icon_download_PL"></span>
                        </a>
                    </div>
                    <div class="functional_right">
                        <div class="wrap_download_selected">
                            <label class="download_selected__label">
                                <input type="checkbox" class="hidden">
                                <span class="custom_checkbox_pa"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="wrap_description_PL">
                    <div class="description_name">Файл с логотипом</div>
                    <div class="description_data">4.5 MB, 05.08.2019 11:48</div>
                </div>
            </li>
            <li>
                <div class="wrap_icon_description_PL">
                    <div class="wrap_icon_download">
                        <span class="icon_file">
                            <img src="{{ asset('img/psd_file.svg') }}" alt="x_file">
                        </span>
                        <a class="wrap_download_all">
                            <div class="wrap_download_all__header">Загрузить</div>
                            <span class="icon_download_PL"></span>
                        </a>
                    </div>
                    <div class="functional_right">
                        <div class="wrap_download_selected">
                            <label class="download_selected__label">
                                <input type="checkbox" class="hidden">
                                <span class="custom_checkbox_pa"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="wrap_description_PL">
                    <div class="description_name">Файл с логотипом</div>
                    <div class="description_data">4.5 MB, 05.08.2019 11:48</div>
                </div>
            </li>
        </ul>

        <!-- Две кноки, - Загрузить всё и Загрузить выбранное -->
        <div class="wrap_btn_MS wrapper">
            <a href="#">
                <span class="icon_btn_downLoad_all"></span>
                <span class="text_btn">Загрузить всё</span>
            </a>
            <a href="#">
                <span class="icon_btn_download_select"></span>
                <span class="text_btn">Загрузить выбранное</span>
            </a>
        </div>
    </div>

    <!-- END Личный кабинет -->

    {{-- Табы прайс лист / Маркетинговая поддержка --}}
    <script>
      window.addEventListener('load', () => {
        let pl = document.getElementById('PL')
        let ms = document.getElementById('MS')

        pl.addEventListener('click', show_pl)
        ms.addEventListener('click', show_ms)
        show_pl()
        // Показываю наличие / Скрываю маркет
        function show_pl () {
          document.querySelector('.wrap_select_search').style.display = 'none'
          document.querySelector('.list_result').style.display = 'none'
          document.querySelector('#full_width').style.display = ''
          pl.classList.add('active_tab')
          ms.classList.remove('active_tab')
        }

        // Скрываю наличие / Показываю маркет
        function show_ms () {
          document.querySelector('.wrap_select_search').style.display = ''
          document.querySelector('.list_result').style.display = ''
          document.querySelector('#full_width').style.display = 'none'
          pl.classList.remove('active_tab')
          ms.classList.add('active_tab')
        }
      })
    </script>

    {{-- Скрипт select выбор материала, скрываю / показываю --}}
    <script>
      window.addEventListener('load', () => {
        let materials = document.getElementById('materials')
        materials.addEventListener('change', show_list_download)

        function show_list_download (e) {

          let list_result = document.querySelectorAll('.list_result li')

          if (e.target.value === 'all_files') {
            for (let i = 0; i < list_result.length; i++) {
              list_result[i].style.display = ''
            }
          } else if (e.target.value === 'logo') {
            for (let i = 0; i < list_result.length; i++) {
              if (list_result[i].dataset.li !== 'logo') {
                list_result[i].style.display = 'none'
              } else {
                list_result[i].style.display = ''
              }
            }
          } else if (e.target.value === 'instruction') {
            for (let i = 0; i < list_result.length; i++) {
              if (list_result[i].dataset.li !== 'instruction') {
                list_result[i].style.display = 'none'
              } else {
                list_result[i].style.display = ''
              }
            }
          }
        }
      })
    </script>

    {{-- Скрипт для полной ширины таблицы на планшетах и мобильных --}}
    <script>
      window.addEventListener('load', () => {
        let window_width = window.innerWidth
          || document.documentElement.clientWidth
          || document.body.clientWidth

        if (window_width < 1024) {

          let full_width = document.getElementById('full_width')

          if (full_width.classList.contains('wrapper')) {
            full_width.classList.remove('wrapper')
          }
        } else {

          let full_width = document.getElementById('full_width')

          if (!full_width.classList.contains('wrapper')) {
            full_width.classList.add('wrapper')
          }
        }
      })
    </script>

    <!-- Перелинковка -->
    <script>
      window.addEventListener('load', () => {

        let wi = window.innerWidth
          || document.documentElement.clientWidth
          || document.body.clientWidth

        if (wi < 768) {
          let all_views = []

          function collect_items () {
            let breadcrumbs_UC = document.querySelector('.breadcrumbs_UC')
            let list_result_UC = document.querySelector('.list_result_UC')

            let breadcrumbs_PL = document.querySelector('.breadcrumbs_PL')
            let list_result_PL = document.querySelector('.list_result_PL')
            let wrap_btn_PL = document.querySelector('.wrap_btn_PL')
            let full_width = document.getElementById('full_width')

            let marketing_support = document.querySelector('.marketing_support')
            let list_result_MS = document.querySelector('.list_result_MS')
            let wrap_btn_MS = document.querySelector('.wrap_btn_MS')

            return [breadcrumbs_UC,
              list_result_UC,
              breadcrumbs_PL,
              //list_result_PL,
              list_result_MS,
              //wrap_btn_PL,
              wrap_btn_MS,
              marketing_support,
              full_width
            ]
          }

          collect_items()

          function hide_all () {
            let all_views = collect_items()
            for (let i = 0; i < all_views.length; i++) {
              all_views[i].style.display = 'none'
            }

            let return_to_menu = document.getElementById('return_to_menu')
            return_to_menu.style.display = 'none'

            let wrap_mob_LK = document.querySelector('.wrap_mob_LK')
            wrap_mob_LK.style.width = '100%'

            return all_views
          }

          // Показать Выгрузку каталога
          let UC = document.getElementById('UC')
          UC.addEventListener('click', show_UC)

          function show_UC () {
            let arr = hide_all()
            arr[0].style.display = 'block'
            arr[1].style.display = 'block'
            auxiliary()
          }

          // Показать Прайс-листы
            /*          let PL = document.getElementById('PL')
             PL.addEventListener('click', show_PL)*/

          function show_PL () {
            let arr = hide_all()
            arr[2].style.display = 'block'
            //arr[3].style.display = 'block'
            arr[6].style.display = 'block'
            auxiliary()
          }

          // Маркетинговая поддержка
          let MS = document.getElementById('MS')
          MS.addEventListener('click', show_MS)

          function show_MS () {
            let arr = hide_all()
            arr[3].style.display = 'block'
            arr[4].style.display = 'block'
            arr[5].style.display = 'block'
            auxiliary()
          }

          // Вспомогательная функция, которая работает с шапкой страницы и убирает меню
          function auxiliary () {
            let wrap_data_basket = document.querySelector('.wrap_data_basket')
            wrap_data_basket.style.display = 'none'

            let wrap_data_user_line_LK = document.querySelector('.wrap_data_user_line_LK')
            wrap_data_user_line_LK.style.display = 'none'

            let return_to_menu = document.getElementById('return_to_menu')
            return_to_menu.style.display = 'inline-block'

            let wrap_mob_LK = document.querySelector('.wrap_mob_LK')
            wrap_mob_LK.style.width = 'auto'

            let wrap_scroll_for_md = document.querySelector('.wrap_scroll_for_md')
            wrap_scroll_for_md.style.display = 'none'

            return_to_menu.addEventListener('click', go_to_menu)
          }

          // Собираю все элементы у которых есть класс go_menu, и по клику отправляю всех в меню
          let go_menu = document.querySelectorAll('.go_menu')
          for (let i = 0; i < go_menu.length; i++) {
            go_menu[i].addEventListener('click', go_to_menu)
          }

          // Возвращаем всё на место, показываем меню
          function go_to_menu () {
            let wrap_data_basket = document.querySelector('.wrap_data_basket')
            wrap_data_basket.style.display = 'flex'

            let wrap_data_user_line_LK = document.querySelector('.wrap_data_user_line_LK')
            wrap_data_user_line_LK.style.display = 'block'

            let return_to_menu = document.getElementById('return_to_menu')
            return_to_menu.style.display = 'none'

            let wrap_mob_LK = document.querySelector('.wrap_mob_LK')
            wrap_mob_LK.style.width = '100%'

            let wrap_scroll_for_md = document.querySelector('.wrap_scroll_for_md')
            wrap_scroll_for_md.style.display = 'block'

            hide_all()
          }

        } else {

          let all_views = []

          function collect_items () {
            let breadcrumbs_UC = document.querySelector('.breadcrumbs_UC')
            let list_result_UC = document.querySelector('.list_result_UC')

            let list_result_PL = document.querySelector('.list_result_PL')
            let list_result = document.querySelector('.list_result')
            let wrap_btn_PL = document.querySelector('.wrap_btn_PL')
            let wrap_select_search = document.querySelector('.wrap_select_search')
            let full_width = document.getElementById('full_width')

            let marketing_support = document.querySelector('.marketing_support')
            let list_result_MS = document.querySelector('.list_result_MS')
            let wrap_btn_MS = document.querySelector('.wrap_btn_MS')

            return [breadcrumbs_UC,
              list_result_UC,
              //breadcrumbs_PL,
              //list_result_PL,
              list_result_MS,
              //wrap_btn_PL,
              wrap_btn_MS,
              marketing_support,
              full_width,
              list_result,
              wrap_select_search
            ]
          }

          collect_items()

          function hide_all () {
            let all_views = collect_items()
            for (let i = 0; i < all_views.length; i++) {
              all_views[i].style.display = 'none'
            }

            let return_to_menu = document.getElementById('return_to_menu')
            return_to_menu.style.display = 'none'

            let wrap_mob_LK = document.querySelector('.wrap_mob_LK')
            wrap_mob_LK.style.width = '100%'

            return all_views
          }

          // Показать Выгрузку каталога
          let UC = document.getElementById('UC')
          UC.addEventListener('click', show_UC)

          function show_UC () {
            let arr = hide_all()
            arr[0].style.display = 'block'
            arr[1].style.display = 'block'
            auxiliary()
          }

          // Показать Прайс-листы
            /*          let PL = document.getElementById('PL')
             PL.addEventListener('click', show_PL)*/

          function show_PL (e) {
            let arr = hide_all()
            arr[2].style.display = 'block'
            //arr[3].style.display = 'block'
            arr[5].style.display = 'block'

            if (e.target.textContent === 'Прайс-листы/Наличие') {
              let pl = document.querySelector('#PL')
              let wrap_tab = document.querySelectorAll('.wrap_tab h3')
              for (let i = 0; i < wrap_tab.length; i++) {
                wrap_tab[i].classList.remove('active_tab')
              }
              console.log(pl)
              pl.setAttribute('class', 'active_tab')
            }
            auxiliary()
          }

          // Маркетинговая поддержка
          let MS = document.getElementById('MS')
          MS.addEventListener('click', show_MS)

          function show_MS (e) {
            console.log(e.target.textContent)
            let arr = hide_all()
            arr[3].style.display = 'block'
            arr[6].style.display = 'block'
            arr[7].style.display = 'flex'

            if (e.target.textContent === 'Маркетинговая поддержка') {
              let ms = document.querySelector('#MS')
              let wrap_tab = document.querySelectorAll('.wrap_tab h3')
              for (let i = 0; i < wrap_tab.length; i++) {
                wrap_tab[i].classList.remove('active_tab')
              }
              ms.setAttribute('class', 'active_tab')
            }
            auxiliary()
          }

          // Вспомогательная функция, которая работает с шапкой страницы и убирает меню
          function auxiliary () {
            let wrap_data_basket = document.querySelector('.wrap_data_basket')
            wrap_data_basket.style.display = 'none'

              /*        let wrap_data_user_line_LK = document.querySelector('.wrap_data_user_line_LK')
               wrap_data_user_line_LK.style.display = 'none'*/

              /*            let return_to_menu = document.getElementById('return_to_menu')
               return_to_menu.style.display = 'inline-block'*/

            let wrap_mob_LK = document.querySelector('.wrap_mob_LK')
            wrap_mob_LK.style.width = 'auto'

              /*        let wrap_scroll_for_md = document.querySelector('.wrap_scroll_for_md')
               wrap_scroll_for_md.style.display = 'none'*/

            return_to_menu.addEventListener('click', go_to_menu)
          }

          // Собираю все элементы у которых есть класс go_menu, и по клику отправляю всех в меню
          let go_menu = document.querySelectorAll('.go_menu')
          for (let i = 0; i < go_menu.length; i++) {
            go_menu[i].addEventListener('click', go_to_menu)
          }

          // Возвращаем всё на место, показываем меню
          function go_to_menu () {
            let wrap_data_basket = document.querySelector('.wrap_data_basket')
            wrap_data_basket.style.display = 'flex'

              /*        let wrap_data_user_line_LK = document.querySelector('.wrap_data_user_line_LK')
               wrap_data_user_line_LK.style.display = 'block'*/

              /*            let return_to_menu = document.getElementById('return_to_menu')
               return_to_menu.style.display = 'none'*/

            let wrap_mob_LK = document.querySelector('.wrap_mob_LK')
            wrap_mob_LK.style.width = '100%'

              /*        let wrap_scroll_for_md = document.querySelector('.wrap_scroll_for_md')
               wrap_scroll_for_md.style.display = 'block'*/

            hide_all()
          }
        }
      })
    </script>

    {{-- Обновляем в том случае если, изменение окна больше или меньше на 100px --}}
    <script>
      // Обновляем в том случае если, изменение окна больше или меньше на 100px
      /*      let scr_width = window.innerWidth
       || document.documentElement.clientWidth
       || document.body.clientWidth*/
      window.addEventListener('resize', (e) => {
        let res = scr_width - e.target.innerWidth
        if (res > 200) {
          location.reload()
        }
        if (res < -200) {
          location.reload()
        }
      })
    </script>

@endsection