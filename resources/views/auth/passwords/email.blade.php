@extends('layouts.app', ['title' => 'email'])

@section('content')
<div class="container" style="margin-top: 175px;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="background: #292929;">
                <div class="card-header">{{ __('Сбросить пароль') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Получить ссылку на почту') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{{--<div class="forgot_page">
    <!-- Лев Leo -->
    <div class="bg"></div>
    <div class="wrapper">


        <ul class="breadcrumbs reset_email">
            <li><a href="/">Главная</a></li>
            <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
            <li><a href="#">Забыли пароль?</a></li>
        </ul>

        <h1 class="reset_email_h1 MBold">{{ __('Забыли пароль?') }}</h1>
        <p class="forgot__text">Введите эл. почту, с которой вы регистрировались. Мы вышлем вам ссылку для смены пароля.</p>

        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <form action="{{ route('password.email') }}">
            @csrf
            <div class="input-field reset_email">
                <input type="email" id="forgot__email" type="email" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                <label for="email">{{ __('E-Mail') }}</label>
            </div>
            @error('email')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
            <input type="submit" class="send_mail" value="Отправить">
        </form>
    </div>
</div>--}}
{{--<div class="forgot_page">
    <!-- Лев Leo -->
    <div class="bg"></div>
    <div class="wrapper">


        <ul class="breadcrumbs reset_email">
            <li><a href="/">Главная</a></li>
            <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
            <li><a href="#">Забыли пароль?</a></li>
        </ul>

        <h1>{{ __('Забыли пароль?') }}</h1>
        <p class="forgot__text">Введите эл. почту, с которой вы регистрировались. Мы вышлем вам ссылку для смены пароля</p>

        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <form action="{{ route('password.email') }}">
            <div class="input-field reset_email">
                <input type="email" id="forgot__email" name="email" required value="{{ old('email') }}">
                <label for="email">{{ __('E-Mail') }}</label>
            </div>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <input type="submit" class="send_mail" value="Отправить">
        </form>
    </div>
</div>--}}
@endsection
