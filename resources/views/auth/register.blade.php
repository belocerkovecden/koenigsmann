@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 150px; margin-bottom: 250px;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="background: #292929;">
                <div class="card-header">{{ __('Регистрация') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Логин') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Пароль') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Повторите пароль') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn" style="background: #454545;">
                                    {{ __('Регистрация') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{{--<div class="signup">
    <!-- Лев Leo -->
    <div class="bg"></div>
    <div class="wrapper">


        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li>&nbsp;&nbsp;&frasl;&nbsp;&nbsp;</li>
            <li><a href="#">Регистрация</a></li>
        </ul>

        <h1>Регистрация</h1>
        <form action="#">
            <div class="input-field contacts_input">
                <input type="text" id="name" name="name" required>
                <label for="name">Имя</label>
            </div>
            <div class="input-field contacts_input">
                <input type="text" id="surname" name="surname" required>
                <label for="surname">Фамилия</label>
            </div>
            <div class="input-field contacts_input">
                <input type="email" id="email" name="email" required>
                <label for="email">Email</label>
            </div>
            <div class="input-field contacts_input">
                <input type="password" id="password" minlength="6" name="password" required>
                <label for="password">Создайте пароль</label>
                <label for="togglePassword"></label>
                <input type="checkbox" id="togglePassword">
            </div>

            <input type="submit" class="signup_submit" value="Зарегистрироваться">
        </form>
    </div>
</div>--}}

@endsection
