window.addEventListener('load', () => {

  let block_phone =  document.querySelector('.block-phone')
  let close_popup = document.querySelector('.close_popup')
  let label_span = document.querySelector('.label_span')
  let mobile768 = document.querySelector('.mobile768')
  let back_g = document.querySelector('.back_g')
  let phone = document.getElementById('phone')

  block_phone.addEventListener('click', show_popup_call_me)
  mobile768.addEventListener('click', show_popup_call_me)
  close_popup.addEventListener('click', hide_popup_call_me)
  back_g.addEventListener('click', hide_popup_call_me)

  function show_popup_call_me (e) {
    //e.preventDefault()
    back_g.style.display = 'block'
  }

  function hide_popup_call_me (e) {
    //e.preventDefault()

    let block_search__icon_close = document.querySelector('.block-search__icon-close')
    let close_popup = document.querySelector('.close_popup')
    let back_g = document.querySelector('.back_g')
    if (e.target.className === 'back_g'
      || e.target.className === 'close_popup'
      || e.target.className === 'close_popup__text'
      || e.target.className === 'close_popup__icon'
      || e.target.className === 'block-search__icon-close'
    ) {
      back_g.style.display = 'none'
    }
  }

// по фокусу убираю span
  //phone.addEventListener('focus', () => label_span.style.display = 'none')
  //phone.addEventListener('mouseenter', () => label_span.style.display = 'none')
  //phone.addEventListener('mouseover', () => label_span.style.display = 'none')

// Увеличиваю input поиска по фокусу
  let inp = document.querySelector('.block-search__input')

  if(inp) {
    inp.addEventListener('focus', big_inp)

    function big_inp () {
      let block_phone = document.querySelector('.block-phone')
      block_phone.style.transition = 'none'
      block_phone.style.display = 'none'

      let login = document.querySelector('.login')
      login.style.transition = 'none'
      login.style.display = 'none'

      let block_search = document.querySelector('.block-search')
      block_search.style.transition = 'none'
      block_search.style.maxWidth = '226px'
      block_search.style.minWidth = '226px'
      block_search.style.marginRight = '60px'

      let block_search__icon_close = document.querySelector('.block-search__icon-close')
      block_search__icon_close.style.display = 'inline-block'

      let scr_width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth

      if(scr_width < 1280) {
        let block_search__icon_search = document.querySelector('.block-search__icon-search')
        block_search__icon_search.style.display = 'none'
        block_search.style.borderBottom = '2px solid #900020'
        block_search.style.display = 'flex'
      }

    }

    inp.addEventListener('blur', small_inp)

    function small_inp () {
      let block_phone = document.querySelector('.block-phone')
      block_phone.style.transition = 'none'
      block_phone.style.display = ''

      let login = document.querySelector('.login')
      login.style.transition = 'none'
      login.style.display = ''

      let block_search = document.querySelector('.block-search')
      block_search.style.transition = 'none'
      block_search.style.maxWidth = ''
      block_search.style.minWidth = ''
      block_search.style.marginRight = ''

      let block_search__icon_close = document.querySelector('.block-search__icon-close')
      block_search__icon_close.style.display = 'none'

      let scr_width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth

      if(scr_width < 1280) {
        let block_search__icon_search = document.querySelector('.block-search__icon-search')
        block_search__icon_search.style.display = ''
        block_search.style.borderBottom = ''
        block_search.style.display = ''
      }
    }
  }

})


