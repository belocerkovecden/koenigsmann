window.addEventListener('load', () => {

  // Скрипт анимации инпутов
  let textarea_field = document.querySelectorAll('.input-field .data_string')
  let input_field = document.querySelectorAll('.input-field input')
  for (let i = 0; i < input_field.length; i++) {
    input_field[i].addEventListener('focus', ani)
    input_field[i].addEventListener('blur', ani_back)

    if (textarea_field[i]) {
      textarea_field[i].addEventListener('focus', ani)
      textarea_field[i].addEventListener('blur', ani_back)
    }
  }

  function ani (e) {
    e.target.nextElementSibling.style.top = '8px'
    e.target.nextElementSibling.style.fontSize = '12px'
  }

  function ani_back (e) {
    if (!e.target.value) {
      e.target.nextElementSibling.style.top = ''
      e.target.nextElementSibling.style.fontSize = ''
    } else {
      //e.target.style.borderBottom = '1px solid #1B749F'
    }
  }

  // Скрипт показа меню на планшете и меньше
  let sandwich = document.querySelector('.sandwich')
  if (sandwich) {
    sandwich.addEventListener('click', show_menu)
  }

  function show_menu (e) {
    e.preventDefault()

    let second_line = document.querySelector('.second-line')

    let header = document.querySelector('.header')

    if (second_line) {
      if (second_line.id !== 'menu_tablet') {
        second_line.setAttribute('id', 'menu_tablet')
        //header.setAttribute('id', 'header_style_menu')
        e.target.setAttribute('id', 'sandwich')

        let header_menu = document.querySelector('.header-menu')
        header_menu.style.display = 'block'
      } else {
        second_line.setAttribute('id', '')
        //header.setAttribute('id', '')
        e.target.setAttribute('id', '')

        let header_menu = document.querySelector('.header-menu')
        header_menu.style.display = 'none'
      }
    }

    let second_line_LK = document.querySelector('.second-line_LK')

    if (second_line_LK) {
      if (second_line_LK.id !== 'menu_tablet_LK') {
        second_line_LK.setAttribute('id', 'menu_tablet_LK')
        header.setAttribute('id', 'header_style_menu')
        e.target.setAttribute('id', 'sandwich')

        let logo = document.querySelector('.logo')
        if (window.getComputedStyle(logo).display === 'none') {
          logo.style.display = 'block'
          let wrap_data_basket = document.querySelector('.wrap_data_basket')
          wrap_data_basket.style.display = 'none'

          let wrap_mob_LK = document.querySelector('.wrap_mob_LK')
          wrap_mob_LK.style.width = 'auto'
        }

      } else {
        second_line_LK.setAttribute('id', '')
        header.setAttribute('id', '')
        e.target.setAttribute('id', '')

        let logo = document.querySelector('.logo')
        if (window.getComputedStyle(logo).display === 'block') {
          logo.style.display = 'none'
          let wrap_data_basket = document.querySelector('.wrap_data_basket')
          wrap_data_basket ? wrap_data_basket.style.display = 'flex' : ''
        }

        let wrap_mob_LK = document.querySelector('.wrap_mob_LK')
        wrap_mob_LK ? wrap_mob_LK.style.width = '100%' : ''
      }
    }
  }

  // Обновляем, - в том случае если, изменение окна больше или меньше на 100px
  let screen_width = window.innerWidth
  window.addEventListener('resize', (e) => {
    let res = screen_width - e.target.innerWidth

    if (res > 100) {
      location.reload()
    }
    if (res < -100) {
      location.reload()
    }

/*    let second_line = document.querySelector('.second-line')

    if(second_line) {
      if (second_line.id !== 'menu_tablet') {
        second_line.setAttribute('id', 'menu_tablet')
        //header.setAttribute('id', 'header_style_menu')
        e.target.setAttribute('id', 'sandwich')
      } else {
        second_line.setAttribute('id', '')
        //header.setAttribute('id', '')
        e.target.setAttribute('id', '')
      }
    }*/

  })

  // Обновляем в том случае если, изменение окна больше или меньше на 100px
  let scr_width = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth
  window.addEventListener('resize', (e) => {
    let res = scr_width - e.target.innerWidth
    if (res > 200) {
      location.reload()
    }
    if (res < -200) {
      location.reload()
    }
  })

  let wi = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth

/*  if (wi < 1023) {
    let angle_right_white = document.querySelectorAll('.angle_right_white')
    for (let i = 0; i < angle_right_white.length; i++) {
      if (angle_right_white[i].textContent.length > 22) {
        let end_str = angle_right_white[i].textContent.substr(-1)
        end_str = '-' + end_str
        angle_right_white[i].textContent = angle_right_white[i].textContent.slice(0, 22) + end_str
      }
    }
  }*/

  // На ширине меньше 767 переношу иконку + ссылку в первую линию personal_area.html
  if (wi < 767) {
    let wrap_data_basket = document.querySelector('.wrap_data_basket')

    if (wrap_data_basket) {
      let wrap_data_basket_clone = wrap_data_basket.cloneNode(true)
      wrap_data_basket.remove()
      let container = document.querySelector('.wrap_first_line_LK .wrap_mob_LK')
      let elem_before = document.querySelector('.wrap_mob_LK .sandwich')
      container.insertBefore(wrap_data_basket_clone, elem_before)

      // Меняю текст "Данные аккаунта" на "Аккаунт"
      let _account = document.querySelector('.wrap_data_account > h2')
      _account.textContent = 'Аккаунт'
    }
  }

  /*** Скрипт карусели "фото товара" ***/
  let chevron_left = document.querySelector('#btn_left')
  let chevron_right = document.querySelector('#btn_right')
  let list_img = document.querySelector('.wrap-brand')

  chevron_right ? chevron_right.addEventListener('click', move_right) : ''
  chevron_right ? chevron_right.addEventListener('touch', move_right) : ''
  chevron_left ? chevron_left.addEventListener('click', move_left) : ''
  chevron_left ? chevron_left.addEventListener('touch', move_left) : ''

  function move_right (e) {
    e.preventDefault()
    let el_container_width = list_img.offsetWidth
    list_img.scrollLeft += el_container_width
  }

  function move_left (e) {
    e.preventDefault()
    let el_container_width = list_img.offsetWidth
    list_img.scrollLeft -= el_container_width
  }

  // Показать все города
  let show_all = document.getElementById('show_all')
  if (show_all) {
    show_all.addEventListener('click', (e) => {
      e.preventDefault()

      let ul_list = document.querySelector('.city_list__ul-list')
      // ul_list.style.overflowY = 'scroll'
      ul_list.style.maxHeight = 'none'
      e.target.style.display = 'none'
    })
  }


  /* Эмуляция клика по селекту */ // TODO Не работает!
  function emulateClick (target) {
    var click = new CustomEvent('click')
    target.dispatchEvent(click)
  }

  let down = document.querySelectorAll('.down')

  for (let i = 0; i < down.length; i++) {
    down[i].addEventListener('click', option_show)
  }

  function option_show () {
    let city_list = document.getElementById('city_list')
    emulateClick(city_list)
  }
})

/* Маска для телефона */
window.addEventListener('load', () => {
  /*** Маска телефона для всплывающей формы ***/
  $('#phone').mask('+7 ( 999 ) 999-99-99')
  $('#contact-face__phone').mask('+7 ( 999 ) 999-99-99')
  $('#user__phone').mask('+7 ( 999 ) 999-99-99')
})



