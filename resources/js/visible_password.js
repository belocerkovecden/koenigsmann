window.addEventListener('load', () => {
  let togglePassword = document.querySelector('#togglePassword')

  if(togglePassword) {
    togglePassword.addEventListener('click', show_hide_password)
  }

  function show_hide_password (e) {
    let password = document.querySelector('#password')
    let label = document.querySelector('label[for="togglePassword"]')

    if (password.type === 'password') {
      password.type = 'text';
      label.classList.add('checked');
    } else {
      password.type = 'password';
      label.classList.remove('checked');
    }
  }

})

