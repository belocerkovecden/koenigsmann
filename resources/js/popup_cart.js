window.addEventListener('load', () => {
  let back_g3 = document.querySelector('.back_g3')
  let cart_link = document.querySelector('.cart_link')  


  if(cart_link) {
    cart_link.addEventListener('click', show_popup_signin)
    back_g3.addEventListener('click', hide_popup_signin)
  }


  function show_popup_signin (e) {
    back_g3.style.display = 'block'
  }

  function hide_popup_signin (e) {
    let close_popup = document.querySelector('.close_popup')
    let back_g3 = document.querySelector('.back_g3')
    if (e.target.className === 'back_g3'
      || e.target.className === 'close_popup'
      || e.target.className === 'close_popup__text'
      || e.target.className === 'close_popup__icon'
    ) {
      back_g3.style.display = 'none'
    }
  }
})


