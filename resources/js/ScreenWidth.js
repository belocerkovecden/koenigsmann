class ScreenWidth {

  getScreenWidth () {
    let width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth
    return width
  }

/*  getDocumentHeight () {
    var ua = navigator.userAgent.toLowerCase()
    var isOpera = (ua.indexOf('opera') > -1)
    var isIE = (!isOpera && ua.indexOf('msie') > -1)
    return Math.max(document.compatMode != 'CSS1Compat' ? document.body.scrollHeight : document.documentElement.scrollHeight, getViewportHeight())
  }*/

  getViewportHeight () {
    var ua = navigator.userAgent.toLowerCase()
    var isOpera = (ua.indexOf('opera') > -1)
    var isIE = (!isOpera && ua.indexOf('msie') > -1)
    return ((document.compatMode || isIE) && !isOpera) ? (document.compatMode == 'CSS1Compat') ? document.documentElement.clientHeight : document.body.clientHeight : (document.parentWindow || document.defaultView).innerHeight
  }

}
