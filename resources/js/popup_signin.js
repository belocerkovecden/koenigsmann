window.addEventListener('load', () => {
  let back_g2 = document.querySelector('.back_g2')
  let login = document.querySelector('.login')  
  let signin_login = document.getElementById('signin_login')
  let signin_password = document.getElementById('signin_password')

  login.addEventListener('click', show_popup_signin)
  if(back_g2) {
    back_g2.addEventListener('click', hide_popup_signin)
  }
  if(signin_login) {
    signin_login.addEventListener('input', lock_unlock_button)
  }
  if(signin_password) {
    signin_password.addEventListener('input', lock_unlock_button)
  }

  function show_popup_signin (e) {
    back_g2.style.display = 'block'
  }

  function hide_popup_signin (e) {
    let close_popup = document.querySelector('.close_popup')
    let back_g2 = document.querySelector('.back_g2')
    if (e.target.className === 'back_g2'
      || e.target.className === 'close_popup'
      || e.target.className === 'close_popup__text'
      || e.target.className === 'close_popup__icon'
    ) {
      back_g2.style.display = 'none'
    }
  }

  function lock_unlock_button (e) {
    let signin_button = document.querySelector('.signin_button')
    let signin_login = document.getElementById('signin_login')
    let signin_password = document.getElementById('signin_password')
    if (signin_login.value.length != 0 && signin_password.value.length != 0) {
      signin_button.classList.add("active")
    }else{
      signin_button.classList.remove("active")
    }
  }

})


